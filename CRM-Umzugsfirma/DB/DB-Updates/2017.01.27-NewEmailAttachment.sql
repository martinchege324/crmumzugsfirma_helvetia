USE [CrmUmzugsfirma_helvetia]
GO

insert into [CrmUmzugsfirma_helvetia].[dbo].[EmailAttachment]
values (4, 'Cleaningservices', 3, 0, 0, 0, 0, 'reinigungsleistungen.pdf', 'application/pdf')

GO

update [CrmUmzugsfirma_helvetia].[dbo].[AccountEmailConfig] set
	SenderAddressSales = 'sales@helvetiatransporte.ch',
	SenderAddressAccounting = 'accounting@helvetiatransporte.ch'
where AccountId = 4

GO
