﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.DB
{
    public class AccountMapper
    {
        public Account GetAccountByUserId(string userId)
        {
            using (var db = new ApplicationDbContext())
            {
                var result = (from a in db.Accounts
                              join ua in db.User_Accounts
                                  on a.AccountId equals ua.AccountId
                              where ua.UserId.Equals(userId)
                              select a
                              );
                return result.SingleOrDefault();
            }
        }


    }
}