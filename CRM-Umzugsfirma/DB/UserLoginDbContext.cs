﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using CRM_Umzugsfirma.Models;

namespace CRM_Umzugsfirma.DB
{
    public class UserLoginDbContext : IdentityDbContext<ApplicationUser>
    {
        public UserLoginDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static UserLoginDbContext Create()
        {
            return new UserLoginDbContext();
        }

    }

}