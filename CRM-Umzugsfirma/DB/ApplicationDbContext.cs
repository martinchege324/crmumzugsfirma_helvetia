﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing;
using CRM_Umzugsfirma.Models;

namespace CRM_Umzugsfirma.DB
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, CRM_Umzugsfirma.Migrations.Configuration>("DefaultConnection"));
            
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountEmailConfig> AccountEmailConfigs { get; set; }
        public DbSet<AccountEmployee> AccountEmployees { get; set; }
        public DbSet<EmailType> EmailTypes { get; set; }
        public DbSet<EmailType_de> EmailTypes_de { get; set; }
        public DbSet<EmailAttachment> EmailAttachments { get; set; }
        public DbSet<EmailAttachment_de> EmailAttachments_de { get; set; }
        public DbSet<User_Account> User_Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Appointment> Appointments { get; set; }
        public DbSet<AppointmentView> AppointmentViews { get; set; }
        public DbSet<AppointmentService> AppointmentServices { get; set; }
        public DbSet<AppointmentDelivery> AppointmentDeliveries { get; set; }
        public DbSet<Note> Notes { get; set; }
        public DbSet<Offer> Offers { get; set; }
        public DbSet<OfferCleaning> OfferCleanings { get; set; }
        public DbSet<OfferTransport> OfferTransports { get; set; }
        public DbSet<PriceRate> PriceRates { get; set; }
        public DbSet<PriceRate_de> PriceRates_de { get; set; }
        public DbSet<PriceAdditRate> PriceAdditRates { get; set; }
        public DbSet<PriceAdditRate_de> PriceAdditRates_de { get; set; }
        public DbSet<Offer_PriceAddit> Offer_PriceAddits { get; set; }
        public DbSet<Offer_PriceAddit_de> Offer_PriceAddits_de { get; set; }
        public DbSet<DefinedPackMaterial> DefinedPackMaterials { get; set; }
        public DbSet<DefinedPackMaterial_de> DefinedPackMaterials_de { get; set; }
        public DbSet<Offer_DefinedPackMaterial> Offer_DefinedPackMaterials { get; set; }
        public DbSet<OfferPackMaterial> OfferPackMaterials { get; set; }
        public DbSet<Receipt> Receipts { get; set; }
        public DbSet<ReceiptUmzug> ReceiptUmzugs { get; set; }
        public DbSet<ReceiptReinigung> ReceiptReinigungs { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillCleaning> BillCleanings { get; set; }
        public DbSet<BillTransport> BillTransports { get; set; }
        public DbSet<Bill_PriceAddit> Bill_PriceAddits { get; set; }
        public DbSet<Bill_PriceAddit_de> Bill_PriceAddits_de { get; set; }
        public DbSet<BillPackMaterial> BillPackMaterials { get; set; }
        public DbSet<Bill_DefinedPackMaterial> Bill_DefinedPackMaterials { get; set; }
        public DbSet<Bill_DefinedPackMaterial_de> Bill_DefinedPackMaterials_de { get; set; }
        public DbSet<AutoJob> AutoJobs { get; set; }
        public DbSet<AutoJobSentEmail> AutoJobSentEmails { get; set; }
        public DbSet<EmailMarketing> EmailMarketings { get; set; }
        public DbSet<EmailMarketingAttachment> EmailMarketingAttachments { get; set; }
        public DbSet<EmailMarketingCustomer> EmailMarketingCustomers { get; set; }

        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //enable-migrations
            //add-migration InitialCreate
            //update-database

            // prevent table names will be pluralized (accounts -> account)
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Configure AccountId as PK for AccountEmailConfig
            modelBuilder.Entity<AccountEmailConfig>()
                .HasKey(e => e.AccountId);
            modelBuilder.Entity<User_Account>()
                .HasKey(u => u.UserId);

            #region Account
            // Account
            EntityTypeConfiguration<Account> accountEntity = modelBuilder.Entity<Account>();
            accountEntity.HasOptional(a => a.AccountEmailConfig) // Mark AccountEmailConfig is optional for Account (1-1 connection)
                        .WithRequired(ad => ad.Account) // Create inverse relationship
                        .WillCascadeOnDelete(true); // AccountEmailConfig will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<AccountEmployee>(a => a.AccountEmployees) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(true);
            accountEntity.HasMany<EmailType>(a => a.EmailTypes) //mark 1-* connection
                        .WithRequired(a => a.Account) // Create inverse relationship
                        .HasForeignKey(ua => ua.AccountId)
                        .WillCascadeOnDelete(true); // EmailType will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<EmailType_de>(a => a.EmailTypes_de) //mark 1-* connection
                        .WithRequired(a => a.Account) // Create inverse relationship
                        .HasForeignKey(ua => ua.AccountId)
                        .WillCascadeOnDelete(true); // EmailType will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<EmailAttachment>(a => a.EmailAttachments) //mark 1-* connection
                        .WithRequired(a => a.Account) // Create inverse relationship
                        .HasForeignKey(ua => ua.AccountId)
                        .WillCascadeOnDelete(true); // EmailAttachment will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<EmailAttachment_de>(a => a.EmailAttachments_de) //mark 1-* connection
                       .WithRequired(a => a.Account) // Create inverse relationship
                       .HasForeignKey(ua => ua.AccountId)
                       .WillCascadeOnDelete(true); // EmailAttachment will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<User_Account>(a => a.User_Accounts) //mark 1-* connection
                        .WithRequired(a => a.Account) // Create inverse relationship
                        .HasForeignKey(ua => ua.AccountId)
                        .WillCascadeOnDelete(true); // User_Account will be automatically deleted, if Account will be deleted
            accountEntity.HasMany<Customer>(a => a.Customers) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<Offer>(a => a.Offers) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<Receipt>(a => a.Receipts) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<Bill>(a => a.Bills) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<PriceRate>(a => a.PriceRates) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<PriceRate_de>(a => a.PriceRates_de) //mark 1-* connection
                       .WithRequired(c => c.Account) // Create inverse relationship
                       .HasForeignKey(c => c.AccountId)
                       .WillCascadeOnDelete(false);
            accountEntity.HasMany<PriceAdditRate>(a => a.PriceAdditRates) //mark 1-* connection
                        .WithRequired(c => c.Account) // Create inverse relationship
                        .HasForeignKey(c => c.AccountId)
                        .WillCascadeOnDelete(false);
            accountEntity.HasMany<PriceAdditRate_de>(a => a.PriceAdditRates_de) //mark 1-* connection
                       .WithRequired(c => c.Account) // Create inverse relationship
                       .HasForeignKey(c => c.AccountId)
                       .WillCascadeOnDelete(false);

            accountEntity.HasMany<DefinedPackMaterial>(a => a.DefinedPackMaterials) //mark 1-* connection
                .WithRequired(c => c.Account) // Create inverse relationship
                .HasForeignKey(c => c.AccountId)
                .WillCascadeOnDelete(false);
            accountEntity.HasMany<DefinedPackMaterial_de>(a => a.DefinedPackMaterials_de) //mark 1-* connection
               .WithRequired(c => c.Account) // Create inverse relationship
               .HasForeignKey(c => c.AccountId)
               .WillCascadeOnDelete(false);
            accountEntity.HasMany<AutoJob>(a => a.AutoJobs) //mark 1-* connection
                .WithRequired(c => c.Account) // Create inverse relationship
                .HasForeignKey(c => c.AccountId)
                .WillCascadeOnDelete(false);
            accountEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            accountEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            accountEntity.Property(p => p.ModifyUserId).IsOptional();

            #region AccountStuff
            EntityTypeConfiguration<AccountEmployee> accountEmployeeEntity = modelBuilder.Entity<AccountEmployee>();
            accountEmployeeEntity.HasMany<Offer>(a => a.Offers) //mark 1-* connection
                .WithOptional(c => c.ContactPerson) // Create inverse relationship
                .HasForeignKey(c => c.AccountEmployeeId)
                .WillCascadeOnDelete(false);
            accountEmployeeEntity.Property(p => p.UserId).IsOptional();
            accountEmployeeEntity.Property(p => p.Email).IsOptional();
            accountEmployeeEntity.Property(p => p.Phone).IsOptional();
            accountEmployeeEntity.Property(p => p.Mobile).IsOptional();
            accountEmployeeEntity.Property(p => p.Street).IsOptional();
            accountEmployeeEntity.Property(p => p.City).IsOptional();
            accountEmployeeEntity.Property(p => p.CountryCode).IsOptional();
            accountEmployeeEntity.Property(p => p.Note).IsOptional();
            accountEmployeeEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            accountEmployeeEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            accountEmployeeEntity.Property(p => p.ModifyUserId).IsOptional();
            accountEmployeeEntity.Property(p => p.DeleteDate).HasColumnType("datetime2").IsOptional();
            accountEmployeeEntity.Property(p => p.DeleteUserId).IsOptional();
            #endregion

            #endregion

            #region Customer
            // Customer
            EntityTypeConfiguration<Customer> customerEntity = modelBuilder.Entity<Customer>();
            customerEntity.HasMany<Appointment>(c => c.Appointments) //mark 1-* connection to Notes
                        .WithRequired(n => n.Customer) // Create inverse relationship
                        .HasForeignKey(n => n.CustomerId);
            customerEntity.HasMany<Note>(c => c.Notes) //mark 1-* connection to Notes
                        .WithRequired(n => n.Customer) // Create inverse relationship
                        .HasForeignKey(n => n.CustomerId);
            customerEntity.HasMany<Offer>(c => c.Offers) //mark 1-* connection
                        .WithRequired(o => o.Customer) // Create inverse relationship
                        .HasForeignKey(o => o.CustomerId);
            customerEntity.HasMany<Receipt>(c => c.Receipts) //mark 1-* connection
                        .WithRequired(o => o.Customer) // Create inverse relationship
                        .HasForeignKey(o => o.CustomerId);
            customerEntity.HasMany<Bill>(c => c.Bills) //mark 1-* connection
                        .WithRequired(o => o.Customer) // Create inverse relationship
                        .HasForeignKey(o => o.CustomerId);
            customerEntity.HasMany<EmailMarketingCustomer>(c => c.EmailMarketingCustomers) //mark 1-* connection
                        .WithRequired(emc => emc.Customer) // Create inverse relationship
                        .HasForeignKey(emc => emc.CustomerId)
                        .WillCascadeOnDelete(true);
            customerEntity.Property(p => p.TitleId).IsOptional();
            customerEntity.Property(p => p.LastName).IsOptional();
            customerEntity.Property(p => p.FirstName).IsOptional();
            customerEntity.Property(p => p.CompanyName).IsOptional();
            customerEntity.Property(p => p.CustomerSource).IsOptional();
            customerEntity.Property(p => p.OtherSource).IsOptional();
            customerEntity.Property(p => p.CustomerSource_de).IsOptional();
            customerEntity.Property(p => p.OtherSource_de).IsOptional();
            customerEntity.Property(p => p.CompanyContactPerson).IsOptional();
            customerEntity.Property(p => p.CountryCode).IsOptional();
            customerEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            customerEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            customerEntity.Property(p => p.DeleteDate).HasColumnType("datetime2").IsOptional();
            customerEntity.Property(p => p.ModifyUserId).IsOptional();
            customerEntity.Property(p => p.DeleteUserId).IsOptional();
            customerEntity.Property(p => p.ContractTypeChangeDate).HasColumnType("datetime2").IsOptional();
            #endregion

            #region Appointment
            // Appointments
            EntityTypeConfiguration<Appointment> appointmentEntity = modelBuilder.Entity<Appointment>();
            appointmentEntity.HasOptional(a => a.AppointmentView) // Mark AppointmentView is optional for Appointment (1-1 connection)
                .WithRequired(ad => ad.Appointment) // Create inverse relationship
                .WillCascadeOnDelete(true); // AppointmentView will be automatically deleted, if Appointment will be deleted
            appointmentEntity.HasOptional(a => a.AppointmentService) // Mark AppointmentService is optional for Appointment (1-1 connection)
                .WithRequired(ad => ad.Appointment) // Create inverse relationship
                .WillCascadeOnDelete(true); // AppointmentService will be automatically deleted, if Appointment will be deleted
            appointmentEntity.HasOptional(a => a.AppointmentDelivery) // Mark AppointmentDelivery is optional for Appointment (1-1 connection)
                .WithRequired(ad => ad.Appointment) // Create inverse relationship
                .WillCascadeOnDelete(true); // AppointmentDelivery will be automatically deleted, if Appointment will be deleted
            appointmentEntity.Property(p => p.OfferId).IsOptional();
            
            appointmentEntity.Property(p => p.OfferInnerId).IsOptional();
            appointmentEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            appointmentEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            appointmentEntity.Property(p => p.ModifyUserId).IsOptional();
            appointmentEntity.Property(p => p.AccountId).HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new IndexAttribute("IX_AccountId")));

            #region AppointmentView
            // AppointmentViews
            EntityTypeConfiguration<AppointmentView> appointmentViewEntity = modelBuilder.Entity<AppointmentView>();
            appointmentViewEntity.HasKey(p => p.AppointmentId);
            appointmentViewEntity.Property(p => p.MeetPlace).IsOptional();
            appointmentViewEntity.Property(p => p.ViewDateTime).HasColumnType("datetime2").IsOptional();
            #endregion

            #region AppointmentService
            // AppointmentServices
            EntityTypeConfiguration<AppointmentService> appointmentServiceEntity = modelBuilder.Entity<AppointmentService>();
            appointmentServiceEntity.HasKey(p => p.AppointmentId);
            appointmentServiceEntity.Property(p => p.PaymentType).IsOptional();
            appointmentServiceEntity.Property(p => p.UmzugDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.UmzugDateTime2).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.UmzugDateTime3).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.PackDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.PackOutDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.EntsorgungDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.ReinigungDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.Reinigung2DateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.LagerungDateTime).HasColumnType("datetime2").IsOptional();
            appointmentServiceEntity.Property(p => p.TransportDateTime).HasColumnType("datetime2").IsOptional();
            #endregion

            #region AppointmentDelivery
            // AppointmentDeliveries
            EntityTypeConfiguration<AppointmentDelivery> appointmentDeliveryEntity = modelBuilder.Entity<AppointmentDelivery>();
            appointmentDeliveryEntity.HasKey(p => p.AppointmentId);
            appointmentDeliveryEntity.Property(p => p.Delivery).IsOptional();
            appointmentDeliveryEntity.Property(p => p.DeliveryDateTime).HasColumnType("datetime2").IsOptional();
            #endregion

            #endregion

            #region Note
            // Notes
            EntityTypeConfiguration<Note> noteEntity = modelBuilder.Entity<Note>();
            noteEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            noteEntity.Property(p => p.DeleteDate).HasColumnType("datetime2").IsOptional();
            noteEntity.Property(p => p.DeleteUserId).IsOptional();
            #endregion

            #region Offer
            // Offer
            EntityTypeConfiguration<Offer> offerEntity = modelBuilder.Entity<Offer>();
            offerEntity.HasMany<OfferCleaning>(o => o.OfferCleanings) //mark 1-* connection to OfferCleaning
                        .WithRequired(n => n.Offer) // Create inverse relationship
                        .HasForeignKey(n => n.OfferId)
                        .WillCascadeOnDelete(true);
            offerEntity.HasMany<OfferTransport>(o => o.OfferTransports) //mark 1-* connection to OfferTransport
                        .WithRequired(n => n.Offer) // Create inverse relationship
                        .HasForeignKey(n => n.OfferId)
                        .WillCascadeOnDelete(true);
            offerEntity.HasMany<Receipt>(o => o.Receipts) //mark 1-* connection to OfferTransport
                        .WithRequired(n => n.Offer) // Create inverse relationship
                        .HasForeignKey(n => n.OfferId)
                        .WillCascadeOnDelete(false);
            offerEntity.HasMany<Offer_PriceAddit>(o => o.Offer_PriceAddits) //mark 1-* connection to Notes
                        .WithRequired(n => n.Offer) // Create inverse relationship
                        .HasForeignKey(n => n.OfferId);
            offerEntity.HasMany<Offer_PriceAddit_de>(o => o.Offer_PriceAddits_de) //mark 1-* connection to Notes
                       .WithRequired(n => n.Offer) // Create inverse relationship
                       .HasForeignKey(n => n.OfferId);
            offerEntity.HasMany<Offer_DefinedPackMaterial>(o => o.Offer_DefinedPackMaterials) // Mark 1-* connection to Offer_DefinedPackMaterials
                        .WithRequired(op => op.Offer) // Create inverse relationship
                        .HasForeignKey(op => op.OfferId)
                        .WillCascadeOnDelete(true); // Offer_DefinedPackMaterials will be automatically deleted, if Offer will be deleted
            offerEntity.HasMany<OfferPackMaterial>(o => o.OfferPackMaterials) // Mark OfferPackMaterial is optional for Offer (1-1 connection)
                        .WithRequired(op => op.Offer) // Create inverse relationship
                        .HasForeignKey(op => op.OfferId)
                        .WillCascadeOnDelete(true); // OfferPackMaterial will be automatically deleted, if Offer will be deleted
            offerEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            offerEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.ModifyUserId).IsOptional();
            offerEntity.Property(p => p.AcceptDate).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.ReleaseDate).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.ReleaseUserId).IsOptional();
            offerEntity.Property(p => p.AccountEmployeeId).IsOptional();
            offerEntity.Property(p => p.AdrOutHasLift).IsOptional();
            offerEntity.Property(p => p.AdrOutBuildingType).IsOptional();
            offerEntity.Property(p => p.AdrInHasLift).IsOptional();
            offerEntity.Property(p => p.AdrInBuildingType).IsOptional();
            offerEntity.Property(p => p.AdrOutHasLift2).IsOptional();
            offerEntity.Property(p => p.AdrOutBuildingType2).IsOptional();
            offerEntity.Property(p => p.AdrInHasLift2).IsOptional();
            offerEntity.Property(p => p.AdrInBuildingType2).IsOptional();
            offerEntity.Property(p => p.AdrOutHasLift3).IsOptional();
            offerEntity.Property(p => p.AdrOutBuildingType3).IsOptional();
            offerEntity.Property(p => p.AdrInHasLift3).IsOptional();
            offerEntity.Property(p => p.AdrInBuildingType3).IsOptional();
            offerEntity.Property(p => p.UmzugDateTime).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.UmzugInDateTime).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.UmzugCostWay).IsOptional();
            offerEntity.Property(p => p.UmzugPriceRateId).IsOptional();
            offerEntity.Property(p => p.UmzugCostChangedPers).IsOptional();
            offerEntity.Property(p => p.UmzugCostChangedVeh).IsOptional();
            offerEntity.Property(p => p.UmzugCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.UmzugCostAdditFreeTextPlus1Price).IsOptional();
            offerEntity.Property(p => p.UmzugCostAdditFreeTextPlus2Price).IsOptional();
            offerEntity.Property(p => p.UmzugCostAdditFreeTextMinus1Price).IsOptional();
            offerEntity.Property(p => p.UmzugCostDiscount).IsOptional();
            offerEntity.Property(p => p.UmzugCostDiscount2).IsOptional();
            offerEntity.Property(p => p.UmzugCostHighSecurityPrice).IsOptional();
            offerEntity.Property(p => p.UmzugCostFixPrice).IsOptional();
            offerEntity.Property(p => p.PackDateTime).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.PackCostWay).IsOptional();
            offerEntity.Property(p => p.PackPriceRateId).IsOptional();
            offerEntity.Property(p => p.PackCostChangedPers).IsOptional();
            offerEntity.Property(p => p.PackCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.PackCostAdditFreeTextPlus1Price).IsOptional();
            offerEntity.Property(p => p.PackCostAdditFreeTextPlus2Price).IsOptional();
            offerEntity.Property(p => p.PackCostAdditFreeTextMinus1Price).IsOptional();
            offerEntity.Property(p => p.PackCostDiscount).IsOptional();
            offerEntity.Property(p => p.PackCostDiscount2).IsOptional();
            offerEntity.Property(p => p.PackOutDateTime).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.PackOutCostWay).IsOptional();
            offerEntity.Property(p => p.PackOutPriceRateId).IsOptional();
            offerEntity.Property(p => p.PackOutCostChangedPers).IsOptional();
            offerEntity.Property(p => p.PackOutCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.PackOutCostAdditFreeTextPlus1Price).IsOptional();
            offerEntity.Property(p => p.PackOutCostAdditFreeTextPlus2Price).IsOptional();
            offerEntity.Property(p => p.PackOutCostAdditFreeTextMinus1Price).IsOptional();
            offerEntity.Property(p => p.PackOutCostDiscount).IsOptional();
            offerEntity.Property(p => p.PackOutCostDiscount2).IsOptional();
            offerEntity.Property(p => p.EntsorgungDateTime).HasColumnType("datetime2").IsOptional();
            offerEntity.Property(p => p.EntsorgungPersonPriceRateId).IsOptional();
            offerEntity.Property(p => p.EntsorgungPersonCostChangedPers).IsOptional();
            offerEntity.Property(p => p.EntsorgungPersonCostChangedVeh).IsOptional();
            offerEntity.Property(p => p.EntsorgungPersonCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostWay).IsOptional();
            offerEntity.Property(p => p.EntsorgungPriceRateId).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostFixPrice).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostAdditFreeTextPlus1Price).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostAdditFreeTextPlus2Price).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostAdditFreeTextMinus1Price).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostDiscount).IsOptional();
            offerEntity.Property(p => p.EntsorgungCostHighSecurityPrice).IsOptional();
            offerEntity.Property(p => p.LagerungPriceRateId).IsOptional();
            offerEntity.Property(p => p.LagerungCostChangedPrice).IsOptional();
            offerEntity.Property(p => p.LagerungCostFixPrice).IsOptional();

            #region OfferCleaning
            // OfferCleanings
            EntityTypeConfiguration<OfferCleaning> offerCleaningEntity = modelBuilder.Entity<OfferCleaning>();
            offerCleaningEntity.Property(p => p.ReinigungType).IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungPriceRateId).IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungCostChangedPrice).IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungHourlyCostChangedPers).IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungHourlyCostChangedPrice).IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungDateTime).HasColumnType("datetime2").IsOptional();
            offerCleaningEntity.Property(p => p.ReinigungCostFixPrice).IsOptional();
            offerCleaningEntity.Property(p => p.AdditServiceDuebellocher).IsOptional();
            offerCleaningEntity.Property(p => p.AdditServiceHochdruckreiniger).IsOptional();
            #endregion

            #region OfferTransport
            // OfferCleanings
            EntityTypeConfiguration<OfferTransport> offerTransportEntity = modelBuilder.Entity<OfferTransport>();
            offerTransportEntity.Property(p => p.TransportDateTime).HasColumnType("datetime2").IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostWay).IsOptional();
            offerTransportEntity.Property(p => p.TransportCostFixRatePrice).IsOptional();
            offerTransportEntity.Property(p => p.TransportPriceRateId).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostChangedPers).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostChangedVeh).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostChangedTrailer).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostChangedPrice).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus1Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus2Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus3Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus4Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus5Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus6Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextPlus7Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextMinus1Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostAdditFreeTextMinus2Price).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostDiscount).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostDiscount2).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostHighSecurityPrice).IsOptional();
            //offerTransportEntity.Property(p => p.TransportCostFixPrice).IsOptional();
            #endregion


            #endregion

            #region Receipt
            // Receipt
            EntityTypeConfiguration<Receipt> receiptEntity = modelBuilder.Entity<Receipt>();
            receiptEntity.HasMany<ReceiptUmzug>(o => o.ReceiptUmzugs) // Mark ReceiptUmzug is optional for Receipt
                .WithRequired(op => op.Receipt) // Create inverse relationship
                .HasForeignKey(op => op.ReceiptId)
                .WillCascadeOnDelete(true); // ReceiptUmzug will be automatically deleted, if Receipt will be deleted
            receiptEntity.HasMany<ReceiptReinigung>(o => o.ReceiptReinigungs) // Mark ReceiptUmzug is optional for Receipt
                .WithRequired(op => op.Receipt) // Create inverse relationship
                .HasForeignKey(op => op.ReceiptId)
                .WillCascadeOnDelete(true); // ReceiptUmzug will be automatically deleted, if Receipt will be deleted
            receiptEntity.Property(p => p.ReceiptInnerSubId).IsOptional();
            receiptEntity.Ignore(p => p.ReceiptInnerIdExtern);
            receiptEntity.Property(p => p.AppointmentId).IsOptional();
            receiptEntity.Property(p => p.JobDateTime).HasColumnType("datetime2").IsOptional();
            receiptEntity.Property(p => p.ClosedDate).HasColumnType("datetime2").IsOptional();
            receiptEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            receiptEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            receiptEntity.Property(p => p.ModifyUserId).IsOptional();

            // ReceiptUmzug
            EntityTypeConfiguration<ReceiptUmzug> receiptUmzugEntity = modelBuilder.Entity<ReceiptUmzug>();

            // ReceiptReinigung
            EntityTypeConfiguration<ReceiptReinigung> receiptReinigungEntity = modelBuilder.Entity<ReceiptReinigung>();
            #endregion

            #region Bill
            // Bill
            EntityTypeConfiguration<Bill> billEntity = modelBuilder.Entity<Bill>();
            billEntity.HasMany<BillCleaning>(o => o.BillCleanings) //mark 1-* connection to BillCleaning
                .WithRequired(n => n.Bill) // Create inverse relationship
                .HasForeignKey(n => n.BillId)
                .WillCascadeOnDelete(true);
            billEntity.HasMany<BillTransport>(o => o.BillTransports) //mark 1-* connection to BillTransport
                .WithRequired(n => n.Bill) // Create inverse relationship
                .HasForeignKey(n => n.BillId)
                .WillCascadeOnDelete(true);
            billEntity.HasMany<Bill_PriceAddit>(o => o.Bill_PriceAddits) //mark 1-* connection to Notes
                        .WithRequired(n => n.Bill) // Create inverse relationship
                        .HasForeignKey(n => n.BillId);
            billEntity.HasMany<Bill_PriceAddit_de>(o => o.Bill_PriceAddits_de) //mark 1-* connection to Notes
                       .WithRequired(n => n.Bill) // Create inverse relationship
                       .HasForeignKey(n => n.BillId);
            billEntity.HasMany<Bill_DefinedPackMaterial>(o => o.Bill_DefinedPackMaterials) // Mark 1-* connection to Bill_DefinedPackMaterials
                        .WithRequired(b => b.Bill) // Create inverse relationship
                        .HasForeignKey(b => b.BillId)
                        .WillCascadeOnDelete(true); // Bill_DefinedPackMaterials will be automatically deleted, if Offer will be deleted
            billEntity.HasMany<Bill_DefinedPackMaterial_de>(o => o.Bill_DefinedPackMaterials_de) // Mark 1-* connection to Bill_DefinedPackMaterials
                        .WithRequired(b => b.Bill) // Create inverse relationship
                        .HasForeignKey(b => b.BillId)
                        .WillCascadeOnDelete(true); // Bill_DefinedPackMaterials will be automatically deleted, if Offer will be deleted
            billEntity.HasMany<BillPackMaterial>(o => o.BillPackMaterials) // Mark BillPackMaterial is optional for Bill (1-1 connection)
                        .WithRequired(op => op.Bill) // Create inverse relationship
                        .HasForeignKey(op => op.BillId)
                        .WillCascadeOnDelete(true); // BillPackMaterial will be automatically deleted, if Bill will be deleted
            billEntity.Property(p => p.ReceiptId).IsOptional();
            billEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            billEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();
            billEntity.Property(p => p.ModifyUserId).IsOptional();
            billEntity.Property(p => p.ToPayDate).HasColumnType("datetime2");
            billEntity.Property(p => p.PayedDate).HasColumnType("datetime2").IsOptional();
            billEntity.Property(p => p.PayedUserId).IsOptional();
            billEntity.Property(p => p.ReminderDate).HasColumnType("datetime2").IsOptional();
            billEntity.Property(p => p.ReminderUserId).IsOptional();
            billEntity.Property(p => p.PreCollectionDate).HasColumnType("datetime2").IsOptional();
            billEntity.Property(p => p.PreCollectionUserId).IsOptional();
            billEntity.Property(p => p.SendStateId).IsOptional();
            billEntity.Property(p => p.UmzugPerPrice).IsOptional();
            billEntity.Property(p => p.UmzugCostService).IsOptional();
            billEntity.Property(p => p.UmzugCostService2).IsOptional();
            billEntity.Property(p => p.UmzugCostWay).IsOptional();
            billEntity.Property(p => p.UmzugCostAdditFreeTextPlus1Price).IsOptional();
            billEntity.Property(p => p.UmzugCostAdditFreeTextPlus2Price).IsOptional();
            billEntity.Property(p => p.UmzugCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.UmzugCostAdditFreeTextMinus2Price).IsOptional();
            billEntity.Property(p => p.UmzugCostDiscount).IsOptional();
            billEntity.Property(p => p.UmzugCostDiscount2).IsOptional();
            billEntity.Property(p => p.UmzugCostDamage).IsOptional();
            billEntity.Property(p => p.UmzugCostPartialPayment).IsOptional();
            billEntity.Property(p => p.UmzugCostPartialByCash).IsOptional();
            billEntity.Property(p => p.UmzugCostSub).IsOptional();
            billEntity.Property(p => p.UmzugCost).IsOptional();
            billEntity.Property(p => p.UmzugCostFixPrice).IsOptional();
            billEntity.Property(p => p.UmzugCostHighSecurityPrice).IsOptional();
            billEntity.Property(p => p.PackPerPrice).IsOptional();
            billEntity.Property(p => p.PackCostService).IsOptional();
            billEntity.Property(p => p.PackCostService2).IsOptional();
            billEntity.Property(p => p.PackCostWay).IsOptional();
            billEntity.Property(p => p.PackCostAdditFreeTextPlus1Price).IsOptional();
            billEntity.Property(p => p.PackCostAdditFreeTextPlus2Price).IsOptional();
            billEntity.Property(p => p.PackCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.PackCostAdditFreeTextMinus2Price).IsOptional();
            billEntity.Property(p => p.PackCostDiscount).IsOptional();
            billEntity.Property(p => p.PackCostDiscount2).IsOptional();
            billEntity.Property(p => p.PackCostDamage).IsOptional();
            billEntity.Property(p => p.PackCostPartialPayment).IsOptional();
            billEntity.Property(p => p.PackCostPartialByCash).IsOptional();
            billEntity.Property(p => p.PackCostSub).IsOptional();
            billEntity.Property(p => p.PackCost).IsOptional();
            billEntity.Property(p => p.PackCostFixPrice).IsOptional();
            billEntity.Property(p => p.PackCostHighSecurityPrice).IsOptional();
            billEntity.Property(p => p.PackOutPerPrice).IsOptional();
            billEntity.Property(p => p.PackOutCostService).IsOptional();
            billEntity.Property(p => p.PackOutCostService2).IsOptional();
            billEntity.Property(p => p.PackOutCostWay).IsOptional();
            billEntity.Property(p => p.PackOutCostAdditFreeTextPlus1Price).IsOptional();
            billEntity.Property(p => p.PackOutCostAdditFreeTextPlus2Price).IsOptional();
            billEntity.Property(p => p.PackOutCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.PackOutCostAdditFreeTextMinus2Price).IsOptional();
            billEntity.Property(p => p.PackOutCostDiscount).IsOptional();
            billEntity.Property(p => p.PackOutCostDiscount2).IsOptional();
            billEntity.Property(p => p.PackOutCostDamage).IsOptional();
            billEntity.Property(p => p.PackOutCostPartialPayment).IsOptional();
            billEntity.Property(p => p.PackOutCostPartialByCash).IsOptional();
            billEntity.Property(p => p.PackOutCostSub).IsOptional();
            billEntity.Property(p => p.PackOutCost).IsOptional();
            billEntity.Property(p => p.PackOutCostFixPrice).IsOptional();
            billEntity.Property(p => p.PackOutCostHighSecurityPrice).IsOptional();
            billEntity.Property(p => p.EntsorgungPerPrice).IsOptional();
            billEntity.Property(p => p.EntsorgungCostService).IsOptional();
            billEntity.Property(p => p.EntsorgungCostAdditFreeTextPlus1Price).IsOptional();
            billEntity.Property(p => p.EntsorgungCostAdditFreeTextPlus2Price).IsOptional();
            billEntity.Property(p => p.EntsorgungCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.EntsorgungCostAdditFreeTextMinus2Price).IsOptional();
            billEntity.Property(p => p.EntsorgungCostDiscount).IsOptional();
            billEntity.Property(p => p.EntsorgungCostDiscount2).IsOptional();
            billEntity.Property(p => p.EntsorgungCostPartialPayment).IsOptional();
            billEntity.Property(p => p.EntsorgungCostPartialByCash).IsOptional();
            billEntity.Property(p => p.EntsorgungCostSub).IsOptional();
            billEntity.Property(p => p.EntsorgungCost).IsOptional();
            billEntity.Property(p => p.EntsorgungCostFixPrice).IsOptional();
            billEntity.Property(p => p.LagerungPerPrice).IsOptional();
            billEntity.Property(p => p.LagerungCostService).IsOptional();
            billEntity.Property(p => p.LagerungCostAdditFreeTextPlus1Price).IsOptional();
            billEntity.Property(p => p.LagerungCostAdditFreeTextPlus2Price).IsOptional();
            billEntity.Property(p => p.LagerungCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.LagerungCostAdditFreeTextMinus2Price).IsOptional();
            billEntity.Property(p => p.LagerungCostDiscount).IsOptional();
            billEntity.Property(p => p.LagerungCostDiscount2).IsOptional();
            billEntity.Property(p => p.LagerungCostPartialPayment).IsOptional();
            billEntity.Property(p => p.LagerungCostPartialByCash).IsOptional();
            billEntity.Property(p => p.LagerungCostSub).IsOptional();
            billEntity.Property(p => p.LagerungCost).IsOptional();
            billEntity.Property(p => p.LagerungCostFixPrice).IsOptional();
           
            billEntity.Property(p => p.PackMaterialCostDiscount).IsOptional();
            billEntity.Property(p => p.PackMaterialCostAdditFreeTextMinus1Price).IsOptional();
            billEntity.Property(p => p.PackMaterialDeliverPrice).IsOptional();
            billEntity.Property(p => p.PackMaterialPickupPrice).IsOptional();
            billEntity.Property(p => p.PackMaterialCost).IsOptional();
            billEntity.Property(p => p.BillCost).IsOptional();
            billEntity.Property(p => p.PreCollectionCost).IsOptional();

            #region BillCleaning
            // BillCleanings
            EntityTypeConfiguration<BillCleaning> billCleaningEntity = modelBuilder.Entity<BillCleaning>();
            billCleaningEntity.Property(p => p.ReinigungType).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungPerPrice).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungHourlyPerPrice).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungHourlyCostService).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostAdditFreeTextPlus1Price).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostAdditFreeTextPlus2Price).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostAdditFreeTextMinus1Price).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostAdditFreeTextMinus2Price).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostDiscount).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostDiscount2).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostDamage).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostPartialPayment).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostPartialByCash).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostSub).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCost).IsOptional();
            billCleaningEntity.Property(p => p.ReinigungCostFixPrice).IsOptional();
            #endregion

            #region BillTransport
            // BillTransports
            EntityTypeConfiguration<BillTransport> billTransportEntity = modelBuilder.Entity<BillTransport>();
            billTransportEntity.Property(p => p.TransportCostFixRatePrice).IsOptional();
            billTransportEntity.Property(p => p.TransportCostService2).IsOptional();
            #endregion

            #endregion

            #region PriceRates
            //   PriceRate
            EntityTypeConfiguration<PriceRate> priceRateEntity = modelBuilder.Entity<PriceRate>();
            priceRateEntity.Property(p => p.CountPriceRangeMax).IsOptional();
            // PriceAdditRate
            EntityTypeConfiguration<PriceAdditRate> priceAdditRateEntity = modelBuilder.Entity<PriceAdditRate>();
            priceAdditRateEntity.HasMany<Offer_PriceAddit>(c => c.Offer_PriceAddits) //mark 1-* connection to Notes
                        .WithRequired(n => n.PriceAdditRate) // Create inverse relationship
                        .HasForeignKey(n => n.PriceAdditRateId);
            priceAdditRateEntity.HasMany<Bill_PriceAddit>(c => c.Bill_PriceAddits) //mark 1-* connection to Notes
                        .WithRequired(n => n.PriceAdditRate) // Create inverse relationship
                        .HasForeignKey(n => n.PriceAdditRateId);
            EntityTypeConfiguration<PriceAdditRate_de> priceAdditRateEntity_DE = modelBuilder.Entity<PriceAdditRate_de>();
            priceAdditRateEntity_DE.HasMany<Offer_PriceAddit_de>(c => c.Offer_PriceAddits_de) //mark 1-* connection to Notes
                        .WithRequired(n => n.PriceAdditRate_de) // Create inverse relationship
                        .HasForeignKey(n => n.PriceAdditRateId);
            priceAdditRateEntity_DE.HasMany<Bill_PriceAddit_de>(c => c.Bill_PriceAddits_de) //mark 1-* connection to Notes
                        .WithRequired(n => n.PriceAdditRate_de) // Create inverse relationship
                        .HasForeignKey(n => n.PriceAdditRateId);


            #endregion

            #region DefinedPackMaterial

            EntityTypeConfiguration<DefinedPackMaterial> definedPackMatEntity = modelBuilder.Entity<DefinedPackMaterial>();
            definedPackMatEntity.HasMany<Offer_DefinedPackMaterial>(c => c.Offer_DefinedPackMaterials) //mark 1-* connection to Offer_DefinedPackMaterial
                .WithRequired(n => n.DefinedPackMaterial) // Create inverse relationship
                .HasForeignKey(n => n.DefinedPackMaterialId);
            definedPackMatEntity.HasMany<Bill_DefinedPackMaterial>(c => c.Bill_DefinedPackMaterials) //mark 1-* connection to Bill_DefinedPackMaterial
                .WithRequired(n => n.DefinedPackMaterial) // Create inverse relationship
                .HasForeignKey(n => n.DefinedPackMaterialId);
            definedPackMatEntity.Property(p => p.PriceRent).IsOptional();
            definedPackMatEntity.Property(p => p.PriceBuy).IsOptional();

            #endregion

            #region OfferPackMaterial

            EntityTypeConfiguration<OfferPackMaterial> offPackMatEntity = modelBuilder.Entity<OfferPackMaterial>();
            offPackMatEntity.HasKey(x => x.PackMaterialId);
            offPackMatEntity.Property(p => p.PackMaterialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            offPackMatEntity.Property(p => p.Text).IsOptional();
            offPackMatEntity.Property(p => p.PricePerPiece).IsOptional();
            offPackMatEntity.Property(p => p.CountNumber).IsOptional();
            offPackMatEntity.Property(p => p.EndPrice).IsOptional();

            #endregion

            #region BillPackMaterial

            EntityTypeConfiguration<BillPackMaterial> billPackMatEntity = modelBuilder.Entity<BillPackMaterial>();
            billPackMatEntity.HasKey(x => x.PackMaterialId);
            billPackMatEntity.Property(p => p.PackMaterialId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            billPackMatEntity.Property(p => p.Text).IsOptional();
            billPackMatEntity.Property(p => p.PricePerPiece).IsOptional();
            billPackMatEntity.Property(p => p.CountNumber).IsOptional();
            billPackMatEntity.Property(p => p.EndPrice).IsOptional();

            #endregion

            #region AutoJob
            // AutoJob
            EntityTypeConfiguration<AutoJob> autoJobEntity = modelBuilder.Entity<AutoJob>();
            autoJobEntity.HasMany<AutoJobSentEmail>(aj => aj.AutoJobSentEmails) // Mark 1-* AutoJobSentEmail for AutoJob
                        .WithRequired(aj => aj.AutoJob) // Create inverse relationship
                        .HasForeignKey(ajse => ajse.AutoJobId)
                        .WillCascadeOnDelete(true); // AutoJobSentEmail will be automatically deleted, if AutoJob will be deleted
            autoJobEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            autoJobEntity.Property(p => p.CalledUrl).IsOptional();
            autoJobEntity.Property(p => p.CalledSourceIP).IsOptional();
            autoJobEntity.Property(p => p.AffectedDbTable).IsOptional();
            autoJobEntity.Property(p => p.AffectedIdList).IsOptional();
            autoJobEntity.Property(p => p.AffectedEmailList).IsOptional();
            autoJobEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();

            #region AutoJobSentEmail
            // AutoJobSentEmail
            EntityTypeConfiguration<AutoJobSentEmail> autoJobSentEmailEntity = modelBuilder.Entity<AutoJobSentEmail>();
            autoJobSentEmailEntity.Property(p => p.Type).IsOptional();
            autoJobSentEmailEntity.Property(p => p.AffectedDbTable).IsOptional();
            autoJobSentEmailEntity.Property(p => p.AffectedId).IsOptional();
            autoJobSentEmailEntity.Property(p => p.SentEmailAddress).IsOptional();
            #endregion

            #endregion

            #region EmailMarketing
            // EmailMarketing
            EntityTypeConfiguration<EmailMarketing> emailMarketingEntity = modelBuilder.Entity<EmailMarketing>();
            emailMarketingEntity.HasMany<EmailMarketingAttachment>(em => em.EmailMarketingAttachments) // Mark 1-* EmailMarketingAttachment for EmailMarketing
                        .WithRequired(ema => ema.EmailMarketing) // Create inverse relationship
                        .HasForeignKey(ema => ema.EmailMarketingId)
                        .WillCascadeOnDelete(false);
            emailMarketingEntity.HasMany<EmailMarketingCustomer>(em => em.EmailMarketingCustomers) // Mark 1-* EmailMarketingCustomer for EmailMarketing
                        .WithRequired(emc => emc.EmailMarketing) // Create inverse relationship
                        .HasForeignKey(emc => emc.EmailMarketingId)
                        .WillCascadeOnDelete(false);
            emailMarketingEntity.Property(p => p.Description).IsOptional();
            emailMarketingEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            emailMarketingEntity.Property(p => p.ModifyDate).HasColumnType("datetime2").IsOptional();

            #region EmailMarketingAttachment
            // EmailMarketingAttachment
            EntityTypeConfiguration<EmailMarketingAttachment> emailMarketingAttachmentEntity = modelBuilder.Entity<EmailMarketingAttachment>();
            emailMarketingAttachmentEntity.Property(p => p.Description).IsOptional();
            #endregion

            #region EmailMarketingCustomer
            // EmailMarketingCustomer
            EntityTypeConfiguration<EmailMarketingCustomer> emailMarketingCustomerEntity = modelBuilder.Entity<EmailMarketingCustomer>();
            emailMarketingCustomerEntity.Property(p => p.CreateDate).HasColumnType("datetime2");
            emailMarketingCustomerEntity.Property(p => p.AffectedDbTable).IsOptional();
            emailMarketingCustomerEntity.Property(p => p.AffectedId).IsOptional();
            emailMarketingCustomerEntity.Property(p => p.AffectedInnerId).IsOptional();
            emailMarketingCustomerEntity.Property(p => p.EmailContent).IsOptional();
            emailMarketingCustomerEntity.Property(p => p.EmailAttachmentNameList).IsOptional();
            #endregion

            #endregion


            #region System

            EntityTypeConfiguration<Log> logEntity = modelBuilder.Entity<Log>();
            logEntity.Property(p => p.CreateDate).HasColumnType("datetime2");

            #endregion
        }
    }
}