﻿using CRM_Umzugsfirma.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Admin.Models;

namespace CRM_Umzugsfirma.DB
{
    //public class InitDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    public class InitDbInitializer : CreateDatabaseIfNotExists<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            UserLoginDbContext dbApplication = new UserLoginDbContext();
            dbApplication.Database.Initialize(true);

            RoleManager<ApplicationRole> _roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new UserLoginDbContext()));
            ApplicationUserManager _userManager = HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();

            // create roles and users
            initializeAdminUser(_userManager, _roleManager);

            // create account and emailconfigs
            List<Account> defaultAccounts = new List<Account>();
            defaultAccounts.Add(new Account() { InnerAccountId = 7, AccountName = "Helvetia Transporte AG", Street = "Bernerstrasse 335", Zip = 8952, City = "Schlieren", Phone = "0848 800 990", Fax = "0848 800 991", Email = "info@helvetiatransporte.ch", WebPage = "www.helvetiatransporte.ch", GoogleEmail = "helvetiatransport@gmail.com", CreateDate = DateTime.Now, IsActive = true, AccountEmailConfig = new AccountEmailConfig() { Host = "mail.cyon.ch", Port = 587, UseSSL = true, SenderAccount = "info@helvetiatransporte.ch", SenderAccountPass = "besey123456", SenderDisplayName = "Helvetia Transporte AG", SenderAddressSales = "sales@helvetiatransporte.ch", SenderAddressAccounting = "accounting@helvetiatransporte.ch" } });

            defaultAccounts.ForEach(a => context.Accounts.Add(a));
            context.SaveChanges();

            // create relation user to account
            List<User_Account> defaultUserAccounts = new List<User_Account>();
            ApplicationUser user = _userManager.FindByEmail("info@helvetiatransporte.ch");
            if (user != null)
            {
                defaultUserAccounts.Add(new User_Account() { UserId = user.Id, AccountId = 3 });
            }
            defaultUserAccounts.ForEach(ua => context.User_Accounts.Add(ua));
            context.SaveChanges();

            // create EmailTypes
            List<EmailType> defaultEmailTypes = new List<EmailType>();
            defaultEmailTypes.Add(new EmailType() { AccountId = 3, Name = "ViewAppointment", MailSubject = "Appoinment Confirmation" });
            defaultEmailTypes.Add(new EmailType() { AccountId = 3, Name = "ServiceAppointment", MailSubject = "Confirmation of the order" });
            defaultEmailTypes.Add(new EmailType() { AccountId = 3, Name = "Offer", MailSubject = "Offer - Helvetia Transporte AG" });
            defaultEmailTypes.Add(new EmailType() { AccountId = 3, Name = "Bill", MailSubject = "Invoice - Helvetia Transporte AG" });
            defaultEmailTypes.ForEach(a => context.EmailTypes.Add(a));
            context.SaveChanges();
            // create Attachments
            List<EmailAttachment> defaultEmailAttachments = new List<EmailAttachment>();
            defaultEmailAttachments.Add(new EmailAttachment() { AccountId = 3, Name = "AGB", SortOrder = 1, FileName = "agb.pdf", MimeType = "application/pdf", IsViewAppointmentDefault = false, IsServiceAppointmentDefault = false, IsOfferDefault = true, IsBillDefault = false });
            defaultEmailAttachments.Add(new EmailAttachment() { AccountId = 3, Name = "Checklist", SortOrder = 2, FileName = "umzugs-checkliste.pdf", MimeType = "application/pdf", IsViewAppointmentDefault = false, IsServiceAppointmentDefault = false, IsOfferDefault = true, IsBillDefault = false });
            defaultEmailAttachments.Add(new EmailAttachment() { AccountId = 3, Name = "Cleaningtips", SortOrder = 3, FileName = "reinigungstipps.pdf", MimeType = "application/pdf", IsViewAppointmentDefault = false, IsServiceAppointmentDefault = false, IsOfferDefault = false, IsBillDefault = false });

            base.Seed(context);
        }

        private void initializeAdminUser(ApplicationUserManager _userManager, RoleManager<ApplicationRole> _roleManager)
        {

            IdentityResult result;

            // add role: AccountUser
            var exists = _roleManager.RoleExists("AccountUser");
            if (!exists)
            {
                result = _roleManager.Create(new ApplicationRole("AccountUser"));
            }

            // create user
            var user = _userManager.FindByEmail("info@helvetiatransporte.ch");
            if (user == null)
            {
                user = new ApplicationUser { UserName = "info@helvetiatransporte.ch", Email = "info@helvetiatransporte.ch" };
                result = _userManager.Create(user, "123456");

                // confirm email adress
                var token = _userManager.GenerateEmailConfirmationToken(user.Id);
                result = _userManager.ConfirmEmail(user.Id, token);

                // add role to user
                result = _userManager.AddToRole(user.Id, "AccountUser");
            }

        }
    }
}