﻿function markFormRequiredFieldAsterisk(parentid) {
    $('#' + parentid).find('input[data-val-required]:not(:checkbox),select[data-val-required],textarea[data-val-required]').each(function () {
        $(this).parent().parent().find('label[for="' + this.name + '"]').addClass('req-field');
    });
    $('#' + parentid).find('input.datepicker[data-val-required],input.datepicker-control[data-val-required],div.datepicker-control > input[data-val-required]').each(function () {
        $(this).parent().parent().find('label.control-label').addClass('req-field');
    });
}

$(document).ready(function () {
    markFormRequiredFieldAsterisk("validator-form");

    var input = $('.input-validation-error:first');

    if (input) {
        input.focus();
    }
});

//In View Formular dann: spinnershow war für popup zu öffnen nach Klick
//<script type="text/javascript">
//    $(document).ready(function () {
//        markFormRequiredFieldAsterisk("offerten-form");
//        $("#offerten-form").submit(function (event) {
//            spinnerShow();
//        });
//    });
//</script>
