﻿function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
function roundTo5Cents(number) {
    return (Math.round(number * 20) / 20);
}
function getDateOwnFormat(dateString) {
    if (!dateString) return null;
    var dateSplit = dateString.split(".");
    if (dateSplit.length != 3) return null;

    var day = dateSplit[0];
    var month = dateSplit[1];
    var year = dateSplit[2];
    
    if (day < 1 || day > 31 || month < 1 || month > 12 || year < 1900 || year > 2050) return null;

    return new Date(year+"-"+month+"-"+day);
}

function findactivejsevents(parentid) {
    $('#Receipt_TotalCost').focus(function () {
        var price = 0;
        var helpPrice = 0;

        if ($('#ReceiptUmzug_UmzugCost').length > 0) {
            helpPrice = $('#ReceiptUmzug_UmzugCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_UmzugExpensesCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_UmzugWayCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_UmzugPackCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_EntsorgungCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_EntsorgungPauschalCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost1').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost2').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost3').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost4').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost5').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalCost6').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalSubCost1').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalSubCost2').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
            helpPrice = $('#ReceiptUmzug_AdditionalSubCost3').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
        } else if ($('#ReceiptReinigung_ReinigungCost').length > 0) {
            helpPrice = $('#ReceiptReinigung_ReinigungCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_HourlyCostSum').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost1').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost2').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost3').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost4').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost5').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalCost6').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalSubCost1').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalSubCost2').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
            helpPrice = $('#ReceiptReinigung_AdditionalSubCost3').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price -= parseFloat(helpPrice);
        }

        // check if costfix or costhigh exists
        helpPrice = $('#Receipt_CostFix').val();
        if (helpPrice != "" && isNumeric(helpPrice) && helpPrice > 0) {
            price = parseFloat(helpPrice);
        }
        helpPrice = $('#Receipt_CostHigh').val();
        if (helpPrice != "" && isNumeric(helpPrice) && helpPrice > 0 && price > helpPrice) {
            price = parseFloat(helpPrice);
        }

        price = (Math.round((price) * 100) / 100);
        if (price % 1 == 0) {
            this.value = price.toString();
        }
        else {
            this.value = price.toFixed(2);
        }
    });
    $('#ReceiptUmzug_UmzugCost').focus(function () {
        if ($('#ReceiptUmzug_UmzugCost').val() != "") return;

        var price = 0;
        var helpPrice = $('#ReceiptUmzug_UmzugRateCost').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
        else price = 0;
        if (price == 0) return;

        var duration = 0;
        var helpDuration = $('#ReceiptUmzug_UmzugDuration').val();
        if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
        else duration = 0;
        if (duration == 0) return;

        price = (Math.round((price * duration) * 100) / 100);
        if (price % 1 == 0) {
            this.value = price.toString();
        } else {
            this.value = price.toFixed(2);
        }
    });
    $('#ReceiptUmzug_EntsorgungCost').focus(function () {
        if ($('#ReceiptUmzug_EntsorgungCost').val() != "") return;

        var price = 0;
        var helpPrice = $('#ReceiptUmzug_EntsorgungRateCost').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
        else price = 0;
        if (price == 0) return;

        var duration = 0;
        var helpDuration = $('#ReceiptUmzug_EntsorgungVolume').val();
        if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
        else duration = 0;
        if (duration == 0) return;

        price = (Math.round((price * duration) * 100) / 100);
        if (price % 1 == 0) {
            this.value = price.toString();
        } else {
            this.value = price.toFixed(2);
        }
    });
    $('#ReceiptReinigung_HourlyCostSum').focus(function () {
        if ($('#ReceiptReinigung_HourlyCostSum').val() != "") return;

        var price = 0;
        var helpPrice = $('#ReceiptReinigung_HourlyCostRate').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
        else price = 0;
        if (price == 0) return;

        var duration = 0;
        var helpDuration = $('#ReceiptReinigung_HourlyCostDuration').val();
        if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
        else duration = 0;
        if (duration == 0) return;

        price = (Math.round((price * duration) * 100) / 100);
        if (price % 1 == 0) {
            this.value = price.toString();
        } else {
            this.value = price.toFixed(2);
        }
    });
}

$(document).ready(function () {
    findactivejsevents("validator-form");
});
