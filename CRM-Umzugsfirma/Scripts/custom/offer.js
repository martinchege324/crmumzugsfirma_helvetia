﻿function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function findactivatedropchangeshow(parentid) {
    $('#HelperUmzugPriceRateId').change(function () {
        if (this.value !== "")
        {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_UmzugCostChangedPers').val(split[2]);
            $('#Offer_UmzugCostChangedVeh').val(split[3]);
            $('#Offer_UmzugCostChangedPrice').val(split[1]);

            var Expensesctrl = $('#umzugadditcontainer').find('input[data-metatag="Expenses"]');
            if (Expensesctrl !== undefined)
            {
                Expensesctrl.val(parseInt(split[2]) * parseInt(Expensesctrl.data("priceperpiece")));
            }
        }
        else
        {
            $(this.dataset.onchange).hide();
            $('#Offer_UmzugCostChangedPers').val('');
            $('#Offer_UmzugCostChangedVeh').val('');
            $('#Offer_UmzugCostChangedPrice').val('');
        }
    });
    $('#Offer_UmzugCostSum').focus(function () {
        updateEstimateUmzug($('#Offer_UmzugCostSum'));
    });
    $('#Offer_UmzugCostEstimated, #Offer_UmzugCostHighSecurityPrice').focus(function () {
        updateEstimateUmzug($('#Offer_UmzugCostSum'));

        if ($('#Offer_UmzugCostSum').val() == "") return;

        var additionprice = 0;
        var priceminus = $('#Offer_UmzugCostDiscount').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_UmzugCostDiscount2').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_UmzugCostAdditFreeTextMinus1Price').val();
        if (priceminus != "") additionprice += parseInt(priceminus);

        var estimateCost = "0";
        var price = $('#Offer_UmzugCostSum').val().split("-");
        if (price.length == 2) {
            pricemin = parseInt(price[0]) - additionprice;
            pricemax = parseInt(price[1]) - additionprice;
            estimateCost = pricemin + "-" + pricemax;
        }
        else {
            pricemax = parseInt(price[0]) - additionprice;
            estimateCost = pricemax;
        }

        $("#Offer_UmzugCostEstimated").val(estimateCost);

        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
        if (!($("#Offer_UmzugCostHighSecurityManual").is(":checked")) || ($("#Offer_UmzugCostHighSecurityPrice").val() == 0)) {
            $("#Offer_UmzugCostHighSecurityPrice").val((parseInt((parseInt(pricemax) * 1.2) / 10) + 1) * 10);
        }
        $('#Offer_UmzugCostFixPrice').val((parseInt((parseInt(pricemax) * 1.1) / 10) + 1) * 10);
    });
    $("#Offer_UmzugCostHighSecurityPrice").change(function () {
        if (this.value > 0) {
            $("#Offer_UmzugCostHighSecurityManual").prop("checked", true);
        }
    });
    $('#HelperPackPriceRateId').change(function () {
        if (this.value !== "") {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_PackCostChangedPers').val(split[2]);
            $('#Offer_PackCostChangedPrice').val(split[1]);

            var Expensesctrl = $('#packadditcontainer').find('input[data-metatag="Expenses"]');
            if (Expensesctrl !== undefined) {
                Expensesctrl.val(parseInt(split[2]) * parseInt(Expensesctrl.data("priceperpiece")));
            }
        }
        else {
            $(this.dataset.onchange).hide();
            $('#Offer_PackCostChangedPers').val('');
            $('#Offer_PackCostChangedPrice').val('');
        }
    });
    $('#Offer_PackCostSum').focus(function () {
        updateEstimatePack($('#Offer_PackCostSum'));
    });
    $('#Offer_PackCostEstimated, #Offer_PackCostHighSecurityPrice').focus(function () {
        updateEstimatePack($('#Offer_PackCostSum'));

        if ($('#Offer_PackCostSum').val() == "") return;

        var additionprice = 0;
        var priceminus = $('#Offer_PackCostDiscount').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_PackCostDiscount2').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_PackCostAdditFreeTextMinus1Price').val();
        if (priceminus != "") additionprice += parseInt(priceminus);

        var estimateCost = "0";
        var price = $('#Offer_PackCostSum').val().split("-");
        if (price.length == 2) {
            pricemin = parseInt(price[0]) - additionprice;
            pricemax = parseInt(price[1]) - additionprice;
            estimateCost = pricemin + "-" + pricemax;
        }
        else {
            pricemax = parseInt(price[0]) - additionprice;
            estimateCost = pricemax;
        }

        $("#Offer_PackCostEstimated").val(estimateCost);

        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
        if (!($("#Offer_PackCostHighSecurityManual").is(':checked')) || ($("#Offer_PackCostHighSecurityPrice").val() == 0)) {
            $("#Offer_PackCostHighSecurityPrice").val((parseInt((parseInt(pricemax) * 1.2) / 10) + 1) * 10);
        }
        $('#Offer_PackCostFixPrice').val((parseInt((parseInt(pricemax) * 1.1) / 10) + 1) * 10);
    });
    $("#Offer_PackCostHighSecurityPrice").change(function () {
        if (this.value > 0) {
            $("#Offer_PackCostHighSecurityManual").prop("checked", true);
        }
    });
    $('#HelperPackOutPriceRateId').change(function () {
        if (this.value !== "") {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_PackOutCostChangedPers').val(split[2]);
            $('#Offer_PackOutCostChangedPrice').val(split[1]);

            var Expensesctrl = $('#packoutadditcontainer').find('input[data-metatag="Expenses"]');
            if (Expensesctrl !== undefined) {
                Expensesctrl.val(parseInt(split[2]) * parseInt(Expensesctrl.data("priceperpiece")));
            }
        }
        else {
            $(this.dataset.onchange).hide();
            $('#Offer_PackOutCostChangedPers').val('');
            $('#Offer_PackOutCostChangedPrice').val('');
        }
    });
    $('#Offer_PackOutCostSum').focus(function () {
        updateEstimatePackOut($('#Offer_PackOutCostSum'));
    });
    $('#Offer_PackOutCostEstimated, #Offer_PackOutCostHighSecurityPrice').focus(function () {
        updateEstimatePackOut($('#Offer_PackOutCostSum'));

        if ($('#Offer_PackOutCostSum').val() == "") return;

        var additionprice = 0;
        var priceminus = $('#Offer_PackOutCostDiscount').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_PackOutCostDiscount2').val();
        if (priceminus != "") additionprice += parseInt(priceminus);
        priceminus = $('#Offer_PackOutCostAdditFreeTextMinus1Price').val();
        if (priceminus != "") additionprice += parseInt(priceminus);

        var estimateCost = "0";
        var price = $('#Offer_PackOutCostSum').val().split("-");
        if (price.length == 2) {
            pricemin = parseInt(price[0]) - additionprice;
            pricemax = parseInt(price[1]) - additionprice;
            estimateCost = pricemin + "-" + pricemax;
        }
        else {
            pricemax = parseInt(price[0]) - additionprice;
            estimateCost = pricemax;
        }

        $("#Offer_PackOutCostEstimated").val(estimateCost);

        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
        if (!($("#Offer_PackOutCostHighSecurityManual").is(':checked')) || ($("#Offer_PackOutCostHighSecurityPrice").val() == 0)) {
            $("#Offer_PackOutCostHighSecurityPrice").val((parseInt((parseInt(pricemax) * 1.2) / 10) + 1) * 10);
        }
        $('#Offer_PackOutCostFixPrice').val((parseInt((parseInt(pricemax) * 1.1) / 10) + 1) * 10);
    });
    $("#Offer_PackOutCostHighSecurityPrice").change(function () {
        if (this.value > 0) {
            $("#Offer_PackOutCostHighSecurityManual").prop("checked", true);
        }
    });
    $('#HelperEntsorgungPersonPriceRateId').change(function () {
        if (this.value !== "") {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_EntsorgungPersonCostChangedPers').val(split[2]);
            $('#Offer_EntsorgungPersonCostChangedVeh').val(split[3]);
            $('#Offer_EntsorgungPersonCostChangedPrice').val(split[1]);

            var Expensesctrl = $('#entsorgungadditcontainer').find('input[data-metatag="Expenses"]');
            if (Expensesctrl !== undefined) {
                Expensesctrl.val(parseInt(split[2]) * parseInt(Expensesctrl.data("priceperpiece")));
            }
        }
        else {
            $(this.dataset.onchange).hide();
            $('#Offer_EntsorgungPersonCostChangedPers').val('0');
            $('#Offer_EntsorgungPersonCostChangedVeh').val('0');
            $('#Offer_EntsorgungPersonCostChangedPrice').val('0');
        }
    });
    $('#HelperEntsorgungPriceRateId').change(function () {
        if (this.value !== "") {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_EntsorgungCostChangedPrice').val(split[1]);
        }
        else {
            $(this.dataset.onchange).hide();
            $('#Offer_EntsorgungCostChangedPrice').val('0');
        }
    });
    $('#Offer_EntsorgungCostEstimated, #Offer_EntsorgungCostHighSecurityPrice').focus(function () {
        var ctrl = $('#Offer_EntsorgungCostEstimated');
        var pricemax = 0;
        var pricemin = 0;
        var pricevolumemax = 0;
        var pricevolumemin = 0;
        var additionprice = 0;
        var subtractprice = 0;

        var pricemax = $('#Offer_EntsorgungPersonCostChangedPrice').val();
        if (pricemax != "") pricemax = parseInt(pricemax);
        if ((pricemax != 0) && ($('#Offer_EntsorgungDuration').val() != "")) {
            var time = $('#Offer_EntsorgungDuration').val().split("-");
            if (time.length == 2) {
                pricemin = pricemax * parseInt(time[0]);
                pricemax = pricemax * parseInt(time[1]);
            }
            else {
                pricemax = pricemax * parseInt(time[0]);
            }

            var additpr = $('#Offer_EntsorgungCostWay').val();
            if (additpr != "") additionprice += parseInt(additpr);
        }
        else {
            pricemin = 0;
            pricemax = 0;
        }

        var pricevolumemax = parseInt($('#Offer_EntsorgungCostChangedPrice').val());
        if ((pricevolumemax != 0) && ($('#Offer_EntsorgungVolume').val() != "")) {
            var volume = 0;
            var volumemin = parseInt(0);
            var tempVolume = $('#Offer_EntsorgungVolume').val().split("-");
            if (tempVolume != "") {
                if (tempVolume.length == 2) {
                    if (tempVolume[0] != "") volumemin = parseInt(tempVolume[0]);
                    if (tempVolume[1] != "") volume = parseInt(tempVolume[1]);
                }
                else {
                    if (tempVolume[0] != "") volume = parseInt(tempVolume[0]);
                }
            }

            if (volumemin > 0) {
                pricevolumemin = parseInt(pricevolumemax) * parseInt(volumemin);
            }
            if (volume > 0) {
                pricevolumemax = parseInt(pricevolumemax) * parseInt(volume);
            }

            var additpr = $('#Offer_EntsorgungFixCost').val();
            if (additpr != "") additionprice += parseInt(additpr);
        }
        else {
            pricevolumemin = 0;
            pricevolumemax = 0;
        }

        if (pricemax > 0 || pricemin > 0 || pricevolumemax > 0 || pricevolumemin > 0)
        {
            if ($('#Offer_EntsorgungCostAddit').is(':checked')) {
                $('#entsorgungadditcontainer').find('input:checked').each(function () {
                    var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
                    if (intprice != "") {
                        additionprice += parseInt(intprice);
                    }
                });
                var priceplus = $('#Offer_EntsorgungCostAdditFreeTextPlus1Price').val();
                if (priceplus != "") additionprice += parseInt(priceplus);
                priceplus = $('#Offer_EntsorgungCostAdditFreeTextPlus2Price').val();
                if (priceplus != "") additionprice += parseInt(priceplus);
            }

            var priceminus = $('#Offer_EntsorgungCostDiscount').val();
            if (priceminus != "") subtractprice += parseInt(priceminus);
            priceminus = $('#Offer_EntsorgungCostAdditFreeTextMinus1Price').val();
            if (priceminus != "") subtractprice += parseInt(priceminus);

            if (pricevolumemin > 0) {
                if (pricemin == 0 && pricemax > 0) pricemin = pricemax;
                pricemin = pricemin + pricevolumemin;
            }
            if (pricevolumemax > 0) pricemax = pricemax + pricevolumemax;

            if (pricemin > 0) {
                pricemin = pricemin + additionprice - subtractprice;
                if (pricevolumemin == 0 && pricevolumemax > 0) pricemin = pricemin + pricevolumemax;
            }
            if (pricemax > 0) {
                pricemax = pricemax + additionprice - subtractprice;

                ctrl[0].value = pricemax;
                // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                if (!($("#Offer_EntsorgungCostHighSecurityManual").is(':checked')) || ($("#Offer_EntsorgungCostHighSecurityPrice").val() == 0)) {
                    $("#Offer_EntsorgungCostHighSecurityPrice").val((parseInt((parseInt(pricemax) * 1.2) / 10) + 1) * 10);
                }
                $('#Offer_EntsorgungCostFixPrice').val((parseInt((parseInt(pricemax) * 1.1) / 10) + 1) * 10);

                if (pricemin > 0) {
                    ctrl[0].value = pricemin + "-" + pricemax;
                }
            }
        }
    });
    $("#Offer_EntsorgungCostHighSecurityPrice").change(function () {
        if (this.value > 0) {
            $("#Offer_EntsorgungCostHighSecurityManual").prop("checked", true);
        }
    });
    $('#HelperLagerungPriceRateId').change(function () {
        if (this.value !== "") {
            $(this.dataset.onchange).show();
            var split = this.value.split("|");
            $('#Offer_LagerungCostChangedPrice').val(split[1]);
        }
        else {
            $(this.dataset.onchange).hide();
            $('#Offer_LagerungCostChangedPrice').val('');
        }
    });
    $('#Offer_LagerungCostEstimated').focus(function () {
        var ctrl = $('#Offer_LagerungCostEstimated');
        var price = parseInt($('#Offer_LagerungCostChangedPrice').val());
        if (price == 0) return;

        var volume = 0;
        var volumemax = parseInt(0);
        var tempVolume = $('#Offer_LagerungVolume').val().split("-");
        if (tempVolume != "") {
            if (tempVolume.length == 2) {
                if (tempVolume[0] != "") volume = parseInt(tempVolume[0]);
                if (tempVolume[1] != "") volumemax = parseInt(tempVolume[1]);
            }
            else {
                if (tempVolume[0] != "") volume = parseInt(tempVolume[0]);
            }
        }
        if (volume == 0) return;

        var additionprice = 0;
        var priceplus = $('#Offer_LagerungCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_LagerungCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);

        var priceminus = $('#Offer_LagerungCostAdditFreeTextMinus1Price').val();
        if (priceminus != "") subtractprice = parseInt(priceminus);

        var pricemax = parseInt(0);
        if (volumemax > 0) {
            pricemax = parseInt(price) * parseInt(volumemax);
        }
        price = parseInt(price) * parseInt(volume);
        var estimateCost = "0";
        if (additionprice > 0) {
            price += parseInt(additionprice);
            if (pricemax > 0) pricemax += parseInt(additionprice);
        }
        if (subtractprice > 0) {
            price -= parseInt(subtractprice);
            if (pricemax > 0) pricemax -= parseInt(subtractprice);
        }

        if (pricemax > 0) {
            ctrl[0].value = price + "-" + pricemax;
            $('#Offer_LagerungCostFixPrice').val((parseInt((parseInt(pricemax) * 1.1) / 10) + 1) * 10);
        }
        else {
            ctrl[0].value = price;
            $('#Offer_LagerungCostFixPrice').val((parseInt((parseInt(price) * 1.1) / 10) + 1) * 10);
        }
    });
    $('#Offer_PackMaterialCostSum').focus(function () {
        var ctrl = $('#Offer_PackMaterialCostSum');

        var totalPrice = parseFloat(0);
        $('#definedpackmaterialcontainer').find('input[data-selectable]').each(function () {
            updateDefinedPackMaterialSubCost(this);
            if (this.value !== undefined && isNumeric(this.value)) {
                totalPrice += parseFloat(this.value);
            }
        });
        $('#freepackmaterialcontainer').find('input[data-selectable]').each(function () {
            updatePackMaterialSubCost(this);
            if (this.value !== undefined && isNumeric(this.value)) {
                totalPrice += parseFloat(this.value);
            }
        });

        var priceminus = $('#Offer_PackMaterialCostDiscount').val();
        if (priceminus != "" && isNumeric(priceminus)) totalPrice -= parseFloat(priceminus);

        if ($('#Offer_PackMaterialDeliver')[0].checked)
        {
            var deliverPrice = $('#Offer_PackMaterialDeliverPrice').val();
            if (deliverPrice != "" && isNumeric(deliverPrice)) totalPrice += parseFloat(deliverPrice);
        }

        if ($('#Offer_PackMaterialPickup')[0].checked) {
            var deliverPrice = $('#Offer_PackMaterialPickupPrice').val();
            if (deliverPrice != "" && isNumeric(deliverPrice)) totalPrice += parseFloat(deliverPrice);
        }

        ctrl[0].value = (Math.round(totalPrice * 100) / 100).toFixed(2);
    });
    $('#Offer_AccountEmployeeId').change(function () {
        if (this.value !== "" && this.value !== "0") {
            $(this.dataset.onchange).hide();
            $('#Offer_ContactPersonText').val('');
        }
        else {
            $(this.dataset.onchange).show();
        }
    });

}
function updateEstimateUmzug(ctrl)
{
    var pricemax = $('#Offer_UmzugCostChangedPrice').val();
    if (pricemax == 0) return;

    var pricemin = pricemax;
    var additionprice = 0;
    var additpr = $('#Offer_UmzugCostWay').val();
    if (additpr != "") additionprice += parseInt(additpr);
    if ($('#Offer_UmzugCostAddit').is(':checked')) {
        $('#umzugadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "") {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Offer_UmzugCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_UmzugCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
    }

    if ($('#Offer_UmzugDuration').val() == "") return;

    var estimateCost = "0";
    var time = $('#Offer_UmzugDuration').val().split("-");
    if (time.length == 2) {
        pricemin = pricemin * parseInt(time[0]) + additionprice;
        pricemax = pricemax * parseInt(time[1]) + additionprice;
        estimateCost = pricemin + "-" + pricemax;
    }
    else {
        pricemax = pricemax * parseInt(time[0]) + additionprice;
        estimateCost = pricemax;
    }
    ctrl[0].value = estimateCost;
}
function updateEstimatePack(ctrl) {
    var pricemax = $('#Offer_PackCostChangedPrice').val();
    if (pricemax == 0) return;

    var pricemin = pricemax;
    var additionprice = 0;
    var additpr = $('#Offer_PackCostWay').val();
    if (additpr != "") additionprice += parseInt(additpr);
    if ($('#Offer_PackCostAddit').is(':checked')) {
        $('#packadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "") {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Offer_PackCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_PackCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
    }

    if ($('#Offer_PackDuration').val() == "") return;

    var estimateCost = "0";
    var time = $('#Offer_PackDuration').val().split("-");
    if (time.length == 2) {
        pricemin = pricemin * parseInt(time[0]) + additionprice;
        pricemax = pricemax * parseInt(time[1]) + additionprice;
        estimateCost = pricemin + "-" + pricemax;
    }
    else {
        pricemax = pricemax * parseInt(time[0]) + additionprice;
        estimateCost = pricemax;
    }
    ctrl[0].value = estimateCost;
}
function updateEstimatePackOut(ctrl) {
    var pricemax = $('#Offer_PackOutCostChangedPrice').val();
    if (pricemax == 0) return;

    var pricemin = pricemax;
    var additionprice = 0;
    var additpr = $('#Offer_PackOutCostWay').val();
    if (additpr != "") additionprice += parseInt(additpr);
    if ($('#Offer_PackOutCostAddit').is(':checked')) {
        $('#packoutadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "") {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Offer_PackOutCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_PackOutCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
    }

    if ($('#Offer_PackOutDuration').val() == "") return;

    var estimateCost = "0";
    var time = $('#Offer_PackOutDuration').val().split("-");
    if (time.length == 2) {
        pricemin = pricemin * parseInt(time[0]) + additionprice;
        pricemax = pricemax * parseInt(time[1]) + additionprice;
        estimateCost = pricemin + "-" + pricemax;
    }
    else {
        pricemax = pricemax * parseInt(time[0]) + additionprice;
        estimateCost = pricemax;
    }
    ctrl[0].value = estimateCost;
}
function updateEstimateTransport(ctrl) {

    // which Tariff is selected: Flat_rate (CostFix) or HourlyEmployee (PriceRateId)

    // if flat-rate is set, take this price and return
    var pricemax = $('#Offer_OfferTransport_TransportCostFixRatePrice').val();
    if (pricemax != "" && isNumeric(pricemax) && pricemax > 0) {
        ctrl[0].value = pricemax;
        return;
    }

    // Else: Check HourlyEmployee Tariff if set
    var pricemax = $('#Offer_OfferTransport_TransportCostChangedPrice').val();
    if (pricemax == 0) return;

    var pricemin = pricemax;
    var additionprice = 0;
    var additpr = $('#Offer_OfferTransport_TransportCostWay').val();
    if (additpr != "") additionprice += parseInt(additpr);
    if ($('#Offer_OfferTransport_TransportCostAddit').is(':checked')) {
        var priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus3Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus4Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus5Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus6Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
        priceplus = $('#Offer_OfferTransport_TransportCostAdditFreeTextPlus7Price').val();
        if (priceplus != "") additionprice += parseInt(priceplus);
    }

    if ($('#Offer_OfferTransport_TransportDuration').val() == "") return;

    var estimateCost = "0";
    var time = $('#Offer_OfferTransport_TransportDuration').val().split("-");
    if (time.length == 2) {
        pricemin = pricemin * parseInt(time[0]) + additionprice;
        pricemax = pricemax * parseInt(time[1]) + additionprice;
        estimateCost = pricemin + "-" + pricemax;
    }
    else {
        pricemax = pricemax * parseInt(time[0]) + additionprice;
        estimateCost = pricemax;
    }
    ctrl[0].value = estimateCost;
}

// packmaterial functions
function initDefinedPackMaterial() {
    $('#definedpackmaterialcontainer').find('select[data-onchange]').each(function () {
        var selectedValue = $("#" + this.id + "Id").val();
        if (selectedValue > 0) {
            $("#" + this.id + " option[value^='" + selectedValue + "|']").prop("selected", true);
            updateDefinedPackMaterialDetailValues(this, false);
        }
    });
}
function updateDefinedPackMaterialDetailValues(ctrl, setDetailControlsFromDropdownValue) {
    // set the detail values from the selected packmaterial
    var split = ctrl.value.split("|");
    $("#" + ctrl.id + "Id").val(split[0]);
    // check if rent is possible
    if (split[1] != "") {
        // take priceRent and enable rent (if previous was disabled)
        var radioRent = $("#RentBuy_Miete_" + ctrl.dataset.onchange);
        var radioBuy = $("#RentBuy_Kauf_" + ctrl.dataset.onchange);
        if (setDetailControlsFromDropdownValue) {
            $("#" + ctrl.id.replace('_DefinedPackMaterial', '_PiecePrice')).val(split[1]);
            radioRent.prop("disabled", false);
            radioRent.prop("checked", "checked");
            radioBuy.removeAttr("checked");
        }
        radioRent.attr('data-onchangeval', split[1]);
        radioBuy.attr('data-onchangeval', split[2]);
        // attach onchange event for Rent/Buy Selection
        $('#definedpackmaterialcontainer').find('input:radio[data-onchangeval]').change(function () {
            $("#OfferDefinedPackMaterials_" + this.id.substr(this.id.length - 1) + "__PiecePrice").val(this.dataset.onchangeval);
        });
    } else {
        // take priceBuy and disable rent
        var radioRent = $("#RentBuy_Miete_" + ctrl.dataset.onchange);
        if (setDetailControlsFromDropdownValue) {
            $("#" + ctrl.id.replace('_DefinedPackMaterial', '_PiecePrice')).val(split[2]);
            radioRent.removeAttr("checked");
            $("#RentBuy_Kauf_" + ctrl.dataset.onchange).prop("checked", "checked");
        }
        radioRent.prop("disabled", "disabled");
    }
}
$('#definedpackmaterialcontainer').find('select[data-onchange]').change(function() {
    if (this.value == "") {
        // remove the actual detail values, if selected packmaterial is set to empty value (so clean the actual line)
        $("#" + this.id + "Id").val("");
        var radioRent = $("#RentBuy_Miete_" + this.dataset.onchange);
        radioRent.prop("disabled", false);
        radioRent.prop("checked", "checked");
        $("#RentBuy_Kauf_" + this.dataset.onchange).removeAttr("checked");
        $("#" + this.id.replace('_DefinedPackMaterial', '_PiecePrice')).val("");
        $("#" + this.id.replace('_DefinedPackMaterial', '_CountNumber')).val(0);
        $("#" + this.id.replace('_DefinedPackMaterial', '_EndPrice')).val("");
    } else {
        updateDefinedPackMaterialDetailValues(this, true);
    }
});
$('#definedpackmaterialcontainer').find('input[data-selectable]').focus(function () {
    updateDefinedPackMaterialSubCost(this);
    $(this.dataset.selectable).show();
});
function updateDefinedPackMaterialSubCost(ctrl) {
    var idName = ctrl.id;
    var piecePrice = 0;
    var temppiecePrice = $('#' + idName.replace('EndPrice', 'PiecePrice')).val();
    if (temppiecePrice != "" && isNumeric(temppiecePrice)) piecePrice = parseFloat(temppiecePrice);
    else piecePrice = 0;

    var countPiece = 0;
    var tempcountPiece = $('#' + idName.replace('EndPrice', 'CountNumber')).val();
    if (tempcountPiece != "" && isNumeric(tempcountPiece)) countPiece = parseFloat(tempcountPiece);
    else countPiece = 0;

    ctrl.value = (Math.round(piecePrice * countPiece * 100) / 100).toFixed(2);
}
$('#freepackmaterialcontainer').find('input[data-selectable]').focus(function () {
    updatePackMaterialSubCost(this);
    $(this.dataset.selectable).show();
});
function updatePackMaterialSubCost(ctrl) {
    var idName = ctrl.id;
    var piecePrice = 0;
    var temppiecePrice = $('#' + idName.replace('EndPrice', 'PricePerPiece')).val();
    if (temppiecePrice != "" && isNumeric(temppiecePrice)) piecePrice = parseFloat(temppiecePrice);
    else piecePrice = 0;

    var countPiece = 0;
    var tempcountPiece = $('#' + idName.replace('EndPrice', 'CountNumber')).val();
    if (tempcountPiece != "" && isNumeric(tempcountPiece)) countPiece = parseFloat(tempcountPiece);
    else countPiece = 0;

    ctrl.value = (Math.round(piecePrice * countPiece * 100) / 100).toFixed(2);
}

$(document).ready(function () {
    findactivatedropchangeshow("validator-form");
    initDefinedPackMaterial();
});
