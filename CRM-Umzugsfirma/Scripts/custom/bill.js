﻿function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function findactivejsevents(parentid) {
    $('#Bill_UmzugCostSub').focus(function () {
        updateUmzug();
    });
    $('#Bill_UmzugCost').focus(function () {
        updateUmzug();
    });
    $('#Bill_UmzugCostHighSecurity').click(function () {
        updateUmzug();
    });
    $('#Bill_PackCostSub').focus(function () {
        updatePack();
    });
    $('#Bill_PackCost').focus(function () {
        updatePack();
    });
    $('#Bill_PackCostHighSecurity').click(function () {
        updatePack();
    });
    $('#Bill_PackOutCostSub').focus(function () {
        updatePackOut();
    });
    $('#Bill_PackOutCost').focus(function () {
        updatePackOut();
    });
    $('#Bill_PackOutCostHighSecurity').click(function () {
        updatePackOut();
    });
    $('#Bill_EntsorgungCostSub').focus(function () {
        updateEntsorgung();
    });
    $('#Bill_EntsorgungCost').focus(function () {
        updateEntsorgung();
    });
    $('#Bill_EntsorgungCostHighSecurity').click(function () {
        updateEntsorgung();
    });
    $('#Bill_LagerungCostSub').focus(function () {
        updateLagerung();
    });
    $('#Bill_LagerungCost').focus(function () {
        updateLagerung();
    });
    $('#Bill_PackMaterialCost').focus(function () {
        updatePackMaterial();
    });
    $('#Bill_BillCostString').focus(function () {
        if ($('#Bill_UmzugActive').is(':checked')) updateUmzug();
        if ($('#Bill_PackActive').is(':checked')) updatePack();
        if ($('#Bill_PackOutActive').is(':checked')) updatePackOut();
        if ($('#Bill_ReinigungActive').is(':checked')) updateReinigung();
        if ($('#Bill_EntsorgungActive').is(':checked')) updateEntsorgung();
        if ($('#Bill_TransportActive').is(':checked')) updateTransport();
        if ($('#Bill_LagerungActive').is(':checked')) updateLagerung();
        if ($('#Bill_PackMaterialActive').is(':checked')) updatePackMaterial();

        var price = 0;
        var helpPrice = 0;
        if ($('#Bill_UmzugActive').is(':checked')) {
            helpPrice = $('#Bill_UmzugCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_PackActive').is(':checked')) {
            helpPrice = $('#Bill_PackCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_PackOutActive').is(':checked')) {
            helpPrice = $('#Bill_PackOutCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_ReinigungActive').is(':checked')) {
            helpPrice = $('#Bill_BillCleaning_ReinigungCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_EntsorgungActive').is(':checked')) {
            helpPrice = $('#Bill_EntsorgungCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_TransportActive').is(':checked')) {
            helpPrice = $('#Bill_BillTransport_TransportCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_LagerungActive').is(':checked')) {
            helpPrice = $('#Bill_LagerungCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        if ($('#Bill_PackMaterialActive').is(':checked')) {
            helpPrice = $('#Bill_PackMaterialCost').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);
        }
        helpPrice = $('#Bill_PreCollectionCost').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price += parseFloat(helpPrice);

        price = (Math.round((price) * 100) / 100);
        if (price % 1 == 0) {
            this.value = price.toString();
        }
        else {
            this.value = price.toFixed(2);
        }
    });
}

function updateUmzug() {
    var price = 0;
    var helpPrice = $('#Bill_UmzugPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
    else price = 0;
    if (price == 0) return;

    var duration = 0;
    var helpDuration = $('#Bill_UmzugDuration').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    else duration = 0;
    if (duration == 0) return;
    
    price = price * duration;

    // pricesegment2
    duration = 0;
    helpDuration = $('#Bill_UmzugDuration2').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    if (duration != 0) {
        helpPrice = $('#Bill_UmzugPerPrice2').val();
        if (helpPrice != "" && isNumeric(helpPrice)) {
            price = price + (parseInt(helpPrice) * duration);
        }
    }

    var additionprice = 0;
    var additpr = $('#Bill_UmzugCostWay').val();
    if (additpr != "" && isNumeric(additpr)) additionprice += parseInt(additpr);
    if ($('#Bill_UmzugCostAddit').is(':checked')) {
        $('#umzugadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "" && isNumeric(intprice)) {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Bill_UmzugCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
        priceplus = $('#Bill_UmzugCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
    }

    var subprice = 0;
    var priceminus = $('#Bill_UmzugCostDiscount').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_UmzugCostDiscount2').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_UmzugCostAdditFreeTextMinus1Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_UmzugCostAdditFreeTextMinus2Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set temporary price
    price = (Math.round((price + additionprice - subprice) * 100) / 100);
    if (price % 1 == 0)
    {
        $('#Bill_UmzugCostSub')[0].value = price;
    }
    else
    {
        $('#Bill_UmzugCostSub')[0].value = price.toFixed(2);
    }

    // has highprice
    if ($('#Bill_UmzugCostHighSecurity').is(':checked')) {
        var helpPrice = $('#Bill_UmzugCostHighSecurityPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice) && (price > helpPrice)) price = parseFloat(helpPrice);
    }
    // is fix price
    if ($('#Bill_UmzugCostFix').is(':checked')) {
        var helpPrice = $('#Bill_UmzugCostFixPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseFloat(helpPrice);
    }

    subprice = 0;
    priceminus = $('#Bill_UmzugCostDamage').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_UmzugCostPartialPayment').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_UmzugCostPartialByCash').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set end price
    price = (Math.round((price - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_UmzugCost')[0].value = price;
    }
    else {
        $('#Bill_UmzugCost')[0].value = price.toFixed(2);
    }
}
function updatePack() {
    var price = 0;
    var helpPrice = $('#Bill_PackPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
    else price = 0;
    if (price == 0) return;

    var duration = 0;
    var helpDuration = $('#Bill_PackDuration').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    else duration = 0;
    if (duration == 0) return;

    price = price * duration;

    // pricesegment2
    duration = 0;
    helpDuration = $('#Bill_PackDuration2').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    if (duration != 0) {
        helpPrice = $('#Bill_PackPerPrice2').val();
        if (helpPrice != "" && isNumeric(helpPrice)) {
            price = price + (parseInt(helpPrice) * duration);
        }
    }

    var additionprice = 0;
    var additpr = $('#Bill_PackCostWay').val();
    if (additpr != "" && isNumeric(additpr)) additionprice += parseInt(additpr);
    if ($('#Bill_PackCostAddit').is(':checked')) {
        $('#packadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "" && isNumeric(intprice)) {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Bill_PackCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
        priceplus = $('#Bill_PackCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
    }

    var subprice = 0;
    var priceminus = $('#Bill_PackCostDiscount').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackCostDiscount2').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackCostAdditFreeTextMinus1Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackCostAdditFreeTextMinus2Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set temporary price
    price = (Math.round((price + additionprice - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_PackCostSub')[0].value = price;
    }
    else {
        $('#Bill_PackCostSub')[0].value = price.toFixed(2);
    }

    // has highprice
    if ($('#Bill_PackCostHighSecurity').is(':checked')) {
        var helpPrice = $('#Bill_PackCostHighSecurityPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice) && (price > helpPrice)) price = parseFloat(helpPrice);
    }
    // is fix price
    if ($('#Bill_PackCostFix').is(':checked')) {
        var helpPrice = $('#Bill_PackCostFixPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseFloat(helpPrice);
    }

    subprice = 0;
    priceminus = $('#Bill_PackCostDamage').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackCostPartialPayment').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackCostPartialByCash').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set end price
    price = (Math.round((price - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_PackCost')[0].value = price;
    }
    else {
        $('#Bill_PackCost')[0].value = price.toFixed(2);
    }
}
function updatePackOut() {
    var price = 0;
    var helpPrice = $('#Bill_PackOutPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
    else price = 0;
    if (price == 0) return;

    var duration = 0;
    var helpDuration = $('#Bill_PackOutDuration').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    else duration = 0;
    if (duration == 0) return;

    price = price * duration;

    // pricesegment2
    duration = 0;
    helpDuration = $('#Bill_PackOutDuration2').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    if (duration != 0) {
        helpPrice = $('#Bill_PackOutPerPrice2').val();
        if (helpPrice != "" && isNumeric(helpPrice)) {
            price = price + (parseInt(helpPrice) * duration);
        }
    }

    var additionprice = 0;
    var additpr = $('#Bill_PackOutCostWay').val();
    if (additpr != "" && isNumeric(additpr)) additionprice += parseInt(additpr);
    if ($('#Bill_PackOutCostAddit').is(':checked')) {
        $('#packoutadditcontainer').find('input:checked').each(function () {
            var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
            if (intprice != "" && isNumeric(intprice)) {
                additionprice += parseInt(intprice);
            }
        });
        var priceplus = $('#Bill_PackOutCostAdditFreeTextPlus1Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
        priceplus = $('#Bill_PackOutCostAdditFreeTextPlus2Price').val();
        if (priceplus != "") additionprice += parseFloat(priceplus);
    }

    var subprice = 0;
    var priceminus = $('#Bill_PackOutCostDiscount').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackOutCostDiscount2').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackOutCostAdditFreeTextMinus1Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackOutCostAdditFreeTextMinus2Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set temporary price
    price = (Math.round((price + additionprice - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_PackOutCostSub')[0].value = price;
    }
    else {
        $('#Bill_PackOutCostSub')[0].value = price.toFixed(2);
    }

    // has highprice
    if ($('#Bill_PackOutCostHighSecurity').is(':checked')) {
        var helpPrice = $('#Bill_PackOutCostHighSecurityPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice) && (price > helpPrice)) price = parseFloat(helpPrice);
    }
    // is fix price
    if ($('#Bill_PackOutCostFix').is(':checked')) {
        var helpPrice = $('#Bill_PackOutCostFixPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseFloat(helpPrice);
    }

    subprice = 0;
    priceminus = $('#Bill_PackOutCostDamage').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackOutCostPartialPayment').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_PackOutCostPartialByCash').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set end price
    price = (Math.round((price - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_PackOutCost')[0].value = price;
    }
    else {
        $('#Bill_PackOutCost')[0].value = price.toFixed(2);
    }
}
function updateEntsorgung() {
    var price = 0;
    var pricevolume = 0;
    var additionprice = 0;
    var priceplus = "";

    var helpPrice = $('#Bill_EntsorgungPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) pricevolume = parseInt(helpPrice);
    else pricevolume = 0;
    if (pricevolume > 0) {
        var duration = 0;
        var helpDuration = $('#Bill_EntsorgungVolume').val();
        if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
        else duration = 0;

        if (duration > 0) {
            pricevolume = pricevolume * duration;

            priceplus = $('#Bill_EntsorgungFixCost').val();
            if (priceplus != "")  additionprice += parseInt(priceplus);
        }
    }

    helpPrice = $('#Bill_EntsorgungPersonPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
    else price = 0;
    if (price > 0) {
        var duration = 0;
        var helpDuration = $('#Bill_EntsorgungDuration').val();
        if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
        else duration = 0;

        if (duration > 0) {
            price = price * duration;

            priceplus = $('#Bill_EntsorgungCostWay').val();
            if (priceplus != "") additionprice += parseInt(priceplus);
        }
    }

    if (pricevolume > 0) {
        price += pricevolume;
    }

    if (price > 0) {
        if ($('#Bill_EntsorgungCostAddit').is(':checked')) {
            $('#entsorgungadditcontainer').find('input:checked').each(function () {
                var intprice = $('#' + this.id.replace("_Selected", "_Price")).val();
                if (intprice != "" && isNumeric(intprice)) {
                    additionprice += parseInt(intprice);
                }
            });
            priceplus = $('#Bill_EntsorgungCostAdditFreeTextPlus1Price').val();
            if (priceplus != "") additionprice += parseFloat(priceplus);
            priceplus = $('#Bill_EntsorgungCostAdditFreeTextPlus2Price').val();
            if (priceplus != "") additionprice += parseFloat(priceplus);
        }

        var subprice = 0;
        var priceminus = $('#Bill_EntsorgungCostDiscount').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
        priceminus = $('#Bill_EntsorgungCostDiscount2').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
        priceminus = $('#Bill_EntsorgungCostAdditFreeTextMinus1Price').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
        priceminus = $('#Bill_EntsorgungCostAdditFreeTextMinus2Price').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

        // set temporary price
        price = (Math.round((price + additionprice - subprice) * 100) / 100);
        if (price % 1 == 0) {
            $('#Bill_EntsorgungCostSub')[0].value = price;
        }
        else {
            $('#Bill_EntsorgungCostSub')[0].value = price.toFixed(2);
        }

        // has highprice
        if ($('#Bill_EntsorgungCostHighSecurity').is(':checked')) {
            var helpPrice = $('#Bill_EntsorgungCostHighSecurityPrice').val();
            if (helpPrice != "" && isNumeric(helpPrice) && (price > helpPrice)) price = parseFloat(helpPrice);
        }
        // is fix price
        if ($('#Bill_EntsorgungCostFix').is(':checked')) {
            var helpPrice = $('#Bill_EntsorgungCostFixPrice').val();
            if (helpPrice != "" && isNumeric(helpPrice)) price = parseFloat(helpPrice);
        }

        subprice = 0;
        priceminus = $('#Bill_EntsorgungCostPartialPayment').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
        priceminus = $('#Bill_EntsorgungCostPartialByCash').val();
        if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

        // set end price
        price = (Math.round((price - subprice) * 100) / 100);
        if (price % 1 == 0) {
            $('#Bill_EntsorgungCost')[0].value = price;
        }
        else {
            $('#Bill_EntsorgungCost')[0].value = price.toFixed(2);
        }
    }
}
function updateLagerung() {
    var price = 0;
    var helpPrice = $('#Bill_LagerungPerPrice').val();
    if (helpPrice != "" && isNumeric(helpPrice)) price = parseInt(helpPrice);
    else price = 0;
    if (price == 0) return;

    var duration = 0;
    var helpDuration = $('#Bill_LagerungVolume').val();
    if (helpDuration != "" && isNumeric(helpDuration)) duration = parseFloat(helpDuration);
    else duration = 0;
    if (duration == 0) return;

    price = price * duration;

    var additionprice = 0;
    var priceplus = $('#Bill_LagerungCostAdditFreeTextPlus1Price').val();
    if (priceplus != "") additionprice += parseFloat(priceplus);
    priceplus = $('#Bill_LagerungCostAdditFreeTextPlus2Price').val();
    if (priceplus != "") additionprice += parseFloat(priceplus);

    var subprice = 0;
    var priceminus = $('#Bill_LagerungCostDiscount').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_LagerungCostDiscount2').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_LagerungCostAdditFreeTextMinus1Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_LagerungCostAdditFreeTextMinus2Price').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    // set temporary price
    price = (Math.round((price + additionprice - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_LagerungCostSub')[0].value = price;
    }
    else {
        $('#Bill_LagerungCostSub')[0].value = price.toFixed(2);
    }

    if ($('#Bill_LagerungCostFix').is(':checked')) {
        var helpPrice = $('#Bill_LagerungCostFixPrice').val();
        if (helpPrice != "" && isNumeric(helpPrice)) price = parseFloat(helpPrice);
    }
    subprice = 0;
    priceminus = $('#Bill_LagerungCostPartialPayment').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);
    priceminus = $('#Bill_LagerungCostPartialByCash').val();
    if (priceminus != "" && isNumeric(priceminus)) subprice += parseFloat(priceminus);

    price = (Math.round((price - subprice) * 100) / 100);
    if (price % 1 == 0) {
        $('#Bill_LagerungCost')[0].value = price;
    }
    else {
        $('#Bill_LagerungCost')[0].value = price.toFixed(2);
    }
}

// packmaterial functions
function initDefinedPackMaterial() {
    $('#definedpackmaterialcontainer').find('select[data-onchange]').each(function () {
        var selectedValue = $("#" + this.id + "Id").val();
        if (selectedValue > 0) {
            $("#" + this.id + " option[value^='" + selectedValue + "|']").prop("selected", true);
            updateDefinedPackMaterialDetailValues(this, false);
        }
    });
}
function updateDefinedPackMaterialDetailValues(ctrl, setDetailControlsFromDropdownValue) {
    // set the detail values from the selected packmaterial
    var split = ctrl.value.split("|");
    $("#" + ctrl.id + "Id").val(split[0]);
    // check if rent is possible
    if (split[1] != "") {
        // take priceRent and enable rent (if previous was disabled)
        var radioRent = $("#RentBuy_Miete_" + ctrl.dataset.onchange);
        var radioBuy = $("#RentBuy_Kauf_" + ctrl.dataset.onchange);
        if (setDetailControlsFromDropdownValue) {
            $("#" + ctrl.id.replace('_DefinedPackMaterial', '_PiecePrice')).val(split[1]);
            radioRent.prop("disabled", false);
            radioRent.prop("checked", "checked");
            radioBuy.removeAttr("checked");
        }
        radioRent.attr('data-onchangeval', split[1]);
        radioBuy.attr('data-onchangeval', split[2]);
        // attach onchange event for Rent/Buy Selection
        $('#definedpackmaterialcontainer').find('input:radio[data-onchangeval]').change(function () {
            $("#BillDefinedPackMaterials_" + this.id.substr(this.id.length - 1) + "__PiecePrice").val(this.dataset.onchangeval);
        });
    } else {
        // take priceBuy and disable rent
        var radioRent = $("#RentBuy_Miete_" + ctrl.dataset.onchange);
        if (setDetailControlsFromDropdownValue) {
            $("#" + ctrl.id.replace('_DefinedPackMaterial', '_PiecePrice')).val(split[2]);
            radioRent.removeAttr("checked");
            $("#RentBuy_Kauf_" + ctrl.dataset.onchange).prop("checked", "checked");
        }
        radioRent.prop("disabled", "disabled");
    }
}
$('#definedpackmaterialcontainer').find('select[data-onchange]').change(function () {
    if (this.value == "") {
        // remove the actual detail values, if selected packmaterial is set to empty value (so clean the actual line)
        $("#" + this.id + "Id").val("");
        var radioRent = $("#RentBuy_Miete_" + this.dataset.onchange);
        radioRent.prop("disabled", false);
        radioRent.prop("checked", "checked");
        $("#RentBuy_Kauf_" + this.dataset.onchange).removeAttr("checked");
        $("#" + this.id.replace('_DefinedPackMaterial', '_PiecePrice')).val("");
        $("#" + this.id.replace('_DefinedPackMaterial', '_CountNumber')).val(0);
        $("#" + this.id.replace('_DefinedPackMaterial', '_EndPrice')).val("");
    } else {
        updateDefinedPackMaterialDetailValues(this, true);
    }
});
$('#definedpackmaterialcontainer').find('input[data-selectable]').focus(function () {
    updateDefinedPackMaterialSubCost(this);
    $(this.dataset.selectable).show();
});
function updateDefinedPackMaterialSubCost(ctrl) {
    var idName = ctrl.id;
    var piecePrice = 0;
    var temppiecePrice = $('#' + idName.replace('EndPrice', 'PiecePrice')).val();
    if (temppiecePrice != "" && isNumeric(temppiecePrice)) piecePrice = parseFloat(temppiecePrice);
    else piecePrice = 0;

    var countPiece = 0;
    var tempcountPiece = $('#' + idName.replace('EndPrice', 'CountNumber')).val();
    if (tempcountPiece != "" && isNumeric(tempcountPiece)) countPiece = parseFloat(tempcountPiece);
    else countPiece = 0;

    ctrl.value = (Math.round(piecePrice * countPiece * 100) / 100).toFixed(2);
}
$('#freepackmaterialcontainer').find('input[data-selectable]').focus(function () {
    updatePackMaterialSubCost(this);
    $(this.dataset.selectable).show();
});
function updatePackMaterialSubCost(ctrl) {
    var idName = ctrl.id;
    var piecePrice = 0;
    var temppiecePrice = $('#' + idName.replace('EndPrice', 'PricePerPiece')).val();
    if (temppiecePrice != "" && isNumeric(temppiecePrice)) piecePrice = parseFloat(temppiecePrice);
    else piecePrice = 0;

    var countPiece = 0;
    var tempcountPiece = $('#' + idName.replace('EndPrice', 'CountNumber')).val();
    if (tempcountPiece != "" && isNumeric(tempcountPiece)) countPiece = parseFloat(tempcountPiece);
    else countPiece = 0;

    ctrl.value = (Math.round(piecePrice * countPiece * 100) / 100).toFixed(2);
}
function updatePackMaterial() {
    var ctrl = $('#Bill_PackMaterialCost');

    var totalPrice = parseFloat(0);
    $('#definedpackmaterialcontainer').find('input[data-selectable]').each(function () {
        updateDefinedPackMaterialSubCost(this);
        if (this.value !== undefined && isNumeric(this.value)) {
            totalPrice += parseFloat(this.value);
        }
    });
    $('#freepackmaterialcontainer').find('input[data-selectable]').each(function () {
        updatePackMaterialSubCost(this);
        if (this.value !== undefined && isNumeric(this.value)) {
            totalPrice += parseFloat(this.value);
        }
    });

    var priceminus = $('#Bill_PackMaterialCostDiscount').val();
    if (priceminus != "" && isNumeric(priceminus)) totalPrice -= parseFloat(priceminus);

    if ($('#Bill_PackMaterialDeliver')[0].checked) {
        var deliverPrice = $('#Bill_PackMaterialDeliverPrice').val();
        if (deliverPrice != "" && isNumeric(deliverPrice)) totalPrice += parseFloat(deliverPrice);
    }

    if ($('#Bill_PackMaterialPickup')[0].checked) {
        var deliverPrice = $('#Bill_PackMaterialPickupPrice').val();
        if (deliverPrice != "" && isNumeric(deliverPrice)) totalPrice += parseFloat(deliverPrice);
    }

    ctrl[0].value = totalPrice;
}


$(document).ready(function () {
    findactivejsevents("validator-form");
    initDefinedPackMaterial();
});
