﻿var tinymceInitialized = false;

function loadEmailChangeData(url, ctrlSubjectId, ctrlBodyId, loaderId) {
    // show loader-gif
    var loaderIsActive = false;
    if (loaderId != undefined) {
        $("#" + loaderId).show();
        loaderIsActive = true;
    }

    // get data
    $.ajax({
        type: "GET",
        async: false,
        url: url,
        contentType: "application/json"
    }).done(function (res) {
        $("#" + ctrlSubjectId).val(res.Subject);
        $("#" + ctrlBodyId).val(res.Body);
        initializeTinymce(ctrlBodyId);
    }).fail(function (jqXHR, textStatus) {
        alert("Error loading e-mail texts. If the error persists, deselect Checkbox and send the default emailtext. Details about the error message: " + textStatus);
    }).always(function() {
        if (loaderIsActive) {
            $("#" + loaderId).hide();
        }
    });
}

function initializeTinymce(ctrlId) {
    // initialize tinymce from mail body textarea, if not already initialized
    if (!tinymceInitialized) {
        tinymce.init({
            selector: '#'+ctrlId,
            height: 500,
            menubar: false,
            plugins: ['lists link textcolor image',
                'code contextmenu fullpage'],
            toolbar: 'undo redo |  bold italic underline forecolor fontsizeselect | link image | bullist numlist | code',
            fontsize_formats: '10px 11px 12px 14px 18px 22px 26px',
            relative_urls: false,
            remove_script_host: false,
            branding: false
        });
        tinymceInitialized = true;
    }
}