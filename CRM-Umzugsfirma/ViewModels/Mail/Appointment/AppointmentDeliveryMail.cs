﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Mail.Appointment
{
    public class AppointmentDeliveryMail
    {
        public MainMailData MainMailData { get; set; }
        public string Date { get; set; }
        public string TimeFrom { get; set; }
        public string TimeTo { get; set; }

        public string Comment { get; set; }
    }
}