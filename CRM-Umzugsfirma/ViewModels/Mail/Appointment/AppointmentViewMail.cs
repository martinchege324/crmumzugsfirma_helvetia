﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Mail.Appointment
{
    public class AppointmentViewMail
    {
        public MainMailData MainMailData { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

        public string Comment { get; set; }
    }
}