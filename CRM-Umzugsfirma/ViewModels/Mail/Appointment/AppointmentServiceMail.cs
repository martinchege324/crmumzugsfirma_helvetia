﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace CRM_Umzugsfirma.ViewModels.Mail.Appointment
{
    public class AppointmentServiceMail
    {
        public MainMailData MainMailData { get; set; }

        //public string PaymentType { get; set; }
        public string DateUmzug { get; set; }
        public string TimeUmzug { get; set; }
        public string DateUmzug2 { get; set; }
        public string TimeUmzug2 { get; set; }
        public string DateUmzug3 { get; set; }
        public string TimeUmzug3 { get; set; }
        public string DatePack { get; set; }
        public string TimePack { get; set; }
        public string DatePackOut { get; set; }
        public string TimePackOut { get; set; }
        public string DateEntsorgung { get; set; }
        public string TimeEntsorgung { get; set; }
        public string DateReinigung { get; set; }
        public string TimeReinigung { get; set; }
        public string DateReinigungRelease { get; set; }
        public string TimeReinigungRelease { get; set; }
        public string DateReinigung2 { get; set; }
        public string TimeReinigung2 { get; set; }
        public string DateReinigung2Release { get; set; }
        public string TimeReinigung2Release { get; set; }
        public string DateLagerung { get; set; }
        public string TimeLagerung { get; set; }
        public string DateTransport { get; set; }
        public string TimeTransport { get; set; }

        public string Comment { get; set; }
        public virtual AppointmentService AppointmentService { get; set; }
    }
}
