﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Mail
{
    public class OfferMail
    {
        public MainMailData MainMailData { get; set; }
        public Offer.ViewAppointmentState ViewAppointmentStateId { get; set; }
        public string LinkOfferAccept { get; set; }
    }
}