﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Mail
{
    public class MainMailData
    {
        public string MailTitle { get; set; }
        public string PathAccount { get; set; }
        public string PathFooterLogo { get; set; }

        public int InnerAccountId { get; set; }
        public string AccountName { get; set; }
        public string AccountStreet { get; set; }
        public int AccountZip { get; set; }
        public string AccountCity { get; set; }
        public string AccountPhone { get; set; }
        public string AccountFax { get; set; }
        public string AccountMail { get; set; }
        public string AccountWebAddress { get; set; }
        public string AccountContactName { get; set; }

        public string CustomerSalutation { get; set; }
        public string CustomerName { get; set; }
        public string CustomerStreet { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerPhone { get; set; }
    }
}