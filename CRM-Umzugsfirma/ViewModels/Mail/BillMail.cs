﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Mail
{
    public class BillMail
    {
        public MainMailData MainMailData { get; set; }
        public decimal BillCost { get; set; }
        public bool CostInclTax { get; set; }
        public bool CostExclTax { get; set; }
        public bool CostFreeTax { get; set; }
        public float PreCollectionCost { get; set; }
        public DateTime BillToPayUntil { get; set; }
    }
}