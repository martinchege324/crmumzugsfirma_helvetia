﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Print
{
    public class BillPrintViewModel
    {
        public Bill Bill { get; set; }
        public List<OfferPriceAdditCosts> UmzugAdditCost { get; set; }
        public List<OfferPriceAdditCosts> PackAdditCost { get; set; }
        public List<OfferPriceAdditCosts> PackOutAdditCost { get; set; }
        public List<OfferPriceAdditCosts> ReinigungAdditCost { get; set; }
        public List<OfferPriceAdditCosts> Reinigung2AdditCost { get; set; }
        public List<OfferPriceAdditCosts> EntsorgungAdditCost { get; set; }
        public List<Bill_DefinedPackMaterial> DefinedPackMaterials { get; set; }
        public List<BillPackMaterial> PackMaterials { get; set; }
        
        public Account Account { get; set; }
        public string AdrOutTitle { get; set; }
        public string AdrInTitle { get; set; }

        public Customer Customer { get; set; }
        public string CustomerSalutation { get; set; }

        public string PathAccount { get; set; }
    }
}