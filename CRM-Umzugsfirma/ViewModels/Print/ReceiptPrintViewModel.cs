﻿using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.ViewModels.Print
{
    public class ReceiptPrintViewModel
    {
        public Receipt Receipt { get; set; }

        public ReceiptUmzug ReceiptUmzug { get; set; }

        public ReceiptReinigung ReceiptReinigung { get; set; }

        public Account Account { get; set; }

        public Customer Customer { get; set; }
        public string CustomerSalutation { get; set; }

        public string PathAccount { get; set; }
    }
}