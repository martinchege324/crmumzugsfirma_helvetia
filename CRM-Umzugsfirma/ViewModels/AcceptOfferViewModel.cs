﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels
{
    public class AcceptOfferViewModel
    {
        public int OfferId { get; set; }
        [Display(Name = "Offer number.")]
        public int OfferInnerId { get; set; }

        public int AccountId { get; set; }
        public int AccountInnerId { get; set; }
        public string AccountName { get; set; }
        public string AccountStreet { get; set; }
        public string AccountCity { get; set; }
        public string AccountPhone { get; set; }
        public string AccountMail { get; set; }
        public string AccountContactName { get; set; }

        public int CustomerId { get; set; }
        [Display(Name = "Customer number.")]
        public int CustomerInnerId { get; set; }
        [Display(Name = "Customer Type")]
        public MarketType CustomerMarketId { get; set; }
        [Display(Name = "Customer")]
        public string Name { get; set; }
        public string Salutation { get; set; }
        [Display(Name = "Telephone")]
        public string CustomerPhone { get; set; }
        [Display(Name = "Mobile")]
        public string CustomerMobile { get; set; }
        [Display(Name = "Email")]
        public string CustomerEmail { get; set; }

        [Display(Name = "Message to the customer advisor.")]
        public string Comment { get; set; }
        [Display(Name = "Link to the Offer:")]
        public string LinkToOffer { get; set; }
    }
}