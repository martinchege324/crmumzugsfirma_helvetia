﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.ViewModels.Job
{
    public class AutoJobViewModel
    {
        public int AutoJobId { get; set; }

        public bool FoundObject { get; set; }

        public List<string> IdList { get; set; }

        public string ResultMessageToShow { get; set; }
        public string InfoMessageToShow { get; set; }
    }
}