﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public enum MarketType
    {
        Private_person = 1,
        Company = 2
    }

    public enum TitleType
    {
        Please_choose = 0,
        Mrs = 1,
        Mr = 2,
    }

    public enum ContractType
    {
        Lead = 1,
        Customer = 2
    }

    public enum ContactStageType
    {
        Lead = 1,
        Visit = 2,
        Offer = 3,
        Contract = 4,
        Invoice = 5
    }

    public class Customer
    {
        [Display(Name = "CustomerId")]
        public int CustomerId { get; set; }

        [Required]
        [Display(Name = "CustomerInnerId")]
        public int CustomerInnerId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [Display(Name = "Type")]
        public MarketType MarketId { get; set; }

        [Required]
        [Display(Name = "Salutation")]
        public TitleType TitleId { get; set; }

        [Required(ErrorMessage = "Enter last name")]
        [StringLength(200)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter First Name.")]
        [StringLength(200)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Enter Company Name.")]
        [StringLength(200)]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Enter Contact Person.")]
        [StringLength(200)]
        [Display(Name = "Contact Person")]
        public string CompanyContactPerson { get; set; }

        [Required(ErrorMessage = "Enter Street Name")]
        [MaxLength(200)]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Enter postal code.")]
        [Range(1000, 99999, ErrorMessage = "Postcode only allowed in 4-5 characters.")]
        [Display(Name = "Postal Code")]
        public int Zip { get; set; }

        [Required(ErrorMessage = "Enter City Name.")]
        [StringLength(100)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Choose country.")]
        [StringLength(2)]
        [Display(Name = "Country")]
        public string CountryCode { get; set; }

        [StringLength(50)]
        [Display(Name = "Telephone")]
        public string Phone { get; set; }

        [StringLength(50)]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [StringLength(50)]
        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [StringLength(100)]
        [Display(Name = "E-Mail")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        public string Email { get; set; }

        [StringLength(50)]
        [Display(Name = "Customer Source")]
        public string CustomerSource { get; set; }

        [StringLength(100)]
        [Display(Name = "Other Source")]   
        public string OtherSource { get; set; }
        [StringLength(50)]
        [Display(Name = "Customer Source")]
        public string CustomerSource_de { get; set; }

        [StringLength(100)]
        [Display(Name = "Other Source")]
        public string OtherSource_de { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [Display(Name = "Created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }
        public DateTime DeleteDate { get; set; }
        [StringLength(128)]
        public string DeleteUserId { get; set; }

        [Display(Name = "Type")]
        public ContractType ContractType { get; set; }
        [Display(Name = "Change Date")]
        public DateTime ContractTypeChangeDate { get; set; }

        [Display(Name = "Stage")]
        public ContactStageType ContactStage { get; set; }
        [Display(Name = "In Contact")]
        public bool ContactAlreadyHave { get; set; }

        public virtual ICollection<Appointment> Appointments { get; set; }
        public virtual ICollection<Note> Notes { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
        public virtual ICollection<Receipt> Receipts { get; set; }
        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<EmailMarketingCustomer> EmailMarketingCustomers { get; set; }
    }
}