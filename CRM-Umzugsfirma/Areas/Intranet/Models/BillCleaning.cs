﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class BillCleaning
    {
        public int BillCleaningId { get; set; }

        [Required]
        public int BillId { get; set; }
        public virtual Bill Bill { get; set; }

        public int SectorId { get; set; }

        [Display(Name = "cleaning Type")]
        public CleaningType ReinigungType { get; set; }
        [StringLength(200)]
        [Display(Name = "Manual Input (cleaning Type)")]
        public string ReinigungTypeFreeText { get; set; }
        [StringLength(100)]
        [Display(Name = "Room [3.5]")]
        public string ReinigungRoom { get; set; }
        [Display(Name = "Tariff Price")]
        public int ReinigungPerPrice { get; set; }

        [StringLength(100)]
        [Display(Name = "Duration")]
        public string ReinigungHourlyDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int ReinigungHourlyPerPrice { get; set; }
        public decimal ReinigungHourlyCostService { get; set; }

        [StringLength(300)]
        [Display(Name = "Optional: Services (for partial cleaning or construction cleaning services)")]
        public string ReinigungWishedServiceFreeText { get; set; }

        // ToDo Delete after new CleaningService (not needed, because ReinigungRoom is new field)
        [Display(Name = "Text in Invoice [3.5 Room]")]
        public string ReinigungTarifText { get; set; }

        [Display(Name = "Date")]
        public string ReinigungDate { get; set; }
        [Display(Name = "Extra cost")]
        public bool ReinigungCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string ReinigungCostAdditFreeTextPlus1 { get; set; }
        public decimal ReinigungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string ReinigungCostAdditFreeTextPlus2 { get; set; }
        public decimal ReinigungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string ReinigungCostAdditFreeTextMinus1 { get; set; }
        public decimal ReinigungCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string ReinigungCostAdditFreeTextMinus2 { get; set; }
        public decimal ReinigungCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal ReinigungCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal ReinigungCostDiscount2 { get; set; }
        [Display(Name = "Price")]
        public decimal ReinigungCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool ReinigungCostFix { get; set; }
        public decimal ReinigungCostFixPrice { get; set; }
        [Display(Name = "Claims payment")]
        public decimal ReinigungCostDamage { get; set; }
        [Display(Name = "Deposit")]
        public decimal ReinigungCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal ReinigungCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal ReinigungCost { get; set; }

    }
}