﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AppointmentView
    {
        public enum MeetPlaceType
        {
            At_the_customer = 1,
            Via_Phone = 2,
            Other = 9
        }

        public int AppointmentId { get; set; }
        public virtual Appointment Appointment { get; set; }

        [Display(Name = "Inspection location")]
        public MeetPlaceType MeetPlace { get; set; }

        [StringLength(30)]
        [Display(Name = "Date")]
        public string ViewDate { get; set; }
        [StringLength(30)]
        public string ViewTime { get; set; }
        public DateTime? ViewDateTime { get; set; }
        [StringLength(50)]
        public string GoogleEventViewId { get; set; }
    }
}