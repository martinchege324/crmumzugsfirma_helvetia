﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class OfferTransport
    {
        public int OfferTransportId { get; set; }

        [Required]
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        public int SectorId { get; set; }

        [StringLength(300)]
        [Display(Name = "Optional: transport-Type Text (comes in Pdf)")]
        public string TransportTypeFreeText { get; set; }

        [StringLength(30)]
        [Display(Name = "Transport Date")]
        public string TransportDate { get; set; }
        public DateTime? TransportDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string TransportTime { get; set; }
        [Display(Name = "Duration [h]")]
        public string TransportDuration { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int TransportCostWay { get; set; }

        [Display(Name = "Package Price-Tariff")]
        public int? TransportCostFixRatePrice { get; set; }

        [Display(Name = "Hourly Tariff")]
        public int? TransportPriceRateId { get; set; }
        [StringLength(100)]
        public string TransportPriceRateDescr { get; set; }
        [Display(Name = "MA")]
        public int TransportCostChangedPers { get; set; }
        [Display(Name = "Truck")]
        public int TransportCostChangedVeh { get; set; }
        [Display(Name = "Pendant")]
        public int TransportCostChangedTrailer { get; set; }
        [Display(Name = "CHF-Approach")]
        public int TransportCostChangedPrice { get; set; }

        [Display(Name = "Extra_cost")]
        public bool TransportCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string TransportCostAdditFreeTextPlus1 { get; set; }
        public int TransportCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus2 { get; set; }
        public int TransportCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus3 { get; set; }
        public int TransportCostAdditFreeTextPlus3Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus4 { get; set; }
        public int TransportCostAdditFreeTextPlus4Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus5 { get; set; }
        public int TransportCostAdditFreeTextPlus5Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus6 { get; set; }
        public int TransportCostAdditFreeTextPlus6Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus7 { get; set; }
        public int TransportCostAdditFreeTextPlus7Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further_Deductions")]
        public string TransportCostAdditFreeTextMinus1 { get; set; }
        public int TransportCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextMinus2 { get; set; }
        public int TransportCostAdditFreeTextMinus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Costs")]
        public string TransportCostSum { get; set; }
        [Display(Name = "Discount")]
        public int TransportCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public int TransportCostDiscount2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Package Price or Estimated Costs (depending on the chosen tariff)")]
        public string TransportCostEstimated { get; set; }
        [Display(Name = "Cost ceiling")]
        public bool TransportCostHighSecurity { get; set; }
        public int TransportCostHighSecurityPrice { get; set; }
        [Display(Name = "Set Manually")]
        public bool TransportCostHighSecurityManual { get; set; }
        [Display(Name = "Flat rate")]
        public bool TransportCostFix { get; set; }
        public int TransportCostFixPrice { get; set; }
    }
}