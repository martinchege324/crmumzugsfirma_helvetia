﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Offer_DefinedPackMaterial
    {
        public int Offer_DefinedPackMaterialId { get; set; }

        public int OfferId { get; set; }
        public Offer Offer { get; set; }

        public int DefinedPackMaterialId { get; set; }
        public DefinedPackMaterial DefinedPackMaterial { get; set; }

        [Display(Name = "Rental fee / Purchase")]
        public MieteKaufType RentBuy { get; set; }
        [Display(Name = "Unit Price")]
        [StringLength(20)]
        public string PiecePrice { get; set; }
        [Display(Name = "Number")]
        public int CountNumber { get; set; }
        [Display(Name = "Total")]
        [StringLength(20)]
        public string EndPrice { get; set; }

    }
    public class Offer_DefinedPackMaterial_de
    {
        [Key]
        public int Offer_DefinedPackMaterialId { get; set; }

        public int OfferId { get; set; }
        public Offer Offer { get; set; }

        public int DefinedPackMaterialId { get; set; }
        public DefinedPackMaterial_de DefinedPackMaterial_de { get; set; }

        [Display(Name = "Rental fee / Purchase")]
        public MieteKaufType RentBuy { get; set; }
        [Display(Name = "Unit Price")]
        [StringLength(20)]
        public string PiecePrice { get; set; }
        [Display(Name = "Number")]
        public int CountNumber { get; set; }
        [Display(Name = "Total")]
        [StringLength(20)]
        public string EndPrice { get; set; }

    }
}