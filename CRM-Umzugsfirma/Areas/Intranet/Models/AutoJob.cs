﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AutoJob
    {
        public enum AutoJobType
        {
            OpenOfferWithoutFeedback = 1,
            AfterFinishedServiceThankmail = 2
        }

        public int AutoJobId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        public AutoJobType Type { get; set; }

        public DateTime CreateDate { get; set; }

        [StringLength(300)]
        public string CalledUrl { get; set; }

        [StringLength(50)]
        public string CalledSourceIP { get; set; }

        public bool Successful { get; set; }
        public string ErrorMessage { get; set; }

        public int AffectedCount { get; set; }

        [StringLength(50)]
        public string AffectedDbTable { get; set; }

        public string AffectedIdList { get; set; }

        public string AffectedEmailList { get; set; }

        public DateTime ModifyDate { get; set; }

        public virtual ICollection<AutoJobSentEmail> AutoJobSentEmails { get; set; }
    }
}