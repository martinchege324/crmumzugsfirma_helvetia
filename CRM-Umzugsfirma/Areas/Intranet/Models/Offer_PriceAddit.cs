﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Offer_PriceAddit
    {
        [Key]
        public int Offer_PriceAdditId { get; set; }

        [Required]
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        [Required]
        public int PriceAdditRateId { get; set; }
        public virtual PriceAdditRate PriceAdditRate { get; set; }

        public int EndPrice { get; set; }
    }
    public class Offer_PriceAddit_de
    {
        [Key]
        public int Offer_PriceAdditId { get; set; }

        [Required]
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        [Required]
        public int PriceAdditRateId { get; set; }
        public virtual PriceAdditRate_de PriceAdditRate_de { get; set; }

        public int EndPrice { get; set; }
    }
}