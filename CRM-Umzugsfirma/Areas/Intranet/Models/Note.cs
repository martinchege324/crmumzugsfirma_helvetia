﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Note
    {
        public int NoteId { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Display(Name = "Date")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }

        [Required(ErrorMessage = "Enter the grade.")]
        [Display(Name = "New Note")]
        public string Body { get; set; }

        [Required]
        public bool IsDeleted { get; set; }
        public DateTime DeleteDate { get; set; }
        [StringLength(128)]
        public string DeleteUserId { get; set; }
    }
}