﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AccountEmployee
    {
        public int AccountEmployeeId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [Display(Name = "TitleId")]
        public TitleType TitleId { get; set; }

        [Required(ErrorMessage = "Enter last name.")]
        [StringLength(200)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Enter first name.")]
        [StringLength(200)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [NotMapped]
        public string FullName => $"{FirstName} {LastName}";

        [StringLength(100)]
        [Display(Name = "E-Mail")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        public string Email { get; set; }

        [StringLength(50)]
        [Display(Name = "Telephone")]
        public string Phone { get; set; }

        [StringLength(50)]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [MaxLength(200)]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Range(1000, 99999, ErrorMessage = "Postcode only allowed in 4-5 characters.")]
        [Display(Name = "Postcode")]
        public int Zip { get; set; }

        [StringLength(100)]
        [Display(Name = "Place")]
        public string City { get; set; }

        [StringLength(2)]
        [Display(Name = "Country")]
        public string CountryCode { get; set; }

        [Display(Name = "Team")]
        public AccountDepartment AccountDepartment { get; set; }

        [Display(Name = "Note")]
        public string Note { get; set; }

        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }

        [Display(Name = "Created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime? ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }
        public DateTime? DeleteDate { get; set; }
        [StringLength(128)]
        public string DeleteUserId { get; set; }

        public virtual ICollection<Offer> Offers { get; set; }
    }
}