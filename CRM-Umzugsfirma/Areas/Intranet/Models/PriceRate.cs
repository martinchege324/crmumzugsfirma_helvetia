﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class PriceRate
    {
        [Key]
        public int PriceRateId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string ServiceType { get; set; }

        [Required]
        [StringLength(200)]
        public string Descr { get; set; }

        public int CountFirst { get; set; }
        public int CountSecond { get; set; }
        public int CountPrice { get; set; }
        public int CountPriceRangeMax { get; set; }

        [StringLength(50)]
        public string DescrOnChangeFirst { get; set; }

        [StringLength(50)]
        public string DescrOnChangeSecond { get; set; }

        [StringLength(50)]
        public string DescrOnChangePrice { get; set; }
    }
    public class PriceRate_de
    {
        [Key]
        public int PriceRateId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string ServiceType { get; set; }

        [Required]
        [StringLength(200)]
        public string Descr { get; set; }

        public int CountFirst { get; set; }
        public int CountSecond { get; set; }
        public int CountPrice { get; set; }
        public int CountPriceRangeMax { get; set; }

        [StringLength(50)]
        public string DescrOnChangeFirst { get; set; }

        [StringLength(50)]
        public string DescrOnChangeSecond { get; set; }

        [StringLength(50)]
        public string DescrOnChangePrice { get; set; }
    }
}