﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class BillPackMaterial
    {
        public int PackMaterialId { get; set; }

        [Required]
        public int BillId { get; set; }
        public virtual Bill Bill { get; set; }

        [StringLength(100)]
        [Display(Name="Text")]
        public string Text { get; set; }
        [Display(Name = "Rental fee / Purchase")]
        public MieteKaufType MieteKauf { get; set; }
        [Display(Name = "Unit Price")]
        public string PricePerPiece { get; set; }
        [Display(Name = "Number")]
        public int CountNumber { get; set; }
        [Display(Name = "Total")]
        public string EndPrice { get; set; }
    }
}