﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AppointmentService
    {

        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }
        public int AppointmentId { get; set; }
        
        public virtual Appointment Appointment { get; set; }
        [Required]
        [Display(Name = "Move")]
        public bool ServiceUmzug { get; set; }

        [StringLength(30)]
        [Display(Name = "Moving Date on")]
        public string UmzugDate { get; set; }
        [StringLength(30)]
        public string UmzugTime { get; set; }
        public DateTime? UmzugDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationUmzug { get; set; }
        [Display(Name = "MA")]
        public int CountPersonUmzug { get; set; }
        [Display(Name = "Truck")]
        public int CountVehUmzug { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerUmzug { get; set; }
        [StringLength(100)]
        public string UmzugWhereFrom { get; set; }
        [StringLength(100)]
        public string UmzugWhereTo { get; set; }
        [StringLength(50)]
        public string GoogleEventUmzugId { get; set; }

        [Display(Name = "Further Relocation Dates")]
        public bool DateUmzugMoreAppointment { get; set; }
        [StringLength(30)]
        [Display(Name = "Moving Date 2 on")]
        public string UmzugDate2 { get; set; }
        [StringLength(30)]
        public string UmzugTime2 { get; set; }
        public DateTime? UmzugDateTime2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationUmzug2 { get; set; }
        [Display(Name = "MA")]
        public int CountPersonUmzug2 { get; set; }
        [Display(Name = "Truck")]
        public int CountVehUmzug2 { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerUmzug2 { get; set; }
        [StringLength(50)]
        public string GoogleEventUmzugId2 { get; set; }
        [StringLength(30)]
        [Display(Name = "Moving Date 3 on")]
        public string UmzugDate3 { get; set; }
        [StringLength(30)]
        public string UmzugTime3 { get; set; }
        public DateTime? UmzugDateTime3 { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationUmzug3 { get; set; }
        [Display(Name = "MA")]
        public int CountPersonUmzug3 { get; set; }
        [Display(Name = "Truck")]
        public int CountVehUmzug3 { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerUmzug3 { get; set; }
        [StringLength(50)]
        public string GoogleEventUmzugId3 { get; set; }

        [Required]
        [Display(Name = "Packing Service")]
        public bool ServicePack { get; set; }

        [StringLength(30)]
        [Display(Name = "Packing Date")]
        public string PackDate { get; set; }
        [StringLength(30)]
        public string PackTime { get; set; }
        public DateTime? PackDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationPack { get; set; }
        [Display(Name = "MA")]
        public int CountPersonPack { get; set; }
        [Display(Name = "Truck")]
        public int CountVehPack { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerPack { get; set; }
        [StringLength(50)]
        public string GoogleEventPackId { get; set; }

        [Required]
        [Display(Name = "Unpacking Service")]
        public bool ServicePackOut { get; set; }

        [StringLength(30)]
        [Display(Name = "Packing Date")]
        public string PackOutDate { get; set; }
        [StringLength(30)]
        public string PackOutTime { get; set; }
        public DateTime? PackOutDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationPackOut { get; set; }
        [Display(Name = "MA")]
        public int CountPersonPackOut { get; set; }
        [Display(Name = "Truck")]
        public int CountVehPackOut { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerPackOut { get; set; }
        [StringLength(50)]
        public string GoogleEventPackOutId { get; set; }

        [Required]
        [Display(Name = "Disposal")]
        public bool ServiceEntsorgung { get; set; }

        [StringLength(30)]
        [Display(Name = "Disposal Date at")]
        public string EntsorgungDate { get; set; }
        [StringLength(30)]
        public string EntsorgungTime { get; set; }
        public DateTime? EntsorgungDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationEntsorgung { get; set; }
        [Display(Name = "MA")]
        public int CountPersonEntsorgung { get; set; }
        [Display(Name = "Truck")]
        public int CountVehEntsorgung { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerEntsorgung { get; set; }
        [StringLength(50)]
        public string GoogleEventEntsorgungId { get; set; }

        [Required]
        [Display(Name = "cleaning")]
        public bool ServiceReinigung { get; set; }

        [StringLength(30)]
        [Display(Name = "cleaning Date on")]
        public string ReinigungDate { get; set; }
        [StringLength(30)]
        public string ReinigungTime { get; set; }
        public DateTime? ReinigungDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Deadline")]
        public string DateReinigungRelease { get; set; }
        [StringLength(100)]
        public string TimeReinigungRelease { get; set; }
        [StringLength(100)]
        public string ReleaseTimeReinigung { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationReinigung { get; set; }
        [StringLength(50)]
        public string GoogleEventReinigungId { get; set; }

        [Required]
        [Display(Name = "cleaning 2")]
        public bool ServiceReinigung2 { get; set; }

        [StringLength(30)]
        [Display(Name = "cleaning Date on")]
        public string Reinigung2Date { get; set; }
        [StringLength(30)]
        public string Reinigung2Time { get; set; }
        public DateTime? Reinigung2DateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Deadline")]
        public string DateReinigung2Release { get; set; }
        [StringLength(100)]
        public string TimeReinigung2Release { get; set; }
        [StringLength(100)]
        public string ReleaseTimeReinigung2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationReinigung2 { get; set; }
        [StringLength(50)]
        public string GoogleEventReinigung2Id { get; set; }

        [Required]
        [Display(Name = "Storage")]
        public bool ServiceLagerung { get; set; }

        [StringLength(30)]
        [Display(Name = "Storage at")]
        public string LagerungDate { get; set; }
        [StringLength(30)]
        public string LagerungTime { get; set; }
        public DateTime? LagerungDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationLagerung { get; set; }
        [StringLength(50)]
        public string GoogleEventLagerungId { get; set; }

        [Required]
        [Display(Name = "transport")]
        public bool ServiceTransport { get; set; }

        [StringLength(30)]
        [Display(Name = "transport at")]
        public string TransportDate { get; set; }
        [StringLength(30)]
        public string TransportTime { get; set; }
        public DateTime? TransportDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string DurationTransport { get; set; }
        [Display(Name = "MA")]
        public int CountPersonTransport { get; set; }
        [Display(Name = "Truck")]
        public int CountVehTransport { get; set; }
        [Display(Name = "Pendant")]
        public int CountTrailerTransport { get; set; }
        [StringLength(100)]
        public string TransportWhereFrom { get; set; }
        [StringLength(100)]
        public string TransportWhereTo { get; set; }
        [StringLength(50)]
        public string GoogleEventTransportId { get; set; }

        [NotMapped]
        public string BookedServices
        {
            get
            {
                string s = string.Empty;
               
                if (ServiceUmzug) s += ", Move";
                if (ServicePack) s += ", Packing";
                if (ServicePackOut) s += ", Unpacking";
                if (ServiceEntsorgung) s += ", Disposal";
                if (ServiceReinigung || ServiceReinigung2) s += ", cleaning";
                if (ServiceLagerung) s += ", Storage";
                if (ServiceTransport) s += ", Transport";
                if (s.Length > 0) s = s.Substring(2);
                return s;
            }
        }
        public string PaymentServices
        {
            get {
                string p = string.Empty;
                p = PaymentType;
                return p;
            }
        }

    }
}