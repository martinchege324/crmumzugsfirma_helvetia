﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.Enums
{
    public enum CleaningType
    {
        Please_choose = 0,
        Apartment_cleaning_incl_point_acceptance_guarantee = 1,
        Apartment_cleaning_incl_point_broom_clean = 2,
        EFH_bind_cleaning_inkl_point_purchase_guarantee = 3,
        EFH_bind_cleaning_inkl_point_broom_clean = 4,
        RFH_bind_cleaning_incl_point_purchase_guarantee = 5,
        RFH_bind_cleaning_incl_point_broom_clean = 6,
        Construction_cleaning = 21,
        Construction_cleaning_inl_point_acceptance_guarantee = 22,
        Cleaning_incl_point_broom_clean = 23,
        Maintenance_Cleaning = 31,
        Business_Cleaning = 41,
        Office_Cleaning = 42,
        Storage_room_bind_cleaning = 43
    }
}