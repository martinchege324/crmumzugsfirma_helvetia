﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.Enums
{
    public enum AccountDepartment
    {
        Not_defined = 0,
        Administration = 1,
        External_Sales = 10,
        Office_Staff = 11,
        Implementation_Staff = 20
    }
}