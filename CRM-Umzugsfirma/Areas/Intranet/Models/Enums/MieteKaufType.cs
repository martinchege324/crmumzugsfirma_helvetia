﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.Enums
{
    public enum MieteKaufType
    {
        Rental_fee = 1,
        Purchase = 2,
    }
}