﻿namespace CRM_Umzugsfirma.Areas.Intranet.Models.Enums
{
    public enum PayType
    {
        Bar = 1,
        Invoice = 2
    }
}