﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class EmailTextContainer
    {
        [Display(Name = "Title")]
        public string Subject { get; set; }

        [Display(Name = "Text")]
        [AllowHtml]
        //[DataType(DataType.Html)]
        public string Body { get; set; }
    }
}