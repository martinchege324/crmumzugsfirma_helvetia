﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Account
    {
        public int AccountId { get; set; }

        [Required]
        public int InnerAccountId { get; set; }

        [Required(ErrorMessage = "Enter Name.")]
        [StringLength(200)]
        [Display(Name = "Name")]
        public string AccountName { get; set; }

        [Required(ErrorMessage = "Enter street.")]
        [MaxLength(200)]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Enter postal code.")]
        [Range(1000, 99999, ErrorMessage = "Postcode only allowed in 4-5 characters.")]
        [Display(Name = "Postcode")]
        public int Zip { get; set; }

        [Required(ErrorMessage = "City.")]
        [StringLength(100)]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Enter phone.")]
        [StringLength(50)]
        [Display(Name = "Telephone")]
        public string Phone { get; set; }

        [StringLength(50)]
        [Display(Name = "Fax")]
        public string Fax { get; set; }

        [StringLength(50)]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }

        [StringLength(100)]
        [Display(Name = "Contact Name")]
        public string ContactName { get; set; }

        [StringLength(50)]
        [Display(Name = "Contact Name Tel")]
        public string ContactPhone { get; set; }

        [Required(ErrorMessage = "Enter e-mail.")]
        [StringLength(100)]
        [Display(Name = "E-Mail")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Enter homepage address.")]
        [StringLength(150)]
        [Display(Name = "Homepage")]
        public string WebPage { get; set; }

        [Required(ErrorMessage = "Enter Google Email.")]
        [StringLength(100)]
        [Display(Name = "Google E-Mail")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        public string GoogleEmail { get; set; }

        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }

        public bool IsActive { get; set; }

        public virtual AccountEmailConfig AccountEmailConfig { get; set; }
        public virtual ICollection<AccountEmployee> AccountEmployees { get; set; }
        public virtual ICollection<EmailType> EmailTypes { get; set; }
        public virtual ICollection<EmailType_de> EmailTypes_de { get; set; }
        public virtual ICollection<EmailAttachment> EmailAttachments { get; set; }
        public virtual ICollection<EmailAttachment_de> EmailAttachments_de { get; set; }
        public virtual ICollection<User_Account> User_Accounts { get; set; }
        public virtual ICollection<Customer> Customers { get; set; }
        public virtual ICollection<Offer> Offers { get; set; }
        public virtual ICollection<Receipt> Receipts { get; set; }
        public virtual ICollection<Bill> Bills { get; set; }
        public virtual ICollection<PriceRate> PriceRates { get; set; }
        public virtual ICollection<PriceRate_de> PriceRates_de { get; set; }
        public virtual ICollection<PriceAdditRate> PriceAdditRates { get; set; }
        public virtual ICollection<PriceAdditRate_de> PriceAdditRates_de { get; set; }
        public virtual ICollection<DefinedPackMaterial> DefinedPackMaterials { get; set; }
        public virtual ICollection<DefinedPackMaterial_de> DefinedPackMaterials_de { get; set; }
        public virtual ICollection<AutoJob> AutoJobs { get; set; }
    }
}