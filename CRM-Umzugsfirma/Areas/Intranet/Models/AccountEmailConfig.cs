﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AccountEmailConfig
    {
        public int AccountId { get; set; }

        [Required]
        [StringLength(200)]
        public string Host { get; set; }

        [Required]
        public int Port { get; set; }

        [Required]
        public bool UseSSL { get; set; }

        [Required]
        [StringLength(100)]
        public string SenderAccount { get; set; }

        [Required]
        [StringLength(100)]
        public string SenderAccountPass { get; set; }

        [StringLength(100)]
        public string SenderDisplayName { get; set; }
        [StringLength(200)]
        public string ReplyAddress { get; set; }

        [StringLength(100)]
        public string SenderAddressSales { get; set; }

        [StringLength(100)]
        public string SenderAddressAccounting { get; set; }

        public Account Account { get; set; }
    }
}