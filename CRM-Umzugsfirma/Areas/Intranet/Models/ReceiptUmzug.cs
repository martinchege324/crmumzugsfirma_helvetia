﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class ReceiptUmzug
    {
        public Guid ReceiptUmzugId { get; set; }

        public Guid ReceiptId { get; set; }
        public virtual Receipt Receipt { get; set; }

        [Display(Name = "Duration [h]")]
        public string UmzugDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public string UmzugRateCost { get; set; }
        [Display(Name = "Total [CHF]")]
        public string UmzugCost { get; set; }
        [Display(Name = "Expenditure")]
        public string UmzugspesenCost { get; set; }
        [Display(Name = "Directions / Return Trip")]
        public string UmzugWayCost { get; set; }
        [Display(Name = "Packing_Material")]
        public string UmzugPackCost { get; set; }
        [Display(Name = "Volume [m3]")]
        public string EntsorgungVolume { get; set; }
        [Display(Name = "Approach [CHF]")]
        public string EntsorgungRateCost { get; set; }
        [Display(Name = "Total [CHF]")]
        public string EntsorgungCost { get; set; }
        [Display(Name = "Expenditure at the Disposal Site")]
        public string EntsorgungPauschalCost { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText1 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost1 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText2 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost2 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText3 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost3 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText4 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost4 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText5 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost5 { get; set; }
        [Display(Name = "Zuschlag6 Text")]
        public string AdditionalText6 { get; set; }
        [Display(Name = "Zuschlag6 Costs [CHF]")]
        public string AdditionalCost6 { get; set; }

        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText1 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost1 { get; set; }
        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText2 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost2 { get; set; }
        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText3 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost3 { get; set; }
    }
}