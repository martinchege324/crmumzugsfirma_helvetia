﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Offer
    {
        public enum YesNoType
        {
            Yes = 1,
            no = 2,
        }

        public enum YesNoSelectType
        {
            Please_choose = 0,
            Yes = 1,
            no = 2,
        }

        public enum OfferState
        {
            waiting_for_customer = 1,
            confirmed = 2,
            Appointment_created = 3,
            canceled = 7
        }

        public enum PaymentType
        {
            Bar = 1,
            Invoice = 2
        }

        public enum ParkLicenseType
        {
            no = 1,
            Yes_Customer = 2,
            Yes_we = 3
        }

        public enum ViewAppointmentState
        {
            no = 1,
            made = 2,
            does_not_want = 3
        }

        public enum BuildingType
        {
            Please_choose = 0,
            EFH = 1,
            MFH = 2,
            RFH = 3,
            Business_slash_Office = 10,
            Warehouse = 11
        }

        public enum InstallFurnitureType
        {
            Please_choose = 0,
            Customer = 1,
            Helvetia_Transporte_AG = 2
        }

        public enum FloorType
        {
            Please_choose = 0,
            UG = 21,
            EG = 22,
            Hochparterre = 23,
            emp_1 = 1,
            emp_2 = 2,
            emp_3 = 3,
            emp_4 = 4,
            emp_5 = 5,
            emp_6 = 6,
            emp_7 = 7,
            emp_8 = 8,
            emp_9 = 9,
            emp_10 = 10,
            emp_10_plus_ = 11
        }

        [Display(Name = "Offer Id")]
        public int OfferId { get; set; }

        [Required]
        [Display(Name = "Offer number")]
        public int OfferInnerId { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Display(Name = "Created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }
        [Display(Name = "Confirmed on")]
        public DateTime AcceptDate { get; set; }
        [Display(Name = "Appointment created on")]
        public DateTime ReleaseDate { get; set; }
        [StringLength(128)]
        public string ReleaseUserId { get; set; }

        [Required]
        [Display(Name = "Was standing")]
        public OfferState OfferStateId { get; set; }
        [Required]
        [Display(Name = "Payment")]
        public PaymentType PaymentId { get; set; }
        [Required]
        [Display(Name = "Sightseeing")]
        public ViewAppointmentState ViewAppointmentStateId { get; set; }

        [Display(Name = "Contact Person")]
        public int? AccountEmployeeId { get; set; }
        public virtual AccountEmployee ContactPerson { get; set; }
        [StringLength(200)]
        [Display(Name = "Contact Person (Free Text)")]
        public string ContactPersonText { get; set; }

        // From
        [Required(ErrorMessage = "Enter street name.")]
        [MaxLength(200)]
        [Display(Name = "Street")]
        public string AdrOutStreet { get; set; }
        [Required(ErrorMessage = "Enter postal code.")]
        [Range(1000, 99999, ErrorMessage = "Postcode only allowed in 4-5 characters.")]
        [Display(Name = "Postcode")]
        public int AdrOutZip { get; set; }
        [Required(ErrorMessage = "Enter City.")]
        [StringLength(100)]
        [Display(Name = "City")]
        public string AdrOutCity { get; set; }
        [Required(ErrorMessage = "Enter Country.")]
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrOutCountryCode { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrOutFloor { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrOutFloorType { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrOutHasLift { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrOutBuildingType { get; set; }
        // To
        [StringLength(200)]
        [Display(Name = "Street")]
        public string AdrInStreet { get; set; }
        [Display(Name = "Postcode")]
        public int AdrInZip { get; set; }
        [StringLength(100)]
        [Display(Name = "City")]
        public string AdrInCity { get; set; }
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrInCountryCode { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrInFloor { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrInFloorType { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrInHasLift { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrInBuildingType { get; set; }

        // Auszugsadresse2
        [Display(Name = "2. From")]
        public bool AdrOut2 { get; set; }
        [MaxLength(200)]
        [Display(Name = "Street")]
        public string AdrOutStreet2 { get; set; }
        [Display(Name = "Postcode")]
        public int AdrOutZip2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Place")]
        public string AdrOutCity2 { get; set; }
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrOutCountryCode2 { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrOutFloor2 { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrOutFloorType2 { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrOutHasLift2 { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrOutBuildingType2 { get; set; }
            // Einzugsadresse2
        [Display(Name = "2. To")]
        public bool AdrIn2 { get; set; }
        [StringLength(200)]
        [Display(Name = "Street")]
        public string AdrInStreet2 { get; set; }
        [Display(Name = "Postcode")]
        public int AdrInZip2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Place")]
        public string AdrInCity2 { get; set; }
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrInCountryCode2 { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrInFloor2 { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrInFloorType2 { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrInHasLift2 { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrInBuildingType2 { get; set; }

        // Auszugsadresse3
        [Display(Name = "3. From")]
        public bool AdrOut3 { get; set; }
        [MaxLength(200)]
        [Display(Name = "Street")]
        public string AdrOutStreet3 { get; set; }
        [Display(Name = "Postcode")]
        public int AdrOutZip3 { get; set; }
        [StringLength(100)]
        [Display(Name = "Place")]
        public string AdrOutCity3 { get; set; }
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrOutCountryCode3 { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrOutFloor3 { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrOutFloorType3 { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrOutHasLift3 { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrOutBuildingType3 { get; set; }
        // Einzugsadresse3
        [Display(Name = "3. To")]
        public bool AdrIn3 { get; set; }
        [StringLength(200)]
        [Display(Name = "Street")]
        public string AdrInStreet3 { get; set; }
        [Display(Name = "Postcode")]
        public int AdrInZip3 { get; set; }
        [StringLength(100)]
        [Display(Name = "Place")]
        public string AdrInCity3 { get; set; }
        [StringLength(2)]
        [Display(Name = "Country")]
        public string AdrInCountryCode3 { get; set; }
        [StringLength(50)]
        [Display(Name = "Floor")]
        public string AdrInFloor3 { get; set; }
        [Display(Name = "Floor")]
        public FloorType AdrInFloorType3 { get; set; }
        [Display(Name = "Lift")]
        public YesNoType AdrInHasLift3 { get; set; }
        [Display(Name = "Building")]
        public BuildingType AdrInBuildingType3 { get; set; }

        // Move
        [Display(Name = "Move")]
        public bool UmzugActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Moving Date")]
        public string UmzugDate { get; set; }
        public DateTime? UmzugDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string UmzugTime { get; set; }
        [Display(Name = "Move-in Date")]
        public string UmzugInDate { get; set; }
        public DateTime? UmzugInDateTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string UmzugDuration { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int UmzugCostWay { get; set; }
        [Display(Name = "Dismantling and Construction")]
        public InstallFurnitureType UmzugInstallFurnitureType { get; set; }
        [StringLength(100)]
        public string UmzugInstallFurniture { get; set; }
        [Display(Name = "Tariff")]
        public int? UmzugPriceRateId { get; set; }
        [StringLength(100)]
        public string UmzugPriceRateDescr { get; set; }
        [Display(Name = "MA")]
        public int UmzugCostChangedPers { get; set; }
        [Display(Name = "Truck")]
        public int UmzugCostChangedVeh { get; set; }
        [Display(Name = "Pendant")]
        public int UmzugCostChangedTrailer { get; set; }
        [Display(Name = "CHF-Approach")]
        public int UmzugCostChangedPrice { get; set; }
        [Display(Name = "Extra cost")]
        public bool UmzugCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string UmzugCostAdditFreeTextPlus1 { get; set; }
        public int UmzugCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string UmzugCostAdditFreeTextPlus2 { get; set; }
        public int UmzugCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string UmzugCostAdditFreeTextMinus1 { get; set; }
        public int UmzugCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Costs")]
        public string UmzugCostSum { get; set; }
        [Display(Name = "Discount")]
        public int UmzugCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public int UmzugCostDiscount2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Estimated Costs")]
        public string UmzugCostEstimated { get; set; }
        [Display(Name = "Cost ceiling")]
        public bool UmzugCostHighSecurity { get; set; }
        public int UmzugCostHighSecurityPrice { get; set; }
        [Display(Name = "Set Manually")]
        public bool UmzugCostHighSecurityManual { get; set; }
        [Display(Name = "Flat_rate")]
        public bool UmzugCostFix { get; set; }
        public int UmzugCostFixPrice { get; set; }

        // packing_service
        [Display(Name = "Packing service")]
        public bool PackActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Packing Date")]
        public string PackDate { get; set; }
        public DateTime? PackDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string PackTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string PackDuration { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int PackCostWay { get; set; }
        [Display(Name = "Tariff")]
        public int? PackPriceRateId { get; set; }
        [StringLength(100)]
        public string PackPriceRateDescr { get; set; }
        [Display(Name = "MA")]
        public int PackCostChangedPers { get; set; }
        [Display(Name = "CHF-Approach")]
        public int PackCostChangedPrice { get; set; }
        [Display(Name = "Extra cost")]
        public bool PackCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string PackCostAdditFreeTextPlus1 { get; set; }
        public int PackCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string PackCostAdditFreeTextPlus2 { get; set; }
        public int PackCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string PackCostAdditFreeTextMinus1 { get; set; }
        public int PackCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Costs")]
        public string PackCostSum { get; set; }
        [Display(Name = "Discount")]
        public int PackCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public int PackCostDiscount2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Estimated Costs")]
        public string PackCostEstimated { get; set; }
        [Display(Name = "Cost ceiling")]
        public bool PackCostHighSecurity { get; set; }
        public int PackCostHighSecurityPrice { get; set; }
        [Display(Name = "Set Manually")]
        public bool PackCostHighSecurityManual { get; set; }
        [Display(Name = "Flat rate")]
        public bool PackCostFix { get; set; }
        public int PackCostFixPrice { get; set; }

        // unpacking_service
        [Display(Name = "Unpacking")]
        public bool PackOutActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Packing Date")]
        public string PackOutDate { get; set; }
        public DateTime? PackOutDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string PackOutTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string PackOutDuration { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int PackOutCostWay { get; set; }
        [Display(Name = "Tariff")]
        public int? PackOutPriceRateId { get; set; }
        [StringLength(100)]
        public string PackOutPriceRateDescr { get; set; }
        [Display(Name = "MA")]
        public int PackOutCostChangedPers { get; set; }
        [Display(Name = "CHF-Approach")]
        public int PackOutCostChangedPrice { get; set; }
        [Display(Name = "Extra cost")]
        public bool PackOutCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string PackOutCostAdditFreeTextPlus1 { get; set; }
        public int PackOutCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string PackOutCostAdditFreeTextPlus2 { get; set; }
        public int PackOutCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string PackOutCostAdditFreeTextMinus1 { get; set; }
        public int PackOutCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Costs")]
        public string PackOutCostSum { get; set; }
        [Display(Name = "Discount")]
        public int PackOutCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public int PackOutCostDiscount2 { get; set; }
        [StringLength(100)]
        [Display(Name = "Estimated Costs")]
        public string PackOutCostEstimated { get; set; }
        [Display(Name = "Cost ceiling")]
        public bool PackOutCostHighSecurity { get; set; }
        public int PackOutCostHighSecurityPrice { get; set; }
        [Display(Name = "Set Manually")]
        public bool PackOutCostHighSecurityManual { get; set; }
        [Display(Name = "Flat rate")]
        public bool PackOutCostFix { get; set; }
        public int PackOutCostFixPrice { get; set; }

        // disposal
        [Display(Name = "Disposal")]
        public bool EntsorgungActive { get; set; }
        [Display(Name = "Tariff")]
        public int? EntsorgungPersonPriceRateId { get; set; }
        [StringLength(100)]
        public string EntsorgungPersonPriceRateDescr { get; set; }
        [Display(Name = "MA")]
        public int EntsorgungPersonCostChangedPers { get; set; }
        [Display(Name = "Truck")]
        public int EntsorgungPersonCostChangedVeh { get; set; }
        [Display(Name = "Pendant")]
        public int EntsorgungPersonCostChangedTrailer { get; set; }
        [Display(Name = "CHF-Approach")]
        public int EntsorgungPersonCostChangedPrice { get; set; }
        [StringLength(30)]
        [Display(Name = "Disposal Date")]
        public string EntsorgungDate { get; set; }
        public DateTime? EntsorgungDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string EntsorgungTime { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string EntsorgungDuration { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int EntsorgungCostWay { get; set; }
        [Display(Name = "Extra cost")]
        public bool EntsorgungCostAddit { get; set; }
        [Display(Name = "Volume Tariff")]
        public int? EntsorgungPriceRateId { get; set; }
        [StringLength(100)]
        public string EntsorgungPriceRateDescr { get; set; }
        [Display(Name = "CHF-Approach")]
        public int EntsorgungCostChangedPrice { get; set; }
        [StringLength(100)]
        [Display(Name = "Disposal Costs Flat rate")]
        public string EntsorgungFixCost { get; set; }
        [StringLength(100)]
        [Display(Name = "Estimated Volume [m3]")]
        public string EntsorgungVolume { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string EntsorgungCostAdditFreeTextPlus1 { get; set; }
        public int EntsorgungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string EntsorgungCostAdditFreeTextPlus2 { get; set; }
        public int EntsorgungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string EntsorgungCostAdditFreeTextMinus1 { get; set; }
        public int EntsorgungCostAdditFreeTextMinus1Price { get; set; }
        [Display(Name = "Discount")]
        public int EntsorgungCostDiscount { get; set; }
        [StringLength(100)]
        [Display(Name = "Estimated Costs")]
        public string EntsorgungCostEstimated { get; set; }
        [Display(Name = "Cost ceiling")]
        public bool EntsorgungCostHighSecurity { get; set; }
        public int EntsorgungCostHighSecurityPrice { get; set; }
        [Display(Name = "Set Manually")]
        public bool EntsorgungCostHighSecurityManual { get; set; }
        [Display(Name = "Flat rate")]
        public bool EntsorgungCostFix { get; set; }
        public int EntsorgungCostFixPrice { get; set; }

        // cleaning
        public virtual ICollection<OfferCleaning> OfferCleanings { get; set; }

        [Display(Name = "Cleaning")]
        public bool ReinigungActive { get; set; }

        [NotMapped]
        public virtual OfferCleaning OfferCleaning
        {
            get => OfferCleanings?.FirstOrDefault(s => s.SectorId == 1);
            set
            {
                if (OfferCleanings == null) OfferCleanings = new List<OfferCleaning>();
                if (OfferCleaning != null) OfferCleanings.Remove(OfferCleaning);
                OfferCleanings.Add(value);
            }
        }

        // cleaning 2
        [Display(Name = "Cleaning 2")]
        public bool Reinigung2Active { get; set; }

        [NotMapped]
        public virtual OfferCleaning OfferCleaning2
        {
            get => OfferCleanings?.FirstOrDefault(s => s.SectorId == 2);
            set
            {
                if (OfferCleanings == null) OfferCleanings = new List<OfferCleaning>();
                if (OfferCleaning2 != null) OfferCleanings.Remove(OfferCleaning2);
                OfferCleanings.Add(value);
            }
        }

        // Storage
        [Display(Name = "Storage")]
        public bool LagerungActive { get; set; }
        [Display(Name = "Tariff")]
        public int? LagerungPriceRateId { get; set; }
        [StringLength(100)]
        public string LagerungPriceRateDescr { get; set; }
        [Display(Name = "CHF-Approach")]
        public int LagerungCostChangedPrice { get; set; }
        [StringLength(100)]
        [Display(Name = "Volume [m3]")]
        public string LagerungVolume { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string LagerungCostAdditFreeTextPlus1 { get; set; }
        public int LagerungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string LagerungCostAdditFreeTextPlus2 { get; set; }
        public int LagerungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string LagerungCostAdditFreeTextMinus1 { get; set; }
        public int LagerungCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Costs")]
        public string LagerungCostEstimated { get; set; }
        [Display(Name = "Flat_rate")]
        public bool LagerungCostFix { get; set; }
        public int LagerungCostFixPrice { get; set; }

        // transport
        public virtual ICollection<OfferTransport> OfferTransports { get; set; }

        [Display(Name = "transport")]
        public bool TransportActive { get; set; }

        [NotMapped]
        public virtual OfferTransport OfferTransport
        {
            get => OfferTransports?.FirstOrDefault(s => s.SectorId == 1);
            set
            {
                if (OfferTransports == null) OfferTransports = new List<OfferTransport>();
                if (OfferTransport != null) OfferTransports.Remove(OfferTransport);
                OfferTransports.Add(value);
            }
        }

        // Packing_Material
        [Display(Name = "Packing Material")]
        public bool PackMaterialActive { get; set; }
        [StringLength(100)]
        [Display(Name = "Total")]
        public string PackMaterialCostSum { get; set; }
        [Display(Name = "Reduction")]
        public int PackMaterialCostDiscount { get; set; }
        [Display(Name = "Delivery")]
        public bool PackMaterialDeliver { get; set; }
        public int PackMaterialDeliverPrice { get; set; }
        [Display(Name = "Pickup")]
        public bool PackMaterialPickup { get; set; }
        public int PackMaterialPickupPrice { get; set; }

        // Additional properties
        [Display(Name = "VAT Included.")]
        public bool CostInclTax { get; set; }

        [Display(Name = "VAT Excluded.")]
        public bool CostExclTax { get; set; }

        [Display(Name = "VAT Free.")]
        public bool CostFreeTax { get; set; }
        // Comment
        [Display(Name = "Comment")]
        public string Comment { get; set; }

        // Note
        [Display(Name = "Note")]
        public string Note { get; set; }

        // Email / Expression
        [Required]
        [Display(Name = "E-Mail to Customers")]
        public bool EmailToCustomer { get; set; }

        [StringLength(150)]
        [Display(Name = "E-Mail Address")]
        //[EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        [RegularExpression(pattern: @"(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*",
            ErrorMessage = "Please enter a valid e-mail format.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Email Note")]
        public string EmailNote { get; set; }

        [NotMapped]
        [Display(Name = "Edit standard email text")]
        public bool ChangeEmailText { get; set; }

        [NotMapped]
        public EmailTextContainer ChangeEmailTextContainer { get; set; }


        public virtual ICollection<Receipt> Receipts { get; set; }
        
        public virtual ICollection<Offer_DefinedPackMaterial> Offer_DefinedPackMaterials { get; set; }
        public virtual ICollection<OfferPackMaterial> OfferPackMaterials { get; set; }
        public virtual ICollection<Offer_PriceAddit> Offer_PriceAddits { get; set; }
        public virtual ICollection<Offer_PriceAddit_de> Offer_PriceAddits_de { get; set; }
    }
}