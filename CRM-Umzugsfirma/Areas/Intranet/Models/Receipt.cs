﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Receipt
    {
        public enum ReceiptType
        {
            [Display(Name = "Standard: Move / disposal")]
            Move = 1,
            [Display(Name = "cleaning")]
            cleaning = 2
        }

        public enum ReceiptState
        {
            Open = 1,
            Completed = 3
        }

        public Guid ReceiptId { get; set; }

        [Display(Name = "ReceiptNo.")]
        public int ReceiptInnerId { get; set; }

        public int? ReceiptInnerSubId { get; set; }

        [NotMapped]
        public string ReceiptInnerIdExtern => $"{ReceiptInnerId}{(ReceiptInnerSubId.HasValue ? $".{ReceiptInnerSubId.Value}" : string.Empty)}";

        [Required]
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        public int AppointmentId { get; set; }

        [Required]
        [Display(Name = "Status")]
        public ReceiptState ReceiptStateId { get; set; }

        [Required]
        [Display(Name = "Receipt Type")]
        public ReceiptType ReceiptTypeId { get; set; }

        [Display(Name = "Customer")]
        public string CustomerLine1 { get; set; }
        public string CustomerLine2 { get; set; }
        public string CustomerLine3 { get; set; }
        public string CustomerLine4 { get; set; }
        public string CustomerLine5 { get; set; }

        [Display(Name = "Customer name for Signature field")]
        public string NameSignatureContact { get; set; }

        [Display(Name = "From")]
        public string AdrOut1Line1 { get; set; }
        public string AdrOut1Line2 { get; set; }
        public string AdrOut2Line1 { get; set; }
        public string AdrOut2Line2 { get; set; }
        public string AdrOut3Line1 { get; set; }
        public string AdrOut3Line2 { get; set; }

        [Display(Name = "To")]
        public string AdrIn1Line1 { get; set; }
        public string AdrIn1Line2 { get; set; }
        public string AdrIn2Line1 { get; set; }
        public string AdrIn2Line2 { get; set; }
        public string AdrIn3Line1 { get; set; }
        public string AdrIn3Line2 { get; set; }

        [Display(Name = "Order Deadline")]
        public string JobDate { get; set; }
        public DateTime? JobDateTime { get; set; }
        [Display(Name = "Time")]
        public string JobTime { get; set; }

        [Display(Name = "Flat rate")]
        public string CostFix { get; set; }
        [Display(Name = "Cost ceiling")]
        public string CostHigh { get; set; }
        [Display(Name = "Total [CHF]")]
        public string TotalCost { get; set; }
        [Display(Name = "VAT Included.")]
        public bool CostInclTax { get; set; }
        [Display(Name = "VAT Excluded.")]
        public bool CostExclTax { get; set; }
        [Display(Name = "VAT Free.")]
        public bool CostFreeTax { get; set; }

        [Required]
        [Display(Name = "Payment")]
        public PayType PayTypeId { get; set; }
        public bool PayedCash { get; set; }
        public string PayedCashCost { get; set; }
        public bool PayedBill { get; set; }
        public string PayedBillCost { get; set; }

        [Display(Name = "Completed on")]
        public DateTime ClosedDate { get; set; }
        [StringLength(128)]
        public string ClosedUserId { get; set; }

        // Email / Expression
        [Required]
        [Display(Name = "E-Mail to Customers")]
        public bool EmailToCustomer { get; set; }

        [StringLength(150)]
        [Display(Name = "E-Mail Address")]
        [EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Email Note")]
        public string EmailNote { get; set; }

        [NotMapped]
        [Display(Name = "Edit standard email text")]
        public bool ChangeEmailText { get; set; }

        [NotMapped]
        public EmailTextContainer ChangeEmailTextContainer { get; set; }


        // CreateDate / ModifyDate
        [Display(Name = "Receipt created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        [Display(Name = "Date Modified")]
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }

        public virtual ICollection<ReceiptUmzug> ReceiptUmzugs { get; set; }
        public virtual ICollection<ReceiptReinigung> ReceiptReinigungs { get; set; }
    }
}