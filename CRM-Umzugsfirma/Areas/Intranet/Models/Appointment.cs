﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Appointment
    {
        public enum AppointmentType
        {
            Sightseeing = 1,
            Confirmation_of_the_order = 2,
            Delivery = 3
        }

        public int AppointmentId { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public int AccountId { get; set; }

        public AppointmentType AppointmentTypeId { get; set; }

        public int OfferId { get; set; }

        [Display(Name = "Offers No.")]
        public int OfferInnerId { get; set; }

        [NotMapped]
        [Display(Name = "Date")]
        public DateTime? AppointmentDateTime
        {
            get
            {
                switch (AppointmentTypeId)
                {
                    case Appointment.AppointmentType.Sightseeing:
                        return AppointmentView?.ViewDateTime;
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        return 
                            AppointmentService?.UmzugDateTime
                            ?? AppointmentService?.PackDateTime
                            ?? AppointmentService?.PackOutDateTime
                            ?? AppointmentService?.ReinigungDateTime
                            ?? AppointmentService?.Reinigung2DateTime
                            ?? AppointmentService?.EntsorgungDateTime
                            ?? AppointmentService?.TransportDateTime;
                    case Appointment.AppointmentType.Delivery:
                        return AppointmentDelivery?.DeliveryDateTime;
                    default:
                        return null;
                }
            }
        }

        [NotMapped]
        [Display(Name = "Service")]
        public string BookedServices
        {
            get
            {
                string s = string.Empty;
                switch (AppointmentTypeId)
                {
                    case Appointment.AppointmentType.Sightseeing:
                        s = AppointmentView.MeetPlace.ToString();
                        break;
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        s = AppointmentService.BookedServices.Trim();
                        break;
                    case Appointment.AppointmentType.Delivery:
                        s = $"{AppointmentDelivery.DeliveryProduct.ToString()}{(AppointmentDelivery.DeliveryProduct == AppointmentDelivery.DeliveryProductType.Packing_Material ? $" ({AppointmentDelivery.Delivery})" : string.Empty)}";
                        break;
                    default:
                        break;
                }
                return s;
            }
        }
        public string PaymentServices
        {
            get {
                string p = string.Empty;
                p = AppointmentService.PaymentServices;
                return p;
            }
           
        }

        [Display(Name = "Where")]
        [StringLength(500)]
        public string Where { get; set; }

        [Display(Name = "Calendar Tile Addition")]
        public string CalendarTitleAddition { get; set; }
        [Display(Name = "Calendar Note")]
        public string CalendarNote { get; set; }

        [Required]
        [Display(Name = "E-Mail to Customers")]
        public bool EmailToCustomer { get; set; }

        [StringLength(150)]
        [Display(Name = "E-Mail Address")]
        //[EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        [RegularExpression(pattern: @"(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*",
            ErrorMessage = "Please enter a valid e-mail format.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Email Note")]
        public string EmailNote { get; set; }

        [NotMapped]
        [Display(Name = "Edit standard email text")]
        public bool ChangeEmailText { get; set; }

        [NotMapped]
        public EmailTextContainer ChangeEmailTextContainer { get; set; }


        [Display(Name = "Created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }

        public virtual AppointmentView AppointmentView { get; set; }
        public virtual AppointmentService AppointmentService { get; set; }
        public virtual AppointmentDelivery AppointmentDelivery { get; set; }
    }
}