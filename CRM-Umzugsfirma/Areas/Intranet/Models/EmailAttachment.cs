﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class EmailAttachment
    {
        [Key]
        public int EmailAttachmentId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public int SortOrder { get; set; }

        [Required]
        [StringLength(400)]
        public string FileName { get; set; }

        [Required]
        [StringLength(100)]
        public string MimeType { get; set; }

        [Required]
        public bool IsViewAppointmentDefault { get; set; }

        [Required]
        public bool IsServiceAppointmentDefault { get; set; }

        [Required]
        public bool IsOfferDefault { get; set; }

        [Required]
        public bool IsBillDefault { get; set; }

        public Account Account { get; set; }
    }
    public class EmailAttachment_de
    {
        [Key]
        public int EmailAttachmentId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        [Required]
        public int SortOrder { get; set; }

        [Required]
        [StringLength(400)]
        public string FileName { get; set; }

        [Required]
        [StringLength(100)]
        public string MimeType { get; set; }

        [Required]
        public bool IsViewAppointmentDefault { get; set; }

        [Required]
        public bool IsServiceAppointmentDefault { get; set; }

        [Required]
        public bool IsOfferDefault { get; set; }

        [Required]
        public bool IsBillDefault { get; set; }

        public Account Account { get; set; }
    }
}