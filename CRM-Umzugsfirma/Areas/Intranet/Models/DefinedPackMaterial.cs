﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class DefinedPackMaterial
    {
        [Key]
        public int DefinedPackMaterialId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

        public decimal? PriceRent { get; set; }
        public decimal? PriceBuy { get; set; }

        public virtual ICollection<Offer_DefinedPackMaterial> Offer_DefinedPackMaterials { get; set; }
        public virtual ICollection<Bill_DefinedPackMaterial> Bill_DefinedPackMaterials { get; set; }

    }
    public class DefinedPackMaterial_de
    {
        [Key]
        public int DefinedPackMaterialId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string Title { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

        public decimal? PriceRent { get; set; }
        public decimal? PriceBuy { get; set; }
        public virtual ICollection<Offer_DefinedPackMaterial_de> Offer_DefinedPackMaterials_de { get; set; }
        public virtual ICollection<Bill_DefinedPackMaterial_de> Bill_DefinedPackMaterials_de { get; set; }

    }
}