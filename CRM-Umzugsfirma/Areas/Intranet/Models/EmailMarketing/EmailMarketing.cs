﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing
{
    public class EmailMarketing
    {
        public int EmailMarketingId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public Account Account { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Unique Name")]
        public string Accessor { get; set; }

        [StringLength(400)]
        [Display(Name = "Description")]
        public string Description { get; set; }

        public bool CreatedByUser { get; set; }

        [Required]
        [StringLength(300)]
        [Display(Name = "Email-Title")]
        public string EmailSubject { get; set; }

        [Display(Name = "Email-Text")]
        public string EmailContent { get; set; }

        [Display(Name = "Date Created")]
        public DateTime CreateDate { get; set; }

        [Display(Name = "Date Modified")]
        public DateTime? ModifyDate { get; set; }


        public virtual ICollection<EmailMarketingAttachment> EmailMarketingAttachments { get; set; }
        public virtual ICollection<EmailMarketingCustomer> EmailMarketingCustomers { get; set; }
    }
}