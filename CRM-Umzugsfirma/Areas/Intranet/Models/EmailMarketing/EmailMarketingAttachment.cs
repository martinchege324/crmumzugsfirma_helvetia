﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing
{
    public class EmailMarketingAttachment
    {
        public int EmailMarketingAttachmentId { get; set; }

        [Required]
        public int EmailMarketingId { get; set; }
        public EmailMarketing EmailMarketing { get; set; }

        [Required]
        [StringLength(400)]
        public string FileName { get; set; }

        [Required]
        [StringLength(100)]
        public string MimeType { get; set; }

        [Required]
        [StringLength(410)]
        public string FileFullName { get; set; }

        public byte[] FileContent { get; set; }

        [StringLength(300)]
        public string Description { get; set; }
    }
}