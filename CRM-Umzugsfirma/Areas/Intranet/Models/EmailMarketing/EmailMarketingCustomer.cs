﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing
{
    public class EmailMarketingCustomer
    {
        public int EmailMarketingCustomerId { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }

        [Required]
        public int EmailMarketingId { get; set; }
        public EmailMarketing EmailMarketing { get; set; }

        [Display(Name = "Date Created")]
        public DateTime CreateDate { get; set; }

        [StringLength(50)]
        public string AffectedDbTable { get; set; }

        public int? AffectedId { get; set; }

        public int? AffectedInnerId { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Sent to")]
        public string EmailAddress { get; set; }

        [Required]
        [StringLength(300)]
        [Display(Name = "Email-Title")]
        public string EmailSubject { get; set; }

        public string EmailContent { get; set; }

        [StringLength(300)]
        public string EmailAttachmentNameList { get; set; }
    }
}