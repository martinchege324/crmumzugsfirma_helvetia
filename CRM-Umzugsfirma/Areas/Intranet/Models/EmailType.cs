﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class EmailType
    {
        [Key]
        public int EmailTypeId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(400)]
        public string MailSubject { get; set; }

        public Account Account { get; set; }
    }
    public class EmailType_de
    {
        [Key]
        public int EmailTypeId { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(400)]
        public string MailSubject { get; set; }

        public Account Account { get; set; }
    }
}