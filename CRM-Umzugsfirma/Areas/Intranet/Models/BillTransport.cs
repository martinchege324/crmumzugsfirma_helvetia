﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class BillTransport
    {
        public int BillTransportId { get; set; }

        [Required]
        public int BillId { get; set; }
        public virtual Bill Bill { get; set; }

        public int SectorId { get; set; }

        [Display(Name = "Date")]
        public string TransportDate { get; set; }

        [StringLength(300)]
        [Display(Name = "Optional: transport-Type Text (comes in Pdf)")]
        public string TransportTypeFreeText { get; set; }

        [Display(Name = "Package Price-Tariff")]
        public int? TransportCostFixRatePrice { get; set; }

        [StringLength(100)]
        [Display(Name = "Duration")]
        public string TransportDuration { get; set; }
        [Display(Name = "Hourly [CHF]")]
        public int TransportPerPrice { get; set; }
        public decimal TransportCostService { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration.")]
        public string TransportDuration2 { get; set; }
        [Display(Name = "Hourly [CHF]")]
        public int TransportPerPrice2 { get; set; }
        public decimal? TransportCostService2 { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int TransportCostWay { get; set; }
        [Display(Name = "Extra cost")]
        public bool TransportCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string TransportCostAdditFreeTextPlus1 { get; set; }
        public decimal TransportCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus2 { get; set; }
        public decimal TransportCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus3 { get; set; }
        public decimal TransportCostAdditFreeTextPlus3Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus4 { get; set; }
        public decimal TransportCostAdditFreeTextPlus4Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus5 { get; set; }
        public decimal TransportCostAdditFreeTextPlus5Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus6 { get; set; }
        public decimal TransportCostAdditFreeTextPlus6Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextPlus7 { get; set; }
        public decimal TransportCostAdditFreeTextPlus7Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string TransportCostAdditFreeTextMinus1 { get; set; }
        public decimal TransportCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string TransportCostAdditFreeTextMinus2 { get; set; }
        public decimal TransportCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal TransportCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal TransportCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal TransportCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool TransportCostFix { get; set; }
        public decimal TransportCostFixPrice { get; set; }
        public bool TransportCostHighSecurity { get; set; }
        public int TransportCostHighSecurityPrice { get; set; }
        [Display(Name = "Claims payment")]
        public decimal TransportCostDamage { get; set; }
        [Display(Name = "Deposit")]
        public decimal TransportCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal TransportCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal TransportCost { get; set; }

    }
}