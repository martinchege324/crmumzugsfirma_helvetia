﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class ReceiptReinigung
    {
        public Guid ReceiptReinigungId { get; set; }

        public Guid ReceiptId { get; set; }
        public virtual Receipt Receipt { get; set; }

        public string AbgabeDate { get; set; }
        public string AbgabeTime { get; set; }

        [Display(Name = "cleaning Type-Text")]
        public string CleaningTypeText { get; set; }
        [Display(Name = "Additional text (eg room number)")]
        public string Room { get; set; }
        [Display(Name = "Optional: Services (for partial cleaning or construction cleaning services)")]
        public string WishedServices { get; set; }

        [Display(Name = "Tariff Package Price")]
        public string ReinigungCost { get; set; }

        [Display(Name = "Duration [h]")]
        public string HourlyCostDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public string HourlyCostRate { get; set; }
        [Display(Name = "Total [CHF]")]
        public string HourlyCostSum { get; set; }

        [Display(Name = "Additional Text")]
        public string AdditionalText1 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost1 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText2 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost2 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText3 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost3 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText4 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost4 { get; set; }
        [Display(Name = "Additional Text")]
        public string AdditionalText5 { get; set; }
        [Display(Name = "Additional Costs [CHF]")]
        public string AdditionalCost5 { get; set; }
        [Display(Name = "Zuschlag6 Text")]
        public string AdditionalText6 { get; set; }
        [Display(Name = "Zuschlag6 Costs [CHF]")]
        public string AdditionalCost6 { get; set; }

        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText1 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost1 { get; set; }
        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText2 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost2 { get; set; }
        [Display(Name = "Additional SubCost Text")]
        public string AdditionalSubText3 { get; set; }
        [Display(Name = "Additional SubCost Costs [CHF]")]
        public string AdditionalSubCost3 { get; set; }
    }
}