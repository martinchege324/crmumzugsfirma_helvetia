﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using CRM_Umzugsfirma;

using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class Bill
    {
        public enum BillState
        {
            Open = 0,
            Memory = 2,
            Reminder = 3,
            Paid = 9
        }

        public enum PaymentTerm
        {
            In_7_Days = 7,
            In_14_Days = 14,
            In_31_Days = 31,
        }

        public enum SendState
        {
            Expression = 0,
            Email = 1
        }

        [Display(Name = "Bill Id")]
        public int BillId { get; set; }

        [Required]
        [Display(Name = "Invoice No")]
        public int BillInnerId { get; set; }

        public int OfferId { get; set; }

        public Guid ReceiptId { get; set; }

        [Required]
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Display(Name = "Created on")]
        public DateTime CreateDate { get; set; }
        [StringLength(128)]
        public string CreateUserId { get; set; }
        public DateTime ModifyDate { get; set; }
        [StringLength(128)]
        public string ModifyUserId { get; set; }
        [Display(Name = "Due date on")]
        public DateTime ToPayDate { get; set; }
        [Display(Name = "Paid on")]
        public DateTime PayedDate { get; set; }
        [StringLength(128)]
        public string PayedUserId { get; set; }
        [Display(Name = "Reminiscent of")]
        public DateTime ReminderDate { get; set; }
        [StringLength(128)]
        public string ReminderUserId { get; set; }
        [Display(Name = "Reminder on")]
        public DateTime PreCollectionDate { get; set; }
        [StringLength(128)]
        public string PreCollectionUserId { get; set; }

        [Required]
        [Display(Name = "Status")]
        public BillState BillStateId { get; set; }
        [Required]
        [Display(Name = "Payment Deadline")]
        public PaymentTerm PaymentTermId { get; set; }
        [Required]
        [Display(Name = "Sent by")]
        public SendState SendStateId { get; set; }

        // Move
        [Display(Name = "Move")]
        public bool UmzugActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Date")]
        public string UmzugDate { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string UmzugDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int UmzugPerPrice { get; set; }
        public decimal UmzugCostService { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string UmzugDuration2 { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int UmzugPerPrice2 { get; set; }
        public decimal? UmzugCostService2 { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int UmzugCostWay { get; set; }
        [Display(Name = "Extra cost")]
        public bool UmzugCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string UmzugCostAdditFreeTextPlus1 { get; set; }
        public decimal UmzugCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string UmzugCostAdditFreeTextPlus2 { get; set; }
        public decimal UmzugCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string UmzugCostAdditFreeTextMinus1 { get; set; }
        public decimal UmzugCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string UmzugCostAdditFreeTextMinus2 { get; set; }
        public decimal UmzugCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal UmzugCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal UmzugCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal UmzugCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool UmzugCostFix { get; set; }
        public decimal UmzugCostFixPrice { get; set; }
        public bool UmzugCostHighSecurity { get; set; }
        public int UmzugCostHighSecurityPrice { get; set; }
        [Display(Name = "Claims payment")]
        public decimal UmzugCostDamage { get; set; }
        [Display(Name = "Deposit")]
        public decimal UmzugCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal UmzugCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal UmzugCost { get; set; }

        // packing_service
        [Display(Name = "Packing")]
        public bool PackActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Date")]
        public string PackDate { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string PackDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int PackPerPrice { get; set; }
        public decimal PackCostService { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string PackDuration2 { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int PackPerPrice2 { get; set; }
        public decimal? PackCostService2 { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int PackCostWay { get; set; }
        [Display(Name = "Extra_cost")]
        public bool PackCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string PackCostAdditFreeTextPlus1 { get; set; }
        public decimal PackCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string PackCostAdditFreeTextPlus2 { get; set; }
        public decimal PackCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string PackCostAdditFreeTextMinus1 { get; set; }
        public decimal PackCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string PackCostAdditFreeTextMinus2 { get; set; }
        public decimal PackCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal PackCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal PackCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal PackCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool PackCostFix { get; set; }
        public decimal PackCostFixPrice { get; set; }
        public bool PackCostHighSecurity { get; set; }
        public int PackCostHighSecurityPrice { get; set; }
        [Display(Name = "Claims payment")]
        public decimal PackCostDamage { get; set; }
        [Display(Name = "Deposit")]
        public decimal PackCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal PackCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal PackCost { get; set; }

        // unpacking_service
        [Display(Name = "Unpacking")]
        public bool PackOutActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Date")]
        public string PackOutDate { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string PackOutDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int PackOutPerPrice { get; set; }
        public decimal PackOutCostService { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string PackOutDuration2 { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int PackOutPerPrice2 { get; set; }
        public decimal? PackOutCostService2 { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int PackOutCostWay { get; set; }
        [Display(Name = "Extra cost")]
        public bool PackOutCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string PackOutCostAdditFreeTextPlus1 { get; set; }
        public decimal PackOutCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string PackOutCostAdditFreeTextPlus2 { get; set; }
        public decimal PackOutCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string PackOutCostAdditFreeTextMinus1 { get; set; }
        public decimal PackOutCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string PackOutCostAdditFreeTextMinus2 { get; set; }
        public decimal PackOutCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal PackOutCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal PackOutCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal PackOutCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool PackOutCostFix { get; set; }
        public decimal PackOutCostFixPrice { get; set; }
        public bool PackOutCostHighSecurity { get; set; }
        public int PackOutCostHighSecurityPrice { get; set; }
        [Display(Name = "Claims payment")]
        public decimal PackOutCostDamage { get; set; }
        [Display(Name = "Deposit")]
        public decimal PackOutCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal PackOutCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal PackOutCost { get; set; }

        // disposal
        [Display(Name = "Disposal")]
        public bool EntsorgungActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Date")]
        public string EntsorgungDate { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration")]
        public string EntsorgungDuration { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int EntsorgungPersonPerPrice { get; set; }
        public decimal EntsorgungPersonCostService { get; set; }
        [Display(Name = "Directions / Return Trip [CHF]")]
        public int EntsorgungCostWay { get; set; }
        [Display(Name = "Extra cost")]
        public bool EntsorgungCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Volume [m3]")]
        public string EntsorgungVolume { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int EntsorgungPerPrice { get; set; }
        public decimal EntsorgungCostService { get; set; }
        [StringLength(100)]
        [Display(Name = "Disposal Costs Flat rate")]
        public string EntsorgungFixCost { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string EntsorgungCostAdditFreeTextPlus1 { get; set; }
        public decimal EntsorgungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string EntsorgungCostAdditFreeTextPlus2 { get; set; }
        public decimal EntsorgungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string EntsorgungCostAdditFreeTextMinus1 { get; set; }
        public decimal EntsorgungCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string EntsorgungCostAdditFreeTextMinus2 { get; set; }
        public decimal EntsorgungCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal EntsorgungCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal EntsorgungCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal EntsorgungCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool EntsorgungCostFix { get; set; }
        public decimal EntsorgungCostFixPrice { get; set; }
        public bool EntsorgungCostHighSecurity { get; set; }
        public int EntsorgungCostHighSecurityPrice { get; set; }
        [Display(Name = "Deposit")]
        public decimal EntsorgungCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal EntsorgungCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal EntsorgungCost { get; set; }

        // cleaning
        public virtual ICollection<BillCleaning> BillCleanings { get; set; }

        [Display(Name = "Cleaning")]
        public bool ReinigungActive { get; set; }
        [NotMapped]
        public virtual BillCleaning BillCleaning
        {
            get => BillCleanings?.FirstOrDefault(s => s.SectorId == 1);
            set
            {
                if (BillCleanings == null) BillCleanings = new List<BillCleaning>();
                if (BillCleaning != null) BillCleanings.Remove(BillCleaning);
                BillCleanings.Add(value);
            }
        }

        // cleaning 2
        [Display(Name = "Cleaning 2")]
        public bool Reinigung2Active { get; set; }

        [NotMapped]
        public virtual BillCleaning BillCleaning2
        {
            get => BillCleanings?.FirstOrDefault(s => s.SectorId == 2);
            set
            {
                if (BillCleanings == null) BillCleanings = new List<BillCleaning>();
                if (BillCleaning2 != null) BillCleanings.Remove(BillCleaning2);
                BillCleanings.Add(value);
            }
        }

        // Storage
        [Display(Name = "Storage")]
        public bool LagerungActive { get; set; }
        [StringLength(100)]
        [Display(Name = "Volume [m3]")]
        public string LagerungVolume { get; set; }
        [Display(Name = "Approach [CHF]")]
        public int LagerungPerPrice { get; set; }
        public decimal LagerungCostService { get; set; }
        [StringLength(30)]
        [Display(Name = "Period of")]
        public string LagerungDatePeriodFrom { get; set; }
        [StringLength(30)]
        [Display(Name = "to")]
        public string LagerungDatePeriodTo { get; set; }

        //Create a reminder field in the database
        [Display(Name = "Reminder Date")]
        public string LagerungDateReminder { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string LagerungCostAdditFreeTextPlus1 { get; set; }
        public decimal LagerungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string LagerungCostAdditFreeTextPlus2 { get; set; }
        public decimal LagerungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string LagerungCostAdditFreeTextMinus1 { get; set; }
        public decimal LagerungCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        public string LagerungCostAdditFreeTextMinus2 { get; set; }
        public decimal LagerungCostAdditFreeTextMinus2Price { get; set; }
        [Display(Name = "Discount")]
        public decimal LagerungCostDiscount { get; set; }
        [Display(Name = "Concession")]
        public decimal LagerungCostDiscount2 { get; set; }
        [Display(Name = "Subtotal")]
        public decimal LagerungCostSub { get; set; }
        [Display(Name = "Flat rate")]
        public bool LagerungCostFix { get; set; }
        public decimal LagerungCostFixPrice { get; set; }
        [Display(Name = "Deposit")]
        public decimal LagerungCostPartialPayment { get; set; }
        [Display(Name = "Bar Paid")]
        public decimal LagerungCostPartialByCash { get; set; }
        [Display(Name = "Amount")]
        public decimal LagerungCost { get; set; }
        
        public int ReminderPeriod
        {
            get
            {
                DateTime LastReminder = DateTime.Parse(CreateDate.ToShortDateString());
                DateTime CurrentDay = DateTime.Parse(DateTime.Now.ToShortDateString());
                int s = (int) CurrentDay.Subtract(LastReminder).Days;
 
                return s;
               
            }
           
         }
        
        // transport
        public virtual ICollection<BillTransport> BillTransports { get; set; }

        [Display(Name = "Transport")]
        public bool TransportActive { get; set; }

        [NotMapped]
        public virtual BillTransport BillTransport
        {
            get => BillTransports?.FirstOrDefault(s => s.SectorId == 1);
            set
            {
                if (BillTransports == null) BillTransports = new List<BillTransport>();
                if (BillTransport != null) BillTransports.Remove(BillTransport);
                BillTransports.Add(value);
            }
        }

        // Packing Material
        [Display(Name = "Packing Material")]
        public bool PackMaterialActive { get; set; }
        [StringLength(30)]
        [Display(Name = "Date")]
        public string PackMaterialDate { get; set; }
        [Display(Name = "Reduction")]
        public int PackMaterialCostDiscount { get; set; }
        [StringLength(100)]
        [Display(Name = "Further Deductions")]
        public string PackMaterialCostAdditFreeTextMinus1 { get; set; }
        public int PackMaterialCostAdditFreeTextMinus1Price { get; set; }
        [Display(Name = "Delivery")]
        public bool PackMaterialDeliver { get; set; }
        public int PackMaterialDeliverPrice { get; set; }
        [Display(Name = "Pickup")]
        public bool PackMaterialPickup { get; set; }
        public int PackMaterialPickupPrice { get; set; }
        [Display(Name = "Amount")]
        public string PackMaterialCost { get; set; }

        // Cost
        [Display(Name = "Pre-Collection Cost")]
        public int PreCollectionCost { get; set; }
        [StringLength(50)]
        [Display(Name = "Amount Total")]
        public string BillCostString { get; set; }
        public decimal BillCost { get; set; }
        [Display(Name = "VAT Included.")]
        public bool CostInclTax { get; set; }
        [Display(Name = "VAT Excluded.")]
        public bool CostExclTax { get; set; }
        [Display(Name = "VAT Free.")]
        public bool CostFreeTax { get; set; }

        // Email / Expression
        [Required]
        [Display(Name = "E-Mail to Customers")]
        public bool EmailToCustomer { get; set; }

        [StringLength(150)]
        [Display(Name = "E-Mail Address")]
        //[EmailAddress(ErrorMessage = "Please enter a valid e-mail format.")]
        [RegularExpression(pattern: @"(([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)(\s*(;|,)\s*|\s*$))*",
            ErrorMessage = "Please enter a valid e-mail format.")]
        public string EmailAddress { get; set; }

        [Display(Name = "Email Note")]
        public string EmailNote { get; set; }

        [NotMapped]
        [Display(Name = "Edit standard email text")]
        public bool ChangeEmailText { get; set; }

        [NotMapped]
        public EmailTextContainer ChangeEmailTextContainer { get; set; }


        public virtual ICollection<Bill_DefinedPackMaterial> Bill_DefinedPackMaterials { get; set; }
        public virtual ICollection<Bill_DefinedPackMaterial_de> Bill_DefinedPackMaterials_de { get; set; }
        public virtual ICollection<BillPackMaterial> BillPackMaterials { get; set; }
        public virtual ICollection<Bill_PriceAddit> Bill_PriceAddits { get; set; }
        public virtual ICollection<Bill_PriceAddit_de> Bill_PriceAddits_de { get; set; }
    }
}