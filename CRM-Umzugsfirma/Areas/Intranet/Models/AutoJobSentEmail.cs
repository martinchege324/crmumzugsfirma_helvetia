﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AutoJobSentEmail
    {
        public int AutoJobSentEmailId { get; set; }

        [Required]
        public int AutoJobId { get; set; }
        public virtual AutoJob AutoJob { get; set; }

        public AutoJob.AutoJobType Type { get; set; }

        [StringLength(50)]
        public string AffectedDbTable { get; set; }

        public int? AffectedId { get; set; }

        [StringLength(100)]
        public string SentEmailAddress { get; set; }
    }
}