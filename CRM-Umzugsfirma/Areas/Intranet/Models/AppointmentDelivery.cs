﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class AppointmentDelivery
    {
        public enum DeliveryProductType
        {
            Packing_Material = 1,
            Castle_Atelier = 5
        }

        public enum DeliveryType
        {
            Delivery = 1,
            Pickup = 2
        }

        public int AppointmentId { get; set; }
        public virtual Appointment Appointment { get; set; }

        [Display(Name = "Deliverable")]
        public DeliveryProductType DeliveryProduct { get; set; }

        [Display(Name = "Delivery Type")]
        public DeliveryType Delivery { get; set; }

        [StringLength(30)]
        [Display(Name = "Date")]
        public string DeliveryDate { get; set; }
        [StringLength(30)]
        public string DeliveryTimeFrom { get; set; }
        [StringLength(30)]
        public string DeliveryTimeTo { get; set; }
        public DateTime? DeliveryDateTime { get; set; }
        [StringLength(50)]
        public string GoogleEventDeliveryId { get; set; }
    }
}