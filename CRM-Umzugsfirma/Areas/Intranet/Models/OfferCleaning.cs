﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class OfferCleaning
    {
        public int OfferCleaningId { get; set; }

        [Required]
        public int OfferId { get; set; }
        public virtual Offer Offer { get; set; }

        public int SectorId { get; set; }
        [Display(Name = "cleaning Type")]
        public CleaningType ReinigungType { get; set; }
        [StringLength(200)]
        [Display(Name = "Manual Input (cleaning Type)")]
        public string ReinigungTypeFreeText { get; set; }
        [Display(Name = "Tariff (Flat rate)")]
        public int? ReinigungPriceRateId { get; set; }
        [Display(Name = "Tariff Price")]
        public int ReinigungCostChangedPrice { get; set; }
        [Display(Name = "Tariff (Hourly)")]
        public int? ReinigungHourlyPriceRateId { get; set; }
        [Display(Name = "MA")]
        public int ReinigungHourlyCostChangedPers { get; set; }
        [Display(Name = "CHF-Approach")]
        public int ReinigungHourlyCostChangedPrice { get; set; }
        [StringLength(100)]
        [Display(Name = "Duration [h]")]
        public string ReinigungHourlyDuration { get; set; }
        [StringLength(100)]
        public string ReinigungPriceRateDescr { get; set; }
        [StringLength(300)]
        [Display(Name = "Optional: Services (for partial cleaning or construction cleaning services)")]
        public string ReinigungWishedServiceFreeText { get; set; }

        [Display(Name = "Text in Offer")]
        [StringLength(300)]
        public string ReinigungTarifText { get; set; }

        [Display(Name = "cleaning Date")]
        public string ReinigungDate { get; set; }
        public DateTime? ReinigungDateTime { get; set; }
        [Display(Name = "Starting Work")]
        public string ReinigungTime { get; set; }
        [Display(Name = "Deadline")]
        public string ReinigungDateCommit { get; set; }
        [Display(Name = "Delivery Time")]
        public string ReinigungTimeCommit { get; set; }
        [Display(Name = "Fill dowel holes")]
        public Offer.YesNoType AdditServiceDuebellocher { get; set; }
        [Display(Name = "High pressure cleaner")]
        public Offer.YesNoType AdditServiceHochdruckreiniger { get; set; }
        [Display(Name = "Extra cost")]
        public bool ReinigungCostAddit { get; set; }
        [StringLength(100)]
        [Display(Name = "Other Costs")]
        public string ReinigungCostAdditFreeTextPlus1 { get; set; }
        public int ReinigungCostAdditFreeTextPlus1Price { get; set; }
        [StringLength(100)]
        public string ReinigungCostAdditFreeTextPlus2 { get; set; }
        public int ReinigungCostAdditFreeTextPlus2Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Deduction")]
        public string ReinigungCostAdditFreeTextMinus1 { get; set; }
        public int ReinigungCostAdditFreeTextMinus1Price { get; set; }
        [StringLength(100)]
        [Display(Name = "Package Price or Estimated Costs (depending on the chosen tariff)")]
        public string ReinigungCostSum { get; set; }
        [Display(Name = "Flat rate")]
        public bool ReinigungCostFix { get; set; }
        public int ReinigungCostFixPrice { get; set; }

    }
}