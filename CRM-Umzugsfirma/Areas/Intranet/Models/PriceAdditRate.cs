﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.Models
{
    public class PriceAdditRate
    {
        [Key]
        public int PriceAdditRateId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string ServiceType { get; set; }

        [Required]
        [StringLength(100)]
        public string Descr { get; set; }

        public int Price { get; set; }

        [StringLength(50)]
        public string MetaTag { get; set; }

        [StringLength(50)]
        public string CalcType { get; set; }

        [StringLength(50)]
        public string DescrToShow { get; set; }

        public virtual ICollection<Offer_PriceAddit> Offer_PriceAddits { get; set; }
        public virtual ICollection<Bill_PriceAddit> Bill_PriceAddits { get; set; }
    }
    public class PriceAdditRate_de
    {
        [Key]
        public int PriceAdditRateId { get; set; }

        [Required]
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        [Required]
        [StringLength(50)]
        public string ServiceType { get; set; }

        [Required]
        [StringLength(100)]
        public string Descr { get; set; }

        public int Price { get; set; }

        [StringLength(50)]
        public string MetaTag { get; set; }

        [StringLength(50)]
        public string CalcType { get; set; }

        [StringLength(50)]
        public string DescrToShow { get; set; }
        public virtual ICollection<Offer_PriceAddit_de> Offer_PriceAddits_de { get; set; }
        public virtual ICollection<Bill_PriceAddit_de> Bill_PriceAddits_de { get; set; }
    }
}