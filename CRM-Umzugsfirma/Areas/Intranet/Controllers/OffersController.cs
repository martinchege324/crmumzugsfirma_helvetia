﻿using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers.Offers;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class OffersController : Controller
    {
        // Special Route for the whole control
        // GET: Intranet/Offers
        public ActionResult Index(string sortOrder, int? page, int? reset)
        {
            // reset searchmodel
            if ((reset != null) && (reset == 1))
            {
                if (Session["OfferSearchModel"] != null) Session.Remove("OfferSearchModel");
                return RedirectToAction("Index");
            }

            OfferViewModel model = new OfferViewModel() { SearchFilter = new OfferSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["OfferSearchModel"] != null) model.SearchFilter = (OfferSearchModel)Session["OfferSearchModel"];

            return View(getPagedListFromDB(model, sortOrder, page));
        }

        // GET: Intranet/Print
        public ActionResult Print(string sortOrder)
        {
            OfferViewModel model = new OfferViewModel() { SearchFilter = new OfferSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["OfferSearchModel"] != null) model.SearchFilter = (OfferSearchModel)Session["OfferSearchModel"];
            model.PrintVersion = true;

            return PartialView("~/Areas/Intranet/Views/Offers/ResultList.cshtml", getPagedListFromDB(model, sortOrder, 1, 99999));
        }

        // GET: Intranet/Csv
        public ActionResult Csv(string sortOrder)
        {
            OfferViewModel model = new OfferViewModel() { SearchFilter = new OfferSearchModel() };
            if (Session["OfferSearchModel"] != null) model.SearchFilter = (OfferSearchModel)Session["OfferSearchModel"];
            model.PrintVersion = true;

            List<OfferResultModel> offers = null;
            using (var db = new ApplicationDbContext())
            {
                offers = getListFromDB(db, model, sortOrder).ToList();
            }

            return File(mapListToCsv(offers), "text/csv", "Offers.csv");
        }

        // Special Route for the whole control
        // POST: Intranet/Offers 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(OfferViewModel model)
        {
            if (Session["OfferSearchModel"] != null) Session.Remove("OfferSearchModel");
            Session.Add("OfferSearchModel", model.SearchFilter);
            return View(getPagedListFromDB(model, string.Empty, 1));
        }

        // Special Route for the whole control
        // POST: Intranet/Offers/Details/2
        public async Task<ActionResult> Details(int id)
        {
            OfferManager manager = new OfferManager();
            Offer offer = manager.GetOfferById(id, true);
            if (offer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(offer.CreateUserId);
                if (user != null)
                {
                    offer.CreateUserId = user.UserName;
                }
            }

            // translate service names
            string s = string.Empty;
            if (offer.UmzugActive) s += ",Move";
            if (offer.PackActive) s += ",Pack";
            if (offer.PackOutActive) s += ",Unpack";
            if (offer.EntsorgungActive) s += ",Disposal";
            if (offer.ReinigungActive || offer.Reinigung2Active) s += ",cleaning";
            if (offer.TransportActive) s += ",Transport";
            if (offer.LagerungActive) s += ",Storage";
            if (s.Length > 0) s = s.Substring(1);
            ViewBag.BookedServices = s;

            return View(offer);
        }

        #region Helpers

        private OfferViewModel getPagedListFromDB(OfferViewModel model, string sortOrder, int? page, int? pageSize = null)
        {
            // sorting
            ViewBag.SortIdParam = String.IsNullOrEmpty(sortOrder) || sortOrder == "offerid" ? "offerid_desc" : "offerid";
            ViewBag.SortStateIdParam = sortOrder == "stateid" ? "stateid_desc" : "stateid";

            // paging (install in pm: Install-Package PagedList.Mvc)


            using (var db = new ApplicationDbContext())
            {
                IQueryable<OfferResultModel> query = getListFromDB(db, model, sortOrder);

                // paging (install in pm: Install-Package PagedList.Mvc)
                int pageNumber = (page ?? 1);
                IPagedList<OfferResultModel> pagedList = query.ToPagedList(pageNumber, pageSize ?? 10);

                model.ResultList = pagedList;
            }
            return model;
        }

        private IQueryable<OfferResultModel> getListFromDB(ApplicationDbContext db, OfferViewModel model, string sortOrder)
        {
            int accountId = ((UserIdentifier) Session["UserIdentifier"]).AccountId;

            IQueryable<OfferResultModel> query = (from a in db.Offers
                join c in db.Customers on a.CustomerId equals c.CustomerId
                join oc_temp in db.OfferCleanings.Where(s => s.SectorId == 1) on a.OfferId equals oc_temp.OfferId into goc
                from oc in goc.DefaultIfEmpty()
                join oc_temp2 in db.OfferCleanings.Where(s => s.SectorId == 2) on a.OfferId equals oc_temp2.OfferId into goc2
                from oc2 in goc2.DefaultIfEmpty()
                join ot_temp in db.OfferTransports.Where(s => s.SectorId == 1) on a.OfferId equals ot_temp.OfferId into got
                from ot in got.DefaultIfEmpty()
                where a.AccountId == accountId
                select new OfferResultModel()
                {
                    OfferId = a.OfferId,
                    CustomerId = a.CustomerId,
                    AccountId = a.AccountId,
                    OfferStateId = a.OfferStateId,
                    ViewAppointmentStateId = a.ViewAppointmentStateId,
                    OfferInnerId = a.OfferInnerId,
                    ServiceUmzug = a.UmzugActive,
                    ServicePack = a.PackActive,
                    ServicePackOut = a.PackOutActive,
                    ServiceEntsorgung = a.EntsorgungActive,
                    ServiceReinigung = a.ReinigungActive,
                    ServiceReinigung2 = a.Reinigung2Active,
                    ServiceTransport = a.TransportActive,
                    ServiceLagerung = a.LagerungActive,
                    ServicePackMaterial = a.PackMaterialActive,
                    DateUmzugFromString = a.UmzugDate,
                    TimeUmzugFrom = a.UmzugTime,
                    UmzugDateTime = a.UmzugDateTime,
                    DatePackFromString = a.PackDate,
                    TimePackFrom = a.PackTime,
                    PackDateTime = a.PackDateTime,
                    DatePackOutFromString = a.PackOutDate,
                    TimePackOutFrom = a.PackOutTime,
                    PackOutDateTime = a.PackOutDateTime,
                    DateReinigungFromString = oc.ReinigungDate,
                    TimeReinigungFrom = oc.ReinigungTime,
                    ReinigungDateTime = oc.ReinigungDateTime,
                    DateReinigung2FromString = oc2.ReinigungDate,
                    TimeReinigung2From = oc2.ReinigungTime,
                    Reinigung2DateTime = oc2.ReinigungDateTime,
                    DateTransportFromString = ot.TransportDate,
                    TimeTransportFrom = ot.TransportTime,
                    TransportDateTime = ot.TransportDateTime,
                    Where = a.AdrOutStreet + ", " + (!string.IsNullOrEmpty(a.AdrOutCountryCode) ? a.AdrOutCountryCode + "-" : "") + a.AdrOutZip + " " + a.AdrOutCity,
                    CustomerMarketId = c.MarketId,
                    LastName = c.LastName,
                    FirstName = c.FirstName,
                    TitleId = c.TitleId,
                    CompanyName = c.CompanyName,
                    CompanyContactPerson = c.CompanyContactPerson,
                    Phone = c.Phone,
                    Email = c.Email,
                    CreateDate = a.CreateDate,
                    CreateUserName = a.CreateUserId
                });

            // search filter
            if (model.SearchFilter.IsFilled())
            {
                query = mapFilterToQuery(query, model.SearchFilter);
            }

            // order
            switch (sortOrder)
            {
                case "offerid":
                    query = query.OrderBy(s => s.OfferInnerId);
                    break;
                case "offerid_desc":
                    query = query.OrderByDescending(s => s.OfferInnerId);
                    break;
                case "stateid":
                    query = query.OrderBy(s => s.OfferStateId);
                    break;
                case "stateid_desc":
                    query = query.OrderByDescending(s => s.OfferStateId);
                    break;
                default:
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
            }

            return query;
        }

        private IQueryable<OfferResultModel> mapFilterToQuery(IQueryable<OfferResultModel> query, OfferSearchModel filter)
        {
            if (filter.OfferInnerId.HasValue) query = query.Where(q => q.OfferInnerId == filter.OfferInnerId.Value);
            if (filter.OfferStateId > 0) query = query.Where(q => q.OfferStateId == filter.OfferStateId);
            if (filter.ViewAppointmentStateId > 0) query = query.Where(q => q.ViewAppointmentStateId == filter.ViewAppointmentStateId);
            if (filter.ServiceTypeId > 0)
            {
                if (filter.ServiceTypeId == ServiceTypes.Move) query = query.Where(q => q.ServiceUmzug);
                if (filter.ServiceTypeId == ServiceTypes.Packing_Service) query = query.Where(q => q.ServicePack);
                if (filter.ServiceTypeId == ServiceTypes.Unpacking_Service) query = query.Where(q => q.ServicePackOut);
                if (filter.ServiceTypeId == ServiceTypes.Disposal) query = query.Where(q => q.ServiceEntsorgung);
                if (filter.ServiceTypeId == ServiceTypes.cleaning) query = query.Where(q => q.ServiceReinigung || q.ServiceReinigung2);
                if (filter.ServiceTypeId == ServiceTypes.Transport) query = query.Where(q => q.ServiceTransport);
                if (filter.ServiceTypeId == ServiceTypes.Storage) query = query.Where(q => q.ServiceLagerung);
            }
            if (filter.DateServiceFrom.HasValue)
            {
                // filter date only for service, that is checked by user. Disposal and Storage does not have appointments, so take the most meaningful
                switch (filter.ServiceTypeId)
                {
                    case ServiceTypes.Move:
                    case ServiceTypes.Disposal:
                        query = query.Where(q => q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0);
                        break;
                    case ServiceTypes.Packing_Service:
                        query = query.Where(q => (q.PackDateTime != null && q.PackDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                || (q.PackDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0));
                        break;
                    case ServiceTypes.Unpacking_Service:
                        query = query.Where(q => (q.PackOutDateTime != null && q.PackOutDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.PackOutDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0));
                        break;
                    case ServiceTypes.cleaning:
                        query = query.Where(q => (q.ReinigungDateTime != null && q.ReinigungDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.Reinigung2DateTime != null && q.Reinigung2DateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.ReinigungDateTime == null && q.Reinigung2DateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0));
                        break;
                    case ServiceTypes.Transport:
                        query = query.Where(q => (q.TransportDateTime != null && q.TransportDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.TransportDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0));
                        break;
                    default:
                        query = query.Where(q => (q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.PackDateTime != null && q.PackDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.PackOutDateTime != null && q.PackOutDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.ReinigungDateTime != null && q.ReinigungDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.Reinigung2DateTime != null && q.Reinigung2DateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0)
                                                 || (q.TransportDateTime != null && q.TransportDateTime.Value.CompareTo(filter.DateServiceFrom.Value) >= 0));
                        break;
                }
            }
            if (filter.DateServiceTo.HasValue)
            {
                DateTime filterDateServiceTo = filter.DateServiceTo.Value.AddDays(1).AddSeconds(-1);
                // filter date only for service, that is checked by user. Disposal and Storage does not have appointments, so take the most meaningful
                switch (filter.ServiceTypeId)
                {
                    case ServiceTypes.Move:
                    case ServiceTypes.Disposal:
                        query = query.Where(q => q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0);
                        break;
                    case ServiceTypes.Packing_Service:
                        query = query.Where(q => (q.PackDateTime != null && q.PackDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.PackDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0));
                        break;
                    case ServiceTypes.Unpacking_Service:
                        query = query.Where(q => (q.PackOutDateTime != null && q.PackOutDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.PackOutDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0));
                        break;
                    case ServiceTypes.cleaning:
                        query = query.Where(q => (q.ReinigungDateTime != null && q.ReinigungDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.Reinigung2DateTime != null && q.Reinigung2DateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.ReinigungDateTime == null && q.Reinigung2DateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0));
                        break;
                    case ServiceTypes.Transport:
                        query = query.Where(q => (q.TransportDateTime != null && q.TransportDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.TransportDateTime == null && q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0));
                        break;
                    default:
                        query = query.Where(q => (q.UmzugDateTime != null && q.UmzugDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.PackDateTime != null && q.PackDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.PackOutDateTime != null && q.PackOutDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.ReinigungDateTime != null && q.ReinigungDateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.Reinigung2DateTime != null && q.Reinigung2DateTime.Value.CompareTo(filterDateServiceTo) <= 0)
                                                 || (q.TransportDateTime != null && q.TransportDateTime.Value.CompareTo(filterDateServiceTo) <= 0));
                        break;
                }
            }
            if (filter.DateCreateFrom.HasValue)
            {
                query = query.Where(q => q.CreateDate.CompareTo(filter.DateCreateFrom.Value) >= 0);
            }
            if (filter.DateCreateTo.HasValue)
            {
                DateTime filterDateCreateTo = filter.DateCreateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(q => q.CreateDate.CompareTo(filterDateCreateTo) <= 0);
            }
            return query;
        }

        private Stream mapListToCsv(List<OfferResultModel> offers)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(ms, Encoding.UTF8);
            csvWriter.WriteLine("Id;Service;Customer;Anrede;Kontaktperson_NurFirma;Adresse;Telephone;Email;Termin_am;Was standing;Erstellungsdatum");

            offers.ForEach(o => csvWriter.WriteLine(o.OfferInnerId.ToString()
                                              + ";" + o.BookedServices
                                              + ";" + (o.CustomerMarketId == MarketType.Private_person ? o.LastName + " " + o.FirstName : o.CompanyName)
                                              + ";" + (o.TitleId == TitleType.Mrs ? "Mrs" : "Mr")
                                              + ";" + o.CompanyContactPerson
                                              + ";" + o.Where
                                              + ";" + o.Phone
                                              + ";" + o.Email
                                              + ";" + (!string.IsNullOrEmpty(o.DateUmzugFromString) ? o.DateUmzugFromString + " - " + o.TimeUmzugFrom
                                                  : ((!string.IsNullOrEmpty(o.DatePackFromString)) ? o.DatePackFromString + " - " + o.TimePackFrom
                                                      : ((!string.IsNullOrEmpty(o.DatePackOutFromString)) ? o.DatePackOutFromString + " - " + o.TimePackOutFrom
                                                            : ((!string.IsNullOrEmpty(o.DateReinigungFromString)) ? o.DateReinigungFromString + " - " + o.TimeReinigungFrom
                                                                : ((!string.IsNullOrEmpty(o.DateReinigung2FromString)) ? o.DateReinigung2FromString + " - " + o.TimeReinigung2From
                                                                    : (!string.IsNullOrEmpty(o.DateTransportFromString) ? o.DateTransportFromString + " - " + o.TimeTransportFrom : ""))))))
                                              + ";" + o.OfferStateId.ToString()
                                              + ";" + o.CreateDate.ToString()
            ));
            // ADD TRANSPORT ALSO HERE ABOVE

            csvWriter.Flush();
            ms.Position = 0;
            return ms;
        }

        #endregion

    }
}