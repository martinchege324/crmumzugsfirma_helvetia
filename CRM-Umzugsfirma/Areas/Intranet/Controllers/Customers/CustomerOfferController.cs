﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Helpers.CountryCode;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.Managers.Pdf;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Managers.AccountStuff;
using CRM_Umzugsfirma.Managers.PackMaterial;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerOfferController : Controller
    {
        // GET: Intranet/Customer/Details/Offer/1/1
        public ActionResult Index(int customerId, int? id)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            OfferManager manager = new OfferManager();
            return View(manager.GetOffersByCustomer(customerId));
        }

        // GET: Intranet/Customer/Details/Offer/Edit/1/1
        // If Empty creates a new, else edit it
        public ActionResult Edit(int customerId, int? id)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            // create model
            CustomerOfferViewModel model = new CustomerOfferViewModel();
            OfferManager offerManager = new OfferManager();

            List<SelectListItem> contactPersonList = createContactPersonList(customer.AccountId);
            List<SelectListItem> viewAppointmentStateList = createViewAppointmentStateList();
            List<SelectListItem> adrOutBuildingTypeList = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList = createFloorList();
            List<SelectListItem> adrInFloorList = createFloorList();
            List<SelectListItem> adrOutBuildingTypeList2 = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList2 = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList2 = createFloorList();
            List<SelectListItem> adrInFloorList2 = createFloorList();
            List<SelectListItem> adrOutBuildingTypeList3 = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList3 = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList3 = createFloorList();
            List<SelectListItem> adrInFloorList3 = createFloorList();
            var countryCodeManager = new CountryCodeManager();
            List<SelectListItem> adrOutCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);
            List<SelectListItem> adrInCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);
            List<SelectListItem> adrOutCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);
            List<SelectListItem> adrInCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);
            List<SelectListItem> adrOutCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);
            List<SelectListItem> adrInCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(string.Empty);

            List<SelectListItem> umzugInstallFurnitureList = createInstallFurnitureTypeList();
            List<SelectListItem> umzugPriceList = createPriceTarifList(customer.AccountId, "umzug");
            List<SelectListItem> packPriceList = createPriceTarifList(customer.AccountId, "pack");
            List<SelectListItem> packOutPriceList = createPriceTarifList(customer.AccountId, "packout");
            List<SelectListItem> reinigungTypeList = createCleaningTypeList();
            List<SelectListItem> reinigungPriceList = createPriceTarifList(customer.AccountId, "reinigung");
            List<SelectListItem> reinigungHourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly");
            List<SelectListItem> reinigung2TypeList = createCleaningTypeList();
            List<SelectListItem> reinigung2PriceList = createPriceTarifList(customer.AccountId, "reinigung");
            List<SelectListItem> reinigung2HourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly");
            List<SelectListItem> entsorgungPersonPriceList = createPriceTarifList(customer.AccountId, "entsorgungperson");
            List<SelectListItem> entsorgungPriceList = createPriceTarifList(customer.AccountId, "entsorgung");
            List<SelectListItem> transportPriceList = createPriceTarifList(customer.AccountId, "transport");
            List<SelectListItem> lagerungPriceList = createPriceTarifList(customer.AccountId, "lagerung");

            List<SelectListItem> definedPackMaterialList = createDefinedPackMaterialList(customer.AccountId);

            if (id == null)
            {
                // -- Create new
                ViewBag.SubTitle = "Enter new offer";
                ViewBag.SubmitText = "Create";

                model.Offer = new Offer
                {
                    CustomerId = customerId,
                    AccountId = customer.AccountId,
                    EmailAddress = customer.Email,
                    AdrOutStreet = customer.Street,
                    AdrOutZip = customer.Zip,
                    AdrOutCity = customer.City,
                    AdrOutCountryCode = customer.CountryCode,
                    OfferStateId = Offer.OfferState.waiting_for_customer,
                    ViewAppointmentStateId = Offer.ViewAppointmentState.no,
                    AdrOutHasLift = Offer.YesNoType.no,
                    AdrInHasLift = Offer.YesNoType.no,
                    AdrOutHasLift2 = Offer.YesNoType.no,
                    AdrInHasLift2 = Offer.YesNoType.no,
                    AdrOutHasLift3 = Offer.YesNoType.no,
                    AdrInHasLift3 = Offer.YesNoType.no,
                    PaymentId = Offer.PaymentType.Bar,
                    CostInclTax = false,
                    CostExclTax = true,
                    CostFreeTax = false,
                    OfferCleanings = new List<OfferCleaning>()
                    {
                        new OfferCleaning { SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no },
                        new OfferCleaning { SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no }
                    },
                    OfferTransports = new List<OfferTransport>()
                    {
                        new OfferTransport { SectorId = 1 }
                    }
                };

                model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug", null, ",Expenses,");
                model.Offer.UmzugCostAddit = true;
                model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", null, ",Expenses,");
                model.Offer.PackCostAddit = true;
                model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", null, ",Expenses,");
                model.Offer.PackOutCostAddit = true;
                model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung");
                model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2");
                model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung", null);
                model.OfferDefinedPackMaterials = createOfferDefinedPackMaterials();
                model.OfferPackMaterials = createOfferPackMaterials();
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Offer";
                ViewBag.SubmitText = "Save";

                // security check if its own offer
                model.Offer = offerManager.GetOfferById(id.Value);
                if ((model.Offer == null) || (model.Offer.CustomerId != customerId))
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                // set relational depencies to default value for rendering UI
                if (model.Offer.OfferCleanings == null)
                {
                    model.Offer.OfferCleanings = new List<OfferCleaning>()
                    {
                        new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no },
                        new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no }
                    };
                }
                else
                {
                    if (model.Offer.OfferCleaning == null) model.Offer.OfferCleanings.Add(new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no });
                    if (model.Offer.OfferCleaning2 == null) model.Offer.OfferCleanings.Add(new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee, AdditServiceDuebellocher = Offer.YesNoType.no, AdditServiceHochdruckreiniger = Offer.YesNoType.no });
                }
                if (model.Offer.OfferTransports == null)
                {
                    model.Offer.OfferTransports = new List<OfferTransport> { new OfferTransport { OfferId = model.Offer.OfferId, SectorId = 1 } };
                }
                else
                {
                    if (model.Offer.OfferTransport == null) model.Offer.OfferTransports.Add(new OfferTransport { OfferId = model.Offer.OfferId, SectorId = 1 });
                }

                // preset data
                if (string.IsNullOrEmpty(model.Offer.EmailAddress)) model.Offer.EmailAddress = customer.Email;
                if (model.Offer.AdrOutHasLift2 == 0) model.Offer.AdrOutHasLift2 = Offer.YesNoType.no;
                if (model.Offer.AdrInHasLift2 == 0) model.Offer.AdrInHasLift2 = Offer.YesNoType.no;
                if (model.Offer.AdrOutHasLift3 == 0) model.Offer.AdrOutHasLift3 = Offer.YesNoType.no;
                if (model.Offer.AdrInHasLift3 == 0) model.Offer.AdrInHasLift3 = Offer.YesNoType.no;

                // preselect selectlistitems
                if (model.Offer.AccountEmployeeId > 0)
                {
                    selectDefinedSelectItem(ref contactPersonList, model.Offer.AccountEmployeeId.ToString());
                }
                if (model.Offer.ViewAppointmentStateId != Offer.ViewAppointmentState.no)
                {
                    selectDefinedSelectItem(ref viewAppointmentStateList, ((int)model.Offer.ViewAppointmentStateId).ToString());
                }
                if (model.Offer.AdrOutBuildingType != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutBuildingTypeList, ((int)model.Offer.AdrOutBuildingType).ToString());
                }
                if (model.Offer.AdrInBuildingType != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInBuildingTypeList, ((int)model.Offer.AdrInBuildingType).ToString());
                }
                if (model.Offer.AdrOutFloorType != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutFloorList, model.Offer.AdrOutFloorType.ToString());
                }
                if (model.Offer.AdrInFloorType != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInFloorList, model.Offer.AdrInFloorType.ToString());
                }
                if (model.Offer.AdrOutBuildingType2 != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutBuildingTypeList2, ((int)model.Offer.AdrOutBuildingType2).ToString());
                }
                if (model.Offer.AdrInBuildingType2 != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInBuildingTypeList2, ((int)model.Offer.AdrInBuildingType2).ToString());
                }
                if (model.Offer.AdrOutFloorType2 != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutFloorList2, model.Offer.AdrOutFloorType2.ToString());
                }
                if (model.Offer.AdrInFloorType2 != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInFloorList2, model.Offer.AdrInFloorType2.ToString());
                }
                if (model.Offer.AdrOutBuildingType3 != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutBuildingTypeList3, ((int)model.Offer.AdrOutBuildingType3).ToString());
                }
                if (model.Offer.AdrInBuildingType3 != Offer.BuildingType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInBuildingTypeList3, ((int)model.Offer.AdrInBuildingType3).ToString());
                }
                if (model.Offer.AdrOutFloorType3 != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrOutFloorList3, model.Offer.AdrOutFloorType3.ToString());
                }
                if (model.Offer.AdrInFloorType3 != Offer.FloorType.Please_choose)
                {
                    selectDefinedSelectItem(ref adrInFloorList3, model.Offer.AdrInFloorType3.ToString());
                }
                if (model.Offer.UmzugInstallFurnitureType != Offer.InstallFurnitureType.Please_choose)
                {
                    selectDefinedSelectItem(ref umzugInstallFurnitureList, ((int)model.Offer.UmzugInstallFurnitureType).ToString());
                }
                adrOutCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode);
                adrInCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode);
                adrOutCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode2);
                adrInCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode2);
                adrOutCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode3);
                adrInCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode3);

                // preselect umzugprice
                if (model.Offer.UmzugActive)
                {
                    if (model.Offer.UmzugPriceRateId > 0)
                    {
                        model.HelperUmzugPriceRateId = selectPriceListSelectItem(ref umzugPriceList, model.Offer.UmzugPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperUmzugPriceRateId))
                        {
                            string[] priceInfo = model.HelperUmzugPriceRateId.Split('|');
                            model.Offer.UmzugCostChangedPers = model.Offer.UmzugCostChangedPers > 0 ? model.Offer.UmzugCostChangedPers : int.Parse(priceInfo[2]);
                            model.Offer.UmzugCostChangedVeh = model.Offer.UmzugCostChangedVeh > 0 ? model.Offer.UmzugCostChangedVeh : int.Parse(priceInfo[3]);
                            model.Offer.UmzugCostChangedPrice = model.Offer.UmzugCostChangedPrice > 0 ? model.Offer.UmzugCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect umzugadditioncosts
                model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "umzug"));
                // preselect packprice
                if (model.Offer.PackActive)
                {
                    if (model.Offer.PackPriceRateId > 0)
                    {
                        model.HelperPackPriceRateId = selectPriceListSelectItem(ref packPriceList, model.Offer.PackPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperPackPriceRateId))
                        {
                            string[] priceInfo = model.HelperPackPriceRateId.Split('|');
                            model.Offer.PackCostChangedPers = model.Offer.PackCostChangedPers > 0 ? model.Offer.PackCostChangedPers : int.Parse(priceInfo[2]);
                            model.Offer.PackCostChangedPrice = model.Offer.PackCostChangedPrice > 0 ? model.Offer.PackCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect packadditioncosts
                model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "pack"));
                // preselect packoutprice
                if (model.Offer.PackOutActive)
                {
                    if (model.Offer.PackOutPriceRateId > 0)
                    {
                        model.HelperPackOutPriceRateId = selectPriceListSelectItem(ref packOutPriceList, model.Offer.PackOutPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperPackOutPriceRateId))
                        {
                            string[] priceInfo = model.HelperPackOutPriceRateId.Split('|');
                            model.Offer.PackOutCostChangedPers = model.Offer.PackOutCostChangedPers > 0 ? model.Offer.PackOutCostChangedPers : int.Parse(priceInfo[2]);
                            model.Offer.PackOutCostChangedPrice = model.Offer.PackOutCostChangedPrice > 0 ? model.Offer.PackOutCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect packoutadditioncosts
                model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "packout"));
                // preselect reinigungprice
                if (model.Offer.ReinigungActive)
                {
                    if (model.Offer.OfferCleaning.ReinigungType != CleaningType.Please_choose)
                    {
                        selectDefinedSelectItem(ref reinigungTypeList, ((int)model.Offer.OfferCleaning.ReinigungType).ToString());
                    }
                    if (model.Offer.OfferCleaning.ReinigungPriceRateId > 0)
                    {
                        model.HelperReinigungPriceRateId = selectPriceListSelectItem(ref reinigungPriceList, model.Offer.OfferCleaning.ReinigungPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperReinigungPriceRateId))
                        {
                            string[] priceInfo = model.HelperReinigungPriceRateId.Split('|');
                            model.Offer.OfferCleaning.ReinigungCostChangedPrice = model.Offer.OfferCleaning.ReinigungCostChangedPrice > 0 ? model.Offer.OfferCleaning.ReinigungCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                    if (model.Offer.OfferCleaning.ReinigungHourlyPriceRateId > 0)
                    {
                        model.HelperReinigungHourlyPriceRateId = selectPriceListSelectItem(ref reinigungHourlyPriceList, model.Offer.OfferCleaning.ReinigungHourlyPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperReinigungHourlyPriceRateId))
                        {
                            string[] priceInfo = model.HelperReinigungHourlyPriceRateId.Split('|');
                            model.Offer.OfferCleaning.ReinigungHourlyCostChangedPrice = model.Offer.OfferCleaning.ReinigungHourlyCostChangedPrice > 0 ? model.Offer.OfferCleaning.ReinigungHourlyCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect reinigungadditioncosts
                model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "reinigung"));
                // preselect reinigung2price
                if (model.Offer.Reinigung2Active)
                {
                    if (model.Offer.OfferCleaning2.ReinigungType != CleaningType.Please_choose)
                    {
                        selectDefinedSelectItem(ref reinigung2TypeList, ((int)model.Offer.OfferCleaning2.ReinigungType).ToString());
                    }
                    if (model.Offer.OfferCleaning2.ReinigungPriceRateId > 0)
                    {
                        model.HelperReinigung2PriceRateId = selectPriceListSelectItem(ref reinigung2PriceList, model.Offer.OfferCleaning2.ReinigungPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperReinigung2PriceRateId))
                        {
                            string[] priceInfo = model.HelperReinigung2PriceRateId.Split('|');
                            model.Offer.OfferCleaning2.ReinigungCostChangedPrice = model.Offer.OfferCleaning2.ReinigungCostChangedPrice > 0 ? model.Offer.OfferCleaning2.ReinigungCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                    if (model.Offer.OfferCleaning2.ReinigungHourlyPriceRateId > 0)
                    {
                        model.HelperReinigung2HourlyPriceRateId = selectPriceListSelectItem(ref reinigung2HourlyPriceList, model.Offer.OfferCleaning2.ReinigungHourlyPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperReinigung2HourlyPriceRateId))
                        {
                            string[] priceInfo = model.HelperReinigung2HourlyPriceRateId.Split('|');
                            model.Offer.OfferCleaning2.ReinigungHourlyCostChangedPrice = model.Offer.OfferCleaning2.ReinigungHourlyCostChangedPrice > 0 ? model.Offer.OfferCleaning2.ReinigungHourlyCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect reinigung2additioncosts
                model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "reinigung2"));

                // preselect entsorgungprice
                if (model.Offer.EntsorgungActive)
                {
                    if (model.Offer.EntsorgungPersonPriceRateId > 0)
                    {
                        model.HelperEntsorgungPersonPriceRateId = selectPriceListSelectItem(ref entsorgungPersonPriceList, model.Offer.EntsorgungPersonPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperEntsorgungPersonPriceRateId))
                        {
                            string[] priceInfo = model.HelperEntsorgungPersonPriceRateId.Split('|');
                            model.Offer.EntsorgungPersonCostChangedPers = model.Offer.EntsorgungPersonCostChangedPers > 0 ? model.Offer.EntsorgungPersonCostChangedPers : int.Parse(priceInfo[2]);
                            model.Offer.EntsorgungPersonCostChangedVeh = model.Offer.EntsorgungPersonCostChangedVeh > 0 ? model.Offer.EntsorgungPersonCostChangedVeh : int.Parse(priceInfo[3]);
                            model.Offer.EntsorgungPersonCostChangedPrice = model.Offer.EntsorgungPersonCostChangedPrice > 0 ? model.Offer.EntsorgungPersonCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                    if (model.Offer.EntsorgungPriceRateId > 0)
                    {
                        model.HelperEntsorgungPriceRateId = selectPriceListSelectItem(ref entsorgungPriceList, model.Offer.EntsorgungPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperEntsorgungPriceRateId))
                        {
                            string[] priceInfo = model.HelperEntsorgungPriceRateId.Split('|');
                            model.Offer.EntsorgungCostChangedPrice = model.Offer.EntsorgungCostChangedPrice > 0 ? model.Offer.EntsorgungCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect entsorgungadditioncosts
                model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "entsorgung"));
                // preselect transportprice
                if (model.Offer.TransportActive)
                {
                    if (model.Offer.OfferTransport.TransportPriceRateId > 0)
                    {
                        model.HelperTransportPriceRateId = selectPriceListSelectItem(ref transportPriceList, model.Offer.OfferTransport.TransportPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperTransportPriceRateId))
                        {
                            string[] priceInfo = model.HelperTransportPriceRateId.Split('|');
                            model.Offer.OfferTransport.TransportCostChangedPers = model.Offer.OfferTransport.TransportCostChangedPers > 0 ? model.Offer.OfferTransport.TransportCostChangedPers : int.Parse(priceInfo[2]);
                            model.Offer.OfferTransport.TransportCostChangedVeh = model.Offer.OfferTransport.TransportCostChangedVeh > 0 ? model.Offer.OfferTransport.TransportCostChangedVeh : int.Parse(priceInfo[3]);
                            model.Offer.OfferTransport.TransportCostChangedPrice = model.Offer.OfferTransport.TransportCostChangedPrice > 0 ? model.Offer.OfferTransport.TransportCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect lagerungprice
                if (model.Offer.LagerungActive)
                {
                    if (model.Offer.LagerungPriceRateId > 0)
                    {
                        model.HelperLagerungPriceRateId = selectPriceListSelectItem(ref lagerungPriceList, model.Offer.LagerungPriceRateId.ToString());
                        if (!string.IsNullOrEmpty(model.HelperLagerungPriceRateId))
                        {
                            string[] priceInfo = model.HelperLagerungPriceRateId.Split('|');
                            model.Offer.LagerungCostChangedPrice = model.Offer.LagerungCostChangedPrice > 0 ? model.Offer.LagerungCostChangedPrice : int.Parse(priceInfo[1]);
                        }
                    }
                }
                // preselect / render definedpackmaterials (dropdown)
                model.OfferDefinedPackMaterials = createOfferDefinedPackMaterials(offerManager.GetDefinedPackMaterials(model.Offer.OfferId));
                // preselect / render packmaterials (textfield)
                model.OfferPackMaterials = createOfferPackMaterials(offerManager.GetPackMaterials(model.Offer.OfferId));

                // set print crypted id
                ViewBag.PrintId = offerManager.GetCryptedOfferCode(model.Offer);
            }

            // selectlistitems
            ViewBag.ContactPersonList = contactPersonList;
            ViewBag.ViewAppointmentStateList = viewAppointmentStateList;
            ViewBag.AdrOutBuildingList = adrOutBuildingTypeList;
            ViewBag.AdrInBuildingList = adrInBuildingTypeList;
            ViewBag.AdrOutFloorList = adrOutFloorList;
            ViewBag.AdrInFloorList = adrInFloorList;
            ViewBag.AdrOutBuildingList2 = adrOutBuildingTypeList2;
            ViewBag.AdrInBuildingList2 = adrInBuildingTypeList2;
            ViewBag.AdrOutFloorList2 = adrOutFloorList2;
            ViewBag.AdrInFloorList2 = adrInFloorList2;
            ViewBag.AdrOutBuildingList3 = adrOutBuildingTypeList3;
            ViewBag.AdrInBuildingList3 = adrInBuildingTypeList3;
            ViewBag.AdrOutFloorList3 = adrOutFloorList3;
            ViewBag.AdrInFloorList3 = adrInFloorList3;
            ViewBag.AdrOutCountryCodeList = adrOutCountryCodeList;
            ViewBag.AdrInCountryCodeList = adrInCountryCodeList;
            ViewBag.AdrOutCountryCodeList2 = adrOutCountryCodeList2;
            ViewBag.AdrInCountryCodeList2 = adrInCountryCodeList2;
            ViewBag.AdrOutCountryCodeList3 = adrOutCountryCodeList3;
            ViewBag.AdrInCountryCodeList3 = adrInCountryCodeList3;
            ViewBag.UmzugInstallFurnitureList = umzugInstallFurnitureList;
            ViewBag.UmzugPriceList = umzugPriceList;
            ViewBag.PackPriceList = packPriceList;
            ViewBag.PackOutPriceList = packOutPriceList;
            ViewBag.ReinigungTypeList = reinigungTypeList;
            ViewBag.ReinigungPriceList = reinigungPriceList;
            ViewBag.ReinigungHourlyPriceList = reinigungHourlyPriceList;
            ViewBag.Reinigung2TypeList = reinigung2TypeList;
            ViewBag.Reinigung2PriceList = reinigung2PriceList;
            ViewBag.Reinigung2HourlyPriceList = reinigung2HourlyPriceList;
            ViewBag.EntsorgungPersonPriceList = entsorgungPersonPriceList;
            ViewBag.EntsorgungPriceList = entsorgungPriceList;
            ViewBag.TransportPriceList = transportPriceList;
            ViewBag.LagerungPriceList = lagerungPriceList;
            ViewBag.DefinedPackMaterialList = definedPackMaterialList;

            // Default: SendEmailToCustomer to false on every start (also for edit)
            model.Offer.EmailToCustomer = false;

            // Default: Set packmaterial default prices
            if (model.Offer.PackMaterialDeliverPrice == 0 && model.Offer.PackMaterialPickupPrice == 0)
            {
                model.Offer.PackMaterialDeliverPrice = 60;
                model.Offer.PackMaterialPickupPrice = 0;
            }
            if (model.Offer.EntsorgungFixCost == null) model.Offer.EntsorgungFixCost = "160";

            // load email attachments
            EmailManager emailManager = new EmailManager();
            List<EmailAttachment> emailAttachments = emailManager.GetEmailAttachmentsByAccountId(customer.AccountId);
            if (emailAttachments.Count > 0)
            {
                model.EmailAttachments = new List<CheckListViewModel>();
                emailAttachments.ForEach(e => model.EmailAttachments.Add(new CheckListViewModel() { Id = e.Name.ToLower(), Name = e.Name, Selected = e.IsOfferDefault }));
            }

            return View(model);
        }

        // POST: Intranet/Customer/Details/Offer/Edit/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerOfferViewModel model, int customerId)
        {
            // Custom validators
            #region CustomValidator

            if (model.Offer.UmzugActive)
            {
                if (string.IsNullOrEmpty(model.HelperUmzugPriceRateId))
                {
                    ModelState.AddModelError("HelperUmzugPriceRateId", "Please select a tariff.");
                }
                if (string.IsNullOrEmpty(model.Offer.UmzugDuration))
                {
                    ModelState.AddModelError("Offer.UmzugDuration", "Please enter duration.");
                }
            }
            if (model.Offer.PackActive)
            {
                if (string.IsNullOrEmpty(model.HelperPackPriceRateId))
                {
                    ModelState.AddModelError("HelperPackPriceRateId", "Please select a tariff.");
                }
                if (string.IsNullOrEmpty(model.Offer.PackDuration))
                {
                    ModelState.AddModelError("Offer.PackDuration", "Please enter duration.");
                }
            }
            if (model.Offer.PackOutActive)
            {
                if (string.IsNullOrEmpty(model.HelperPackOutPriceRateId))
                {
                    ModelState.AddModelError("HelperPackOutPriceRateId", "Please select a tariff.");
                }
                if (string.IsNullOrEmpty(model.Offer.PackOutDuration))
                {
                    ModelState.AddModelError("Offer.PackOutDuration", "Please enter duration.");
                }
            }
            if (model.Offer.ReinigungActive)
            {
                if (model.Offer.OfferCleaning.ReinigungType == CleaningType.Please_choose && string.IsNullOrEmpty(model.Offer.OfferCleaning.ReinigungTypeFreeText))
                {
                    ModelState.AddModelError("Offer.OfferCleaning.ReinigungTypeFreeText", "Please specify a text for the cleaning type.");
                }
                if (string.IsNullOrEmpty(model.HelperReinigungPriceRateId) && string.IsNullOrEmpty(model.HelperReinigungHourlyPriceRateId))
                {
                    ModelState.AddModelError("HelperReinigungPriceRateId", "Please select a tariff.");
                    ModelState.AddModelError("HelperReinigungHourlyPriceRateId", "Please select a tariff.");
                }
                if (!string.IsNullOrEmpty(model.HelperReinigungPriceRateId))
                {
                    int reinigungCostSumTemp = 0;
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning.ReinigungCostSum) || (!int.TryParse(model.Offer.OfferCleaning.ReinigungCostSum, out reinigungCostSumTemp)) || (reinigungCostSumTemp <= 0))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning.ReinigungCostSum", "Please enter a valid package price");
                    }
                }
                else if (!string.IsNullOrEmpty(model.HelperReinigungHourlyPriceRateId))
                {
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning.ReinigungHourlyDuration))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning.ReinigungHourlyDuration", "Please enter duration");
                    }
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning.ReinigungCostSum))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning.ReinigungCostSum", "Please specify the cost");
                    }
                }
            }
            if (model.Offer.Reinigung2Active)
            {
                if (model.Offer.OfferCleaning2.ReinigungType == CleaningType.Please_choose && string.IsNullOrEmpty(model.Offer.OfferCleaning2.ReinigungTypeFreeText))
                {
                    ModelState.AddModelError("Offer.OfferCleaning2.ReinigungTypeFreeText", "Please specify a text for the cleaning type.");
                }
                if (string.IsNullOrEmpty(model.HelperReinigung2PriceRateId) && string.IsNullOrEmpty(model.HelperReinigung2HourlyPriceRateId))
                {
                    ModelState.AddModelError("HelperReinigung2PriceRateId", "Please select a tariff.");
                    ModelState.AddModelError("HelperReinigung2HourlyPriceRateId", "Please select a tariff.");
                }
                if (!string.IsNullOrEmpty(model.HelperReinigung2PriceRateId))
                {
                    int reinigungCostSumTemp = 0;
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning2.ReinigungCostSum) || (!int.TryParse(model.Offer.OfferCleaning2.ReinigungCostSum, out reinigungCostSumTemp)) || (reinigungCostSumTemp <= 0))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning2.ReinigungCostSum", "Please enter a valid package price");
                    }
                }
                else if (!string.IsNullOrEmpty(model.HelperReinigung2HourlyPriceRateId))
                {
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning2.ReinigungHourlyDuration))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning2.ReinigungHourlyDuration", "Please enter duration");
                    }
                    if (string.IsNullOrEmpty(model.Offer.OfferCleaning2.ReinigungCostSum))
                    {
                        ModelState.AddModelError("Offer.OfferCleaning2.ReinigungCostSum", "Please specify the cost");
                    }
                }
            }
            if (model.Offer.EntsorgungActive)
            {
                if (string.IsNullOrEmpty(model.HelperEntsorgungPriceRateId) && string.IsNullOrEmpty(model.HelperEntsorgungPersonPriceRateId))
                {
                    ModelState.AddModelError("HelperEntsorgungPriceRateId", "Please select a tariff.");
                }
            }
            if (model.Offer.TransportActive)
            {
                if (string.IsNullOrEmpty(model.HelperTransportPriceRateId)
                    && (!model.Offer.OfferTransport.TransportCostFixRatePrice.HasValue || model.Offer.OfferTransport.TransportCostFixRatePrice <= 0))
                {
                    ModelState.AddModelError("Offer.OfferCleaning.TransportCostFixRatePrice", "Please enter a Package Price Tariff. OR ..");
                    ModelState.AddModelError("HelperTransportPriceRateId", ".. OR. Please select an hourly tariff.");
                }
            }
            if (model.Offer.LagerungActive)
            {
                if (string.IsNullOrEmpty(model.HelperLagerungPriceRateId))
                {
                    ModelState.AddModelError("HelperLagerungPriceRateId", "Please select a tariff.");
                }
            }
            if (model.Offer.EmailToCustomer)
            {
                if (string.IsNullOrEmpty(model.Offer.EmailAddress))
                {
                    ModelState.AddModelError("Offer.EmailAddress", "Please specify an e-mail address.");
                }
            }
            #endregion


            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (ModelState.IsValid)
            {
                OfferManager offerManager = new OfferManager();
                if (!model.Offer.AdrOut2) model.Offer.AdrOut3 = false;
                if (!model.Offer.AdrIn2) model.Offer.AdrIn3 = false;

                model = prepareSaveMainData(model);
                if (model.Offer.UmzugActive)
                {
                    model = prepareSaveUmzugService(model, customer.AccountId);
                }
                if (model.Offer.PackActive)
                {
                    model = prepareSavePackService(model, customer.AccountId);
                }
                if (model.Offer.PackOutActive)
                {
                    model = prepareSavePackOutService(model, customer.AccountId);
                }
                if (model.Offer.ReinigungActive)
                {
                    model = prepareSaveReinigungService(model, customer.AccountId, 1);
                }
                if (model.Offer.Reinigung2Active)
                {
                    model = prepareSaveReinigungService(model, customer.AccountId, 2);
                }
                if (model.Offer.EntsorgungActive)
                {
                    model = prepareSaveEntsorgungService(model, customer.AccountId);
                }
                if (model.Offer.TransportActive)
                {
                    model = prepareSaveTransportService(model, customer.AccountId);
                }
                if (model.Offer.LagerungActive)
                {
                    model = prepareSaveLagerungService(model, customer.AccountId);
                }
                if (model.Offer.PackMaterialActive)
                {
                    model = prepareSavePackMaterialService(model, customer.AccountId);
                }

                // email: unselect if no email-address is set
                if (model.Offer.EmailToCustomer && string.IsNullOrEmpty(model.Offer.EmailAddress))
                {
                    model.Offer.EmailToCustomer = false;
                }
                if (!model.Offer.EmailToCustomer && !string.IsNullOrEmpty(model.Offer.EmailAddress))
                {
                    model.Offer.EmailAddress = null;
                }

                // save to DB
                int offerId = 0;
                if (model.Offer.OfferId > 0)
                {
                    offerId = offerManager.EditOffer(model.Offer);
                    // only on edit offer, on creating it is done in create method
                    if (model.Offer.ViewAppointmentStateId == Offer.ViewAppointmentState.made && !customer.ContactAlreadyHave)
                    {
                        customer.ContactAlreadyHave = true;
                        customerManager.EditCustomer(customer);
                    }
                }
                else
                {
                    offerId = offerManager.CreateOffer(model.Offer, customer);
                }
                offerManager.SetSelectedAdditionalCosts(offerId, model);
                if (model.Offer.PackMaterialActive)
                {
                    offerManager.SetDefinedPackMaterials(offerId, model.OfferDefinedPackMaterials);
                    offerManager.SetPackMaterials(offerId, model.OfferPackMaterials);
                }

                if (model.Offer.EmailToCustomer)
                {
                    SendOfferEmail(model, customer, model.EmailAttachments);
                }

                return RedirectToAction("Index", new { customerId = customerId });
            }
            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            if (model.Offer.OfferId <= 0)
            {
                // -- Create new
                ViewBag.SubTitle = "Enter new offer";
                ViewBag.SubmitText = "Create";
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Offer";
                ViewBag.SubmitText = "Save";
            }

            ViewBag.ContactPersonList = createContactPersonList(customer.AccountId, model.Offer.AccountEmployeeId.HasValue ? model.Offer.AccountEmployeeId.ToString() : string.Empty);
            ViewBag.ViewAppointmentStateList = createViewAppointmentStateList(((int)model.Offer.ViewAppointmentStateId).ToString());
            ViewBag.AdrOutBuildingList = createBuildingTypeList(((int)model.Offer.AdrOutBuildingType).ToString());
            ViewBag.AdrInBuildingList = createBuildingTypeList(((int)model.Offer.AdrInBuildingType).ToString());
            ViewBag.AdrOutFloorList = createFloorList(((int)model.Offer.AdrOutFloorType).ToString());
            ViewBag.AdrInFloorList = createFloorList(((int)model.Offer.AdrInFloorType).ToString());
            ViewBag.AdrOutBuildingList2 = createBuildingTypeList(((int)model.Offer.AdrOutBuildingType2).ToString());
            ViewBag.AdrInBuildingList2 = createBuildingTypeList(((int)model.Offer.AdrInBuildingType2).ToString());
            ViewBag.AdrOutFloorList2 = createFloorList(((int)model.Offer.AdrOutFloorType2).ToString());
            ViewBag.AdrInFloorList2 = createFloorList(((int)model.Offer.AdrInFloorType2).ToString());
            ViewBag.AdrOutBuildingList3 = createBuildingTypeList(((int)model.Offer.AdrOutBuildingType3).ToString());
            ViewBag.AdrInBuildingList3 = createBuildingTypeList(((int)model.Offer.AdrInBuildingType3).ToString());
            ViewBag.AdrOutFloorList3 = createFloorList(((int)model.Offer.AdrOutFloorType3).ToString());
            ViewBag.AdrInFloorList3 = createFloorList(((int)model.Offer.AdrInFloorType3).ToString());
            var countryCodeManager = new CountryCodeManager();
            ViewBag.AdrOutCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode);
            ViewBag.AdrInCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode);
            ViewBag.AdrOutCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode2);
            ViewBag.AdrInCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode2);
            ViewBag.AdrOutCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode3);
            ViewBag.AdrInCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode3);
            ViewBag.UmzugInstallFurnitureList = createInstallFurnitureTypeList(((int)model.Offer.UmzugInstallFurnitureType).ToString());
            ViewBag.UmzugPriceList = createPriceTarifList(customer.AccountId, "umzug", model.HelperUmzugPriceRateId);
            ViewBag.PackPriceList = createPriceTarifList(customer.AccountId, "pack", model.HelperPackPriceRateId);
            ViewBag.PackOutPriceList = createPriceTarifList(customer.AccountId, "packout", model.HelperPackOutPriceRateId);
            ViewBag.ReinigungTypeList = createCleaningTypeList(((int)model.Offer.OfferCleaning.ReinigungType).ToString());
            ViewBag.ReinigungPriceList = createPriceTarifList(customer.AccountId, "reinigung", model.HelperReinigungPriceRateId);
            ViewBag.ReinigungHourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly", model.HelperReinigungHourlyPriceRateId);
            ViewBag.Reinigung2TypeList = createCleaningTypeList(((int)model.Offer.OfferCleaning2.ReinigungType).ToString());
            ViewBag.Reinigung2PriceList = createPriceTarifList(customer.AccountId, "reinigung", model.HelperReinigung2PriceRateId);
            ViewBag.Reinigung2HourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly", model.HelperReinigung2HourlyPriceRateId);
            ViewBag.EntsorgungPersonPriceList = createPriceTarifList(customer.AccountId, "entsorgungperson", model.HelperEntsorgungPersonPriceRateId);
            ViewBag.EntsorgungPriceList = createPriceTarifList(customer.AccountId, "entsorgung", model.HelperEntsorgungPriceRateId);
            ViewBag.TransportPriceList = createPriceTarifList(customer.AccountId, "transport", model.HelperTransportPriceRateId);
            ViewBag.LagerungPriceList = createPriceTarifList(customer.AccountId, "lagerung", model.HelperLagerungPriceRateId);
            ViewBag.DefinedPackMaterialList = createDefinedPackMaterialList(customer.AccountId, "");
            // set print crypted id
            ViewBag.PrintId = (new OfferManager()).GetCryptedOfferCode(model.Offer);

            return View(model);
        }

        // GET: Intranet/Customer/Details/Offer/Details/1/1
        public async Task<ActionResult> Details(int customerId, int id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id <= 0))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own offer
            CustomerOfferViewModel model = new CustomerOfferViewModel();
            OfferManager offerManager = new OfferManager();
            model.Offer = offerManager.GetOfferById(id);
            if ((model.Offer == null) || (model.Offer.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);
            model.IsEditable = false;
            // set print crypted id
            ViewBag.PrintId = offerManager.GetCryptedOfferCode(model.Offer);

            // -- Edit
            ViewBag.SubTitle = "View offer";

            // set relational depencies to default value for rendering UI
            if (model.Offer.OfferCleanings == null)
            {
                model.Offer.OfferCleanings = new List<OfferCleaning>()
                {
                    new OfferCleaning {OfferId = model.Offer.OfferId, SectorId = 1},
                    new OfferCleaning {OfferId = model.Offer.OfferId, SectorId = 2}
                };
            }
            else
            {
                if (model.Offer.OfferCleaning == null) model.Offer.OfferCleanings.Add(new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 1 });
                if (model.Offer.OfferCleaning2 == null) model.Offer.OfferCleanings.Add(new OfferCleaning { OfferId = model.Offer.OfferId, SectorId = 2 });
            }
            if (model.Offer.OfferTransports == null)
            {
                model.Offer.OfferTransports = new List<OfferTransport> { new OfferTransport { OfferId = model.Offer.OfferId, SectorId = 1 } };
            }
            else
            {
                if (model.Offer.OfferTransport == null) model.Offer.OfferTransports.Add(new OfferTransport { OfferId = model.Offer.OfferId, SectorId = 1 });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(model.Offer.CreateUserId);
                if (user != null)
                {
                    model.Offer.CreateUserId = user.UserName;
                }
            }

            // dropdownlist
            List<SelectListItem> contactPersonList = createContactPersonList(customer.AccountId);
            List<SelectListItem> viewAppointmentStateList = createViewAppointmentStateList();
            List<SelectListItem> adrOutBuildingTypeList = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList = createFloorList();
            List<SelectListItem> adrInFloorList = createFloorList();
            List<SelectListItem> adrOutBuildingTypeList2 = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList2 = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList2 = createFloorList();
            List<SelectListItem> adrInFloorList2 = createFloorList();
            List<SelectListItem> adrOutBuildingTypeList3 = createBuildingTypeList();
            List<SelectListItem> adrInBuildingTypeList3 = createBuildingTypeList();
            List<SelectListItem> adrOutFloorList3 = createFloorList();
            List<SelectListItem> adrInFloorList3 = createFloorList();
            var countryCodeManager = new CountryCodeManager();
            ViewBag.AdrOutCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode);
            ViewBag.AdrInCountryCodeList = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode);
            ViewBag.AdrOutCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode2);
            ViewBag.AdrInCountryCodeList2 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode2);
            ViewBag.AdrOutCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrOutCountryCode3);
            ViewBag.AdrInCountryCodeList3 = countryCodeManager.GetCountryCodeSelectListItems(model.Offer.AdrInCountryCode3);
            List<SelectListItem> umzugInstallFurnitureTypeList = createInstallFurnitureTypeList();
            List<SelectListItem> umzugPriceList = createPriceTarifList(customer.AccountId, "umzug");
            List<SelectListItem> packPriceList = createPriceTarifList(customer.AccountId, "pack");
            List<SelectListItem> packOutPriceList = createPriceTarifList(customer.AccountId, "packout");
            List<SelectListItem> reinigungTypeList = createCleaningTypeList();
            List<SelectListItem> reinigungPriceList = createPriceTarifList(customer.AccountId, "reinigung");
            List<SelectListItem> reinigungHourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly");
            List<SelectListItem> reinigung2TypeList = createCleaningTypeList();
            List<SelectListItem> reinigung2PriceList = createPriceTarifList(customer.AccountId, "reinigung");
            List<SelectListItem> reinigung2HourlyPriceList = createPriceTarifList(customer.AccountId, "reinigunghourly");
            List<SelectListItem> entsorgungPersonPriceList = createPriceTarifList(customer.AccountId, "entsorgungperson");
            List<SelectListItem> entsorgungPriceList = createPriceTarifList(customer.AccountId, "entsorgung");
            List<SelectListItem> transportPriceList = createPriceTarifList(customer.AccountId, "transport");
            List<SelectListItem> lagerungPriceList = createPriceTarifList(customer.AccountId, "lagerung");
            List<SelectListItem> definedPackMaterialList = createDefinedPackMaterialList(customer.AccountId);
            // preselect selectlistitems
            if (model.Offer.AccountEmployeeId > 0)
            {
                selectDefinedSelectItem(ref contactPersonList, model.Offer.AccountEmployeeId.ToString());
            }
            if (model.Offer.ViewAppointmentStateId != Offer.ViewAppointmentState.no)
            {
                selectDefinedSelectItem(ref viewAppointmentStateList, ((int)model.Offer.ViewAppointmentStateId).ToString());
            }
            if (model.Offer.AdrOutBuildingType != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutBuildingTypeList, ((int)model.Offer.AdrOutBuildingType).ToString());
            }
            if (model.Offer.AdrInBuildingType != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInBuildingTypeList, ((int)model.Offer.AdrInBuildingType).ToString());
            }
            if (model.Offer.AdrOutFloorType != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutFloorList, model.Offer.AdrOutFloorType.ToString());
            }
            if (model.Offer.AdrInFloorType != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInFloorList, model.Offer.AdrInFloorType.ToString());
            }
            if (model.Offer.AdrOutBuildingType2 != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutBuildingTypeList2, ((int)model.Offer.AdrOutBuildingType2).ToString());
            }
            if (model.Offer.AdrInBuildingType2 != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInBuildingTypeList2, ((int)model.Offer.AdrInBuildingType2).ToString());
            }
            if (model.Offer.AdrOutFloorType2 != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutFloorList2, model.Offer.AdrOutFloorType2.ToString());
            }
            if (model.Offer.AdrInFloorType2 != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInFloorList2, model.Offer.AdrInFloorType2.ToString());
            }
            if (model.Offer.AdrOutBuildingType3 != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutBuildingTypeList3, ((int)model.Offer.AdrOutBuildingType3).ToString());
            }
            if (model.Offer.AdrInBuildingType3 != Offer.BuildingType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInBuildingTypeList3, ((int)model.Offer.AdrInBuildingType3).ToString());
            }
            if (model.Offer.AdrOutFloorType3 != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrOutFloorList3, model.Offer.AdrOutFloorType3.ToString());
            }
            if (model.Offer.AdrInFloorType3 != Offer.FloorType.Please_choose)
            {
                selectDefinedSelectItem(ref adrInFloorList3, model.Offer.AdrInFloorType3.ToString());
            }
            if (model.Offer.UmzugInstallFurnitureType != Offer.InstallFurnitureType.Please_choose)
            {
                selectDefinedSelectItem(ref umzugInstallFurnitureTypeList, ((int)model.Offer.UmzugInstallFurnitureType).ToString());
            }
            if (model.Offer.OfferCleaning != null && model.Offer.OfferCleaning.ReinigungType != CleaningType.Please_choose)
            {
                selectDefinedSelectItem(ref reinigungTypeList, model.Offer.OfferCleaning.ReinigungType.ToString());
            }
            if (model.Offer.OfferCleaning2 != null && model.Offer.OfferCleaning2.ReinigungType != CleaningType.Please_choose)
            {
                selectDefinedSelectItem(ref reinigung2TypeList, model.Offer.OfferCleaning2.ReinigungType.ToString());
            }
            selectPriceListSelectItem(ref umzugPriceList, model.Offer.UmzugPriceRateId.ToString());
            model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "umzug"));
            selectPriceListSelectItem(ref packPriceList, model.Offer.PackPriceRateId.ToString());
            model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "pack"));
            selectPriceListSelectItem(ref packOutPriceList, model.Offer.PackOutPriceRateId.ToString());
            model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "packout"));
            selectPriceListSelectItem(ref reinigungPriceList, model.Offer.OfferCleaning.ReinigungPriceRateId.ToString());
            model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "reinigung"));
            selectPriceListSelectItem(ref reinigungHourlyPriceList, model.Offer.OfferCleaning.ReinigungHourlyPriceRateId.ToString());
            selectPriceListSelectItem(ref reinigung2PriceList, model.Offer.OfferCleaning2.ReinigungPriceRateId.ToString());
            model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "reinigung2"));
            selectPriceListSelectItem(ref reinigung2HourlyPriceList, model.Offer.OfferCleaning2.ReinigungHourlyPriceRateId.ToString());
            selectPriceListSelectItem(ref entsorgungPersonPriceList, model.Offer.EntsorgungPersonPriceRateId.ToString());
            model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung", offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "entsorgung"));
            selectPriceListSelectItem(ref entsorgungPriceList, model.Offer.EntsorgungPriceRateId.ToString());
            selectPriceListSelectItem(ref transportPriceList, model.Offer.OfferTransport.TransportPriceRateId.ToString());
            selectPriceListSelectItem(ref lagerungPriceList, model.Offer.LagerungPriceRateId.ToString());
            model.OfferDefinedPackMaterials = createOfferDefinedPackMaterials(offerManager.GetDefinedPackMaterials(model.Offer.OfferId));
            model.OfferPackMaterials = createOfferPackMaterials(offerManager.GetPackMaterials(model.Offer.OfferId));

            ViewBag.ContactPersonList = contactPersonList;
            ViewBag.ViewAppointmentStateList = viewAppointmentStateList;
            ViewBag.AdrOutBuildingList = adrOutBuildingTypeList;
            ViewBag.AdrInBuildingList = adrInBuildingTypeList;
            ViewBag.AdrOutFloorList = adrOutFloorList;
            ViewBag.AdrInFloorList = adrInFloorList;
            ViewBag.AdrOutBuildingList2 = adrOutBuildingTypeList2;
            ViewBag.AdrInBuildingList2 = adrInBuildingTypeList2;
            ViewBag.AdrOutFloorList2 = adrOutFloorList2;
            ViewBag.AdrInFloorList2 = adrInFloorList2;
            ViewBag.AdrOutBuildingList3 = adrOutBuildingTypeList3;
            ViewBag.AdrInBuildingList3 = adrInBuildingTypeList3;
            ViewBag.AdrOutFloorList3 = adrOutFloorList3;
            ViewBag.AdrInFloorList3 = adrInFloorList3;
            ViewBag.UmzugInstallFurnitureList = umzugInstallFurnitureTypeList;
            ViewBag.UmzugPriceList = umzugPriceList;
            ViewBag.PackPriceList = packPriceList;
            ViewBag.PackOutPriceList = packOutPriceList;
            ViewBag.ReinigungTypeList = reinigungTypeList;
            ViewBag.ReinigungPriceList = reinigungPriceList;
            ViewBag.ReinigungHourlyPriceList = reinigungHourlyPriceList;
            ViewBag.Reinigung2TypeList = reinigung2TypeList;
            ViewBag.Reinigung2PriceList = reinigung2PriceList;
            ViewBag.Reinigung2HourlyPriceList = reinigung2HourlyPriceList;
            ViewBag.EntsorgungPersonPriceList = entsorgungPersonPriceList;
            ViewBag.EntsorgungPriceList = entsorgungPriceList;
            ViewBag.TransportPriceList = transportPriceList;
            ViewBag.LagerungPriceList = lagerungPriceList;
            ViewBag.DefinedPackMaterialList = definedPackMaterialList;

            return View("Edit", model);
        }

        // delete. check if googlecalendar exists and delete it also
        // POST: Intranet/Customer/Details/Offer/Delete/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int customerId, int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            OfferManager manager = new OfferManager();
            Offer offer = manager.GetOfferById(id);

            if ((offer == null) || (offer.CustomerId != customerId) || (offer.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // get all dependent appointments. Remove google Calendar, and appointment itself
            AppointmentManager appointmentManager = new AppointmentManager();
            var appointmentList = await appointmentManager.GetAppointmentsByCustomerOfferId(customerId, offer.OfferInnerId);
            foreach (Appointment appointment in appointmentList)
            {
                // delete calendar event
                appointmentManager.CleanGoogleCalendar(appointment);
                appointmentManager.DeleteAppointment(appointment.AppointmentId);
            }

            manager.DeleteOffer(id);
            return RedirectToAction("Index", new { customerId = customerId });
        }

        // state change
        public ActionResult StateAbgesagt(int customerId, int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            OfferManager manager = new OfferManager();
            Offer offer = manager.GetOfferById(id);

            if ((offer == null) || (offer.CustomerId != customerId) || (offer.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // change state to abgesagt
            manager.UpdateOfferState(offer, Offer.OfferState.canceled);

            return RedirectToAction("Index", new { customerId = customerId });
        }

        // state change
        public ActionResult StateWartet(int customerId, int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            OfferManager manager = new OfferManager();
            Offer offer = manager.GetOfferById(id);

            if ((offer == null) || (offer.CustomerId != customerId) || (offer.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // change state to abgesagt
            manager.UpdateOfferState(offer, Offer.OfferState.waiting_for_customer);

            return RedirectToAction("Index", new { customerId = customerId });
        }

        public JsonResult EmailText(int customerId, Offer.ViewAppointmentState viewAppointmentState)
        {
            return Json(getEmailTextContainer(customerId, viewAppointmentState), JsonRequestBehavior.AllowGet);
        }

        private EmailTextContainer getEmailTextContainer(int customerId, Offer.ViewAppointmentState viewAppointmentState)
        {
            var emailTextContainer = new EmailTextContainer();
            var customer = (new CustomerManager()).GetCustomerById(customerId, true);
            var account = (new AccountManager()).GetAccountById(customer.AccountId);
            var customerManager = new CustomerManager();

            if (account.InnerAccountId <= 0) return null;

            // personal email subject-title
            var emailType = (new EmailManager()).GetEmailTypeByAccountId(customer.AccountId, "Offer");
            if (emailType != null && !string.IsNullOrEmpty(emailType.MailSubject)) emailTextContainer.Subject = emailType.MailSubject;
            else emailTextContainer.Subject = string.Format("Offer - {0}", account.AccountName);

            // body
            var offerMailModel = new OfferMail
            {
                ViewAppointmentStateId = viewAppointmentState,
                MainMailData = new MainMailData()
                {
                    InnerAccountId = account.InnerAccountId,
                    AccountName = account.AccountName,
                    AccountStreet = account.Street,
                    AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                    AccountPhone = account.Phone,
                    AccountFax = account.Fax,
                    AccountMail = account.Email,
                    AccountWebAddress = account.WebPage,
                    AccountContactName = account.ContactName,
                    PathAccount = string.Format("~/Content/accountdata/{0}/mail", account.InnerAccountId),
                    PathFooterLogo = "",
                    CustomerSalutation = customerManager.GetSalutation(customer),
                }
            };

            emailTextContainer.Body = this.RenderPartialViewToString(string.Format("{0}/text/Offer.cshtml", offerMailModel.MainMailData.PathAccount), offerMailModel);

            return emailTextContainer;
        }

        private bool SendOfferEmail(CustomerOfferViewModel model, Customer customer, List<CheckListViewModel> emailAttachments)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(customer.AccountId);

            if (account.InnerAccountId > 0)
            {
                // send email
                EmailManager emailManager = new EmailManager();

                // email attachments
                List<Attachment> attachments = new List<Attachment>();

                // render pdf
                PdfManager pdfManager = new PdfManager();
                var pdfResult = pdfManager.GetForOfferAsUrl(model.Offer, this.ControllerContext.RequestContext);
                byte[] pdfResultByte = pdfResult.BuildPdf(this.ControllerContext);
                if (pdfResultByte.Length > 0)
                {
                    MemoryStream ms = new MemoryStream(pdfResultByte);
                    attachments.Add(new Attachment(ms, string.Format("Offer-{0}.pdf", model.Offer.OfferInnerId), "application/pdf"));
                }

                if (emailAttachments != null)
                {
                    IEnumerable<string> tempSelectAttachments = emailAttachments.Where(b => b.Selected).Select(s => s.Name);
                    List<Attachment> tempAttachmentList = emailManager.GetAttachmentofModel(tempSelectAttachments, customer.AccountId, account.InnerAccountId);
                    if (tempAttachmentList != null && tempAttachmentList.Count > 0) attachments.AddRange(tempAttachmentList);
                }

                // email subject and bodytext: load from model if user has changed the text, else initialize it
                string subject = null;
                string body = null;
                // if user has changed text
                if (model.Offer.ChangeEmailText)
                {
                    if (!string.IsNullOrWhiteSpace(model.Offer.ChangeEmailTextContainer?.Subject))
                    {
                        subject = model.Offer.ChangeEmailTextContainer.Subject;
                    }
                    if (!string.IsNullOrWhiteSpace(model.Offer.ChangeEmailTextContainer?.Body))
                    {
                        body = model.Offer.ChangeEmailTextContainer.Body;
                    }
                }
                // else initialize new
                if (string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(body))
                {
                    var emailTextContainer = getEmailTextContainer(customer.CustomerId, model.Offer.ViewAppointmentStateId);
                    subject = subject ?? emailTextContainer.Subject;
                    body = body ?? emailTextContainer.Body;
                }

                #region Special-Key replacments
                // REPLACE special-Keys
                body = emailManager.ReplacementsForOfferMail(body, model.Offer, this.ControllerContext.RequestContext);
                #endregion

                EmailSenderFactory emailSenderFactory = new EmailSenderFactory();
                EmailSender emailSender = emailSenderFactory.CreateEmailSender(account.AccountId.ToString());

                // send email (multiple receiver possible)
                bool isSuccessful = true;
                foreach (var receiverEmail in model.Offer.EmailAddress.Replace(',', ';').Split(';'))
                {
                    // && isSuccessful: if there is an error on multiple receiver-version, then it is false. so let variable always false, that at the end false will be returned
                    isSuccessful = emailSender.SendEmailFromSales(receiverEmail, subject, body, attachments) && isSuccessful;
                }

                return isSuccessful;
            }
            return false;
        }

        private List<SelectListItem> createContactPersonList(int accountId, string selectValue = null)
        {
            AccountEmployeeManager manager = new AccountEmployeeManager();
            return manager.GetAllCustomerConsultantsAsSelectListItem(accountId, true, selectValue);
        }

        private List<SelectListItem> createViewAppointmentStateList(string selectValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var lEnum = Enum.GetValues(typeof(Offer.ViewAppointmentState));
            foreach (Offer.ViewAppointmentState bt in lEnum)
            {
                list.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = bt.ToString().Replace('_', ' '), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return list;
        }

        private List<SelectListItem> createBuildingTypeList(string selectValue = null)
        {
            List<SelectListItem> buildingList = new List<SelectListItem>();
            var buildingEnum = Enum.GetValues(typeof(Offer.BuildingType));
            foreach (Offer.BuildingType bt in buildingEnum)
            {
                buildingList.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = bt.ToString().Replace("_slash_", " / ").Replace('_', ' '), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return buildingList;
        }

        private List<SelectListItem> createFloorList(string selectValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var lEnum = Enum.GetValues(typeof(Offer.FloorType));
            foreach (Offer.FloorType bt in lEnum)
            {
                list.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = bt.ToString().Replace("emp_", "").Replace("_plus_", "+").Replace('_', ' '), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return list;
        }

        private List<SelectListItem> createInstallFurnitureTypeList(string selectValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var lEnum = Enum.GetValues(typeof(Offer.InstallFurnitureType));
            foreach (Offer.InstallFurnitureType bt in lEnum)
            {
                list.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = bt.ToString().Replace('_', ' '), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return list;
        }

        private List<SelectListItem> createPriceTarifList(int accountId, string serviceArea, string selectValue = null)
        {
            List<SelectListItem> tarifList = new List<SelectListItem>();
            tarifList.Add(new SelectListItem() { Value = "", Text = "Please choose" });

            PriceManager manager = new PriceManager();
            List<  PriceRate> priceRates = manager.GetPriceRatesByServiceType(accountId, serviceArea);
            if (priceRates != null)
            {
                // get pricerateid from '1|200|3|1'
                if (selectValue != null && selectValue.Contains('|')) selectValue = selectValue.Split('|')[0];

                foreach (  PriceRate pr in priceRates)
                {
                    tarifList.Add(new SelectListItem() { Value = string.Format("{0}|{1}|{2}|{3}|{4}", pr.PriceRateId, pr.CountPrice, pr.CountFirst, pr.CountSecond, pr.CountPriceRangeMax), Text = pr.Descr, Selected = ((selectValue != null && pr.PriceRateId.ToString().Equals(selectValue)) ? true : false) });
                }
            }
            return tarifList;
        }

        private List<PriceAdditCostsViewModel> createPriceAdditTarifList(int accountId, string serviceArea, List<OfferPriceAdditCosts> selectedAdditCosts = null, string initialDefaultSelected = null)
        {
            List<PriceAdditCostsViewModel> list = new List<PriceAdditCostsViewModel>();
            PriceManager priceManager = new PriceManager();
            List<PriceAdditRate> priceAdditRates = priceManager.GetPriceAdditRatesByServiceType(accountId, serviceArea);
            if (priceAdditRates != null)
            {
                OfferPriceAdditCosts offerPriceAddit = null;
                foreach (PriceAdditRate pr in priceAdditRates)
                {
                    if (selectedAdditCosts != null)
                    {
                        offerPriceAddit = selectedAdditCosts.Find(s => s.PriceAdditRateId == pr.PriceAdditRateId);
                    }
                    list.Add(new PriceAdditCostsViewModel()
                    {
                        Id = pr.PriceAdditRateId,
                        Descr = pr.Descr,
                        DescrToShow = pr.DescrToShow,
                        Price = offerPriceAddit != null ? offerPriceAddit.OverwrittenPrice : pr.Price,
                        MetaTag = pr.MetaTag,
                        PricePerPiece = pr.Price,
                        Selected = (offerPriceAddit != null || (!string.IsNullOrEmpty(initialDefaultSelected) && initialDefaultSelected.Contains(pr.DescrToShow))) ? true : false
                    });
                }
            }

            return list;
        }

        private List<SelectListItem> createCleaningTypeList(string selectValue = null)
        {
            List<SelectListItem> cleaningTypeList = new List<SelectListItem>();
            var cleaningEnum = Enum.GetValues(typeof(CleaningType));
            foreach (var bt in cleaningEnum)
            {
                cleaningTypeList.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = Helpers.Helpers.ReplaceEnumExtensionString(bt.ToString()), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return cleaningTypeList;
        }

        private List<SelectListItem> createDefinedPackMaterialList(int accountId, string selectValue = null)
        {
            DefinedPackMaterialManager manager = new DefinedPackMaterialManager();
            return manager.GetDefinedPackMaterialAsSelectListItems(accountId, true, selectValue);
        }

        private List<Offer_DefinedPackMaterial> createOfferDefinedPackMaterials(List<Offer_DefinedPackMaterial> selectedDefinedPackMaterials = null)
        {
            // In UI selected by dropdown
            List<Offer_DefinedPackMaterial> list = new List<Offer_DefinedPackMaterial>();
            int selectedCount = 0;

            if (selectedDefinedPackMaterials != null && selectedDefinedPackMaterials.Count > 0)
            {
                selectedDefinedPackMaterials.ForEach(x => list.Add(x));
                selectedCount = selectedDefinedPackMaterials.Count;
            }

            for (int i = selectedCount; i < 10; i++)
            {
                list.Add(new Offer_DefinedPackMaterial() { RentBuy = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private List<OfferPackMaterial> createOfferPackMaterials(List<OfferPackMaterial> selectedPackMaterials = null)
        {
            // In UI selected by textfield
            List<OfferPackMaterial> list = new List<OfferPackMaterial>();
            int selectedCount = 0;

            if (selectedPackMaterials != null && selectedPackMaterials.Count > 0)
            {
                selectedPackMaterials.ForEach(x => list.Add(x));
                selectedCount = selectedPackMaterials.Count;
            }

            for (int i = selectedCount; i < 10; i++)
            {
                list.Add(new OfferPackMaterial() { MieteKauf = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private void selectDefinedSelectItem(ref List<SelectListItem> list, string value)
        {
            SelectListItem item = list.SingleOrDefault(a => a.Value == value);
            if (item != null) item.Selected = true;
        }

        private string selectPriceListSelectItem(ref List<SelectListItem> list, string value)
        {
            SelectListItem item = list.SingleOrDefault(a => a.Value.StartsWith(string.Format("{0}|", value)));
            if (item != null)
            {
                item.Selected = true;
                return item.Value;
            }
            return null;
        }

        private CustomerOfferViewModel prepareSaveMainData(CustomerOfferViewModel model)
        {
            // reset CountryCodes for addresses, that are not set
            if (model.Offer.AdrInZip <= 0) model.Offer.AdrInCountryCode = null;
            if (!model.Offer.AdrOut2) model.Offer.AdrOutCountryCode2 = null;
            if (!model.Offer.AdrIn2) model.Offer.AdrInCountryCode2 = null;
            if (!model.Offer.AdrOut3) model.Offer.AdrOutCountryCode3 = null;
            if (!model.Offer.AdrIn3) model.Offer.AdrInCountryCode3 = null;

            return model;
        }

        private CustomerOfferViewModel prepareSaveUmzugService(CustomerOfferViewModel model, int accountId)
        {
            // correct datetime
            if (!string.IsNullOrEmpty(model.Offer.UmzugDate))
            {
                if (DateTime.TryParse(model.Offer.UmzugDate, out var umzugdatetime))
                {
                    model.Offer.UmzugDateTime = umzugdatetime;
                }
            }
            if (!string.IsNullOrEmpty(model.Offer.UmzugInDate))
            {
                if (DateTime.TryParse(model.Offer.UmzugInDate, out var umzugindatetime))
                {
                    model.Offer.UmzugInDateTime = umzugindatetime;
                }
            }
            model.Offer.UmzugTime = Helpers.Helpers.CorrectTimeFormat(model.Offer.UmzugTime);
            // read pricerateid from helper property '1|200|3|1'
            string[] helperPriceRate = model.HelperUmzugPriceRateId.Split('|');
            int priceRateId = 0;
            if (int.TryParse(helperPriceRate[0], out priceRateId))
            {
                // set pricerateid and description
                model.Offer.UmzugPriceRateId = priceRateId;
                PriceManager priceManager = new PriceManager();
                  PriceRate pr = priceManager.GetPriceRateById(accountId, "umzug", model.Offer.UmzugPriceRateId.Value);
                if (pr.CountFirst == model.Offer.UmzugCostChangedPers && pr.CountSecond == model.Offer.UmzugCostChangedVeh && pr.CountPrice == model.Offer.UmzugCostChangedPrice)
                {
                    model.Offer.UmzugPriceRateDescr = pr.Descr;
                }
                else
                {
                    model.Offer.UmzugPriceRateDescr = string.Format("{0} {1} {2}",
                        pr.DescrOnChangeFirst.Replace("[a]", model.Offer.UmzugCostChangedPers.ToString()).TrimStart(),
                        pr.DescrOnChangeSecond.Replace("[a]", model.Offer.UmzugCostChangedVeh.ToString()).TrimStart(),
                        pr.DescrOnChangePrice.Replace("[a]", model.Offer.UmzugCostChangedPrice.ToString()).TrimStart());
                }
                // set Anhänger (Trailer) on price rate description
                if (model.Offer.UmzugCostChangedTrailer > 0)
                {
                    model.Offer.UmzugPriceRateDescr = model.Offer.UmzugPriceRateDescr.Replace("Lieferwagen", $"Lieferwagen und {model.Offer.UmzugCostChangedTrailer} Anhänger");
                }

                // set endprice
                int priceMax = model.Offer.UmzugCostChangedPrice;
                if (priceMax != 0)
                {
                    int priceMin = priceMax;
                    int additionCosts = model.Offer.UmzugCostWay;
                    if (model.Offer.UmzugCostAddit)
                    {
                        model.UmzugPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                        additionCosts += model.Offer.UmzugCostAdditFreeTextPlus1Price + model.Offer.UmzugCostAdditFreeTextPlus2Price;
                    }
                    int subtractCosts = model.Offer.UmzugCostDiscount + model.Offer.UmzugCostDiscount2 + model.Offer.UmzugCostAdditFreeTextMinus1Price;

                    string[] durationTime = model.Offer.UmzugDuration.Split('-');
                    if (durationTime.Length > 1)
                    {
                        priceMin = priceMin * int.Parse(durationTime[0]) + additionCosts;
                        priceMax = priceMax * int.Parse(durationTime[1]) + additionCosts;
                        // for email, check for contains - and split and set in emailmodel (if somewhere happen an error, it should
                        model.Offer.UmzugCostSum = string.Format("{0}-{1}", priceMin, priceMax);
                        priceMin -= subtractCosts;
                        priceMax -= subtractCosts;
                        model.Offer.UmzugCostEstimated = string.Format("{0}-{1}", priceMin, priceMax);
                    }
                    else
                    {
                        priceMax = priceMax * int.Parse(durationTime[0]) + additionCosts;
                        model.Offer.UmzugCostSum = priceMax.ToString();
                        priceMax -= subtractCosts;
                        model.Offer.UmzugCostEstimated = priceMax.ToString();
                    }
                    if (model.Offer.UmzugCostHighSecurity)
                    {
                        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                        if (!model.Offer.UmzugCostHighSecurityManual || model.Offer.UmzugCostHighSecurityPrice <= 0)
                        {
                            model.Offer.UmzugCostHighSecurityPrice = (((int)((priceMax * 1.2) / 10)) + 1) * 10;
                        }
                    }
                    if (model.Offer.UmzugCostFix)
                    {
                        // only if not set, else ev. user has changed value
                        if (model.Offer.UmzugCostFixPrice <= 0) model.Offer.UmzugCostFixPrice = (((int)((priceMax * 1.1) / 10)) + 1) * 10;
                    }
                }
            }
            return model;
        }

        private CustomerOfferViewModel prepareSavePackService(CustomerOfferViewModel model, int accountId)
        {
            // correct datetime
            if (!string.IsNullOrEmpty(model.Offer.PackDate))
            {
                if (DateTime.TryParse(model.Offer.PackDate, out var packdatetime))
                {
                    model.Offer.PackDateTime = packdatetime;
                }
            }
            model.Offer.PackTime = Helpers.Helpers.CorrectTimeFormat(model.Offer.PackTime);
            // read pricerateid from helper property '1|200|3|1'
            string[] helperPriceRate = model.HelperPackPriceRateId.Split('|');
            int priceRateId = 0;
            if (int.TryParse(helperPriceRate[0], out priceRateId))
            {
                // set pricerateid and description
                model.Offer.PackPriceRateId = priceRateId;
                PriceManager priceManager = new PriceManager();
                  PriceRate pr = priceManager.GetPriceRateById(accountId, "pack", model.Offer.PackPriceRateId.Value);
                if (pr.CountFirst == model.Offer.PackCostChangedPers && pr.CountPrice == model.Offer.PackCostChangedPrice)
                {
                    model.Offer.PackPriceRateDescr = pr.Descr;
                }
                else
                {
                    model.Offer.PackPriceRateDescr = string.Format("{0} {1}",
                        pr.DescrOnChangeFirst.Replace("[a]", model.Offer.PackCostChangedPers.ToString()).TrimStart(),
                        pr.DescrOnChangePrice.Replace("[a]", model.Offer.PackCostChangedPrice.ToString()).TrimStart());
                }
                // set endprice
                int priceMax = model.Offer.PackCostChangedPrice;
                if (priceMax != 0)
                {
                    int priceMin = priceMax;
                    int additionCosts = model.Offer.PackCostWay;
                    if (model.Offer.PackCostAddit)
                    {
                        model.PackPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                        additionCosts += model.Offer.PackCostAdditFreeTextPlus1Price + model.Offer.PackCostAdditFreeTextPlus2Price;
                    }
                    int subtractCosts = model.Offer.PackCostDiscount + model.Offer.PackCostDiscount2 + model.Offer.PackCostAdditFreeTextMinus1Price;

                    string[] durationTime = model.Offer.PackDuration.Split('-');
                    if (durationTime.Length > 1)
                    {
                        priceMin = priceMin * int.Parse(durationTime[0]) + additionCosts;
                        priceMax = priceMax * int.Parse(durationTime[1]) + additionCosts;
                        // for email, check for contains - and split and set in emailmodel (if somewhere happen an error, it should
                        model.Offer.PackCostSum = string.Format("{0}-{1}", priceMin, priceMax);
                        priceMin -= subtractCosts;
                        priceMax -= subtractCosts;
                        model.Offer.PackCostEstimated = string.Format("{0}-{1}", priceMin, priceMax);
                    }
                    else
                    {
                        priceMax = priceMax * int.Parse(durationTime[0]) + additionCosts;
                        model.Offer.PackCostSum = priceMax.ToString();
                        priceMax -= subtractCosts;
                        model.Offer.PackCostEstimated = priceMax.ToString();
                    }
                    if (model.Offer.PackCostHighSecurity)
                    {
                        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                        if (!model.Offer.PackCostHighSecurityManual || model.Offer.PackCostHighSecurityPrice <= 0)
                        {
                            model.Offer.PackCostHighSecurityPrice = (((int)((priceMax * 1.2) / 10)) + 1) * 10;
                        }
                    }
                    if (model.Offer.PackCostFix)
                    {
                        // only if not set, else ev. user has changed value
                        if (model.Offer.PackCostFixPrice <= 0) model.Offer.PackCostFixPrice = (((int)((priceMax * 1.1) / 10)) + 1) * 10;
                    }
                }
            }
            return model;
        }

        private CustomerOfferViewModel prepareSavePackOutService(CustomerOfferViewModel model, int accountId)
        {
            // correct datetime
            if (!string.IsNullOrEmpty(model.Offer.PackOutDate))
            {
                if (DateTime.TryParse(model.Offer.PackOutDate, out var packoutdatetime))
                {
                    model.Offer.PackOutDateTime = packoutdatetime;
                }
            }
            model.Offer.PackOutTime = Helpers.Helpers.CorrectTimeFormat(model.Offer.PackOutTime);
            // read pricerateid from helper property '1|200|3|1'
            string[] helperPriceRate = model.HelperPackOutPriceRateId.Split('|');
            int priceRateId = 0;
            if (int.TryParse(helperPriceRate[0], out priceRateId))
            {
                // set pricerateid and description
                model.Offer.PackOutPriceRateId = priceRateId;
                PriceManager priceManager = new PriceManager();
                  PriceRate pr = priceManager.GetPriceRateById(accountId, "packout", model.Offer.PackOutPriceRateId.Value);
                if (pr.CountFirst == model.Offer.PackOutCostChangedPers && pr.CountPrice == model.Offer.PackOutCostChangedPrice)
                {
                    model.Offer.PackOutPriceRateDescr = pr.Descr;
                }
                else
                {
                    model.Offer.PackOutPriceRateDescr = string.Format("{0} {1}",
                        pr.DescrOnChangeFirst.Replace("[a]", model.Offer.PackOutCostChangedPers.ToString()).TrimStart(),
                        pr.DescrOnChangePrice.Replace("[a]", model.Offer.PackOutCostChangedPrice.ToString()).TrimStart());
                }
                // set endprice
                int priceMax = model.Offer.PackOutCostChangedPrice;
                if (priceMax != 0)
                {
                    int priceMin = priceMax;
                    int additionCosts = model.Offer.PackOutCostWay;
                    if (model.Offer.PackOutCostAddit)
                    {
                        model.PackOutPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                        additionCosts += model.Offer.PackOutCostAdditFreeTextPlus1Price + model.Offer.PackOutCostAdditFreeTextPlus2Price;
                    }
                    int subtractCosts = model.Offer.PackOutCostDiscount + model.Offer.PackOutCostDiscount2 + model.Offer.PackOutCostAdditFreeTextMinus1Price;

                    string[] durationTime = model.Offer.PackOutDuration.Split('-');
                    if (durationTime.Length > 1)
                    {
                        priceMin = priceMin * int.Parse(durationTime[0]) + additionCosts;
                        priceMax = priceMax * int.Parse(durationTime[1]) + additionCosts;
                        // for email, check for contains - and split and set in emailmodel (if somewhere happen an error, it should
                        model.Offer.PackOutCostSum = string.Format("{0}-{1}", priceMin, priceMax);
                        priceMin -= subtractCosts;
                        priceMax -= subtractCosts;
                        model.Offer.PackOutCostEstimated = string.Format("{0}-{1}", priceMin, priceMax);
                    }
                    else
                    {
                        priceMax = priceMax * int.Parse(durationTime[0]) + additionCosts;
                        model.Offer.PackOutCostSum = priceMax.ToString();
                        priceMax -= subtractCosts;
                        model.Offer.PackOutCostEstimated = priceMax.ToString();
                    }
                    if (model.Offer.PackOutCostHighSecurity)
                    {
                        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                        if (!model.Offer.PackOutCostHighSecurityManual || model.Offer.PackOutCostHighSecurityPrice <= 0)
                        {
                            model.Offer.PackOutCostHighSecurityPrice = (((int)((priceMax * 1.2) / 10)) + 1) * 10;
                        }
                    }
                    if (model.Offer.PackOutCostFix)
                    {
                        // only if not set, else ev. user has changed value
                        if (model.Offer.PackOutCostFixPrice <= 0) model.Offer.PackOutCostFixPrice = (((int)((priceMax * 1.1) / 10)) + 1) * 10;
                    }
                }
            }
            return model;
        }

        private CustomerOfferViewModel prepareSaveReinigungService(CustomerOfferViewModel model, int accountId, int sectorId)
        {
            OfferCleaning helperOfferCleaning = null;
            string helperPriceRateId = null;
            string helperHourlyPriceRateId = null;
            if (sectorId == 1)
            {
                helperOfferCleaning = model.Offer.OfferCleaning;
                helperPriceRateId = model.HelperReinigungPriceRateId;
                helperHourlyPriceRateId = model.HelperReinigungHourlyPriceRateId;
            }
            else
            {
                helperOfferCleaning = model.Offer.OfferCleaning2;
                helperPriceRateId = model.HelperReinigung2PriceRateId;
                helperHourlyPriceRateId = model.HelperReinigung2HourlyPriceRateId;
            }

            // correct datetime
            if (!string.IsNullOrEmpty(helperOfferCleaning.ReinigungDate))
            {
                if (DateTime.TryParse(helperOfferCleaning.ReinigungDate, out var reinigungdatetime))
                {
                    helperOfferCleaning.ReinigungDateTime = reinigungdatetime;
                }
            }
            helperOfferCleaning.ReinigungTime = Helpers.Helpers.CorrectTimeFormat(helperOfferCleaning.ReinigungTime);

            // which ReinigungTarif is selected: Flat rate or HourlyRate
            if (!string.IsNullOrEmpty(helperPriceRateId))
            {
                // read pricerateid from helper property '1|200|3|1'
                string[] helperPriceRate = helperPriceRateId.Split('|');
                if (int.TryParse(helperPriceRate[0], out var priceRateId))
                {
                    // set pricerateid and description
                    helperOfferCleaning.ReinigungPriceRateId = priceRateId;
                    PriceManager priceManager = new PriceManager();
                      PriceRate pr = priceManager.GetPriceRateById(accountId, "reinigung", helperOfferCleaning.ReinigungPriceRateId.Value);
                    helperOfferCleaning.ReinigungPriceRateDescr = $"{pr.DescrOnChangeFirst.Replace("[a]", ($"{pr.CountFirst}-{pr.CountFirst}.5")).TrimStart()}";

                    // remove other data
                    helperOfferCleaning.ReinigungHourlyPriceRateId = null;
                    helperOfferCleaning.ReinigungHourlyCostChangedPers = 0;
                    helperOfferCleaning.ReinigungHourlyCostChangedPrice = 0;
                    helperOfferCleaning.ReinigungHourlyDuration = null;
                }
            }
            else if (!string.IsNullOrEmpty(helperHourlyPriceRateId))
            {
                // Hourly Rate
                string[] helperPriceRate = helperHourlyPriceRateId.Split('|');
                if (int.TryParse(helperPriceRate[0], out var priceRateId))
                {
                    // set pricerateid and description
                    helperOfferCleaning.ReinigungHourlyPriceRateId = priceRateId;
                    PriceManager priceManager = new PriceManager();
                      PriceRate pr = priceManager.GetPriceRateById(accountId, "reinigunghourly", helperOfferCleaning.ReinigungHourlyPriceRateId.Value);
                    helperOfferCleaning.ReinigungPriceRateDescr = string.Format("{0} {1}",
                        pr.DescrOnChangeFirst.Replace("[a]", helperOfferCleaning.ReinigungHourlyCostChangedPers.ToString()),
                        pr.DescrOnChangePrice.Replace("[a]", helperOfferCleaning.ReinigungHourlyCostChangedPrice.ToString())).Trim();

                    // remove other data
                    helperOfferCleaning.ReinigungPriceRateId = null;
                    helperOfferCleaning.ReinigungCostChangedPrice = 0;
                }
            }

            if (sectorId == 1)
            {
                model.Offer.OfferCleaning = helperOfferCleaning;
            }
            else
            {
                model.Offer.OfferCleaning2 = helperOfferCleaning;
            }
            return model;
        }

        private CustomerOfferViewModel prepareSaveEntsorgungService(CustomerOfferViewModel model, int accountId)
        {
            // correct datetime
            if (!string.IsNullOrEmpty(model.Offer.EntsorgungDate))
            {
                if (DateTime.TryParse(model.Offer.EntsorgungDate, out var entsorgungdatetime))
                {
                    model.Offer.EntsorgungDateTime = entsorgungdatetime;
                }
            }
            model.Offer.EntsorgungTime = Helpers.Helpers.CorrectTimeFormat(model.Offer.EntsorgungTime);
            int priceMin = 0;
            int priceMax = 0;
            int priceVolumeMin = 0;
            int priceVolumeMax = 0;
            int additionCosts = 0;

            // read personpricerateid from helper property '1|200|3|1'
            if (!string.IsNullOrEmpty(model.HelperEntsorgungPersonPriceRateId))
            {
                string[] helperPersonPriceRate = model.HelperEntsorgungPersonPriceRateId.Split('|');
                int personPriceRateId = 0;
                if (int.TryParse(helperPersonPriceRate[0], out personPriceRateId))
                {
                    // set pricerateid and description
                    model.Offer.EntsorgungPersonPriceRateId = personPriceRateId;
                    PriceManager priceManager = new PriceManager();
                      PriceRate pr = priceManager.GetPriceRateById(accountId, "entsorgungperson", model.Offer.EntsorgungPersonPriceRateId.Value);
                    if (pr.CountFirst == model.Offer.EntsorgungPersonCostChangedPers && pr.CountSecond == model.Offer.EntsorgungPersonCostChangedVeh && pr.CountPrice == model.Offer.EntsorgungPersonCostChangedPrice)
                    {
                        model.Offer.EntsorgungPersonPriceRateDescr = pr.Descr;
                    }
                    else
                    {
                        model.Offer.EntsorgungPersonPriceRateDescr = string.Format("{0} {1} {2}",
                            pr.DescrOnChangeFirst.Replace("[a]", model.Offer.EntsorgungPersonCostChangedPers.ToString()).TrimStart(),
                            pr.DescrOnChangeSecond.Replace("[a]", model.Offer.EntsorgungPersonCostChangedVeh.ToString()).TrimStart(),
                            pr.DescrOnChangePrice.Replace("[a]", model.Offer.EntsorgungPersonCostChangedPrice.ToString()).TrimStart());
                    }
                    // set Anhänger (Trailer) on price rate description
                    if (model.Offer.EntsorgungPersonCostChangedTrailer > 0)
                    {
                        model.Offer.EntsorgungPersonPriceRateDescr = model.Offer.EntsorgungPersonPriceRateDescr.Replace("Lieferwagen", $"Lieferwagen und {model.Offer.EntsorgungPersonCostChangedTrailer} Anhänger");
                    }

                    // set endprice
                    priceMax = model.Offer.EntsorgungPersonCostChangedPrice;
                    if ((priceMax != 0) && (!string.IsNullOrEmpty(model.Offer.EntsorgungDuration)))
                    {
                        additionCosts = model.Offer.EntsorgungCostWay;

                        string[] durationTime = model.Offer.EntsorgungDuration.Split('-');
                        if (durationTime.Length > 1)
                        {
                            priceMin = priceMax * int.Parse(durationTime[0]);
                            priceMax = priceMax * int.Parse(durationTime[1]);
                        }
                        else
                        {
                            priceMax = priceMax * int.Parse(durationTime[0]);
                        }
                    }
                    else
                    {
                        priceMin = 0;
                        priceMax = 0;
                    }
                }
            }

            // read pricerateid from helper property '1|200|3|1'
            if (!string.IsNullOrEmpty(model.HelperEntsorgungPriceRateId))
            {
                string[] helperPriceRate = model.HelperEntsorgungPriceRateId.Split('|');
                int priceRateId = 0;
                if (int.TryParse(helperPriceRate[0], out priceRateId))
                {
                    // set pricerateid and description
                    model.Offer.EntsorgungPriceRateId = priceRateId;
                    PriceManager priceManager = new PriceManager();
                      PriceRate pr = priceManager.GetPriceRateById(accountId, "entsorgung", model.Offer.EntsorgungPriceRateId.Value);
                    if (pr.CountPrice == model.Offer.EntsorgungCostChangedPrice)
                    {
                        model.Offer.EntsorgungPriceRateDescr = pr.Descr;
                    }
                    else
                    {
                        model.Offer.EntsorgungPriceRateDescr = pr.DescrOnChangePrice.Replace("[a]", model.Offer.EntsorgungCostChangedPrice.ToString());
                    }
                    // set endprice
                    priceVolumeMax = model.Offer.EntsorgungCostChangedPrice;
                    if ((priceVolumeMax != 0) && (!string.IsNullOrEmpty(model.Offer.EntsorgungVolume)))
                    {
                        int volume = 0;
                        int volumeMin = 0;
                        string[] splitVolume = model.Offer.EntsorgungVolume.Split('-');
                        if (splitVolume.Length == 2)
                        {
                            int.TryParse(splitVolume[0], out volumeMin);
                            if (!int.TryParse(splitVolume[1], out volume))
                            {
                                volume = 0;
                            }
                        }
                        else if (!int.TryParse(splitVolume[0], out volume))
                        {
                            volume = 0;
                        }

                        int entsorgungFixCost = 0;
                        if (!string.IsNullOrEmpty(model.Offer.EntsorgungFixCost))
                        {
                            int.TryParse(model.Offer.EntsorgungFixCost, out entsorgungFixCost);
                        }
                        additionCosts += entsorgungFixCost;
                        if (volumeMin > 0) priceVolumeMin = (priceVolumeMax * volumeMin);
                        priceVolumeMax = (priceVolumeMax * volume);
                    }
                    else
                    {
                        priceVolumeMin = 0;
                        priceVolumeMax = 0;
                    }
                }
            }

            if (priceMin != 0 || priceMax != 0 || priceVolumeMin != 0 || priceVolumeMax != 0)
            {
                if (model.Offer.EntsorgungCostAddit)
                {
                    model.EntsorgungPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                    additionCosts += model.Offer.EntsorgungCostAdditFreeTextPlus1Price + model.Offer.EntsorgungCostAdditFreeTextPlus2Price;
                }
                int subtractCosts = model.Offer.EntsorgungCostDiscount + model.Offer.EntsorgungCostAdditFreeTextMinus1Price;

                if (priceVolumeMin != 0)
                {
                    if (priceMin == 0 && priceMax > 0) priceMin = priceMax;
                    priceMin += priceVolumeMin;
                }
                if (priceVolumeMax != 0) priceMax += priceVolumeMax;

                if (priceMin > 0)
                {
                    priceMin = priceMin + additionCosts - subtractCosts;
                    if (priceVolumeMin == 0 && priceVolumeMax > 0) priceMin = priceMin + priceVolumeMax;
                }
                if (priceMax > 0)
                {
                    priceMax = priceMax + additionCosts - subtractCosts;
                    model.Offer.EntsorgungCostEstimated = priceMax.ToString();

                    if (priceMin > 0)
                    {
                        model.Offer.EntsorgungCostEstimated = string.Format("{0}-{1}", priceMin, priceMax);
                    }

                    if (model.Offer.EntsorgungCostHighSecurity)
                    {
                        // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                        if (!model.Offer.EntsorgungCostHighSecurityManual || model.Offer.EntsorgungCostHighSecurityPrice <= 0)
                        {
                            model.Offer.EntsorgungCostHighSecurityPrice = (((int)((priceMax * 1.2) / 10)) + 1) * 10;
                        }
                    }
                    if (model.Offer.EntsorgungCostFix)
                    {
                        // only if not set, else ev. user has changed value
                        if (model.Offer.EntsorgungCostFixPrice <= 0) model.Offer.EntsorgungCostFixPrice = (((int)((priceMax * 1.1) / 10)) + 1) * 10;
                    }
                }
            }

            return model;
        }

        private CustomerOfferViewModel prepareSaveTransportService(CustomerOfferViewModel model, int accountId)
        {
            // correct datetime
            if (!string.IsNullOrEmpty(model.Offer.OfferTransport.TransportDate))
            {
                if (DateTime.TryParse(model.Offer.OfferTransport.TransportDate, out var correctdatetime))
                {
                    model.Offer.OfferTransport.TransportDateTime = correctdatetime;
                }
            }
            model.Offer.OfferTransport.TransportTime = Helpers.Helpers.CorrectTimeFormat(model.Offer.OfferTransport.TransportTime);

            int priceMax = 0;
            int priceMin = 0;

            // which Tariff is selected: Flat rate (CostFix) or HourlyEmployee (PriceRateId)
            if (model.Offer.OfferTransport.TransportCostFixRatePrice > 0)
            {
                priceMax = model.Offer.OfferTransport.TransportCostFixRatePrice.Value;

                // remove other data
                model.Offer.OfferTransport.TransportPriceRateId = null;
                model.Offer.OfferTransport.TransportCostChangedPrice = 0;
            }
            else
            {
                // read pricerateid from helper property '1|200|3|1'
                string[] helperPriceRate = model.HelperTransportPriceRateId.Split('|');
                int priceRateId = 0;
                if (int.TryParse(helperPriceRate[0], out priceRateId))
                {
                    // set pricerateid and description
                    model.Offer.OfferTransport.TransportPriceRateId = priceRateId;
                    PriceManager priceManager = new PriceManager();
                      PriceRate pr = priceManager.GetPriceRateById(accountId, "transport", model.Offer.OfferTransport.TransportPriceRateId.Value);
                    if (pr.CountFirst == model.Offer.OfferTransport.TransportCostChangedPers && pr.CountSecond == model.Offer.OfferTransport.TransportCostChangedVeh && pr.CountPrice == model.Offer.OfferTransport.TransportCostChangedPrice)
                    {
                        model.Offer.OfferTransport.TransportPriceRateDescr = pr.Descr;
                    }
                    else
                    {
                        model.Offer.OfferTransport.TransportPriceRateDescr = string.Format("{0} {1} {2}",
                            pr.DescrOnChangeFirst.Replace("[a]", model.Offer.OfferTransport.TransportCostChangedPers.ToString()).TrimStart(),
                            pr.DescrOnChangeSecond.Replace("[a]", model.Offer.OfferTransport.TransportCostChangedVeh.ToString()).TrimStart(),
                            pr.DescrOnChangePrice.Replace("[a]", model.Offer.OfferTransport.TransportCostChangedPrice.ToString()).TrimStart());
                    }

                    // set Anhänger (Trailer) on price rate description
                    if (model.Offer.OfferTransport.TransportCostChangedTrailer > 0)
                    {
                        model.Offer.OfferTransport.TransportPriceRateDescr = model.Offer.OfferTransport.TransportPriceRateDescr.Replace("Lieferwagen", $"Lieferwagen und {model.Offer.OfferTransport.TransportCostChangedTrailer} Anhänger");
                    }

                    priceMax = model.Offer.OfferTransport.TransportCostChangedPrice;

                    int additionCosts = model.Offer.OfferTransport.TransportCostWay;
                    if (model.Offer.OfferTransport.TransportCostAddit)
                    {
                        additionCosts += model.Offer.OfferTransport.TransportCostAdditFreeTextPlus1Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus2Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus3Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus4Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus5Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus6Price +
                                         model.Offer.OfferTransport.TransportCostAdditFreeTextPlus7Price;
                    }
                    string[] durationTime = model.Offer.OfferTransport.TransportDuration.Split('-');
                    if (durationTime.Length > 1)
                    {
                        priceMin = priceMax * int.Parse(durationTime[0]) + additionCosts;
                        priceMax = priceMax * int.Parse(durationTime[1]) + additionCosts;

                        model.Offer.OfferTransport.TransportCostSum = $"{priceMin}-{priceMax}";
                    }
                    else
                    {
                        priceMax = priceMax * int.Parse(durationTime[0]) + additionCosts;
                        model.Offer.OfferTransport.TransportCostSum = priceMax.ToString();
                    }
                }
            }

            // set endprice
            if (priceMax != 0)
            {
                int subtractCosts = model.Offer.OfferTransport.TransportCostDiscount +
                                    model.Offer.OfferTransport.TransportCostDiscount2 +
                                    model.Offer.OfferTransport.TransportCostAdditFreeTextMinus1Price +
                                    model.Offer.OfferTransport.TransportCostAdditFreeTextMinus2Price;
                priceMax -= subtractCosts;

                if (priceMin > 0)
                {
                    priceMin -= subtractCosts;
                    model.Offer.OfferTransport.TransportCostEstimated = $"{priceMin}-{priceMax}";
                }
                else
                {
                    model.Offer.OfferTransport.TransportCostEstimated = priceMax.ToString();
                }

                if (model.Offer.OfferTransport.TransportCostHighSecurity)
                {
                    // set cost highprice only to defined algorithm, if user has not set it manually or the highprice is 0
                    if (!model.Offer.OfferTransport.TransportCostHighSecurityManual || model.Offer.OfferTransport.TransportCostHighSecurityPrice <= 0)
                    {
                        model.Offer.OfferTransport.TransportCostHighSecurityPrice = (((int)((priceMax * 1.2) / 10)) + 1) * 10;
                    }
                }

                if (model.Offer.OfferTransport.TransportCostFix)
                {
                    // only if not set, else ev. user has changed value
                    if (model.Offer.OfferTransport.TransportCostFixPrice <= 0) model.Offer.OfferTransport.TransportCostFixPrice = (((int)((priceMax * 1.1) / 10)) + 1) * 10;
                }
            }

            return model;
        }

        private CustomerOfferViewModel prepareSaveLagerungService(CustomerOfferViewModel model, int accountId)
        {
            // read pricerateid from helper property '1|200|3|1'
            string[] helperPriceRate = model.HelperLagerungPriceRateId.Split('|');
            int priceRateId = 0;
            if (int.TryParse(helperPriceRate[0], out priceRateId))
            {
                // set pricerateid and description
                model.Offer.LagerungPriceRateId = priceRateId;
                PriceManager priceManager = new PriceManager();
                  PriceRate pr = priceManager.GetPriceRateById(accountId, "lagerung", model.Offer.LagerungPriceRateId.Value);
                if (pr.CountPrice == model.Offer.LagerungCostChangedPrice)
                {
                    model.Offer.LagerungPriceRateDescr = pr.Descr;
                }
                else
                {
                    model.Offer.LagerungPriceRateDescr = pr.DescrOnChangePrice.Replace("[a]", model.Offer.LagerungCostChangedPrice.ToString());
                }
                // set endprice
                int price = model.Offer.LagerungCostChangedPrice;
                if ((price != 0) && (!string.IsNullOrEmpty(model.Offer.LagerungVolume)))
                {
                    int volume = 1;
                    int volumeMax = 0;
                    string[] splitVolume = model.Offer.LagerungVolume.Split('-');
                    if (splitVolume.Length == 2)
                    {
                        if (!int.TryParse(splitVolume[0], out volume))
                        {
                            volume = 1;
                        }
                        int.TryParse(splitVolume[1], out volumeMax);
                    }
                    else if (!int.TryParse(splitVolume[0], out volume))
                    {
                        volume = 1;
                    }

                    int additionCosts = model.Offer.LagerungCostAdditFreeTextPlus1Price + model.Offer.LagerungCostAdditFreeTextPlus2Price;
                    int subtractCosts = model.Offer.LagerungCostAdditFreeTextMinus1Price;
                    var priceMax = 0;
                    if (volumeMax > 0) priceMax = (price * volumeMax) + additionCosts - subtractCosts;
                    price = (price * volume) + additionCosts - subtractCosts;
                    model.Offer.LagerungCostEstimated = string.Format("{0}{1}", price, priceMax > 0 ? string.Format("-{0}", priceMax) : string.Empty);

                    if (model.Offer.LagerungCostFix)
                    {
                        // only if not set, else ev. user has changed value
                        if (model.Offer.LagerungCostFixPrice <= 0) model.Offer.LagerungCostFixPrice = (((int)(((priceMax > 0 ? priceMax : price) * 1.1) / 10)) + 1) * 10;
                    }
                }
            }
            return model;
        }

        private CustomerOfferViewModel prepareSavePackMaterialService(CustomerOfferViewModel model, int accountId)
        {
            float priceTotal = 0f;

            // DefinedPackMaterial (UI: selected by dropdown)
            var bookedDefinedPackMaterials = model.OfferDefinedPackMaterials.Where(x => x.DefinedPackMaterialId > 0 && x.CountNumber > 0).ToList();
            foreach (Offer_DefinedPackMaterial opm in bookedDefinedPackMaterials)
            {
                if (!string.IsNullOrEmpty(opm.PiecePrice)) opm.PiecePrice = opm.PiecePrice.Replace(',', '.');

                if (float.TryParse(opm.PiecePrice, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var pricePerPiece))
                {
                    float eachTotal = (opm.CountNumber * pricePerPiece);
                    opm.EndPrice = Helpers.Helpers.FloatToCurrencyFormat(eachTotal);
                    priceTotal += (eachTotal);
                }
            }

            // PackMaterial (UI: selected by textfield)
            var bookedPackMaterials = model.OfferPackMaterials.Where(x => !string.IsNullOrEmpty(x.Text) && x.CountNumber > 0).ToList();
            foreach (OfferPackMaterial opm in bookedPackMaterials)
            {
                if (!string.IsNullOrEmpty(opm.PricePerPiece)) opm.PricePerPiece = opm.PricePerPiece.Replace(',', '.');

                if (float.TryParse(opm.PricePerPiece, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var pricePerPiece))
                {
                    float eachTotal = (opm.CountNumber * pricePerPiece);
                    opm.EndPrice = Helpers.Helpers.FloatToCurrencyFormat(eachTotal);
                    priceTotal += (eachTotal);
                }
            }

            priceTotal -= model.Offer.PackMaterialCostDiscount;
            if (model.Offer.PackMaterialDeliver)
            {
                priceTotal += model.Offer.PackMaterialDeliverPrice;
            }
            if (model.Offer.PackMaterialPickup)
            {
                priceTotal += model.Offer.PackMaterialPickupPrice;
            }

            model.Offer.PackMaterialCostSum = priceTotal.ToString(CultureInfo.InvariantCulture).Replace(',', '.');

            return model;
        }

    }
}