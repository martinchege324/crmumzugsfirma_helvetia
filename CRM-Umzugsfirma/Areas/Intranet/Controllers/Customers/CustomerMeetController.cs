﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Calendar;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Calendar.v3.Data;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.ViewModels.Mail.Appointment;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerMeetController : Controller
    {
        // GET: Intranet/Customer/Details/Meet/1
        public async Task<ActionResult> Index(int customerId, int? id)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            AppointmentManager manager = new AppointmentManager();
            return View(await manager.GetAppointmentsByCustomer(customerId));
        }

        // GET: Intranet/Customer/Details/Meet/Edit/1/1
        // If Empty creates a new, else edit it
        public ActionResult Edit(int customerId, int? id, int? offerId)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            // create model
            CustomerMeetViewModel model = new CustomerMeetViewModel();
     
            
            if (id == null)
            {
                // -- Create new
                ViewBag.SubTitle = "Capture new date";
                ViewBag.SubmitText = "Create";
                Appointment appointment = new Appointment
                {
                    CustomerId = customerId,
                    AccountId = customer.AccountId,
                    AppointmentTypeId = Appointment.AppointmentType.Sightseeing,
                    AppointmentView = new AppointmentView { MeetPlace = AppointmentView.MeetPlaceType.At_the_customer },
                    AppointmentService = new AppointmentService(),
                    AppointmentDelivery = new AppointmentDelivery { DeliveryProduct = AppointmentDelivery.DeliveryProductType.Packing_Material, Delivery = AppointmentDelivery.DeliveryType.Delivery },
                    Where = $"{customer.Street}, {Helpers.Helpers.LocationDescription(customer.Zip, customer.City, customer.CountryCode)}",
                    EmailAddress = customer.Email
                };
                if (offerId.HasValue)
                {
                    appointment.AppointmentTypeId = Appointment.AppointmentType.Confirmation_of_the_order;
                    OfferManager offerManager = new OfferManager();
                    Offer offer = offerManager.GetOfferById(offerId.Value, true, customer.AccountId);
                    if (offer != null && offer.CustomerId == customer.CustomerId)
                    {
                        appointment.OfferId = offer.OfferId;
                        appointment.OfferInnerId = offer.OfferInnerId;
                        if (offer.AdrOutZip > 0 && !string.IsNullOrEmpty(offer.AdrOutCity))
                        {
                            appointment.Where = Helpers.Helpers.LocationDescription(offer.AdrOutZip, offer.AdrOutCity, offer.AdrOutCountryCode);
                        }
                        if (!string.IsNullOrEmpty(offer.EmailAddress)) appointment.EmailAddress = offer.EmailAddress;

                        appointment.AppointmentService.ServiceUmzug = offer.UmzugActive;
                        if (offer.UmzugActive)
                        {
                            appointment.AppointmentService.UmzugDate = offer.UmzugDate;
                            appointment.AppointmentService.UmzugTime = offer.UmzugTime;
                            appointment.AppointmentService.DurationUmzug = offer.UmzugDuration;
                            appointment.AppointmentService.CountPersonUmzug = offer.UmzugCostChangedPers;
                            appointment.AppointmentService.CountVehUmzug = offer.UmzugCostChangedVeh;
                            appointment.AppointmentService.CountTrailerUmzug = offer.UmzugCostChangedTrailer;
                            appointment.AppointmentService.UmzugWhereFrom = Helpers.Helpers.LocationDescription(offer.AdrOutZip, offer.AdrOutCity);
                            appointment.AppointmentService.UmzugWhereTo = Helpers.Helpers.LocationDescription(offer.AdrInZip, offer.AdrInCity);
                        }
                        appointment.AppointmentService.ServicePack = offer.PackActive;
                        if (offer.PackActive)
                        {
                            if (!offer.UmzugActive || (offer.PackDate != offer.UmzugDate || offer.PackTime != offer.UmzugTime))
                            {
                                appointment.AppointmentService.PackDate = offer.PackDate;
                                appointment.AppointmentService.PackTime = offer.PackTime;
                            }
                            appointment.AppointmentService.DurationPack = offer.PackDuration;
                            appointment.AppointmentService.CountPersonPack = offer.PackCostChangedPers;
                        }
                        appointment.AppointmentService.ServicePackOut = offer.PackOutActive;
                        if (offer.PackOutActive)
                        {
                            if (!offer.UmzugActive || (offer.PackOutDate != offer.UmzugDate || offer.PackOutTime != offer.UmzugTime))
                            {
                                appointment.AppointmentService.PackOutDate = offer.PackOutDate;
                                appointment.AppointmentService.PackTime = offer.PackOutTime;
                            }
                            appointment.AppointmentService.DurationPackOut = offer.PackOutDuration;
                            appointment.AppointmentService.CountPersonPackOut = offer.PackOutCostChangedPers;
                        }
                        appointment.AppointmentService.ServiceReinigung = offer.ReinigungActive;
                        if (offer.ReinigungActive)
                        {
                            if (!offer.UmzugActive || (offer.OfferCleaning.ReinigungDate != offer.UmzugDate && offer.OfferCleaning.ReinigungTime != offer.UmzugTime))
                            {
                                appointment.AppointmentService.ReinigungDate = offer.OfferCleaning.ReinigungDate;
                                appointment.AppointmentService.ReinigungTime = offer.OfferCleaning.ReinigungTime;
                            }
                            if (!string.IsNullOrEmpty(offer.OfferCleaning.ReinigungDateCommit) || !string.IsNullOrEmpty(offer.OfferCleaning.ReinigungTimeCommit))
                            {
                                appointment.AppointmentService.DateReinigungRelease = offer.OfferCleaning.ReinigungDateCommit;
                                appointment.AppointmentService.TimeReinigungRelease = offer.OfferCleaning.ReinigungTimeCommit;
                            }
                        }
                        appointment.AppointmentService.ServiceReinigung2 = offer.Reinigung2Active;
                        if (offer.Reinigung2Active)
                        {
                            if (!offer.UmzugActive || (offer.OfferCleaning2.ReinigungDate != offer.UmzugDate && offer.OfferCleaning2.ReinigungTime != offer.UmzugTime))
                            {
                                appointment.AppointmentService.Reinigung2Date = offer.OfferCleaning2.ReinigungDate;
                                appointment.AppointmentService.Reinigung2Time = offer.OfferCleaning2.ReinigungTime;
                            }
                            if (!string.IsNullOrEmpty(offer.OfferCleaning2.ReinigungDateCommit) || !string.IsNullOrEmpty(offer.OfferCleaning2.ReinigungTimeCommit))
                            {
                                appointment.AppointmentService.DateReinigung2Release = offer.OfferCleaning2.ReinigungDateCommit;
                                appointment.AppointmentService.TimeReinigung2Release = offer.OfferCleaning2.ReinigungTimeCommit;
                            }
                        }
                        appointment.AppointmentService.ServiceEntsorgung = offer.EntsorgungActive;
                        if (offer.EntsorgungActive)
                        {
                            appointment.AppointmentService.EntsorgungDate = offer.EntsorgungDate;
                            appointment.AppointmentService.EntsorgungTime = offer.EntsorgungTime;
                            appointment.AppointmentService.DurationEntsorgung = offer.EntsorgungDuration;
                            if (offer.EntsorgungPersonCostChangedPers > 0) appointment.AppointmentService.CountPersonEntsorgung = offer.EntsorgungPersonCostChangedPers;
                            if (offer.EntsorgungPersonCostChangedVeh > 0) appointment.AppointmentService.CountVehEntsorgung = offer.EntsorgungPersonCostChangedVeh;
                            if (offer.EntsorgungPersonCostChangedTrailer > 0) appointment.AppointmentService.CountTrailerEntsorgung = offer.EntsorgungPersonCostChangedTrailer;
                        }
                        appointment.AppointmentService.ServiceTransport = offer.TransportActive;
                        if (offer.TransportActive)
                        {
                            appointment.AppointmentService.TransportDate = offer.OfferTransport.TransportDate;
                            appointment.AppointmentService.TransportTime = offer.OfferTransport.TransportTime;
                            appointment.AppointmentService.DurationTransport = offer.OfferTransport.TransportDuration;
                            appointment.AppointmentService.CountPersonTransport = offer.OfferTransport.TransportCostChangedPers;
                            appointment.AppointmentService.CountVehTransport = offer.OfferTransport.TransportCostChangedVeh;
                            appointment.AppointmentService.CountTrailerTransport = offer.OfferTransport.TransportCostChangedTrailer;
                            appointment.AppointmentService.TransportWhereFrom = Helpers.Helpers.LocationDescription(offer.AdrOutZip, offer.AdrOutCity);
                            appointment.AppointmentService.TransportWhereTo = Helpers.Helpers.LocationDescription(offer.AdrInZip, offer.AdrInCity);
                        }
                        appointment.AppointmentService.ServiceLagerung = offer.LagerungActive;
                    }
                }
                model.Appointment = appointment;
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Date to edit";
                ViewBag.SubmitText = "Save";

                // security check if its own appointment
                AppointmentManager manager = new AppointmentManager();
                model.Appointment = manager.GetAppointmentById(id.Value);
                if ((model.Appointment == null) || (model.Appointment.CustomerId != customerId))
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                // set relational depencies to default value for rendering UI
                if (model.Appointment.AppointmentView == null) model.Appointment.AppointmentView = new AppointmentView { MeetPlace = AppointmentView.MeetPlaceType.At_the_customer };
                if (model.Appointment.AppointmentService == null) model.Appointment.AppointmentService = new AppointmentService();
                if (model.Appointment.AppointmentDelivery == null) model.Appointment.AppointmentDelivery = new AppointmentDelivery { DeliveryProduct = AppointmentDelivery.DeliveryProductType.Packing_Material, Delivery = AppointmentDelivery.DeliveryType.Delivery };

                // preset time
                if (string.IsNullOrEmpty(model.Appointment.EmailAddress)) model.Appointment.EmailAddress = customer.Email;
            }

            // Default: SendEmailToCustomer to false on every start (also for edit)
            model.Appointment.EmailToCustomer = false;

            // load email attachments
            EmailManager emailManager = new EmailManager();
            List<EmailAttachment> emailAttachments = emailManager.GetEmailAttachmentsByAccountId(customer.AccountId);
            if (emailAttachments.Count > 0)
            {
                model.EmailAttachments = new List<CheckListViewModel>();
                emailAttachments.ForEach(e => model.EmailAttachments.Add(new CheckListViewModel()
                {
                    Id = e.Name.ToLower(),
                    Name = e.Name,
                    Selected = (model.Appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing) ? e.IsViewAppointmentDefault :
                        (model.Appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order) ? e.IsServiceAppointmentDefault : false
                }));
            }

            return View(model);
        }

        // POST: Intranet/Customer/Details/Meet/Edit/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerMeetViewModel model, int customerId)
        {
            #region Validate

            // for besichtigung, then check gebuchte_dienstleistung
            if (model.Appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing)
            {
                // check for valid date
                DateTime tempFromDate = DateTime.MinValue;
                if (!DateTime.TryParse(model.Appointment.AppointmentView.ViewDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentView.ViewTime))
                {
                    ModelState.AddModelError("Appointment.AppointmentView.ViewDate", "Valides Date und Time eingeben.");
                }
            }
            else if (model.Appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
            {
                // validate for filled dates when a service is selected
                if (model.Appointment.AppointmentService.ServiceUmzug)
                {
                    // check for valid date
                    DateTime tempFromDate = DateTime.MinValue;
                    if (!DateTime.TryParse(model.Appointment.AppointmentService.UmzugDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.UmzugTime))
                    {
                        ModelState.AddModelError("Appointment.AppointmentService.UmzugDate", "Valides Date und Time eingeben.");
                    }
                    if (model.Appointment.AppointmentService.DateUmzugMoreAppointment)
                    {
                        if (!string.IsNullOrEmpty(model.Appointment.AppointmentService.UmzugDate2))
                        {
                            tempFromDate = DateTime.MinValue;
                            if (!DateTime.TryParse(model.Appointment.AppointmentService.UmzugDate2, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.UmzugTime2))
                            {
                                ModelState.AddModelError("Appointment.AppointmentService.UmzugDate2", "Valides Date und Time eingeben.");
                            }
                        }
                        if (!string.IsNullOrEmpty(model.Appointment.AppointmentService.UmzugDate3))
                        {
                            tempFromDate = DateTime.MinValue;
                            if (!DateTime.TryParse(model.Appointment.AppointmentService.UmzugDate3, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.UmzugTime3))
                            {
                                ModelState.AddModelError("Appointment.AppointmentService.UmzugDate3", "Valides Date und Time eingeben.");
                            }
                        }
                    }
                }
                else
                {
                    if (model.Appointment.AppointmentService.ServicePack)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.PackDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.PackTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.PackDate", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServicePackOut)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.PackOutDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.PackOutTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.PackOutDate", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServiceReinigung)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.ReinigungDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.ReinigungTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.ReinigungDate", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServiceReinigung2)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.Reinigung2Date, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.Reinigung2Time))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.Reinigung2Date", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServiceEntsorgung)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.EntsorgungDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.EntsorgungTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.EntsorgungDate", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServiceTransport)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.TransportDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.TransportTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.TransportDate", "Valides Date und Time eingeben.");
                        }
                    }
                    if (model.Appointment.AppointmentService.ServiceLagerung)
                    {
                        // check for valid date
                        DateTime tempFromDate = DateTime.MinValue;
                        if (!DateTime.TryParse(model.Appointment.AppointmentService.LagerungDate, out tempFromDate) || string.IsNullOrEmpty(model.Appointment.AppointmentService.LagerungTime))
                        {
                            ModelState.AddModelError("Appointment.AppointmentService.LagerungDate", "Valides Date und Time eingeben.");
                        }
                    }
                }
            }
            else if (model.Appointment.AppointmentTypeId == Appointment.AppointmentType.Delivery)
            {
                // check for valid date
                DateTime tempFromDate = DateTime.MinValue;
                if (!DateTime.TryParse(model.Appointment.AppointmentDelivery.DeliveryDate, out tempFromDate))
                {
                    ModelState.AddModelError("Appointment.AppointmentDelivery.DeliveryDate", "Valides Date eingeben.");
                }
            }

            #endregion

            if (ModelState.IsValid)
            {
                Appointment appointment = model.Appointment;

                // typ: clear other fields
                if (appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing)
                {
                    // set default / emtpy value
                    appointment.OfferInnerId = 0;
                    appointment.AppointmentService = null;
                    appointment.AppointmentDelivery = null;

                    // correct mismatched time format strings
                    appointment.AppointmentView.ViewTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentView.ViewTime);
                    appointment.AppointmentView.ViewDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentView.ViewDate, appointment.AppointmentView.ViewTime);
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
                {
                    // set default / emtpy value
                    appointment.AppointmentView = null;
                    appointment.AppointmentDelivery = null;

                    if (!appointment.AppointmentService.ServiceUmzug)
                    {
                        appointment.AppointmentService.UmzugDate = appointment.AppointmentService.UmzugTime = appointment.AppointmentService.DurationUmzug = appointment.AppointmentService.UmzugDate2 = appointment.AppointmentService.UmzugTime2 = appointment.AppointmentService.DurationUmzug2 = appointment.AppointmentService.UmzugDate3 = appointment.AppointmentService.UmzugTime3 = appointment.AppointmentService.DurationUmzug3 = string.Empty;
                        appointment.AppointmentService.DateUmzugMoreAppointment = false;
                    }
                    if (!appointment.AppointmentService.DateUmzugMoreAppointment)
                    {
                        appointment.AppointmentService.UmzugDate2 = appointment.AppointmentService.UmzugTime2 = appointment.AppointmentService.DurationUmzug2 = appointment.AppointmentService.UmzugDate3 = appointment.AppointmentService.UmzugTime3 = appointment.AppointmentService.DurationUmzug3 = string.Empty;
                    }
                    if (!appointment.AppointmentService.ServicePack || (appointment.AppointmentService.PackDate == null)) appointment.AppointmentService.PackDate = appointment.AppointmentService.PackTime = appointment.AppointmentService.DurationPack = string.Empty;
                    if (!appointment.AppointmentService.ServicePackOut || (appointment.AppointmentService.PackOutDate == null)) appointment.AppointmentService.PackOutDate = appointment.AppointmentService.PackOutTime = appointment.AppointmentService.DurationPackOut = string.Empty;
                    if (!appointment.AppointmentService.ServiceEntsorgung || (appointment.AppointmentService.EntsorgungDate == null)) appointment.AppointmentService.EntsorgungDate = appointment.AppointmentService.EntsorgungTime = appointment.AppointmentService.DurationEntsorgung = string.Empty;
                    if (!appointment.AppointmentService.ServiceReinigung || (appointment.AppointmentService.ReinigungDate == null)) appointment.AppointmentService.ReinigungDate = appointment.AppointmentService.ReinigungTime = appointment.AppointmentService.DateReinigungRelease = appointment.AppointmentService.TimeReinigungRelease = appointment.AppointmentService.DurationReinigung = string.Empty;
                    if (!appointment.AppointmentService.ServiceReinigung2 || (appointment.AppointmentService.Reinigung2Date == null)) appointment.AppointmentService.Reinigung2Date = appointment.AppointmentService.Reinigung2Time = appointment.AppointmentService.DateReinigung2Release = appointment.AppointmentService.TimeReinigung2Release = appointment.AppointmentService.DurationReinigung2 = string.Empty;
                    if (!appointment.AppointmentService.ServiceTransport || (appointment.AppointmentService.TransportDate == null)) appointment.AppointmentService.TransportDate = appointment.AppointmentService.TransportTime = appointment.AppointmentService.DurationTransport = string.Empty;
                    if (!appointment.AppointmentService.ServiceLagerung || (appointment.AppointmentService.LagerungDate == null)) appointment.AppointmentService.LagerungDate = appointment.AppointmentService.LagerungTime = appointment.AppointmentService.DurationLagerung = string.Empty;

                    // correct mismatched time format strings
                    appointment.AppointmentService.UmzugTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.UmzugTime);
                    appointment.AppointmentService.PackTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.PackTime);
                    appointment.AppointmentService.PackOutTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.PackOutTime);
                    appointment.AppointmentService.ReinigungTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.ReinigungTime);
                    appointment.AppointmentService.Reinigung2Time = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.Reinigung2Time);
                    appointment.AppointmentService.EntsorgungTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.EntsorgungTime);
                    appointment.AppointmentService.TransportTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.TransportTime);
                    appointment.AppointmentService.LagerungTime = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentService.LagerungTime);
                    appointment.AppointmentService.UmzugDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.UmzugDate, appointment.AppointmentService.UmzugTime);
                    appointment.AppointmentService.UmzugDateTime2 = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.UmzugDate2, appointment.AppointmentService.UmzugTime2);
                    appointment.AppointmentService.UmzugDateTime3 = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.UmzugDate3, appointment.AppointmentService.UmzugTime3);
                    appointment.AppointmentService.PackDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.PackDate, appointment.AppointmentService.PackTime);
                    appointment.AppointmentService.PackOutDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.PackOutDate, appointment.AppointmentService.PackOutTime);
                    appointment.AppointmentService.EntsorgungDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.EntsorgungDate, appointment.AppointmentService.EntsorgungTime);
                    appointment.AppointmentService.ReinigungDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.ReinigungDate, appointment.AppointmentService.ReinigungTime);
                    appointment.AppointmentService.Reinigung2DateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.Reinigung2Date, appointment.AppointmentService.Reinigung2Time);
                    appointment.AppointmentService.TransportDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.TransportDate, appointment.AppointmentService.TransportTime);
                    appointment.AppointmentService.LagerungDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentService.LagerungDate, appointment.AppointmentService.LagerungTime);
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Delivery)
                {
                    // set default / emtpy value
                    appointment.OfferInnerId = 0;
                    appointment.AppointmentView = null;
                    appointment.AppointmentService = null;

                    if (appointment.AppointmentDelivery.DeliveryProduct == AppointmentDelivery.DeliveryProductType.Castle_Atelier)
                    {
                        appointment.AppointmentDelivery.Delivery = AppointmentDelivery.DeliveryType.Delivery;
                    }

                    // correct mismatched time format strings
                    appointment.AppointmentDelivery.DeliveryTimeFrom = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentDelivery.DeliveryTimeFrom);
                    appointment.AppointmentDelivery.DeliveryTimeTo = Helpers.Helpers.CorrectTimeFormat(appointment.AppointmentDelivery.DeliveryTimeTo);
                    appointment.AppointmentDelivery.DeliveryDateTime = Helpers.Helpers.ConvertStringToDateTime(appointment.AppointmentDelivery.DeliveryDate, appointment.AppointmentDelivery.DeliveryTimeFrom);
                }


                // email: unselect if no email-address is set
                if (appointment.EmailToCustomer && string.IsNullOrEmpty(appointment.EmailAddress))
                {
                    appointment.EmailToCustomer = false;
                }
                if (!appointment.EmailToCustomer && !string.IsNullOrEmpty(appointment.EmailAddress))
                {
                    appointment.EmailAddress = null;
                }

                CustomerManager customerManager = new CustomerManager();
                Customer customer = customerManager.GetCustomerById(appointment.CustomerId);

                // update GoogleCalendar Event
                try
                {
                    appointment = updateGoogleCalendarEvent(appointment, customer);
                }
                catch (Exception ex)
                {
                    var logManager = new LogManager();
                    logManager.LogError(ex);
                }

                // DB-Store: create/update
                AppointmentManager manager = new AppointmentManager();
                if (appointment.AppointmentId > 0)
                {
                    manager.EditAppointment(appointment);
                }
                else
                {
                    manager.CreateAppointment(appointment);
                }

                // Customer Stage-Id changes: contact-type if it is lead
                if (appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing && customer.ContactStage < ContactStageType.Visit)
                {
                    customerManager.UpdateContactStageType(customer, ContactStageType.Visit);
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
                {
                    if (customer.ContactStage < ContactStageType.Contract)
                    {
                        customerManager.UpdateContactStageType(customer, ContactStageType.Contract);
                    }
                    if (customer.ContractType == ContractType.Lead)
                    {
                        customerManager.UpdateContractType(customer, ContractType.Customer);
                    }
                    // set new stateid and releaseDate of offer
                    if (appointment.OfferInnerId > 0)
                    {
                        OfferManager offerManager = new OfferManager();
                        Offer offer = offerManager.GetOfferByInnerId(appointment.OfferInnerId, customer.AccountId);
                        if (offer != null)
                        {
                            offerManager.UpdateOfferState(offer, Offer.OfferState.Appointment_created);
                        }
                    }
                }

                // sent email
                if (model.Appointment.EmailToCustomer)
                {
                    SendAppointmentEmail(model.Appointment, customer, model.EmailAttachments);
                }

                return RedirectToAction("Index", new { customerId = customerId });
            }
            else
            {
                // validator invalid, preselect fields
                CustomerManager customerManager = new CustomerManager();
                Customer customer = customerManager.GetCustomerById(customerId, true);
                if (customer == null)
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }
                ViewBag.CustomerId = customerId;
                ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

                if (model.Appointment.AppointmentId <= 0)
                {
                    // -- Create new
                    ViewBag.SubTitle = "Capture new date";
                    ViewBag.SubmitText = "Create";
                }
                else
                {
                    // -- Edit
                    model.IsEdit = true;
                    ViewBag.SubTitle = "Date to edit";
                    ViewBag.SubmitText = "Save";
                }
            }

            return View(model);
        }

        // GET: Intranet/Customer/Details/Meet/Details/1/1
        public async Task<ActionResult> Details(int customerId, int id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id <= 0))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own appointment
            CustomerMeetViewModel model = new CustomerMeetViewModel();
            AppointmentManager manager = new AppointmentManager();
            model.Appointment = manager.GetAppointmentById(id);
            if ((model.Appointment == null) || (model.Appointment.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // set relational depencies to default value for rendering UI
            if (model.Appointment.AppointmentView == null) model.Appointment.AppointmentView = new AppointmentView { MeetPlace = AppointmentView.MeetPlaceType.At_the_customer };
            if (model.Appointment.AppointmentService == null) model.Appointment.AppointmentService = new AppointmentService();
            if (model.Appointment.AppointmentDelivery == null) model.Appointment.AppointmentDelivery = new AppointmentDelivery { DeliveryProduct = AppointmentDelivery.DeliveryProductType.Packing_Material, Delivery = AppointmentDelivery.DeliveryType.Delivery };

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);
            model.IsEditable = false;

            // -- Edit
            ViewBag.SubTitle = "View Appointment";

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(model.Appointment.CreateUserId);
                if (user != null)
                {
                    model.Appointment.CreateUserId = user.UserName;
                }
            }

            return View("Edit", model);
        }

        // delete. check if googlecalendar exists and delete it also
        // POST: Intranet/Customer/Details/Meet/Delete/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int customerId, int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            AppointmentManager manager = new AppointmentManager();
            Appointment appointment = manager.GetAppointmentById(id);

            if ((appointment == null) || (appointment.CustomerId != customerId) || (appointment.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // delete calendar event
            manager.CleanGoogleCalendar(appointment);

            manager.DeleteAppointment(id);
            return RedirectToAction("Index", new { customerId = customerId });
        }

        public JsonResult EmailTextView(int customerId, string viewdate, string viewtime)
        {
            var appointment = new Appointment
            {
                AppointmentTypeId = Appointment.AppointmentType.Sightseeing,
                AppointmentView = new AppointmentView
                {
                    ViewDate = viewdate,
                    ViewTime = viewtime
                }
            };
            return Json(getEmailTextContainer(customerId, appointment), JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmailTextService(int customerId, string emailnote, bool? umzug, string umzugdate, string umzugtime, bool? umzugmoredates, string umzugdate2, string umzugtime2, string umzugdate3, string umzugtime3, bool? pack, string packdate, string packtime, bool? packout, string packoutdate, string packouttime, bool? disposal, string entsorgungdate, string entsorgungtime, bool? cleaning, string reinigungdate, string reinigungtime, string reinigungreleasedate, string reinigungreleasetime, bool? cleaning2, string reinigung2date, string reinigung2time, string reinigung2releasedate, string reinigung2releasetime, bool? lagerung, string lagerungdate, string lagerungtime, bool? transport, string transportdate, string transporttime)
        {
            var appointment = new Appointment
            {
                AppointmentTypeId = Appointment.AppointmentType.Confirmation_of_the_order,
                EmailNote = emailnote,
                AppointmentService = new AppointmentService
                {
                    ServiceUmzug = umzug ?? false,
                    UmzugDate = umzugdate,
                    UmzugTime = umzugtime,
                    DateUmzugMoreAppointment = umzugmoredates ?? false,
                    UmzugDate2 = umzugdate2,
                    UmzugTime2 = umzugtime2,
                    UmzugDate3 = umzugdate3,
                    UmzugTime3 = umzugtime3,
                    ServicePack = pack ?? false,
                    PackDate = packdate,
                    PackTime = packtime,
                    ServicePackOut = packout ?? false,
                    PackOutDate = packoutdate,
                    PackOutTime = packouttime,
                    ServiceEntsorgung = disposal ?? false,
                    EntsorgungDate = entsorgungdate,
                    EntsorgungTime = entsorgungtime,
                    ServiceReinigung = cleaning ?? false,
                    ReinigungDate = reinigungdate,
                    ReinigungTime = reinigungtime,
                    DateReinigungRelease = reinigungreleasedate,
                    TimeReinigungRelease = reinigungreleasetime,
                    ServiceReinigung2 = cleaning2 ?? false,
                    Reinigung2Date = reinigung2date,
                    Reinigung2Time = reinigung2time,
                    DateReinigung2Release = reinigung2releasedate,
                    TimeReinigung2Release = reinigung2releasetime,
                    ServiceTransport = transport ?? false,
                    TransportDate = transportdate,
                    TransportTime = transporttime,
                    ServiceLagerung = lagerung ?? false,
                    LagerungDate = lagerungdate,
                    LagerungTime = lagerungtime
                }
            };
            return Json(getEmailTextContainer(customerId, appointment), JsonRequestBehavior.AllowGet);
        }

        public JsonResult EmailTextDelivery(int customerId, AppointmentDelivery.DeliveryProductType deliveryProduct, AppointmentDelivery.DeliveryType deliveryType, string deliverydate, string deliverytimefrom, string deliverytimeto)
        {
            var appointment = new Appointment
            {
                AppointmentTypeId = Appointment.AppointmentType.Delivery,
                AppointmentDelivery = new AppointmentDelivery
                {
                    DeliveryProduct = deliveryProduct,
                    Delivery = deliveryType,
                    DeliveryDate = deliverydate,
                    DeliveryTimeFrom = deliverytimefrom,
                    DeliveryTimeTo = deliverytimeto
                }
            };
            return Json(getEmailTextContainer(customerId, appointment), JsonRequestBehavior.AllowGet);
        }

        private EmailTextContainer getEmailTextContainer(int customerId, Appointment appointment)
        {
            var emailTextContainer = new EmailTextContainer();
            var customer = (new CustomerManager()).GetCustomerById(customerId, true);
            var account = (new AccountManager()).GetAccountById(customer.AccountId);
            var customerManager = new CustomerManager();
            var emailManager = new EmailManager();

            if (account.InnerAccountId <= 0) return null;

            // Delivery: define default variables
            var deliveryKeyAccessor = string.Empty;
            if (appointment.AppointmentTypeId == Appointment.AppointmentType.Delivery)
            {
                deliveryKeyAccessor = $"{appointment.AppointmentDelivery.DeliveryProduct.ToString()}{(appointment.AppointmentDelivery.DeliveryProduct == AppointmentDelivery.DeliveryProductType.Packing_Material ? $".{appointment.AppointmentDelivery.Delivery.ToString()}" : string.Empty)}";
            }

            // subject
            switch (appointment.AppointmentTypeId)
            {
                case Appointment.AppointmentType.Sightseeing:
                {
                    var emailType = emailManager.GetEmailTypeByAccountId(customer.AccountId, "ViewAppointment");
                    emailTextContainer.Subject = !string.IsNullOrEmpty(emailType?.MailSubject) ? emailType.MailSubject : $"Appoinment Confirmation - {account.AccountName}";
                    break;
                }
                case Appointment.AppointmentType.Confirmation_of_the_order:
                {
                    // personal email subject-title
                    var emailType = emailManager.GetEmailTypeByAccountId(customer.AccountId, "ServiceAppointment");
                    emailTextContainer.Subject = !string.IsNullOrEmpty(emailType?.MailSubject) ? emailType.MailSubject : $"Confirmation_of_the_order - {account.AccountName}";
                    break;
                }
                case Appointment.AppointmentType.Delivery:
                {
                    var emailType = emailManager.GetEmailTypeByAccountId(customer.AccountId, $"DeliveryAppointment.{deliveryKeyAccessor}");
                    emailTextContainer.Subject = !string.IsNullOrEmpty(emailType?.MailSubject)
                        ? emailType.MailSubject
                        : $"{(appointment.AppointmentDelivery.DeliveryProduct == AppointmentDelivery.DeliveryProductType.Packing_Material ? "Delivery" : "Your Date_of_Delivery")} - {account.AccountName}";
                    break;
                }
            }

            // body
            var mainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = $"{account.Zip} {account.City}",
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            switch (appointment.AppointmentTypeId)
            {
                case Appointment.AppointmentType.Sightseeing:
                {
                    var appointmentViewMail = new AppointmentViewMail
                    {
                        MainMailData = mainMailData,
                        Date = appointment.AppointmentView.ViewDate,
                        Time = appointment.AppointmentView.ViewTime
                    };

                    emailTextContainer.Body = this.RenderPartialViewToString($"{appointmentViewMail.MainMailData.PathAccount}/text/ViewAppointment.cshtml", appointmentViewMail);
                    break;
                }
                case Appointment.AppointmentType.Confirmation_of_the_order:
                {
                        var appointmentServiceMail = new AppointmentServiceMail
                        {
                            MainMailData = mainMailData
                        };

                        if (appointment.AppointmentService.ServiceUmzug)
                        {
                            appointmentServiceMail.DateUmzug = appointment.AppointmentService.UmzugDate;
                            appointmentServiceMail.TimeUmzug = appointment.AppointmentService.UmzugTime;
                            if (appointment.AppointmentService.DateUmzugMoreAppointment)
                            {
                                appointmentServiceMail.DateUmzug2 = appointment.AppointmentService.UmzugDate2;
                                appointmentServiceMail.TimeUmzug2 = appointment.AppointmentService.UmzugTime2;
                                appointmentServiceMail.DateUmzug3 = appointment.AppointmentService.UmzugDate3;
                                appointmentServiceMail.TimeUmzug3 = appointment.AppointmentService.UmzugTime3;
                            }
                        }
                        if (appointment.AppointmentService.ServicePack)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.PackDate))
                            {
                                appointmentServiceMail.DatePack = appointment.AppointmentService.PackDate;
                                appointmentServiceMail.TimePack = appointment.AppointmentService.PackTime;
                            }
                            else
                            {
                                appointmentServiceMail.DatePack = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimePack = appointment.AppointmentService.UmzugTime;
                            }
                        }
                        if (appointment.AppointmentService.ServicePackOut)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.PackOutDate))
                            {
                                appointmentServiceMail.DatePackOut = appointment.AppointmentService.PackOutDate;
                                appointmentServiceMail.TimePackOut = appointment.AppointmentService.PackOutTime;
                            }
                            else
                            {
                                appointmentServiceMail.DatePackOut = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimePackOut = appointment.AppointmentService.UmzugTime;
                            }
                        }
                        if (appointment.AppointmentService.ServiceEntsorgung)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.EntsorgungDate))
                            {
                                appointmentServiceMail.DateEntsorgung = appointment.AppointmentService.EntsorgungDate;
                                appointmentServiceMail.TimeEntsorgung = appointment.AppointmentService.EntsorgungTime;
                            }
                            else
                            {
                                appointmentServiceMail.DateEntsorgung = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimeEntsorgung = appointment.AppointmentService.UmzugTime;
                            }
                        }
                        if (appointment.AppointmentService.ServiceReinigung)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.ReinigungDate))
                            {
                                appointmentServiceMail.DateReinigung = appointment.AppointmentService.ReinigungDate;
                                appointmentServiceMail.TimeReinigung = appointment.AppointmentService.ReinigungTime;
                            }
                            else
                            {
                                appointmentServiceMail.DateReinigung = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimeReinigung = appointment.AppointmentService.UmzugTime;
                            }
                            appointmentServiceMail.DateReinigungRelease = appointment.AppointmentService.DateReinigungRelease;
                            appointmentServiceMail.TimeReinigungRelease = appointment.AppointmentService.TimeReinigungRelease;
                        }
                        if (appointment.AppointmentService.ServiceReinigung2)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.Reinigung2Date))
                        {
                            appointmentServiceMail.DateReinigung2 = appointment.AppointmentService.Reinigung2Date;
                            appointmentServiceMail.TimeReinigung2 = appointment.AppointmentService.Reinigung2Time;
                        }
                        else
                        {
                            appointmentServiceMail.DateReinigung2 = appointment.AppointmentService.UmzugDate;
                            appointmentServiceMail.TimeReinigung2 = appointment.AppointmentService.UmzugTime;
                        }
                        appointmentServiceMail.DateReinigung2Release = appointment.AppointmentService.DateReinigung2Release;
                        appointmentServiceMail.TimeReinigung2Release = appointment.AppointmentService.TimeReinigung2Release;
                    }
                        if (appointment.AppointmentService.ServiceTransport)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.TransportDate))
                            {
                                appointmentServiceMail.DateTransport = appointment.AppointmentService.TransportDate;
                                appointmentServiceMail.TimeTransport = appointment.AppointmentService.TransportTime;
                            }
                            else
                            {
                                appointmentServiceMail.DateTransport = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimeTransport = appointment.AppointmentService.UmzugTime;
                            }
                        }
                        if (appointment.AppointmentService.ServiceLagerung)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.LagerungDate))
                            {
                                appointmentServiceMail.DateLagerung = appointment.AppointmentService.LagerungDate;
                                appointmentServiceMail.TimeLagerung = appointment.AppointmentService.LagerungTime;
                            }
                            else
                            {
                                appointmentServiceMail.DateLagerung = appointment.AppointmentService.UmzugDate;
                                appointmentServiceMail.TimeLagerung = appointment.AppointmentService.UmzugTime;
                            }
                        }
                        if (!string.IsNullOrEmpty(appointment.EmailNote))
                        {
                            appointmentServiceMail.Comment = appointment.EmailNote;
                        }

                        emailTextContainer.Body = this.RenderPartialViewToString($"{appointmentServiceMail.MainMailData.PathAccount}/text/ServiceAppointment.cshtml", appointmentServiceMail);
                        break;
                    }
                case Appointment.AppointmentType.Delivery:
                {
                    var appointmentDeliveryMail = new AppointmentDeliveryMail
                    {
                        MainMailData = mainMailData,
                        Date = appointment.AppointmentDelivery.DeliveryDate,
                        TimeFrom = appointment.AppointmentDelivery.DeliveryTimeFrom,
                        TimeTo = appointment.AppointmentDelivery.DeliveryTimeTo
                    };

                    emailTextContainer.Body = this.RenderPartialViewToString($"{appointmentDeliveryMail.MainMailData.PathAccount}/text/DeliveryAppointment-{deliveryKeyAccessor.Replace('.', '-')}.cshtml", appointmentDeliveryMail);
                    break;
                }
            }

            return emailTextContainer;
        }

        public bool SendAppointmentEmail(Appointment appointment, Customer customer, List<CheckListViewModel> emailAttachments)
        {
            Account account = (new AccountManager()).GetAccountById(appointment.AccountId);

            if (account.InnerAccountId > 0)
            {
                EmailManager emailManager = new EmailManager();

                // email attachments
                List<Attachment> attachments = null;
                if (emailAttachments != null)
                {
                    IEnumerable<string> tempSelectAttachments = emailAttachments.Where(b => b.Selected).Select(s => s.Name);
                    attachments = emailManager.GetAttachmentofModel(tempSelectAttachments, customer.AccountId, account.InnerAccountId);
                }

                if (appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing)
                {
                    attachments = addCalendarIcsToAttachment(attachments, "Sightseeing", account.AccountName, appointment.Where, appointment.AppointmentView.ViewDate, appointment.AppointmentView.ViewTime);
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
                {
                    if (appointment.AppointmentService.ServiceUmzug)
                    {
                        attachments = addCalendarIcsToAttachment(attachments, "Move", account.AccountName, appointment.Where, appointment.AppointmentService.UmzugDate, appointment.AppointmentService.UmzugTime, appointment.AppointmentService.DurationUmzug);
                        if (appointment.AppointmentService.DateUmzugMoreAppointment)
                        {
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.UmzugDate2) && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugTime2))
                            {
                                attachments = addCalendarIcsToAttachment(attachments, "Move-Tag2", account.AccountName, appointment.Where, appointment.AppointmentService.UmzugDate2, appointment.AppointmentService.UmzugTime2, appointment.AppointmentService.DurationUmzug2);
                            }
                            if (!string.IsNullOrEmpty(appointment.AppointmentService.UmzugDate3) && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugTime3))
                            {
                                attachments = addCalendarIcsToAttachment(attachments, "Move-Tag3", account.AccountName, appointment.Where, appointment.AppointmentService.UmzugDate3, appointment.AppointmentService.UmzugTime3, appointment.AppointmentService.DurationUmzug3);
                            }
                        }
                    }
                    if (appointment.AppointmentService.ServicePack)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.PackDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "packing_service", account.AccountName, appointment.Where, appointment.AppointmentService.PackDate, appointment.AppointmentService.PackTime, appointment.AppointmentService.DurationPack);
                        }
                    }
                    if (appointment.AppointmentService.ServicePackOut)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.PackOutDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "unpacking_service", account.AccountName, appointment.Where, appointment.AppointmentService.PackOutDate, appointment.AppointmentService.PackOutTime, appointment.AppointmentService.DurationPackOut);
                        }
                    }
                    if (appointment.AppointmentService.ServiceEntsorgung)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.EntsorgungDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "disposal", account.AccountName, appointment.Where, appointment.AppointmentService.EntsorgungDate, appointment.AppointmentService.EntsorgungTime, appointment.AppointmentService.DurationEntsorgung);
                        }
                    }
                    if (appointment.AppointmentService.ServiceReinigung)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.ReinigungDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "cleaning", account.AccountName, appointment.Where, appointment.AppointmentService.ReinigungDate, appointment.AppointmentService.ReinigungTime, appointment.AppointmentService.DurationReinigung);
                        }
                    }
                    if (appointment.AppointmentService.ServiceReinigung2)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.Reinigung2Date))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "cleaning-2", account.AccountName, appointment.Where, appointment.AppointmentService.Reinigung2Date, appointment.AppointmentService.Reinigung2Time, appointment.AppointmentService.DurationReinigung2);
                        }
                    }
                    if (appointment.AppointmentService.ServiceTransport)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.TransportDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "transport", account.AccountName, appointment.Where, appointment.AppointmentService.TransportDate, appointment.AppointmentService.TransportTime);
                        }
                    }
                    if (appointment.AppointmentService.ServiceLagerung)
                    {
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.LagerungDate))
                        {
                            attachments = addCalendarIcsToAttachment(attachments, "Storage", account.AccountName, appointment.Where, appointment.AppointmentService.LagerungDate, appointment.AppointmentService.LagerungTime);
                        }
                    }
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Delivery)
                {
                    // create all day event, if no time is set
                    if (string.IsNullOrEmpty(appointment.AppointmentDelivery.DeliveryTimeFrom))
                    {
                        attachments = addAllDayCalendarIcsToAttachment(attachments, "Delivery", account.AccountName, appointment.Where, appointment.AppointmentDelivery.DeliveryDate);
                    }
                    else
                    {
                        attachments = addRangeCalendarIcsToAttachment(attachments, "Delivery", account.AccountName, appointment.Where, appointment.AppointmentDelivery.DeliveryDate, appointment.AppointmentDelivery.DeliveryTimeFrom, appointment.AppointmentDelivery.DeliveryDate, appointment.AppointmentDelivery.DeliveryTimeTo, 1);
                    }
                }

                // email subject and bodytext: load from model if user has changed the text, else initialize it
                string subject = null;
                string body = null;
                // if user has changed text
                if (appointment.ChangeEmailText)
                {
                    if (!string.IsNullOrWhiteSpace(appointment.ChangeEmailTextContainer?.Subject))
                    {
                        subject = appointment.ChangeEmailTextContainer.Subject;
                    }
                    if (!string.IsNullOrWhiteSpace(appointment.ChangeEmailTextContainer?.Body))
                    {
                        body = appointment.ChangeEmailTextContainer.Body;
                    }
                }
                // else initialize new
                if (string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(body))
                {
                    var emailTextContainer = getEmailTextContainer(customer.CustomerId, appointment);
                    subject = subject ?? emailTextContainer.Subject;
                    body = body ?? emailTextContainer.Body;
                }

                // send email (multiple receiver possible)
                bool isSuccessful = true;
                EmailSender emailSender = (new EmailSenderFactory()).CreateEmailSender(account.AccountId.ToString());
                foreach (var receiverEmail in appointment.EmailAddress.Replace(',', ';').Split(';'))
                {
                    // && isSuccessful: if there is an error on multiple receiver-version, then it is false. so let variable always false, that at the end false will be returned
                    isSuccessful = emailSender.SendEmailFromSales(receiverEmail, subject, body, attachments) && isSuccessful;
                }

                return isSuccessful;
            }
            return false;
        }

        private List<Attachment> addCalendarIcsToAttachment(List<Attachment> attachmentList, string title, string accountName, string location, string startDate, string startTime, string duration = null)
        {
            // parse dates from string
            CalendarManager calendarManager = new CalendarManager();
            DateTime[] parsedDates = calendarManager.ParseFromString(startDate, startTime, calendarManager.AvgDuration(duration));

            return getIcsCalendarAsAttachments(attachmentList, title, accountName, location, parsedDates);
        }

        private List<Attachment> addRangeCalendarIcsToAttachment(List<Attachment> attachmentList, string title, string accountName, string location, string startDate, string startTime, string endDate = null, string endTime = null, int defaultHourOnEmptyEndDate = 1)
        {
            // parse dates from string
            CalendarManager calendarManager = new CalendarManager();
            DateTime[] parsedDates = calendarManager.ParseFromString(startDate, startTime, endDate, endTime, defaultHourOnEmptyEndDate);

            return getIcsCalendarAsAttachments(attachmentList, title, accountName, location, parsedDates);
        }

        private List<Attachment> addAllDayCalendarIcsToAttachment(List<Attachment> attachmentList, string title, string accountName, string location, string startDate)
        {
            // parse dates from string
            CalendarManager calendarManager = new CalendarManager();
            var startDateTime = calendarManager.ParseFromString(startDate, null);
            var parsedDates = new DateTime[] { startDateTime, startDateTime.AddDays(1).AddSeconds(-1) };

            return getIcsCalendarAsAttachments(attachmentList, title, accountName, location, parsedDates);
        }

        private List<Attachment> getIcsCalendarAsAttachments(List<Attachment> attachmentList, string title, string accountName, string location, DateTime[] calenderDateTimes)
        {
            if (attachmentList == null)
            {
                attachmentList = new List<Attachment>();
            }

            var subject = $"{title} - {accountName}";

            // parse dates from string
            CalendarManager calendarManager = new CalendarManager();
            if (calenderDateTimes[0] != null && calenderDateTimes[0] != DateTime.MinValue)
            {
                attachmentList.Add(calendarManager.GetIcsCalendarAsAttachment(subject, location, calenderDateTimes[0], calenderDateTimes[1], $"Kalendereintrag-{title}.ics"));
            }

            return attachmentList;
        }

        private Appointment updateGoogleCalendarEvent(Appointment appointment, Customer customer)
        {
            // validate for calendar-Create or email-sent
            bool calendarChanged = false;
            bool calendarChangedView = false;
            bool calendarChangedUmzug = false;
            bool calendarChangedUmzug2 = false;
            bool calendarChangedUmzug3 = false;
            bool calendarChangedPack = false;
            bool calendarChangedPackOut = false;
            bool calendarChangedEntsorgung = false;
            bool calendarChangedReinigung = false;
            bool calendarChangedReinigung2 = false;
            bool calendarChangedLagerung = false;
            bool calendarChangedTransport = false;
            bool calendarChangedDelivery = false;
            switch (appointment.AppointmentTypeId)
            {
                case Appointment.AppointmentType.Sightseeing:
                    calendarChanged = calendarChangedView = true;
                    break;
                case Appointment.AppointmentType.Confirmation_of_the_order:
                    calendarChanged = calendarChangedUmzug = calendarChangedUmzug2 = calendarChangedUmzug3 = calendarChangedPack = calendarChangedPackOut = calendarChangedEntsorgung = calendarChangedReinigung = calendarChangedReinigung2 = calendarChangedLagerung = calendarChangedTransport = true;
                    break;
                case Appointment.AppointmentType.Delivery:
                    calendarChanged = calendarChangedDelivery = true;
                    break;
                default:
                    break;
            }

            AppointmentManager manager = new AppointmentManager();
            if (appointment.AppointmentId > 0)
            {
                // edit
                calendarChanged = false;
                calendarChangedView = calendarChangedUmzug = calendarChangedUmzug2 = calendarChangedUmzug3 = calendarChangedPack = calendarChangedPackOut = calendarChangedEntsorgung = calendarChangedReinigung = calendarChangedReinigung2 = calendarChangedLagerung = calendarChangedTransport = calendarChangedDelivery = false;

                Appointment dbAppointment = manager.GetAppointmentById(appointment.AppointmentId);
                if (appointment.AppointmentTypeId == Appointment.AppointmentType.Sightseeing)
                {
                    if ((appointment.AppointmentView.ViewDate != dbAppointment.AppointmentView.ViewDate) || (appointment.AppointmentView.ViewTime != dbAppointment.AppointmentView.ViewTime))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedView = true;
                    }
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
                {
                    if ((appointment.AppointmentService.UmzugDate != dbAppointment.AppointmentService.UmzugDate) || (appointment.AppointmentService.UmzugTime != dbAppointment.AppointmentService.UmzugTime)
                        || (appointment.AppointmentService.DurationUmzug != dbAppointment.AppointmentService.DurationUmzug) || (appointment.AppointmentService.CountPersonUmzug != dbAppointment.AppointmentService.CountPersonUmzug)
                        || (appointment.AppointmentService.CountVehUmzug != dbAppointment.AppointmentService.CountVehUmzug) || (appointment.AppointmentService.CountTrailerUmzug != dbAppointment.AppointmentService.CountTrailerUmzug))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedUmzug = true;
                    }
                    if ((appointment.AppointmentService.UmzugDate2 != dbAppointment.AppointmentService.UmzugDate2) || (appointment.AppointmentService.UmzugTime2 != dbAppointment.AppointmentService.UmzugTime2)
                        || (appointment.AppointmentService.DurationUmzug2 != dbAppointment.AppointmentService.DurationUmzug2) || (appointment.AppointmentService.CountPersonUmzug2 != dbAppointment.AppointmentService.CountPersonUmzug2)
                        || (appointment.AppointmentService.CountVehUmzug2 != dbAppointment.AppointmentService.CountVehUmzug2) || (appointment.AppointmentService.CountTrailerUmzug2 != dbAppointment.AppointmentService.CountTrailerUmzug2))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedUmzug2 = true;
                    }
                    if ((appointment.AppointmentService.UmzugDate3 != dbAppointment.AppointmentService.UmzugDate3) || (appointment.AppointmentService.UmzugTime3 != dbAppointment.AppointmentService.UmzugTime3) || (appointment.AppointmentService.DurationUmzug3 != dbAppointment.AppointmentService.DurationUmzug3)
                        || (appointment.AppointmentService.CountPersonUmzug3 != dbAppointment.AppointmentService.CountPersonUmzug3) || (appointment.AppointmentService.CountVehUmzug3 != dbAppointment.AppointmentService.CountVehUmzug3) || (appointment.AppointmentService.CountTrailerUmzug3 != dbAppointment.AppointmentService.CountTrailerUmzug3))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedUmzug3 = true;
                    }
                    if ((appointment.AppointmentService.PackDate != dbAppointment.AppointmentService.PackDate) || (appointment.AppointmentService.PackTime != dbAppointment.AppointmentService.PackTime) || (appointment.AppointmentService.DurationPack != dbAppointment.AppointmentService.DurationPack)
                        || (appointment.AppointmentService.CountPersonPack != dbAppointment.AppointmentService.CountPersonPack) || (appointment.AppointmentService.CountVehPack != dbAppointment.AppointmentService.CountVehPack) || (appointment.AppointmentService.CountTrailerPack != dbAppointment.AppointmentService.CountTrailerPack))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedPack = true;
                    }
                    if ((appointment.AppointmentService.PackOutDate != dbAppointment.AppointmentService.PackOutDate) || (appointment.AppointmentService.PackOutTime != dbAppointment.AppointmentService.PackOutTime) || (appointment.AppointmentService.DurationPackOut != dbAppointment.AppointmentService.DurationPackOut)
                        || (appointment.AppointmentService.CountPersonPackOut != dbAppointment.AppointmentService.CountPersonPackOut) || (appointment.AppointmentService.CountVehPackOut != dbAppointment.AppointmentService.CountVehPackOut) || (appointment.AppointmentService.CountTrailerPackOut != dbAppointment.AppointmentService.CountTrailerPackOut))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedPackOut = true;
                    }
                    if ((appointment.AppointmentService.EntsorgungDate != dbAppointment.AppointmentService.EntsorgungDate) || (appointment.AppointmentService.EntsorgungTime != dbAppointment.AppointmentService.EntsorgungTime) || (appointment.AppointmentService.DurationEntsorgung != dbAppointment.AppointmentService.DurationEntsorgung)
                        || (appointment.AppointmentService.CountPersonEntsorgung != dbAppointment.AppointmentService.CountPersonEntsorgung) || (appointment.AppointmentService.CountVehEntsorgung != dbAppointment.AppointmentService.CountVehEntsorgung) || (appointment.AppointmentService.CountTrailerEntsorgung != dbAppointment.AppointmentService.CountTrailerEntsorgung))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedEntsorgung = true;
                    }
                    if ((appointment.AppointmentService.ReinigungDate != dbAppointment.AppointmentService.ReinigungDate) || (appointment.AppointmentService.ReinigungTime != dbAppointment.AppointmentService.ReinigungTime) || (appointment.AppointmentService.DateReinigungRelease != dbAppointment.AppointmentService.DateReinigungRelease) || (appointment.AppointmentService.TimeReinigungRelease != dbAppointment.AppointmentService.TimeReinigungRelease) || (appointment.AppointmentService.DurationReinigung != dbAppointment.AppointmentService.DurationReinigung))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedReinigung = true;
                    }
                    if ((appointment.AppointmentService.Reinigung2Date != dbAppointment.AppointmentService.Reinigung2Date) || (appointment.AppointmentService.Reinigung2Time != dbAppointment.AppointmentService.Reinigung2Time) || (appointment.AppointmentService.DateReinigung2Release != dbAppointment.AppointmentService.DateReinigung2Release) || (appointment.AppointmentService.TimeReinigung2Release != dbAppointment.AppointmentService.TimeReinigung2Release) || (appointment.AppointmentService.DurationReinigung2 != dbAppointment.AppointmentService.DurationReinigung2))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedReinigung2 = true;
                    }
                    if ((appointment.AppointmentService.TransportDate != dbAppointment.AppointmentService.TransportDate) || (appointment.AppointmentService.TransportTime != dbAppointment.AppointmentService.TransportTime) || (appointment.AppointmentService.DurationTransport != dbAppointment.AppointmentService.DurationTransport)
                        || (appointment.AppointmentService.CountPersonTransport != dbAppointment.AppointmentService.CountPersonTransport) || (appointment.AppointmentService.CountVehTransport != dbAppointment.AppointmentService.CountVehTransport) || (appointment.AppointmentService.CountTrailerTransport != dbAppointment.AppointmentService.CountTrailerTransport))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedTransport = true;
                    }
                    if ((appointment.AppointmentService.LagerungDate != dbAppointment.AppointmentService.LagerungDate) || (appointment.AppointmentService.LagerungTime != dbAppointment.AppointmentService.LagerungTime) || (appointment.AppointmentService.DurationLagerung != dbAppointment.AppointmentService.DurationLagerung))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedLagerung = true;
                    }
                }
                else if (appointment.AppointmentTypeId == Appointment.AppointmentType.Delivery)
                {
                    if ((appointment.AppointmentDelivery.DeliveryDate != dbAppointment.AppointmentDelivery.DeliveryDate) || (appointment.AppointmentDelivery.DeliveryTimeFrom != dbAppointment.AppointmentDelivery.DeliveryTimeFrom)
                        || (appointment.AppointmentDelivery.DeliveryTimeTo != dbAppointment.AppointmentDelivery.DeliveryTimeTo)
                        || (appointment.AppointmentDelivery.Delivery != dbAppointment.AppointmentDelivery.Delivery) || (appointment.AppointmentDelivery.DeliveryProduct != dbAppointment.AppointmentDelivery.DeliveryProduct))
                    {
                        // time changed
                        calendarChanged = true;
                        calendarChangedDelivery = true;
                    }
                }
            }

            if (calendarChanged)
            {
                GoogleCalendarManager googleCalendarManager = new GoogleCalendarManager(customer.AccountId);

                // AppointmentView
                if (calendarChangedView)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentView.GoogleEventViewId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentView.GoogleEventViewId) == string.Empty)
                        {
                            // successful deleted
                            appointment.AppointmentView.GoogleEventViewId = null;
                        }
                    }
                    if (!string.IsNullOrEmpty(appointment.AppointmentView.ViewDate) && !string.IsNullOrEmpty(appointment.AppointmentView.ViewTime))
                    {
                        Event viewEvent = googleCalendarManager.GetEventObject(getGoogleCalendarSubjectForView(appointment, customer), appointment.Where, appointment.CalendarNote, appointment.AppointmentView.ViewDate, appointment.AppointmentView.ViewTime);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentView.GoogleEventViewId = insertedEvent.Id;
                        }

                    }
                }

                // AppointmentService
                // umzug (append other services, if they are on the same day)
                if (calendarChangedUmzug)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventUmzugId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceUmzug && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugDate) && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugTime))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService(appointment.AppointmentService.BookedServices, appointment, customer, appointment.AppointmentService.DurationUmzug, appointment.AppointmentService.CountPersonUmzug, appointment.AppointmentService.CountVehUmzug, appointment.AppointmentService.CountTrailerUmzug, null, appointment.AppointmentService.UmzugWhereFrom, appointment.AppointmentService.UmzugWhereTo);
                        calendarTitle = calendarTitle.Replace("[StartTime]", appointment.AppointmentService.UmzugTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.UmzugDate, appointment.AppointmentService.DurationUmzug);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId = insertedEvent.Id;
                        }
                    }
                }
                if (calendarChangedUmzug2)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventUmzugId2))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId2) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId2 = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceUmzug && appointment.AppointmentService.DateUmzugMoreAppointment && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugDate2) && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugTime2))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("Move", appointment, customer, appointment.AppointmentService.DurationUmzug2, appointment.AppointmentService.CountPersonUmzug2, appointment.AppointmentService.CountVehUmzug2, appointment.AppointmentService.CountTrailerUmzug2);
                        calendarTitle = calendarTitle.Replace("[StartTime]", appointment.AppointmentService.UmzugTime2);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.UmzugDate2, appointment.AppointmentService.DurationUmzug2);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId2 = insertedEvent.Id;
                        }
                    }
                }
                if (calendarChangedUmzug3)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventUmzugId3))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId3) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId3 = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceUmzug && appointment.AppointmentService.DateUmzugMoreAppointment && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugDate3) && !string.IsNullOrEmpty(appointment.AppointmentService.UmzugTime3))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("Move", appointment, customer, appointment.AppointmentService.DurationUmzug3, appointment.AppointmentService.CountPersonUmzug3, appointment.AppointmentService.CountVehUmzug3, appointment.AppointmentService.CountTrailerUmzug3);
                        calendarTitle = calendarTitle.Replace("[StartTime]", appointment.AppointmentService.UmzugTime3);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.UmzugDate3, appointment.AppointmentService.DurationUmzug3);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventUmzugId3 = insertedEvent.Id;
                        }
                    }
                }
                // einpacken (remove first googleevent of only einpacken if exists. Then create only, if really selects and datefrom is not empty (else it can be in umzug date, or it could only be deleted))
                if (calendarChangedPack)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventPackId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventPackId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventPackId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServicePack && !string.IsNullOrEmpty(appointment.AppointmentService.PackDate))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("packing_service", appointment, customer, appointment.AppointmentService.DurationPack, appointment.AppointmentService.CountPersonPack, appointment.AppointmentService.CountVehPack, appointment.AppointmentService.CountTrailerPack);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.PackTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.PackDate, appointment.AppointmentService.DurationPack);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventPackId = insertedEvent.Id;
                        }
                    }
                }
                // auspacken (remove first googleevent of only auspacken if exists. Then create only, if really selects and datefrom is not empty (else it can be in umzug date, or it could only be deleted))
                if (calendarChangedPackOut)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventPackOutId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventPackOutId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventPackOutId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServicePackOut && !string.IsNullOrEmpty(appointment.AppointmentService.PackOutDate))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("unpacking_service", appointment, customer, appointment.AppointmentService.DurationPackOut, appointment.AppointmentService.CountPersonPackOut, appointment.AppointmentService.CountVehPackOut, appointment.AppointmentService.CountTrailerPackOut);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.PackOutTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.PackOutDate, appointment.AppointmentService.DurationPackOut);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventPackOutId = insertedEvent.Id;
                        }
                    }
                }
                // cleaning (same like einpacken)
                if (calendarChangedReinigung)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventReinigungId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventReinigungId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventReinigungId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceReinigung && !string.IsNullOrEmpty(appointment.AppointmentService.ReinigungDate))
                    {
                        string releaseDate = string.Empty;
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.ReinigungDate) && !string.IsNullOrEmpty(appointment.AppointmentService.DateReinigungRelease))
                        {
                            DateTime dtReinigungFrom = DateTime.MinValue;
                            DateTime dtReinigungRelease = DateTime.MinValue;
                            if (DateTime.TryParse(appointment.AppointmentService.ReinigungDate, out dtReinigungFrom) && DateTime.TryParse(appointment.AppointmentService.DateReinigungRelease, out dtReinigungRelease))
                            {
                                double durReinigung = (dtReinigungRelease - dtReinigungFrom).TotalDays;
                                if (durReinigung > 0) appointment.AppointmentService.DurationReinigung = ((int)(durReinigung * 24)).ToString();
                            }
                            releaseDate = string.Format("Deadline {0}, {1} O'clock", appointment.AppointmentService.DateReinigungRelease, appointment.AppointmentService.TimeReinigungRelease);
                        }
                        string calendarTitle = getGoogleCalendarSubjectForService("cleaning", appointment, customer, string.Empty, 0, 0, 0, releaseDate);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.ReinigungTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.ReinigungDate, appointment.AppointmentService.DurationReinigung);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventReinigungId = insertedEvent.Id;
                        }
                    }
                }
                // cleaning2 (same like einpacken)
                if (calendarChangedReinigung2)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventReinigung2Id))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventReinigung2Id) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventReinigung2Id = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceReinigung2 && !string.IsNullOrEmpty(appointment.AppointmentService.Reinigung2Date))
                    {
                        string releaseDate = string.Empty;
                        if (!string.IsNullOrEmpty(appointment.AppointmentService.Reinigung2Date) && !string.IsNullOrEmpty(appointment.AppointmentService.DateReinigung2Release))
                        {
                            DateTime dtReinigungFrom = DateTime.MinValue;
                            DateTime dtReinigungRelease = DateTime.MinValue;
                            if (DateTime.TryParse(appointment.AppointmentService.Reinigung2Date, out dtReinigungFrom) && DateTime.TryParse(appointment.AppointmentService.DateReinigung2Release, out dtReinigungRelease))
                            {
                                double durReinigung = (dtReinigungRelease - dtReinigungFrom).TotalDays;
                                if (durReinigung > 0) appointment.AppointmentService.DurationReinigung2 = ((int)(durReinigung * 24)).ToString();
                            }
                            releaseDate = string.Format("Deadline {0}, {1} O'clock", appointment.AppointmentService.DateReinigung2Release, appointment.AppointmentService.TimeReinigung2Release);
                        }
                        string calendarTitle = getGoogleCalendarSubjectForService("cleaning", appointment, customer, string.Empty, 0, 0, 0, releaseDate);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.Reinigung2Time);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.Reinigung2Date, appointment.AppointmentService.DurationReinigung2);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventReinigung2Id = insertedEvent.Id;
                        }
                    }
                }
                // disposal (same like einpacken)
                if (calendarChangedEntsorgung)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventEntsorgungId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventEntsorgungId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventEntsorgungId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceEntsorgung && !string.IsNullOrEmpty(appointment.AppointmentService.EntsorgungDate))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("disposal", appointment, customer, appointment.AppointmentService.DurationEntsorgung, appointment.AppointmentService.CountPersonEntsorgung, appointment.AppointmentService.CountVehEntsorgung, appointment.AppointmentService.CountTrailerEntsorgung);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.EntsorgungTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.EntsorgungDate, appointment.AppointmentService.DurationEntsorgung);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventEntsorgungId = insertedEvent.Id;
                        }
                    }
                }
                // transport (same like einpacken)
                if (calendarChangedTransport)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventTransportId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventTransportId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventTransportId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceTransport && !string.IsNullOrEmpty(appointment.AppointmentService.TransportDate))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("transport", appointment, customer, appointment.AppointmentService.DurationTransport, appointment.AppointmentService.CountPersonTransport, appointment.AppointmentService.CountVehTransport, appointment.AppointmentService.CountTrailerTransport, null, appointment.AppointmentService.TransportWhereFrom, appointment.AppointmentService.TransportWhereTo);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.TransportTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.TransportDate, appointment.AppointmentService.DurationTransport);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventTransportId = insertedEvent.Id;
                        }
                    }
                }
                // Storage (same like einpacken)
                if (calendarChangedLagerung)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentService.GoogleEventLagerungId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventLagerungId) == string.Empty)
                        {
                            appointment.AppointmentService.GoogleEventLagerungId = null;
                        }
                    }
                    // create only if really selected
                    if (appointment.AppointmentService.ServiceLagerung && !string.IsNullOrEmpty(appointment.AppointmentService.LagerungDate))
                    {
                        string calendarTitle = getGoogleCalendarSubjectForService("Storage", appointment, customer, appointment.AppointmentService.DurationLagerung);
                        calendarTitle = updateGoogleCalendarSubjectStartTime(calendarTitle, appointment.AppointmentService.LagerungTime);
                        Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentService.LagerungDate, appointment.AppointmentService.DurationLagerung);
                        Event insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        if (insertedEvent != null)
                        {
                            appointment.AppointmentService.GoogleEventLagerungId = insertedEvent.Id;
                        }
                    }
                }

                // AppointmentDelivery
                if (calendarChangedDelivery)
                {
                    if (!string.IsNullOrEmpty(appointment.AppointmentDelivery.GoogleEventDeliveryId))
                    {
                        if (googleCalendarManager.DeleteEvent(appointment.AppointmentDelivery.GoogleEventDeliveryId) == string.Empty)
                        {
                            // successful deleted
                            appointment.AppointmentDelivery.GoogleEventDeliveryId = null;
                        }
                    }
                    if (!string.IsNullOrEmpty(appointment.AppointmentDelivery.DeliveryDate))
                    {
                        Event insertedEvent = null;
                        var calendarTitle = getGoogleCalendarSubjectForDelivery(appointment, customer);
                        if (string.IsNullOrEmpty(appointment.AppointmentDelivery.DeliveryTimeFrom))
                        {
                            // create all day event
                            Event viewEvent = googleCalendarManager.GetEventAllDayObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentDelivery.DeliveryDate, null);
                            insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        }
                        else
                        {
                            // create event with start time
                            Event viewEvent = googleCalendarManager.GetEventObject(calendarTitle, appointment.Where, appointment.CalendarNote, appointment.AppointmentDelivery.DeliveryDate, appointment.AppointmentDelivery.DeliveryTimeFrom, appointment.AppointmentDelivery.DeliveryDate, appointment.AppointmentDelivery.DeliveryTimeTo);
                            insertedEvent = googleCalendarManager.InsertEvent(viewEvent);
                        }

                        if (insertedEvent != null)
                        {
                            appointment.AppointmentDelivery.GoogleEventDeliveryId = insertedEvent.Id;
                        }

                    }
                }
            }
            return appointment;
        }

        /// <summary>returns a placeholder for time</summary>
        /// <returns>returns a placeholder [StartTime] for time</returns>
        private string getGoogleCalendarSubjectForService(string selectedService, Appointment appointment, Customer customer, string duration, int countPers = 0, int countVeh = 0, int countTrailer = 0, string additionTime = null, string routeFrom = null, string routeTo = null)
        {
            CustomerManager cm = new CustomerManager();
            string subject = string.Format("{0}: {1}, {2}, [StartTime] O'clock", selectedService, cm.GetTitledName(customer), cm.GetPhonePrioMobile(customer));
            if (!string.IsNullOrEmpty(additionTime)) subject = $"{subject} / {additionTime}";
            if (countPers > 0 || countVeh > 0) subject = $"{subject} / {countPers} MA / {countVeh} Truck";
            if (countTrailer > 0) subject = $"{subject} / {countTrailer} Pendant";
            if (!string.IsNullOrEmpty(duration)) subject = $"{subject} / {duration}h";
            if (!string.IsNullOrEmpty(routeFrom) && !string.IsNullOrEmpty(routeTo)) subject = $"{subject} ({routeFrom} nach {routeTo})";
            if (!string.IsNullOrEmpty(appointment.CalendarTitleAddition)) subject = $"{subject} {appointment.CalendarTitleAddition}";

            return subject.Replace(", ,", ",");
        }

        private string updateGoogleCalendarSubjectStartTime(string subject, string timeFrom)
        {
            if (!string.IsNullOrEmpty(timeFrom)) subject = subject.Replace("[StartTime]", timeFrom);
            else subject = subject.Replace(", [StartTime] O'clock", string.Empty);

            return subject;
        }

        private string getGoogleCalendarSubjectForView(Appointment appointment, Customer customer)
        {
            CustomerManager cm = new CustomerManager();
            string subject = $"{customer.Zip} / Bes. {cm.GetTitledName(customer)}, {cm.GetPhoneAggregate(customer)}";
            if (!string.IsNullOrEmpty(appointment.CalendarTitleAddition)) subject = $"{subject} {appointment.CalendarTitleAddition}";
            if (appointment.AppointmentView.MeetPlace != AppointmentView.MeetPlaceType.At_the_customer) subject = string.Format("{0} ({1})", subject, appointment.AppointmentView.MeetPlace.ToString().Replace('_', ' '));

            return subject;
        }

        private string getGoogleCalendarSubjectForDelivery(Appointment appointment, Customer customer)
        {
            CustomerManager cm = new CustomerManager();
            string subject = $"{appointment.AppointmentDelivery.Delivery} {appointment.AppointmentDelivery.DeliveryProduct.ToString()}: {cm.GetTitledName(customer)}, {cm.GetPhoneAggregate(customer)}";
            if (!string.IsNullOrEmpty(appointment.CalendarTitleAddition)) subject = $"{subject} {appointment.CalendarTitleAddition}";

            return subject;
        }

#region Helpers

        private int calcIntTime(string time)
        {
            int calcedInt = 0;
            int.TryParse(time.Replace(":", ""), out calcedInt);
            return calcedInt;
        }

 #endregion
    }
}