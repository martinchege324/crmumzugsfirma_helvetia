﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerNoteController : Controller
    {
        // GET: Intranet/Customer/Details/Note/1
        public async Task<ActionResult> Index(int customerId)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            CustomerNoteViewModel model = await getModel(customerId);
            return View(model);
        }

        // POST: Intranet/Customer/Details/Note/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(CustomerNoteViewModel model, int customerId)
        {
            if (ModelState.IsValid)
            {
                NoteManager noteManager = new NoteManager();
                noteManager.CreateNote(model.NewNote);

                return RedirectToAction("Index", new { customerId = customerId });
            }

            model = await getModel(customerId);
            return View(model);
        }

        // POST: Intranet/Customer/Details/Note/Delete/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Delete(int customerId, int id)
        {
            NoteManager noteManager = new NoteManager();
            await noteManager.DeleteNote(id);
            return RedirectToAction("Index", new { customerId = customerId });
        }

        private async Task<CustomerNoteViewModel> getModel(int customerId)
        {
            NoteManager noteManager = new NoteManager();
            CustomerNoteViewModel model = new CustomerNoteViewModel();
            model.Notes = await noteManager.GetNotesById(customerId);
            // overwrite all userid with username to better readable
            model.Notes = await overwriteUserId(model.Notes);

            // create new note object
            model.NewNoteCreateUser = (Session["UserName"] != null ? Session["UserName"].ToString() : string.Empty);
            model.NewNote = new Note() { CustomerId = customerId };

            return model;
        }

        private async Task<List<Note>> overwriteUserId(List<Note> notes)
        {
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                Dictionary<string, string> tempCache = new Dictionary<string, string>();
                foreach (Note n in notes)
                {
                    if (tempCache.ContainsKey(n.CreateUserId))
                    {
                        n.CreateUserId = tempCache[n.CreateUserId];
                        continue;
                    }
                    var user = await userManager.FindByIdAsync(n.CreateUserId);
                    if (user != null)
                    {
                        tempCache.Add(n.CreateUserId, user.UserName);
                        n.CreateUserId = user.UserName;
                    }
                }
            }
            return notes;
        }

    }
}