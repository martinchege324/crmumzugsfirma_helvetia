﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.Managers.Pdf;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerReceiptController : Controller
    {
        // GET: Intranet/Customer/Details/Receipt/1
        public ActionResult Index(int customerId)
        {
            var customerManager = new CustomerManager();
            var customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = $"Customer: {(customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName)}";

            var manager = new ReceiptManager();
            return View(manager.GetReceiptsByCustomer(customerId));
        }


        // GET: Intranet/Customer/Details/Receipt/Edit/1/1
        // If Empty creates a new, else edit it
        public ActionResult Edit(int customerId, int? offerId, Guid? id, int? receiptTypeId, int? subServicePart)
        {
            var customerManager = new CustomerManager();
            var customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = $"Customer: {(customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName)}";

            List<SelectListItem> receiptStateList = createReceiptStateList();

            // create model
            var model = new CustomerReceiptViewModel();
            var receiptManager = new ReceiptManager();

            if (id == null)
            {
                // -- Create new
                ViewBag.SubTitle = "Neue Create a receipt";
                ViewBag.SubmitText = "Create";

                model.Receipt = new Receipt
                {
                    CustomerId = customerId,
                    AccountId = customer.AccountId,
                    ReceiptStateId = Receipt.ReceiptState.Open,
                    PayTypeId = PayType.Bar,
                    EmailAddress = customer.Email
                };

                // set type, if defined
                if (receiptTypeId.HasValue)
                {
                    model.Receipt.ReceiptTypeId = ((Receipt.ReceiptType) receiptTypeId.Value);
                    if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.Move)
                        model.ReceiptUmzug = new ReceiptUmzug
                        {
                            ReceiptUmzugId = Guid.NewGuid()
                        };
                    else if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.cleaning)
                        model.ReceiptReinigung = new ReceiptReinigung
                        {
                            ReceiptReinigungId = Guid.NewGuid()
                        };
                }
                else
                {
                    // set default: Move
                    model.Receipt.ReceiptTypeId = Receipt.ReceiptType.Move;
                    model.ReceiptUmzug = new ReceiptUmzug
                    {
                        ReceiptUmzugId = Guid.NewGuid()
                    };
                }

                // take data from offer
                if (offerId.HasValue && offerId.Value > 0)
                {
                    ViewBag.OfferId = offerId;

                    OfferManager offerManager = new OfferManager();
                    Offer offer = offerManager.GetOfferById(offerId.Value, true, customer.AccountId);
                    if (offer != null && offer.CustomerId == customer.CustomerId)
                    {
                        model.Receipt.OfferId = offer.OfferId;
                        model.Receipt.ReceiptInnerId = offer.OfferInnerId;
                        model.Receipt.ReceiptInnerSubId = receiptManager.GetNextPossibleReceiptInnerSubId(customer.AccountId, offerId.Value);
                        model.Receipt.PayTypeId = (offer.PaymentId == Offer.PaymentType.Bar) ? PayType.Bar : PayType.Invoice;
                        model.Receipt.CostInclTax = offer.CostInclTax;
                        model.Receipt.CostExclTax = offer.CostExclTax;
                        model.Receipt.CostFreeTax = offer.CostFreeTax;

                        // set header: customer details
                        if (customer.MarketId == MarketType.Private_person)
                        {
                            model.Receipt.CustomerLine1 = customer.TitleId.ToString();
                            model.Receipt.CustomerLine2 = $"{customer.LastName} {customer.FirstName}";
                            model.Receipt.NameSignatureContact = $"{customer.LastName} {customer.FirstName}";
                        }
                        else
                        {
                            model.Receipt.CustomerLine1 = customer.CompanyName;
                            model.Receipt.CustomerLine2 = customer.CompanyContactPerson;
                            model.Receipt.NameSignatureContact = customer.CompanyName;
                        }

                        model.Receipt.CustomerLine3 = customer.Street;
                        model.Receipt.CustomerLine4 = Helpers.Helpers.LocationDescription(customer.Zip, customer.City, customer.CountryCode);
                        string phone = customer.Phone;
                        if (!string.IsNullOrEmpty(customer.Mobile))
                        {
                            phone = $"{phone}{(!string.IsNullOrEmpty(phone) ? " / " : string.Empty)}{customer.Mobile}";
                        }
                        model.Receipt.CustomerLine5 = phone;

                        // set header: address
                        model.Receipt.AdrOut1Line1 = offer.AdrOutStreet;
                        model.Receipt.AdrOut1Line2 = Helpers.Helpers.LocationDescription(offer.AdrOutZip, offer.AdrOutCity, offer.AdrOutCountryCode);

                        if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.Move)
                        {
                            if (offer.AdrOut2)
                            {
                                model.Receipt.AdrOut2Line1 = offer.AdrOutStreet2;
                                model.Receipt.AdrOut2Line2 = Helpers.Helpers.LocationDescription(offer.AdrOutZip2, offer.AdrOutCity2, offer.AdrOutCountryCode2);
                            }
                            if (offer.AdrOut3)
                            {
                                model.Receipt.AdrOut3Line1 = offer.AdrOutStreet3;
                                model.Receipt.AdrOut3Line2 = Helpers.Helpers.LocationDescription(offer.AdrOutZip3, offer.AdrOutCity3, offer.AdrOutCountryCode3);
                            }
                            model.Receipt.AdrIn1Line1 = offer.AdrInStreet;
                            model.Receipt.AdrIn1Line2 = Helpers.Helpers.LocationDescription(offer.AdrInZip, offer.AdrInCity, offer.AdrInCountryCode);
                            if (offer.AdrIn2)
                            {
                                model.Receipt.AdrIn2Line1 = offer.AdrInStreet2;
                                model.Receipt.AdrIn2Line2 = Helpers.Helpers.LocationDescription(offer.AdrInZip2, offer.AdrInCity2, offer.AdrInCountryCode2);
                            }
                            if (offer.AdrIn3)
                            {
                                model.Receipt.AdrIn3Line1 = offer.AdrInStreet3;
                                model.Receipt.AdrIn3Line2 = Helpers.Helpers.LocationDescription(offer.AdrInZip3, offer.AdrInCity3, offer.AdrInCountryCode3);
                            }
                            // set header: date
                            model.Receipt.JobDate = offer.UmzugDate;
                            model.Receipt.JobTime = offer.UmzugTime;
                            if (DateTime.TryParse(model.Receipt.JobDate, out var tempDate))
                            {
                                model.Receipt.JobDateTime = tempDate;
                            }
                            // set services details
                            model.ReceiptUmzug.UmzugRateCost = offer.UmzugCostChangedPrice > 0 ? offer.UmzugCostChangedPrice.ToString() : string.Empty;
                            model.ReceiptUmzug.UmzugspesenCost = offerManager.GetSelectedAdditionalCosts(offer.OfferId, "umzug", "Expenses").FirstOrDefault()?.OverwrittenPrice.ToString();
                            model.ReceiptUmzug.UmzugWayCost = offer.UmzugCostWay > 0 ? offer.UmzugCostWay.ToString() : string.Empty;
                            model.ReceiptUmzug.UmzugPackCost = offer.PackMaterialCostSum;
                            model.ReceiptUmzug.EntsorgungRateCost = offer.EntsorgungCostChangedPrice > 0 ? offer.EntsorgungCostChangedPrice.ToString() : string.Empty;
                            if (offer.EntsorgungActive && !string.IsNullOrEmpty(offer.EntsorgungFixCost))
                            {
                                model.ReceiptUmzug.EntsorgungPauschalCost = offer.EntsorgungFixCost;
                            }
                            // additionals (zuschlag)
                            var umzugAdditionalCosts = offerManager.GetSelectedAdditionalCosts(offer.OfferId, "umzug").Where(s => string.IsNullOrEmpty(s.MetaTag)).ToList();
                            var additionNextFreeIdx = 1;
                            foreach (var offerPriceAdditCost in umzugAdditionalCosts)
                            {
                                additionNextFreeIdx = setAdditionalCosts(additionNextFreeIdx, model.ReceiptUmzug, offerPriceAdditCost.Descr, offerPriceAdditCost.OverwrittenPrice);
                            }
                            additionNextFreeIdx = setAdditionalCosts(additionNextFreeIdx, model.ReceiptUmzug, offer.UmzugCostAdditFreeTextPlus1, offer.UmzugCostAdditFreeTextPlus1Price);
                            additionNextFreeIdx = setAdditionalCosts(additionNextFreeIdx, model.ReceiptUmzug, offer.UmzugCostAdditFreeTextPlus2, offer.UmzugCostAdditFreeTextPlus2Price);
                            additionNextFreeIdx = setAdditionalCosts(additionNextFreeIdx, model.ReceiptUmzug, offer.EntsorgungCostAdditFreeTextPlus1, offer.EntsorgungCostAdditFreeTextPlus1Price);
                            additionNextFreeIdx = setAdditionalCosts(additionNextFreeIdx, model.ReceiptUmzug, offer.EntsorgungCostAdditFreeTextPlus2, offer.EntsorgungCostAdditFreeTextPlus2Price);
                            // additionalSubs (abzug)
                            additionNextFreeIdx = 1;
                            if (offer.UmzugCostDiscount > 0)
                                additionNextFreeIdx = setAdditionalSubCosts(additionNextFreeIdx, model.ReceiptUmzug, "Discount", offer.UmzugCostDiscount);
                            if (offer.UmzugCostDiscount2 > 0)
                                additionNextFreeIdx = setAdditionalSubCosts(additionNextFreeIdx, model.ReceiptUmzug, "Concession", offer.UmzugCostDiscount2);
                            additionNextFreeIdx = setAdditionalSubCosts(additionNextFreeIdx, model.ReceiptUmzug, offer.UmzugCostAdditFreeTextMinus1, offer.UmzugCostAdditFreeTextMinus1Price);

                            // Costfix or CostHigh
                            if (offer.UmzugCostFix && offer.UmzugCostFixPrice > 0)
                            {
                                model.Receipt.CostFix = offer.UmzugCostFixPrice.ToString();
                                model.ReceiptCostFixText = "Amount aus Move Package Price";
                            }
                            else if (offer.UmzugCostHighSecurity && offer.UmzugCostHighSecurityPrice > 0)
                            {
                                model.Receipt.CostHigh = offer.UmzugCostHighSecurityPrice.ToString();
                                model.ReceiptCostHighText = "Amount aus Move Cost_ceiling";
                            }
                            else if (offer.TransportActive && offer.OfferTransport != null)
                            {
                                if (offer.OfferTransport.TransportCostFix && offer.OfferTransport.TransportCostFixPrice > 0)
                                {
                                    model.Receipt.CostFix = offer.OfferTransport.TransportCostFixPrice.ToString();
                                    model.ReceiptCostFixText = "Amount aus transport Package Price";
                                }
                                else if (offer.OfferTransport.TransportCostHighSecurity && offer.OfferTransport.TransportCostHighSecurityPrice > 0)
                                {
                                    model.Receipt.CostHigh = offer.OfferTransport.TransportCostHighSecurityPrice.ToString();
                                    model.ReceiptCostHighText = "Amount aus transport Cost_ceiling";
                                }

                            }
                        }
                        else if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.cleaning && (offer.ReinigungActive || (subServicePart == 2 && offer.Reinigung2Active)))
                        {
                            OfferCleaning offerCleaning = offer.OfferCleaning;
                            if (subServicePart == 2)
                            {
                                offerCleaning = offer.OfferCleaning2;
                            }
                            model.Receipt.JobDate = offerCleaning.ReinigungDate;
                            model.Receipt.JobTime = offerCleaning.ReinigungTime;
                            model.ReceiptReinigung.AbgabeDate = offerCleaning.ReinigungDateCommit;
                            model.ReceiptReinigung.AbgabeTime = offerCleaning.ReinigungTimeCommit;

                            model.ReceiptReinigung.CleaningTypeText = offerCleaning.ReinigungType != CleaningType.Please_choose ? Helpers.Helpers.ReplaceEnumExtensionString(offerCleaning.ReinigungType.ToString()) : offerCleaning.ReinigungTypeFreeText;
                            model.ReceiptReinigung.WishedServices = offerCleaning.ReinigungWishedServiceFreeText;
                            // Tariff: Costfix
                            if (offerCleaning.ReinigungCostChangedPrice > 0)
                            {
                                model.ReceiptReinigung.ReinigungCost = offerCleaning.ReinigungCostSum;
                            }
                            model.ReceiptReinigung.Room = offerCleaning.ReinigungPriceRateDescr;
                            // Tariff: HourlyRate
                            if (offerCleaning.ReinigungHourlyCostChangedPrice > 0)
                            {
                                model.ReceiptReinigung.HourlyCostRate = offerCleaning.ReinigungHourlyCostChangedPrice.ToString();
                                model.ReceiptReinigung.Room = string.Empty;
                            }
                            // additionals (zuschlag)
                            var reinigungAdditionalCosts = offerManager.GetSelectedAdditionalCosts(offer.OfferId, "cleaning").Where(s => string.IsNullOrEmpty(s.MetaTag)).ToList();
                            var additionNextFreeIdx = 1;
                            foreach (var offerPriceAdditCost in reinigungAdditionalCosts)
                            {
                                additionNextFreeIdx = setAdditionalCostsCleaning(additionNextFreeIdx, model.ReceiptReinigung, offerPriceAdditCost.Descr, offerPriceAdditCost.OverwrittenPrice);
                            }
                            additionNextFreeIdx = setAdditionalCostsCleaning(additionNextFreeIdx, model.ReceiptReinigung, offerCleaning.ReinigungCostAdditFreeTextPlus1, offerCleaning.ReinigungCostAdditFreeTextPlus1Price);
                            additionNextFreeIdx = setAdditionalCostsCleaning(additionNextFreeIdx, model.ReceiptReinigung, offerCleaning.ReinigungCostAdditFreeTextPlus2, offerCleaning.ReinigungCostAdditFreeTextPlus2Price);
                            // additionalSubs (abzug)
                            setAdditionalSubCostsCleaning(1, model.ReceiptReinigung, offerCleaning.ReinigungCostAdditFreeTextMinus1, offerCleaning.ReinigungCostAdditFreeTextMinus1Price);
                        }
                    }
                }
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Receipt";
                ViewBag.SubmitText = "Save";

                // security check if its own receipt
                model.Receipt = receiptManager.GetReceiptById(id.Value);
                model.ReceiptUmzug = model.Receipt.ReceiptUmzugs.FirstOrDefault();
                model.ReceiptReinigung = model.Receipt.ReceiptReinigungs.FirstOrDefault();
                if ((model == null) || (model.Receipt?.CustomerId != customerId))
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                // preselect selectlistitems
                model.OldReceiptState = model.Receipt.ReceiptStateId;
                if (string.IsNullOrEmpty(model.Receipt.EmailAddress)) model.Receipt.EmailAddress = customer.Email;
                if (model.Receipt.ReceiptStateId != Receipt.ReceiptState.Open)
                {
                    selectDefinedSelectItem(ref receiptStateList, ((int)model.Receipt.ReceiptStateId).ToString());
                }

                // set print crypted id
                ViewBag.PrintId = receiptManager.GetCryptedReceiptCode(model.Receipt);
            }

            // selectlistitems
            ViewBag.ReceiptStateList = receiptStateList;
            // Default: SendEmailToCustomer to false on every start (also for edit)
            model.Receipt.EmailToCustomer = false;

            return View("Edit", model);
        }

        // POST: Intranet/Customer/Details/Receipt/Edit/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerReceiptViewModel model, int customerId)
        {
            // Custom validators
            #region CustomValidator

            if (model.Receipt.EmailToCustomer)
            {
                if (string.IsNullOrEmpty(model.Receipt.EmailAddress))
                {
                    ModelState.AddModelError("Receipt.EmailAddress", "Please enter an email address.");
                }
            }
            #endregion

            var customerManager = new CustomerManager();
            var customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            var offerManager = new OfferManager();
            var offer = offerManager.GetOfferById(model.Receipt.OfferId, true);
            if (offer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (ModelState.IsValid)
            {
                var receiptManager = new ReceiptManager();

                // email: unselect if no email-address is set
                if (model.Receipt.EmailToCustomer && string.IsNullOrEmpty(model.Receipt.EmailAddress))
                {
                    model.Receipt.EmailToCustomer = false;
                }
                if (!model.Receipt.EmailToCustomer && !string.IsNullOrEmpty(model.Receipt.EmailAddress))
                {
                    model.Receipt.EmailAddress = null;
                }

                /* preselect different values */
                if (model.Receipt.ReceiptStateId == Receipt.ReceiptState.Completed &&
                    model.OldReceiptState != Receipt.ReceiptState.Completed)
                {
                    model.Receipt.ClosedDate = DateTime.Now;
                    model.Receipt.ClosedUserId = receiptManager.GetUserId(string.Empty);
                }
                // if job date (string) has changed, then update datetime type too
                if (model.Receipt.JobDateTime == null ||
                    !model.Receipt.JobDate.Equals(model.Receipt.JobDateTime.Value.ToString("dd.MM.yyyy")))
                {
                    DateTime tempDate;
                    if (DateTime.TryParse(model.Receipt.JobDate, out tempDate))
                    {
                        model.Receipt.JobDateTime = tempDate;
                    }
                }

                // save to DB
                switch (model.Receipt.ReceiptTypeId)
                {
                    case Receipt.ReceiptType.Move:
                    {
                        model.Receipt.ReceiptUmzugs = new List<ReceiptUmzug>
                        {
                            model.ReceiptUmzug
                        };
                        break;
                    }
                    case Receipt.ReceiptType.cleaning:
                    {
                        model.Receipt.ReceiptReinigungs = new List<ReceiptReinigung>
                        {
                            model.ReceiptReinigung
                        };
                        break;
                    }
                }
                if (model.Receipt.ReceiptId != Guid.Empty)
                {
                    receiptManager.EditReceipt(model.Receipt);
                }
                else
                {
                    receiptManager.CreateReceipt(model.Receipt, customer, offer);
                }

                if (model.Receipt.EmailToCustomer)
                {
                    SendReceiptEmail(model.Receipt, customer, $"Receipt{model.Receipt.ReceiptTypeId}");
                }

                return RedirectToAction("Index", new { customerId = customerId });
            }
            ViewBag.CustomerId = customerId;
            ViewBag.Title = $"Customer: {(customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName)}";

            if (model.Receipt.ReceiptId == Guid.Empty)
            {
                // -- Create new
                ViewBag.SubTitle = "Neue Create a receipt";
                ViewBag.SubmitText = "Create";
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Receipt";
                ViewBag.SubmitText = "Save";
            }

            ViewBag.ReceiptStateList = createReceiptStateList(((int)model.Receipt.ReceiptStateId).ToString());

            // set print crypted id
            ViewBag.PrintId = (new ReceiptManager()).GetCryptedReceiptCode(model.Receipt);

            return View("Edit", model);
        }

        // GET: Intranet/Customer/Details/Receipt/Details/1/[GUID]
        public async Task<ActionResult> Details(int customerId, Guid id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id == Guid.Empty))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own receipt
            var receiptManager = new ReceiptManager();
            CustomerReceiptViewModel model = new CustomerReceiptViewModel();
            model.Receipt = receiptManager.GetReceiptById(id);
            if ((model == null) || (model.Receipt.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            model.ReceiptUmzug = model.Receipt.ReceiptUmzugs.FirstOrDefault();
            model.ReceiptReinigung = model.Receipt.ReceiptReinigungs.FirstOrDefault();

            ViewBag.CustomerId = customerId;
            ViewBag.Title = $"Customer: {(customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName)}";
            model.IsEditable = false;
            // set print crypted id
            ViewBag.PrintId = receiptManager.GetCryptedReceiptCode(model.Receipt);

            // -- Edit
            ViewBag.SubTitle = "Receipt anschauen";
            if (TempData.ContainsKey("SuccessMessage"))
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"].ToString();
            }

            // dropdownlist
            List<SelectListItem> receiptStateList = createReceiptStateList();
            if (model.Receipt.ReceiptStateId != Receipt.ReceiptState.Open)
                selectDefinedSelectItem(ref receiptStateList, ((int)model.Receipt.ReceiptStateId).ToString());
            ViewBag.ReceiptStateList = receiptStateList;

            // preset email-address (is required to send reminder/precollection)
            if (string.IsNullOrEmpty(model.Receipt.EmailAddress)) model.Receipt.EmailAddress = customer.Email;

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(model.Receipt.CreateUserId);
                if (user != null)
                {
                    model.Receipt.CreateUserId = user.UserName;
                }
            }

            return View("Edit", model);
        }

        // POST: Intranet/Customer/Details/Receipt/Delete/1/[GUID]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int customerId, Guid id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            var manager = new ReceiptManager();
            var receipt = manager.GetReceiptById(id);

            if ((receipt == null) || (receipt.CustomerId != customerId) || (receipt.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            manager.DeleteReceipt(id);
            return RedirectToAction("Index", new { customerId = customerId });
        }

        // GET: Intranet/Customer/Details/Receipt/Close/1/[GUID]
        public ActionResult Close(int customerId, Guid id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id == Guid.Empty))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own receipt
            var receiptManager = new ReceiptManager();
            var receipt = receiptManager.GetReceiptById(id);
            if ((receipt == null) || (receipt.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            receiptManager.CloseReceipt(receipt);

            TempData.Add("SuccessMessage", "Der Auftrag wurde erfolgreich abgeschlossen. Sie können den Auftrag jederzeit über 'Edit' wieder eröffnen.");
            return RedirectToAction("Details", new { customerId = customerId, id = id });
        }

        public JsonResult EmailText(int customerId, string mailTemplate)
        {
            return Json(getEmailTextContainer(customerId, mailTemplate), JsonRequestBehavior.AllowGet);
        }

        private EmailTextContainer getEmailTextContainer(int customerId, string mailTemplate = "ReceiptUmzug")
        {
            var emailTextContainer = new EmailTextContainer();
            var customer = (new CustomerManager()).GetCustomerById(customerId, true);
            var account = (new AccountManager()).GetAccountById(customer.AccountId);
            var customerManager = new CustomerManager();

            if (account.InnerAccountId <= 0) return null;

            // check if mailtemplate definition is correct
            var possibleTemplates = new string[] { "ReceiptUmzug", "ReceiptReinigung" };
            if (!possibleTemplates.Contains(mailTemplate))
            {
                mailTemplate = "ReceiptUmzug";
            }

            // personal email subject-title
            var emailType = (new EmailManager()).GetEmailTypeByAccountId(customer.AccountId, $"Receipt.{mailTemplate}");
            if (emailType != null && !string.IsNullOrEmpty(emailType.MailSubject)) emailTextContainer.Subject = emailType.MailSubject;
            else emailTextContainer.Subject = $"Receipt - {account.AccountName}";

            // body

            var mainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = $"{account.Zip} {account.City}",
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            emailTextContainer.Body = this.RenderPartialViewToString($"{mainMailData.PathAccount}/text/Receipt.cshtml", mainMailData);

            return emailTextContainer;
        }

        private bool SendReceiptEmail(Receipt receipt, Customer customer, string mailTemplate = "ReceiptUmzug")
        {
            var account = (new AccountManager()).GetAccountById(customer.AccountId);

            if (account.InnerAccountId <= 0) return false;

            // send email
            EmailManager emailManager = new EmailManager();

            // email attachments
            List<Attachment> attachments = new List<Attachment>();

            // render pdf
            PdfManager pdfManager = new PdfManager();
            var pdfResult = pdfManager.GetForReceiptAsUrl(receipt, this.ControllerContext.RequestContext);
            byte[] pdfResultByte = pdfResult.BuildPdf(this.ControllerContext);
            if (pdfResultByte.Length > 0)
            {
                MemoryStream ms = new MemoryStream(pdfResultByte);
                attachments.Add(new Attachment(ms, "Receipt.pdf", "application/pdf"));
            }

            // email subject and bodytext: load from model if user has changed the text, else initialize it
            string subject = null;
            string body = null;
            // if user has changed text
            if (receipt.ChangeEmailText)
            {
                if (!string.IsNullOrWhiteSpace(receipt.ChangeEmailTextContainer?.Subject))
                {
                    subject = receipt.ChangeEmailTextContainer.Subject;
                }
                if (!string.IsNullOrWhiteSpace(receipt.ChangeEmailTextContainer?.Body))
                {
                    body = receipt.ChangeEmailTextContainer.Body;
                }
            }
            // else initialize new
            if (string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(body))
            {
                var emailTextContainer = getEmailTextContainer(customer.CustomerId, mailTemplate);
                subject = subject ?? emailTextContainer.Subject;
                body = body ?? emailTextContainer.Body;
            }

            EmailSender emailSender = (new EmailSenderFactory()).CreateEmailSender(account.AccountId.ToString());

            // send email (multiple receiver possible)
            bool isSuccessful = true;
            foreach (var receiverEmail in receipt.EmailAddress.Replace(',', ';').Split(';'))
            {
                // && isSuccessful: if there is an error on multiple receiver-version, then it is false. so let variable always false, that at the end false will be returned
                isSuccessful = emailSender.SendEmailFromAccounting(receiverEmail, subject, body, attachments) && isSuccessful;
            }

            return isSuccessful;
        }

        private List<SelectListItem> createReceiptStateList(string selectValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            var lEnum = Enum.GetValues(typeof(Receipt.ReceiptState));
            foreach (Receipt.ReceiptState bt in lEnum)
            {
                list.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = bt.ToString().Replace('_', ' '), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return list;
        }

        #region HelperMethods

        private int setAdditionalCosts(int indexAdditionalCostControl, ReceiptUmzug receiptUmzug, string text, int price)
        {
            if (string.IsNullOrEmpty(text) && price == 0) return indexAdditionalCostControl;

            switch (indexAdditionalCostControl)
            {
                case 1:
                    receiptUmzug.AdditionalText1 = text;
                    receiptUmzug.AdditionalCost1 = price.ToString();
                    break;
                case 2:
                    receiptUmzug.AdditionalText2 = text;
                    receiptUmzug.AdditionalCost2 = price.ToString();
                    break;
                case 3:
                    receiptUmzug.AdditionalText3 = text;
                    receiptUmzug.AdditionalCost3 = price.ToString();
                    break;
                case 4:
                    receiptUmzug.AdditionalText4 = text;
                    receiptUmzug.AdditionalCost4 = price.ToString();
                    break;
                case 5:
                    receiptUmzug.AdditionalText5 = text;
                    receiptUmzug.AdditionalCost5 = price.ToString();
                    break;
                case 6:
                    receiptUmzug.AdditionalText6 = text;
                    receiptUmzug.AdditionalCost6 = price.ToString();
                    break;
            }

            return indexAdditionalCostControl + 1;
        }

        private int setAdditionalSubCosts(int indexAdditionalCostControl, ReceiptUmzug receiptUmzug, string text, int price)
        {
            if (string.IsNullOrEmpty(text) && price == 0) return indexAdditionalCostControl;

            switch (indexAdditionalCostControl)
            {
                case 1:
                    receiptUmzug.AdditionalSubText1 = text;
                    receiptUmzug.AdditionalSubCost1 = price.ToString();
                    break;
                case 2:
                    receiptUmzug.AdditionalSubText2 = text;
                    receiptUmzug.AdditionalSubCost2 = price.ToString();
                    break;
                case 3:
                    receiptUmzug.AdditionalSubText3 = text;
                    receiptUmzug.AdditionalSubCost3 = price.ToString();
                    break;
            }

            return indexAdditionalCostControl + 1;
        }

        private int setAdditionalCostsCleaning(int indexAdditionalCostControl, ReceiptReinigung receiptReinigung, string text, int price)
        {
            if (string.IsNullOrEmpty(text) && price == 0) return indexAdditionalCostControl;

            switch (indexAdditionalCostControl)
            {
                case 1:
                    receiptReinigung.AdditionalText1 = text;
                    receiptReinigung.AdditionalCost1 = price.ToString();
                    break;
                case 2:
                    receiptReinigung.AdditionalText2 = text;
                    receiptReinigung.AdditionalCost2 = price.ToString();
                    break;
                case 3:
                    receiptReinigung.AdditionalText3 = text;
                    receiptReinigung.AdditionalCost3 = price.ToString();
                    break;
                case 4:
                    receiptReinigung.AdditionalText4 = text;
                    receiptReinigung.AdditionalCost4 = price.ToString();
                    break;
                case 5:
                    receiptReinigung.AdditionalText5 = text;
                    receiptReinigung.AdditionalCost5 = price.ToString();
                    break;
                case 6:
                    receiptReinigung.AdditionalText6 = text;
                    receiptReinigung.AdditionalCost6 = price.ToString();
                    break;
            }

            return indexAdditionalCostControl + 1;
        }

        private int setAdditionalSubCostsCleaning(int indexAdditionalCostControl, ReceiptReinigung receiptReinigung, string text, int price)
        {
            if (string.IsNullOrEmpty(text) && price == 0) return indexAdditionalCostControl;

            switch (indexAdditionalCostControl)
            {
                case 1:
                    receiptReinigung.AdditionalSubText1 = text;
                    receiptReinigung.AdditionalSubCost1 = price.ToString();
                    break;
                case 2:
                    receiptReinigung.AdditionalSubText2 = text;
                    receiptReinigung.AdditionalSubCost2 = price.ToString();
                    break;
                case 3:
                    receiptReinigung.AdditionalSubText3 = text;
                    receiptReinigung.AdditionalSubCost3 = price.ToString();
                    break;
            }

            return indexAdditionalCostControl + 1;
        }

        private void selectDefinedSelectItem(ref List<SelectListItem> list, string value)
        {
            SelectListItem item = list.SingleOrDefault(a => a.Value == value);
            if (item != null) item.Selected = true;
        }

        #endregion

    }
}