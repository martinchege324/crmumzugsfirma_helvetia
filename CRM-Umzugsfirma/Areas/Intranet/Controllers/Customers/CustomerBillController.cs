﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.Managers.Pdf;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Managers.PackMaterial;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerBillController : Controller
    {
        // GET: Intranet/Customer/Details/Bill/1/1
        public ActionResult Index(int customerId, int? id)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            BillManager manager = new BillManager();
            return View(manager.GetBillsByCustomer(customerId));
        }


        // GET: Intranet/Customer/Details/Bill/Edit/1/1
        // If Empty creates a new, else edit it
        public ActionResult Edit(int customerId, int? id, int? offerId)
        {
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            // create model
            CustomerBillViewModel model = new CustomerBillViewModel();
            BillManager billManager = new BillManager();

            if (id == null)
            {
                // -- Create new
                ViewBag.SubTitle = "Create a new invoice";
                ViewBag.SubmitText = "Create";

                model.Bill = new Bill
                {
                    CustomerId = customerId,
                    AccountId = customer.AccountId,
                    EmailAddress = customer.Email,
                    BillStateId = Bill.BillState.Open,
                    PaymentTermId = Bill.PaymentTerm.In_7_Days,
                    SendStateId = Bill.SendState.Expression,
                    CostInclTax = false,
                    CostExclTax = true,
                    CostFreeTax = false,
                    BillCleanings = new List<BillCleaning>()
                    {
                        new BillCleaning { SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee },
                        new BillCleaning { SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee }
                    },
                    BillTransports = new List<BillTransport>()
                    {
                        new BillTransport { SectorId = 1 }
                    }
                };

                // take data from offer
                if (offerId.HasValue)
                {
                    OfferManager offerManager = new OfferManager();
                    Offer offer = offerManager.GetOfferById(offerId.Value, true, customer.AccountId);
                    if (offer != null && offer.CustomerId == customer.CustomerId)
                    {
                        model.Bill.OfferId = offer.OfferId;
                        model.Bill.BillCost = 0;
                        model.Bill.CostInclTax = offer.CostInclTax;
                        model.Bill.CostExclTax = offer.CostExclTax;
                        model.Bill.CostFreeTax = offer.CostFreeTax;

                        if (!string.IsNullOrEmpty(offer.EmailAddress)) model.Bill.EmailAddress = offer.EmailAddress;

                        model.Bill.UmzugActive = offer.UmzugActive;
                        if (model.Bill.UmzugActive)
                        {
                            model.Bill.UmzugDate = offer.UmzugDate;
                            model.Bill.UmzugDuration = getMaxIntOfRange(offer.UmzugDuration).ToString();
                            model.Bill.UmzugPerPrice = offer.UmzugCostChangedPrice;
                            model.Bill.UmzugCostWay = offer.UmzugCostWay;
                            model.Bill.UmzugCostAddit = offer.UmzugCostAddit;
                            if (model.Bill.UmzugCostAddit)
                            {
                                model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "umzug"));
                                model.Bill.UmzugCostAdditFreeTextPlus1 = offer.UmzugCostAdditFreeTextPlus1;
                                model.Bill.UmzugCostAdditFreeTextPlus1Price = offer.UmzugCostAdditFreeTextPlus1Price;
                                model.Bill.UmzugCostAdditFreeTextPlus2 = offer.UmzugCostAdditFreeTextPlus2;
                                model.Bill.UmzugCostAdditFreeTextPlus2Price = offer.UmzugCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.UmzugCostAdditFreeTextMinus1 = offer.UmzugCostAdditFreeTextMinus1;
                            model.Bill.UmzugCostAdditFreeTextMinus1Price = offer.UmzugCostAdditFreeTextMinus1Price;
                            model.Bill.UmzugCostDiscount = offer.UmzugCostDiscount;
                            model.Bill.UmzugCostDiscount2 = offer.UmzugCostDiscount2;
                            model.Bill.UmzugCostFix = offer.UmzugCostFix;
                            model.Bill.UmzugCostFixPrice = offer.UmzugCostFixPrice;
                            if (offer.UmzugCostHighSecurity) model.Bill.UmzugCostHighSecurityPrice = offer.UmzugCostHighSecurityPrice;

                            model = prepareSaveUmzugService(model, customer.AccountId);
                        }
                        model.Bill.PackActive = offer.PackActive;
                        if (model.Bill.PackActive)
                        {
                            model.Bill.PackDate = offer.PackDate;
                            model.Bill.PackDuration = getMaxIntOfRange(offer.PackDuration).ToString();
                            model.Bill.PackPerPrice = offer.PackCostChangedPrice;
                            model.Bill.PackCostWay = offer.PackCostWay;
                            model.Bill.PackCostAddit = offer.PackCostAddit;
                            if (model.Bill.PackCostAddit)
                            {
                                model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "pack"));
                                model.Bill.PackCostAdditFreeTextPlus1 = offer.PackCostAdditFreeTextPlus1;
                                model.Bill.PackCostAdditFreeTextPlus1Price = offer.PackCostAdditFreeTextPlus1Price;
                                model.Bill.PackCostAdditFreeTextPlus2 = offer.PackCostAdditFreeTextPlus2;
                                model.Bill.PackCostAdditFreeTextPlus2Price = offer.PackCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.PackCostAdditFreeTextMinus1 = offer.PackCostAdditFreeTextMinus1;
                            model.Bill.PackCostAdditFreeTextMinus1Price = offer.PackCostAdditFreeTextMinus1Price;
                            model.Bill.PackCostDiscount = offer.PackCostDiscount;
                            model.Bill.PackCostDiscount2 = offer.PackCostDiscount2;
                            model.Bill.PackCostFix = offer.PackCostFix;
                            model.Bill.PackCostFixPrice = offer.PackCostFixPrice;
                            if (offer.PackCostHighSecurity) model.Bill.PackCostHighSecurityPrice = offer.PackCostHighSecurityPrice;

                            model = prepareSavePackService(model, customer.AccountId);
                        }
                        model.Bill.PackOutActive = offer.PackOutActive;
                        if (model.Bill.PackOutActive)
                        {
                            model.Bill.PackOutDate = offer.PackOutDate;
                            model.Bill.PackOutDuration = getMaxIntOfRange(offer.PackOutDuration).ToString();
                            model.Bill.PackOutPerPrice = offer.PackOutCostChangedPrice;
                            model.Bill.PackOutCostWay = offer.PackOutCostWay;
                            model.Bill.PackOutCostAddit = offer.PackOutCostAddit;
                            if (model.Bill.PackOutCostAddit)
                            {
                                model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "packout"));
                                model.Bill.PackOutCostAdditFreeTextPlus1 = offer.PackOutCostAdditFreeTextPlus1;
                                model.Bill.PackOutCostAdditFreeTextPlus1Price = offer.PackOutCostAdditFreeTextPlus1Price;
                                model.Bill.PackOutCostAdditFreeTextPlus2 = offer.PackOutCostAdditFreeTextPlus2;
                                model.Bill.PackOutCostAdditFreeTextPlus2Price = offer.PackOutCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.PackOutCostAdditFreeTextMinus1 = offer.PackOutCostAdditFreeTextMinus1;
                            model.Bill.PackOutCostAdditFreeTextMinus1Price = offer.PackOutCostAdditFreeTextMinus1Price;
                            model.Bill.PackOutCostDiscount = offer.PackOutCostDiscount;
                            model.Bill.PackOutCostDiscount2 = offer.PackOutCostDiscount2;
                            model.Bill.PackOutCostFix = offer.PackOutCostFix;
                            model.Bill.PackOutCostFixPrice = offer.PackOutCostFixPrice;
                            if (offer.PackOutCostHighSecurity) model.Bill.PackOutCostHighSecurityPrice = offer.PackOutCostHighSecurityPrice;

                            model = prepareSavePackOutService(model, customer.AccountId);
                        }
                        model.Bill.ReinigungActive = offer.ReinigungActive;
                        if (model.Bill.ReinigungActive)
                        {
                            model.Bill.BillCleaning.ReinigungDate = offer.OfferCleaning.ReinigungDate;
                            model.Bill.BillCleaning.ReinigungType = offer.OfferCleaning.ReinigungType;
                            model.Bill.BillCleaning.ReinigungTypeFreeText = offer.OfferCleaning.ReinigungTypeFreeText;
                            model.Bill.BillCleaning.ReinigungWishedServiceFreeText = offer.OfferCleaning.ReinigungWishedServiceFreeText;

                            // check which price rate id is selected
                            if (offer.OfferCleaning.ReinigungPriceRateId > 0)
                            {
                                model.Bill.BillCleaning.ReinigungRoom = offer.OfferCleaning.ReinigungPriceRateDescr;
                                // ToDo: Delete next line with tariftext since ReinigungRoom is accepted
                                model.Bill.BillCleaning.ReinigungTarifText = offer.OfferCleaning.ReinigungPriceRateDescr;
                                model.Bill.BillCleaning.ReinigungPerPrice = offer.OfferCleaning.ReinigungCostChangedPrice;
                            }
                            else if (offer.OfferCleaning.ReinigungHourlyPriceRateId > 0)
                            {
                                model.Bill.BillCleaning.ReinigungHourlyDuration = getMaxIntOfRange(offer.OfferCleaning.ReinigungHourlyDuration).ToString();
                                model.Bill.BillCleaning.ReinigungHourlyPerPrice = offer.OfferCleaning.ReinigungHourlyCostChangedPrice;
                            }
                            //model.Bill.BillCleaning.ReinigungTarifText = offer.OfferCleaning.ReinigungPriceRateDescr.Replace("cleaning ", "").Replace(" incl. Abnahmegarantie", "");
                            model.Bill.BillCleaning.ReinigungCostAddit = offer.OfferCleaning.ReinigungCostAddit;
                            if (model.Bill.BillCleaning.ReinigungCostAddit)
                            {
                                model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "reinigung"));
                                model.Bill.BillCleaning.ReinigungCostAdditFreeTextPlus1 = offer.OfferCleaning.ReinigungCostAdditFreeTextPlus1;
                                model.Bill.BillCleaning.ReinigungCostAdditFreeTextPlus1Price = offer.OfferCleaning.ReinigungCostAdditFreeTextPlus1Price;
                                model.Bill.BillCleaning.ReinigungCostAdditFreeTextPlus2 = offer.OfferCleaning.ReinigungCostAdditFreeTextPlus2;
                                model.Bill.BillCleaning.ReinigungCostAdditFreeTextPlus2Price = offer.OfferCleaning.ReinigungCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.BillCleaning.ReinigungCostAdditFreeTextMinus1 = offer.OfferCleaning.ReinigungCostAdditFreeTextMinus1;
                            model.Bill.BillCleaning.ReinigungCostAdditFreeTextMinus1Price = offer.OfferCleaning.ReinigungCostAdditFreeTextMinus1Price;
                            model.Bill.BillCleaning.ReinigungCostSub = Helpers.Helpers.GetHigherPrice(offer.OfferCleaning.ReinigungCostSum);

                            model = prepareSaveReinigungService(model, customer.AccountId, 1);
                        }
                        model.Bill.Reinigung2Active = offer.Reinigung2Active;
                        if (model.Bill.Reinigung2Active)
                        {
                            model.Bill.BillCleaning2.ReinigungDate = offer.OfferCleaning2.ReinigungDate;
                            model.Bill.BillCleaning2.ReinigungType = offer.OfferCleaning2.ReinigungType;
                            model.Bill.BillCleaning2.ReinigungTypeFreeText = offer.OfferCleaning2.ReinigungTypeFreeText;
                            model.Bill.BillCleaning2.ReinigungWishedServiceFreeText = offer.OfferCleaning2.ReinigungWishedServiceFreeText;

                            // check which price rate id is selected
                            if (offer.OfferCleaning2.ReinigungPriceRateId > 0)
                            {
                                model.Bill.BillCleaning2.ReinigungRoom = offer.OfferCleaning2.ReinigungPriceRateDescr;
                                // ToDo: Delete next line with tariftext since ReinigungRoom is accepted
                                model.Bill.BillCleaning2.ReinigungTarifText = offer.OfferCleaning2.ReinigungPriceRateDescr;
                                model.Bill.BillCleaning2.ReinigungPerPrice = offer.OfferCleaning2.ReinigungCostChangedPrice;
                            }
                            else if (offer.OfferCleaning2.ReinigungHourlyPriceRateId > 0)
                            {
                                model.Bill.BillCleaning2.ReinigungHourlyDuration = getMaxIntOfRange(offer.OfferCleaning2.ReinigungHourlyDuration).ToString();
                                model.Bill.BillCleaning2.ReinigungHourlyPerPrice = offer.OfferCleaning2.ReinigungHourlyCostChangedPrice;
                            }
                            //model.Bill.BillCleaning2.ReinigungTarifText = offer.OfferCleaning2.ReinigungPriceRateDescr.Replace("cleaning ", "").Replace(" incl. Abnahmegarantie", "");
                            model.Bill.BillCleaning2.ReinigungCostAddit = offer.OfferCleaning2.ReinigungCostAddit;
                            if (model.Bill.BillCleaning2.ReinigungCostAddit)
                            {
                                model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "cleaning2", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "cleaning2"));
                                model.Bill.BillCleaning2.ReinigungCostAdditFreeTextPlus1 = offer.OfferCleaning2.ReinigungCostAdditFreeTextPlus1;
                                model.Bill.BillCleaning2.ReinigungCostAdditFreeTextPlus1Price = offer.OfferCleaning2.ReinigungCostAdditFreeTextPlus1Price;
                                model.Bill.BillCleaning2.ReinigungCostAdditFreeTextPlus2 = offer.OfferCleaning2.ReinigungCostAdditFreeTextPlus2;
                                model.Bill.BillCleaning2.ReinigungCostAdditFreeTextPlus2Price = offer.OfferCleaning2.ReinigungCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.BillCleaning2.ReinigungCostAdditFreeTextMinus1 = offer.OfferCleaning2.ReinigungCostAdditFreeTextMinus1;
                            model.Bill.BillCleaning2.ReinigungCostAdditFreeTextMinus1Price = offer.OfferCleaning2.ReinigungCostAdditFreeTextMinus1Price;
                            model.Bill.BillCleaning2.ReinigungCostSub = Helpers.Helpers.GetHigherPrice(offer.OfferCleaning2.ReinigungCostSum);

                            model = prepareSaveReinigungService(model, customer.AccountId, 2);
                        }
                        model.Bill.TransportActive = offer.TransportActive;
                        if (model.Bill.TransportActive)
                        {
                            model.Bill.BillTransport.TransportTypeFreeText = offer.OfferTransport.TransportTypeFreeText;
                            model.Bill.BillTransport.TransportDate = offer.OfferTransport.TransportDate;
                            model.Bill.BillTransport.TransportDuration = getMaxIntOfRange(offer.OfferTransport.TransportDuration).ToString();
                            model.Bill.BillTransport.TransportCostFixRatePrice = offer.OfferTransport.TransportCostFixRatePrice;
                            model.Bill.BillTransport.TransportPerPrice = offer.OfferTransport.TransportCostChangedPrice;
                            model.Bill.BillTransport.TransportCostWay = offer.OfferTransport.TransportCostWay;
                            model.Bill.BillTransport.TransportCostAddit = offer.OfferTransport.TransportCostAddit;
                            if (model.Bill.BillTransport.TransportCostAddit)
                            {
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus1 = offer.OfferTransport.TransportCostAdditFreeTextPlus1;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus1Price = offer.OfferTransport.TransportCostAdditFreeTextPlus1Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus2 = offer.OfferTransport.TransportCostAdditFreeTextPlus2;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus2Price = offer.OfferTransport.TransportCostAdditFreeTextPlus2Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus3 = offer.OfferTransport.TransportCostAdditFreeTextPlus3;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus3Price = offer.OfferTransport.TransportCostAdditFreeTextPlus3Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus4 = offer.OfferTransport.TransportCostAdditFreeTextPlus4;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus4Price = offer.OfferTransport.TransportCostAdditFreeTextPlus4Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus5 = offer.OfferTransport.TransportCostAdditFreeTextPlus5;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus5Price = offer.OfferTransport.TransportCostAdditFreeTextPlus5Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus6 = offer.OfferTransport.TransportCostAdditFreeTextPlus6;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus6Price = offer.OfferTransport.TransportCostAdditFreeTextPlus6Price;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus7 = offer.OfferTransport.TransportCostAdditFreeTextPlus7;
                                model.Bill.BillTransport.TransportCostAdditFreeTextPlus7Price = offer.OfferTransport.TransportCostAdditFreeTextPlus7Price;
                            }
                            model.Bill.BillTransport.TransportCostAdditFreeTextMinus1 = offer.OfferTransport.TransportCostAdditFreeTextMinus1;
                            model.Bill.BillTransport.TransportCostAdditFreeTextMinus1Price = offer.OfferTransport.TransportCostAdditFreeTextMinus1Price;
                            model.Bill.BillTransport.TransportCostAdditFreeTextMinus2 = offer.OfferTransport.TransportCostAdditFreeTextMinus2;
                            model.Bill.BillTransport.TransportCostAdditFreeTextMinus2Price = offer.OfferTransport.TransportCostAdditFreeTextMinus2Price;
                            model.Bill.BillTransport.TransportCostDiscount = offer.OfferTransport.TransportCostDiscount;
                            model.Bill.BillTransport.TransportCostDiscount2 = offer.OfferTransport.TransportCostDiscount2;
                            model.Bill.BillTransport.TransportCostFix = offer.OfferTransport.TransportCostFix;
                            model.Bill.BillTransport.TransportCostFixPrice = offer.OfferTransport.TransportCostFixPrice;
                            if (offer.OfferTransport.TransportCostHighSecurity) model.Bill.BillTransport.TransportCostHighSecurityPrice = offer.OfferTransport.TransportCostHighSecurityPrice;

                            model = prepareSaveTransportService(model, customer.AccountId);
                        }
                        model.Bill.EntsorgungActive = offer.EntsorgungActive;
                        if (model.Bill.EntsorgungActive)
                        {
                            model.Bill.EntsorgungDate = offer.EntsorgungDate;
                            model.Bill.EntsorgungVolume = getMaxIntOfRange(offer.EntsorgungVolume).ToString();
                            model.Bill.EntsorgungPerPrice = offer.EntsorgungCostChangedPrice;
                            if (string.IsNullOrEmpty(offer.EntsorgungFixCost))
                            {
                                model.Bill.EntsorgungFixCost = "0";
                            }
                            else
                            {
                                model.Bill.EntsorgungFixCost = offer.EntsorgungFixCost;
                            }
                            model.Bill.EntsorgungDuration = getMaxIntOfRange(offer.EntsorgungDuration).ToString();
                            model.Bill.EntsorgungPersonPerPrice = offer.EntsorgungPersonCostChangedPrice;
                            model.Bill.EntsorgungCostWay = offer.EntsorgungCostWay;
                            model.Bill.EntsorgungCostAddit = offer.EntsorgungCostAddit;
                            if (model.Bill.EntsorgungCostAddit)
                            {
                                model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "disposal", offerManager.GetSelectedAdditionalCosts(offer.OfferId, "disposal"));
                                model.Bill.EntsorgungCostAdditFreeTextPlus1 = offer.EntsorgungCostAdditFreeTextPlus1;
                                model.Bill.EntsorgungCostAdditFreeTextPlus1Price = offer.EntsorgungCostAdditFreeTextPlus1Price;
                                model.Bill.EntsorgungCostAdditFreeTextPlus2 = offer.EntsorgungCostAdditFreeTextPlus2;
                                model.Bill.EntsorgungCostAdditFreeTextPlus2Price = offer.EntsorgungCostAdditFreeTextPlus2Price;
                            }
                            model.Bill.EntsorgungCostAdditFreeTextMinus1 = offer.EntsorgungCostAdditFreeTextMinus1;
                            model.Bill.EntsorgungCostAdditFreeTextMinus1Price = offer.EntsorgungCostAdditFreeTextMinus1Price;
                            model.Bill.EntsorgungCostDiscount = offer.EntsorgungCostDiscount;
                            model.Bill.EntsorgungCostFix = offer.EntsorgungCostFix;
                            model.Bill.EntsorgungCostFixPrice = offer.EntsorgungCostFixPrice;
                            if (offer.EntsorgungCostHighSecurity) model.Bill.EntsorgungCostHighSecurityPrice = offer.EntsorgungCostHighSecurityPrice;

                            model = prepareSaveEntsorgungService(model, customer.AccountId);
                        }
                        model.Bill.LagerungActive = offer.LagerungActive;
                        if (model.Bill.LagerungActive)
                        {
                            model.Bill.LagerungDateReminder = model.Bill.CreateDate.ToString();
                            model.Bill.LagerungVolume = getMaxIntOfRange(offer.LagerungVolume).ToString();
                            model.Bill.LagerungPerPrice = offer.LagerungCostChangedPrice;
                            model.Bill.LagerungCostAdditFreeTextPlus1 = offer.LagerungCostAdditFreeTextPlus1;
                            model.Bill.LagerungCostAdditFreeTextPlus1Price = offer.LagerungCostAdditFreeTextPlus1Price;
                            model.Bill.LagerungCostAdditFreeTextPlus2 = offer.LagerungCostAdditFreeTextPlus2;
                            model.Bill.LagerungCostAdditFreeTextPlus2Price = offer.LagerungCostAdditFreeTextPlus2Price;
                            model.Bill.LagerungCostAdditFreeTextMinus1 = offer.LagerungCostAdditFreeTextMinus1;
                            model.Bill.LagerungCostAdditFreeTextMinus1Price = offer.LagerungCostAdditFreeTextMinus1Price;
                            model.Bill.LagerungCostFix = offer.LagerungCostFix;
                            model.Bill.LagerungCostFixPrice = offer.LagerungCostFixPrice;

                            model = prepareSaveLagerungService(model, customer.AccountId);
                        }
                        model.Bill.PackMaterialActive = offer.PackMaterialActive;
                        if (model.Bill.PackMaterialActive)
                        {
                            model.Bill.PackMaterialDate = (model.Bill.UmzugActive ? model.Bill.UmzugDate : string.Empty);
                            List<Offer_DefinedPackMaterial> odpm_List = offerManager.GetDefinedPackMaterials(offer.OfferId);
                            model.BillDefinedPackMaterials = convertOfferDefinedPackToBillDefinedPack(odpm_List);
                            List<OfferPackMaterial> opm_List = offerManager.GetPackMaterials(offer.OfferId);
                            model.BillPackMaterials = convertOfferPackToBillPack(opm_List);
                            model.Bill.PackMaterialCostDiscount = offer.PackMaterialCostDiscount;
                            model.Bill.PackMaterialDeliver = offer.PackMaterialDeliver;
                            model.Bill.PackMaterialDeliverPrice = offer.PackMaterialDeliverPrice;
                            model.Bill.PackMaterialPickup = offer.PackMaterialPickup;
                            model.Bill.PackMaterialPickupPrice = offer.PackMaterialPickupPrice;

                            model = prepareSavePackMaterialService(model, customer.AccountId);
                        }
                        model.Bill.BillCostString = model.Bill.BillCost.ToString().Replace(',', '.');
                    }
                }

                if (model.UmzugPriceAdditCosts == null) model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug");
                if (model.PackPriceAdditCosts == null) model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack");
                if (model.PackOutPriceAdditCosts == null) model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout");
                if (model.ReinigungPriceAdditCosts == null) model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung");
                if (model.Reinigung2PriceAdditCosts == null) model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2");
                if (model.EntsorgungPriceAdditCosts == null) model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung");
                if (model.BillDefinedPackMaterials == null) model.BillDefinedPackMaterials = createBillDefinedPackMaterials();
                if (model.BillPackMaterials == null) model.BillPackMaterials = createBillPackMaterials();
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Invoice";
                ViewBag.SubmitText = "Save";

                // security check if its own bill
                model.Bill = billManager.GetBillById(id.Value);
                if ((model.Bill == null) || (model.Bill.CustomerId != customerId))
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                // set relational depencies to default value for rendering UI
                if (model.Bill.BillCleanings == null)
                {
                    model.Bill.BillCleanings = new List<BillCleaning>()
                    {
                        new BillCleaning { BillId = model.Bill.BillId, SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee },
                        new BillCleaning { BillId = model.Bill.BillId, SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee }
                    };
                }
                else
                {
                    if (model.Bill.BillCleaning == null) model.Bill.BillCleanings.Add(new BillCleaning { BillId = model.Bill.BillId, SectorId = 1, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee });
                    if (model.Bill.BillCleaning2 == null) model.Bill.BillCleanings.Add(new BillCleaning { BillId = model.Bill.BillId, SectorId = 2, ReinigungType = CleaningType.Apartment_cleaning_incl_point_acceptance_guarantee });
                }
                if (model.Bill.BillTransports == null)
                {
                    model.Bill.BillTransports = new List<BillTransport> { new BillTransport { BillId = model.Bill.BillId, SectorId = 1 } };
                }
                else
                {
                    if (model.Bill.BillTransport == null) model.Bill.BillTransports.Add(new BillTransport { BillId = model.Bill.BillId, SectorId = 1 });
                }

                // preset data
                if (string.IsNullOrEmpty(model.Bill.EmailAddress)) model.Bill.EmailAddress = customer.Email;

                // preselect umzugadditioncosts
                model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "umzug", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "umzug"));
                // preselect packadditioncosts
                model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "pack"));
                // preselect packoutadditioncosts
                model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "packout"));
                // preselect reinigungadditioncosts
                model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "reinigung"));
                // preselect reinigung2additioncosts
                model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "reinigung2"));
                // preselect entsorgungadditioncosts
                model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "entsorgung"));
                // preselect / render definedpackmaterials (dropdown)
                model.BillDefinedPackMaterials = createBillDefinedPackMaterials(billManager.GetDefinedPackMaterials(model.Bill.BillId));
                // preselect / render packmaterials (textfield)
                model.BillPackMaterials = createBillPackMaterials(billManager.GetPackMaterials(model.Bill.BillId));

                // set print crypted id
                ViewBag.PrintId = billManager.GetCryptedBillCode(model.Bill);
            }

            // selectlistitems: ReinigungType preselect / render reinigungtype (dropdown)
            if (model.Bill.ReinigungActive && model.Bill.BillCleaning?.ReinigungType != CleaningType.Please_choose)
            {
                ViewBag.ReinigungTypeList = createCleaningTypeList(((int)model.Bill.BillCleaning.ReinigungType).ToString());
            }
            else
            {
                ViewBag.ReinigungTypeList = createCleaningTypeList();
            }
            // selectlistitems: Reinigung2Type preselect / render reinigung2type (dropdown)
            if (model.Bill.Reinigung2Active && model.Bill.BillCleaning2?.ReinigungType != CleaningType.Please_choose)
            {
                ViewBag.Reinigung2TypeList = createCleaningTypeList(((int)model.Bill.BillCleaning2.ReinigungType).ToString());
            }
            else
            {
                ViewBag.Reinigung2TypeList = createCleaningTypeList();
            }

            // selectlistitems
            ViewBag.DefinedPackMaterialList = createDefinedPackMaterialList(customer.AccountId);

            // Default: SendEmailToCustomer to false on every start (also for edit)
            model.Bill.EmailToCustomer = false;

            // Default: Set packmaterial default prices
            if ((model.Bill.PackMaterialDeliverPrice == 0 && model.Bill.PackMaterialPickupPrice == 0))
            {
                model.Bill.PackMaterialDeliverPrice = 60;
                model.Bill.PackMaterialPickupPrice = 0;
            }
            if (model.Bill.EntsorgungFixCost == null) model.Bill.EntsorgungFixCost = "160";

            // load email attachments
            EmailManager emailManager = new EmailManager();
            List<EmailAttachment> emailAttachments = emailManager.GetEmailAttachmentsByAccountId(customer.AccountId);
            if (emailAttachments.Count > 0)
            {
                model.EmailAttachments = new List<CheckListViewModel>();
                emailAttachments.ForEach(e => model.EmailAttachments.Add(new CheckListViewModel() { Id = e.Name.ToLower(), Name = e.Name, Selected = e.IsBillDefault }));
            }

            return View(model);
        }

        // POST: Intranet/Customer/Details/Bill/Edit/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CustomerBillViewModel model, int customerId)
        {
            // Custom validators
            #region CustomValidator

            decimal iTemp = 0;
            if (model.Bill.UmzugActive)
            {
                if (string.IsNullOrEmpty(model.Bill.UmzugDate))
                {
                    ModelState.AddModelError("Bill.UmzugDate", "Please enter date.");
                }
                if (string.IsNullOrEmpty(model.Bill.UmzugDuration) || !decimal.TryParse(model.Bill.UmzugDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                {
                    ModelState.AddModelError("Bill.UmzugDuration", "Please enter Duration in hours. Eg: 3 or 4.");
                }
                if (model.Bill.UmzugPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.UmzugPerPrice", "Please enter Price per hour.");
                }
                if (!string.IsNullOrEmpty(model.Bill.UmzugDuration2))
                {
                    if (!decimal.TryParse(model.Bill.UmzugDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                    {
                        ModelState.AddModelError("Bill.UmzugDuration2", "Please enter Duration in hours. Eg: 3 or 4.");
                    }
                    if (model.Bill.UmzugPerPrice2 <= 0)
                    {
                        ModelState.AddModelError("Bill.UmzugPerPrice2", "Please enter Price per hour.");
                    }
                }
            }
            if (model.Bill.PackActive)
            {
                if (string.IsNullOrEmpty(model.Bill.PackDate))
                {
                    ModelState.AddModelError("Bill.PackDate", "Please enter date.");
                }
                if (string.IsNullOrEmpty(model.Bill.PackDuration) || !decimal.TryParse(model.Bill.PackDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                {
                    ModelState.AddModelError("Bill.PackDuration", "Please enter Duration in hours. Eg: 3 or 4.");
                }
                if (model.Bill.PackPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.PackPerPrice", "Please enter Price per hour.");
                }
                if (!string.IsNullOrEmpty(model.Bill.PackDuration2))
                {
                    if (!decimal.TryParse(model.Bill.PackDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                    {
                        ModelState.AddModelError("Bill.PackDuration2", "Please enter Duration in hours. Eg: 3 or 4.");
                    }
                    if (model.Bill.PackPerPrice2 <= 0)
                    {
                        ModelState.AddModelError("Bill.PackPerPrice2", "Please enter Price per hour.");
                    }
                }
            }
            if (model.Bill.PackOutActive)
            {
                if (string.IsNullOrEmpty(model.Bill.PackOutDate))
                {
                    ModelState.AddModelError("Bill.PackOutDate", "Please enter date.");
                }
                if (string.IsNullOrEmpty(model.Bill.PackOutDuration) || !decimal.TryParse(model.Bill.PackOutDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                {
                    ModelState.AddModelError("Bill.PackOutDuration", "Please enter Duration in hours. Eg: 3 or 4.");
                }
                if (model.Bill.PackOutPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.PackOutPerPrice", "Please enter Price per hour.");
                }
                if (!string.IsNullOrEmpty(model.Bill.PackOutDuration2))
                {
                    if (!decimal.TryParse(model.Bill.PackOutDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                    {
                        ModelState.AddModelError("Bill.PackOutDuration2", "Please enter Duration in hours. Eg: 3 or 4.");
                    }
                    if (model.Bill.PackOutPerPrice2 <= 0)
                    {
                        ModelState.AddModelError("Bill.PackOutPerPrice2", "Please enter Price per hour.");
                    }
                }
            }
            if (model.Bill.ReinigungActive)
            {
                if (string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungDate))
                {
                    ModelState.AddModelError("Bill.BillCleaning.ReinigungDate", "Please enter date.");
                }
                if (model.Bill.BillCleaning.ReinigungType == CleaningType.Please_choose && string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungTypeFreeText))
                {
                    ModelState.AddModelError("Bill.BillCleaning.ReinigungTypeFreeText", "Please specify a text for the cleaning type.");
                }
                // if both tarif are empty
                if ((string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungRoom) || model.Bill.BillCleaning.ReinigungPerPrice <= 0)
                    && (string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungHourlyDuration)
                        || !decimal.TryParse(model.Bill.BillCleaning.ReinigungHourlyDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp)))
                {
                    ModelState.AddModelError("Bill.BillCleaning.ReinigungRoom", "Please enter Room-Text for the Invoice. Example: 3-3.5 Room - OR ..");
                    ModelState.AddModelError("Bill.BillCleaning.ReinigungHourlyDuration", ".. OR - Please enter Duration in hours. Eg: 3 or 4.");
                }
                else if (string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungRoom)
                         && !string.IsNullOrEmpty(model.Bill.BillCleaning.ReinigungHourlyDuration) && model.Bill.BillCleaning.ReinigungHourlyPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.BillCleaning.ReinigungHourlyPerPrice", "Please enter Price per hour.");
                }
            }
            if (model.Bill.Reinigung2Active)
            {
                if (string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungDate))
                {
                    ModelState.AddModelError("Bill.BillCleaning2.ReinigungDate", "Please enter date.");
                }
                if (model.Bill.BillCleaning2.ReinigungType == CleaningType.Please_choose && string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungTypeFreeText))
                {
                    ModelState.AddModelError("Bill.BillCleaning2.ReinigungTypeFreeText", "Please specify a text for the cleaning type.");
                }
                // if both tarif are empty
                if ((string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungRoom) || model.Bill.BillCleaning2.ReinigungPerPrice <= 0)
                    && (string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungHourlyDuration)
                        || !decimal.TryParse(model.Bill.BillCleaning2.ReinigungHourlyDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp)))
                {
                    ModelState.AddModelError("Bill.BillCleaning2.ReinigungRoom", "Please enter Room-Text for the Invoice. Example: 3-3.5 Room - OR ..");
                    ModelState.AddModelError("Bill.BillCleaning2.ReinigungHourlyDuration", ".. ODER - Please enter Duration in hours. Eg: 3 or 4.");
                }
                else if (string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungRoom)
                         && !string.IsNullOrEmpty(model.Bill.BillCleaning2.ReinigungHourlyDuration) && model.Bill.BillCleaning2.ReinigungHourlyPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.BillCleaning2.ReinigungHourlyPerPrice", "Please enter Price per hour.");
                }
            }
            if (model.Bill.EntsorgungActive)
            {
                if ((string.IsNullOrEmpty(model.Bill.EntsorgungVolume) || !decimal.TryParse(model.Bill.EntsorgungVolume.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                    && (string.IsNullOrEmpty(model.Bill.EntsorgungDuration) || !decimal.TryParse(model.Bill.EntsorgungDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp)))
                {
                    ModelState.AddModelError("Bill.EntsorgungVolume", "Please enter volume in m3. Eg 10 or 25.");
                }
                if ((model.Bill.EntsorgungPerPrice <= 0) && (model.Bill.EntsorgungPersonPerPrice <= 0))
                {
                    ModelState.AddModelError("Bill.EntsorgungPerPrice", "Please enter price per m3.");
                }
            }
            if (model.Bill.TransportActive)
            {
                if (string.IsNullOrEmpty(model.Bill.BillTransport.TransportDate))
                {
                    ModelState.AddModelError("Bill.BillTransport.TransportDate", "Please enter date.");
                }
                if (string.IsNullOrEmpty(model.Bill.BillTransport.TransportDuration) || !decimal.TryParse(model.Bill.BillTransport.TransportDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                {
                    ModelState.AddModelError("Bill.BillTransport.TransportDuration", "Please enter Duration in hours. Eg: 3 or 4.");
                }
                // if both tarif are empty
                if ((!model.Bill.BillTransport.TransportCostFixRatePrice.HasValue || model.Bill.BillTransport.TransportCostFixRatePrice <= 0)
                    && model.Bill.BillTransport.TransportPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.BillTransport.TransportCostFixRatePrice", "Please enter a Package Price Tariff. OR ..");
                    ModelState.AddModelError("Bill.BillTransport.TransportPerPrice", ".. OR. Please enter for hourly tariff price per hour.");
                }
                if (!string.IsNullOrEmpty(model.Bill.BillTransport.TransportDuration2))
                {
                    if (!decimal.TryParse(model.Bill.BillTransport.TransportDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                    {
                        ModelState.AddModelError("Bill.BillTransport.TransportDuration2", "Please enter Duration in hours. Eg: 3 or 4.");
                    }
                    if (model.Bill.BillTransport.TransportPerPrice2 <= 0)
                    {
                        ModelState.AddModelError("Bill.BillTransport.TransportPerPrice2", "Please enter Price per hour.");
                    }
                }
            }
            if (model.Bill.LagerungActive)
            {
                if (string.IsNullOrEmpty(model.Bill.LagerungDatePeriodFrom) || string.IsNullOrEmpty(model.Bill.LagerungDatePeriodTo))
                {
                    ModelState.AddModelError("Bill.LagerungDatePeriodFrom", "Please enter date.");
                }
                if (string.IsNullOrEmpty(model.Bill.LagerungVolume) || !decimal.TryParse(model.Bill.LagerungVolume.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out iTemp))
                {
                    ModelState.AddModelError("Bill.LagerungVolume", "Please enter volume in m3. Eg 10 or 25.");
                }
                if (model.Bill.LagerungPerPrice <= 0)
                {
                    ModelState.AddModelError("Bill.LagerungPerPrice", "Please enter price per m3.");
                }
            }
            if (model.Bill.EmailToCustomer)
            {
                if (string.IsNullOrEmpty(model.Bill.EmailAddress))
                {
                    ModelState.AddModelError("Bill.EmailAddress", "Please enter an email address.");
                }
            }
            #endregion

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (ModelState.IsValid)
            {
                model.Bill.BillCost = 0;
                if (model.Bill.UmzugActive)
                {
                    model = prepareSaveUmzugService(model, customer.AccountId);
                }
                if (model.Bill.PackActive)
                {
                    model = prepareSavePackService(model, customer.AccountId);
                }
                if (model.Bill.PackOutActive)
                {
                    model = prepareSavePackOutService(model, customer.AccountId);
                }
                if (model.Bill.ReinigungActive)
                {
                    model = prepareSaveReinigungService(model, customer.AccountId, 1);
                }
                if (model.Bill.Reinigung2Active)
                {
                    model = prepareSaveReinigungService(model, customer.AccountId, 2);
                }
                if (model.Bill.EntsorgungActive)
                {
                    model = prepareSaveEntsorgungService(model, customer.AccountId);
                }
                if (model.Bill.TransportActive)
                {
                    model = prepareSaveTransportService(model, customer.AccountId);
                }
                if (model.Bill.LagerungActive)
                {
                   
                    model = prepareSaveLagerungService(model, customer.AccountId);
                }
                if (model.Bill.PackMaterialActive)
                {
                    model = prepareSavePackMaterialService(model, customer.AccountId);
                }
                if (model.Bill.PreCollectionCost > 0)
                {
                    model.Bill.BillCost += model.Bill.PreCollectionCost;
                }
                model.Bill.BillCostString = model.Bill.BillCost.ToString().Replace(',', '.');

                // email: unselect if no email-address is set
                if (model.Bill.EmailToCustomer && string.IsNullOrEmpty(model.Bill.EmailAddress))
                {
                    model.Bill.EmailToCustomer = false;
                }
                if (!model.Bill.EmailToCustomer && !string.IsNullOrEmpty(model.Bill.EmailAddress))
                {
                    model.Bill.EmailAddress = null;
                }
                if (model.Bill.EmailToCustomer)
                {
                    model.Bill.SendStateId = Bill.SendState.Email;
                }

                // save to DB
                BillManager billManager = new BillManager();
                int billId = 0;
                if (model.Bill.BillId > 0)
                {
                    // update if has changed
                    model.Bill.ToPayDate = model.Bill.CreateDate.AddDays(((int)model.Bill.PaymentTermId));

                    billId = billManager.EditBill(model.Bill);
                }
                else
                {
                    billId = billManager.CreateBill(model.Bill, customer);
                }
                billManager.SetSelectedAdditionalCosts(billId, model);
                if (model.Bill.PackMaterialActive)
                {
                    billManager.SetDefinedPackMaterials(billId, model.BillDefinedPackMaterials);
                    billManager.SetPackMaterials(billId, model.BillPackMaterials);
                }

                // Customer Stage-Id change to Invoice
                if (customer.ContactStage < ContactStageType.Invoice)
                {
                    customerManager.UpdateContactStageType(customer, ContactStageType.Invoice);
                }

                if (model.Bill.EmailToCustomer)
                {
                    SendBillEmail(model.Bill, customer, model.EmailAttachments, "Bill");
                }

                return RedirectToAction("Index", new { customerId = customerId });
            }
            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            if (model.Bill.BillId <= 0)
            {
                // -- Create new
                ViewBag.SubTitle = "Create a new invoice";
                ViewBag.SubmitText = "Create";
            }
            else
            {
                // -- Edit
                model.IsEdit = true;
                ViewBag.SubTitle = "Edit Invoice";
                ViewBag.SubmitText = "Save";
            }

            // selectlistitems
            ViewBag.ReinigungTypeList = createCleaningTypeList(((int)model.Bill.BillCleaning.ReinigungType).ToString());
            ViewBag.Reinigung2TypeList = createCleaningTypeList(((int)model.Bill.BillCleaning2.ReinigungType).ToString());
            ViewBag.DefinedPackMaterialList = createDefinedPackMaterialList(customer.AccountId, "");

            // set print crypted id
            ViewBag.PrintId = (new BillManager()).GetCryptedBillCode(model.Bill);

            return View(model);
        }

        // GET: Intranet/Customer/Details/Bill/Details/1/1
        public async Task<ActionResult> Details(int customerId, int id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id <= 0))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own bill
            CustomerBillViewModel model = new CustomerBillViewModel();
            BillManager billManager = new BillManager();
            model.Bill = billManager.GetBillById(id);
            if ((model.Bill == null) || (model.Bill.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            ViewBag.CustomerId = customerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);
            model.IsEditable = false;
            // set print crypted id
            ViewBag.PrintId = billManager.GetCryptedBillCode(model.Bill);

            // -- Edit
            ViewBag.SubTitle = "View Invoice";
            if (TempData.ContainsKey("SuccessMessage"))
            {
                ViewBag.SuccessMessage = TempData["SuccessMessage"].ToString();
            }
            
            // preset email-address (is required to send reminder/precollection)
            if (string.IsNullOrEmpty(model.Bill.EmailAddress)) model.Bill.EmailAddress = customer.Email;

            // set relational depencies to default value for rendering UI
            if (model.Bill.BillCleanings == null)
            {
                model.Bill.BillCleanings = new List<BillCleaning>()
                {
                    new BillCleaning {BillId = model.Bill.BillId, SectorId = 1},
                    new BillCleaning {BillId = model.Bill.BillId, SectorId = 2}
                };
            }
            else
            {
                if (model.Bill.BillCleaning == null) model.Bill.BillCleanings.Add(new BillCleaning { BillId = model.Bill.BillId, SectorId = 1 });
                if (model.Bill.BillCleaning2 == null) model.Bill.BillCleanings.Add(new BillCleaning { BillId = model.Bill.BillId, SectorId = 2 });
            }
            if (model.Bill.BillTransports == null)
            {
                model.Bill.BillTransports = new List<BillTransport>() { new BillTransport {BillId = model.Bill.BillId, SectorId = 1} };
            }
            else
            {
                if (model.Bill.BillTransport == null) model.Bill.BillTransports.Add(new BillTransport { BillId = model.Bill.BillId, SectorId = 1 });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(model.Bill.CreateUserId);
                if (user != null)
                {
                    model.Bill.CreateUserId = user.UserName;
                }
            }

            model.UmzugPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "move", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "umzug"));
            model.PackPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "pack", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "pack"));
            model.PackOutPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "packout", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "packout"));
            model.ReinigungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "reinigung"));
            model.Reinigung2PriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "reinigung2", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "reinigung2"));
            model.EntsorgungPriceAdditCosts = createPriceAdditTarifList(customer.AccountId, "entsorgung", billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "entsorgung"));
            model.BillDefinedPackMaterials = createBillDefinedPackMaterials(billManager.GetDefinedPackMaterials(model.Bill.BillId));
            model.BillPackMaterials = createBillPackMaterials(billManager.GetPackMaterials(model.Bill.BillId));

            // selectlistitems
            ViewBag.ReinigungTypeList = createCleaningTypeList((model.Bill.ReinigungActive && model.Bill.BillCleaning?.ReinigungType != CleaningType.Please_choose) 
                ? ((int)model.Bill.BillCleaning.ReinigungType).ToString()
                : null);
            ViewBag.Reinigung2TypeList = createCleaningTypeList((model.Bill.Reinigung2Active && model.Bill.BillCleaning2?.ReinigungType != CleaningType.Please_choose)
                ? ((int)model.Bill.BillCleaning2.ReinigungType).ToString()
                : null);
            ViewBag.DefinedPackMaterialList = createDefinedPackMaterialList(customer.AccountId);

            return View("Edit", model);
        }

        // GET: Intranet/Customer/Details/Bill/Reminder/1/1
        public ActionResult Reminder(int customerId, int id, bool isreminder)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id <= 0))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own bill
            BillManager billManager = new BillManager();
            Bill bill = billManager.GetBillById(id);
            if ((bill == null) || (bill.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (string.IsNullOrEmpty(bill.EmailAddress)) bill.EmailAddress = customer.Email;

            if (isreminder)
            {
                billManager.UpdateBillState(bill, Bill.BillState.Memory);
                SendBillEmail(bill, customer, null, "Bill-Reminder");

                TempData.Add("SuccessMessage", string.Format("The reminder has been sent to the email address '{0}'.", bill.EmailAddress));
            }
            else
            {
                if (bill.PreCollectionCost <= 0)
                {
                    bill.PreCollectionCost = 20;
                    bill.BillCost += bill.PreCollectionCost;
                    bill.BillCostString = bill.BillCost.ToString().Replace(',', '.');
                }
                billManager.UpdateBillState(bill, Bill.BillState.Reminder);
                SendBillEmail(bill, customer, null, "Bill-PreCollection");

                TempData.Add("SuccessMessage", string.Format("The reminder has been sent to the email address '{0}'.", bill.EmailAddress));
            }

            return RedirectToAction("Details", new { customerId = customerId, id = id });
        }

        // GET: Intranet/Customer/Details/Bill/Payed/1/1
        public ActionResult Payed(int customerId, int id)
        {
            // check if own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(customerId, true);
            if ((customer == null) || (id <= 0))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            // security check if its own bill
            BillManager billManager = new BillManager();
            Bill bill = billManager.GetBillById(id);
            if ((bill == null) || (bill.CustomerId != customerId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            billManager.UpdateBillState(bill, Bill.BillState.Paid);

            TempData.Add("SuccessMessage", "The Invoice has been successfully marked as Paid");
            return RedirectToAction("Details", new { customerId = customerId, id = id });
        }

        // POST: Intranet/Customer/Details/Bill/Delete/1/1
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int customerId, int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            BillManager manager = new BillManager();
            Bill bill = manager.GetBillById(id);

            if ((bill == null) || (bill.CustomerId != customerId) || (bill.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            manager.DeleteBill(id);
            return RedirectToAction("Index", new { customerId = customerId });
        }

        public JsonResult EmailText(int customerId, string mailTemplate)
        {
            return Json(getEmailTextContainer(customerId, mailTemplate), JsonRequestBehavior.AllowGet);
        }

        private EmailTextContainer getEmailTextContainer(int customerId, string mailTemplate = "Bill", Bill bill = null, int billId = 0)
        {
            var emailTextContainer = new EmailTextContainer();
            var customer = (new CustomerManager()).GetCustomerById(customerId, true);
            var account = (new AccountManager()).GetAccountById(customer.AccountId);
            var customerManager = new CustomerManager();

            if (account.InnerAccountId <= 0) return null;

            // check if mailtemplate definition is correct
            var possibleTemplates = new string[] { "Bill", "Bill-Reminder", "Bill-PreCollection" };
            if (!possibleTemplates.Contains(mailTemplate))
            {
                mailTemplate = "Bill";
            }

            // personal email subject-title
            var emailType = (new EmailManager()).GetEmailTypeByAccountId(customer.AccountId, mailTemplate.Replace('-','.'));
            if (emailType != null && !string.IsNullOrEmpty(emailType.MailSubject)) emailTextContainer.Subject = emailType.MailSubject;
            else emailTextContainer.Subject = string.Format("Invoice - {0}", account.AccountName);

            // body
            var billMailModel = new BillMail
            {
                MainMailData = new MainMailData()
                {
                    InnerAccountId = account.InnerAccountId,
                    AccountName = account.AccountName,
                    AccountStreet = account.Street,
                    AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                    AccountPhone = account.Phone,
                    AccountFax = account.Fax,
                    AccountMail = account.Email,
                    AccountWebAddress = account.WebPage,
                    AccountContactName = account.ContactName,
                    PathAccount = string.Format("~/Content/accountdata/{0}/mail", account.InnerAccountId),
                    PathFooterLogo = "",
                    CustomerSalutation = customerManager.GetSalutation(customer),
                }
            };
            // set additional data to mailModel for Reminder / PreCollection mails
            if (mailTemplate == "Bill-Reminder" || mailTemplate == "Bill-PreCollection")
            {
                if ((bill == null) && (billId > 0))
                {
                    bill = (new BillManager()).GetBillById(billId, true, account.InnerAccountId);
                }
                if (bill != null)
                {
                    billMailModel.BillCost = bill.BillCost;
                    billMailModel.CostInclTax = bill.CostInclTax;
                    billMailModel.CostExclTax = bill.CostExclTax;
                    billMailModel.CostFreeTax = bill.CostFreeTax;
                    billMailModel.PreCollectionCost = bill.PreCollectionCost;
                    billMailModel.BillToPayUntil = bill.ToPayDate;
                }
            }

            emailTextContainer.Body = this.RenderPartialViewToString(string.Format("{0}/text/{1}.cshtml", billMailModel.MainMailData.PathAccount, mailTemplate), billMailModel);

            return emailTextContainer;
        }

        private bool SendBillEmail(Bill bill, Customer customer, List<CheckListViewModel> emailAttachments, string mailTemplate = "Bill")
        {
            var account = (new AccountManager()).GetAccountById(customer.AccountId);

            if (account.InnerAccountId > 0)
            {
                // send email
                EmailManager emailManager = new EmailManager();

                // email attachments
                List<Attachment> attachments = new List<Attachment>();

                // render pdf
                PdfManager pdfManager = new PdfManager();
                var pdfResult = pdfManager.GetForBillAsUrl(bill, this.ControllerContext.RequestContext);
                byte[] pdfResultByte = pdfResult.BuildPdf(this.ControllerContext);
                if (pdfResultByte.Length > 0)
                {
                    MemoryStream ms = new MemoryStream(pdfResultByte);
                    if ((account.InnerAccountId == 7) && (DateTime.Now > new DateTime(2016, 01, 01, 0, 0, 0)))
                    {
                        attachments.Add(new Attachment(ms, "Invoice.pdf", "application/pdf"));
                    }
                    else
                    {
                        attachments.Add(new Attachment(ms, string.Format("Invoice-{0}.pdf", bill.BillInnerId), "application/pdf"));
                    }
                }

                if (emailAttachments != null)
                {
                    IEnumerable<string> tempSelectAttachments = emailAttachments.Where(b => b.Selected).Select(s => s.Name);
                    List<Attachment> tempAttachmentList = emailManager.GetAttachmentofModel(tempSelectAttachments, customer.AccountId, account.InnerAccountId);
                    if (tempAttachmentList != null && tempAttachmentList.Count > 0) attachments.AddRange(tempAttachmentList);
                }

                // email subject and bodytext: load from model if user has changed the text, else initialize it
                string subject = null;
                string body = null;
                // if user has changed text
                if (bill.ChangeEmailText)
                {
                    if (!string.IsNullOrWhiteSpace(bill.ChangeEmailTextContainer?.Subject))
                    {
                        subject = bill.ChangeEmailTextContainer.Subject;
                    }
                    if (!string.IsNullOrWhiteSpace(bill.ChangeEmailTextContainer?.Body))
                    {
                        body = bill.ChangeEmailTextContainer.Body;
                    }
                }
                // else initialize new
                if (string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(body))
                {
                    var emailTextContainer = getEmailTextContainer(customer.CustomerId, mailTemplate, bill);
                    subject = subject ?? emailTextContainer.Subject;
                    body = body ?? emailTextContainer.Body;
                }

                EmailSender emailSender = (new EmailSenderFactory()).CreateEmailSender(account.AccountId.ToString());

                // send email (multiple receiver possible)
                bool isSuccessful = true;
                foreach (var receiverEmail in bill.EmailAddress.Replace(',', ';').Split(';'))
                {
                    // && isSuccessful: if there is an error on multiple receiver-version, then it is false. so let variable always false, that at the end false will be returned
                    isSuccessful = emailSender.SendEmailFromAccounting(receiverEmail, subject, body, attachments) && isSuccessful;
                }

                return isSuccessful;
            }
            return false;
        }


        #region HelperMethods

        private int getMaxIntOfRange(string value)
        {
            if (string.IsNullOrEmpty(value)) return 0;

            if (value.Contains('-'))
            {
                string[] split = value.Split('-');
                value = split[split.Length - 1];
            }
            int iMaxInt = 0;
            int.TryParse(value, out iMaxInt);

            return iMaxInt;
        }

        private List<Bill_DefinedPackMaterial> convertOfferDefinedPackToBillDefinedPack(List<Offer_DefinedPackMaterial> offerDefinedPackMaterials)
        {
            List<Bill_DefinedPackMaterial> list = new List<Bill_DefinedPackMaterial>();
            // get all offerDefinedPackMaterials and create as billDefinedPackMaterials
            offerDefinedPackMaterials.ForEach(x => list.Add(new Bill_DefinedPackMaterial() { DefinedPackMaterialId = x.DefinedPackMaterialId, RentBuy = x.RentBuy, PiecePrice = x.PiecePrice, CountNumber = x.CountNumber, EndPrice = x.EndPrice }));

            // fill to 10
            for (int i = list.Count - 1; i < 10; i++)
            {
                list.Add(new Bill_DefinedPackMaterial() { RentBuy = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private List<BillPackMaterial> convertOfferPackToBillPack(List<OfferPackMaterial> offerList)
        {
            List<BillPackMaterial> list = new List<BillPackMaterial>();
            // get all offerpacks and create as billpacks
            offerList.ForEach(x => list.Add(new BillPackMaterial() { Text = x.Text, MieteKauf = x.MieteKauf, PricePerPiece = x.PricePerPiece, CountNumber = x.CountNumber, EndPrice = x.EndPrice }));

            // fill to 10
            for (int i = list.Count - 1; i < 10; i++)
            {
                list.Add(new BillPackMaterial() { MieteKauf = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private List<PriceAdditCostsViewModel> createPriceAdditTarifList(int accountId, string serviceArea, List<OfferPriceAdditCosts> selectedAdditCosts = null)
        {
            List<PriceAdditCostsViewModel> list = new List<PriceAdditCostsViewModel>();
            PriceManager priceManager = new PriceManager();
            List<PriceAdditRate> priceAdditRates = priceManager.GetPriceAdditRatesByServiceType(accountId, serviceArea);
            if (priceAdditRates != null)
            {
                OfferPriceAdditCosts billPriceAddit = null;
                foreach (PriceAdditRate pr in priceAdditRates)
                {
                    if (selectedAdditCosts != null)
                    {
                        billPriceAddit = selectedAdditCosts.Find(s => s.PriceAdditRateId == pr.PriceAdditRateId);
                    }
                    list.Add(new PriceAdditCostsViewModel()
                    {
                        Id = pr.PriceAdditRateId,
                        Descr = pr.Descr,
                        DescrToShow = pr.DescrToShow,
                        Price = billPriceAddit != null ? billPriceAddit.OverwrittenPrice : pr.Price,
                        Selected = billPriceAddit != null ? true : false
                    });
                }
            }

            return list;
        }

        private List<SelectListItem> createCleaningTypeList(string selectValue = null)
        {
            List<SelectListItem> cleaningTypeList = new List<SelectListItem>();
            var cleaningEnum = Enum.GetValues(typeof(CleaningType));
            foreach (var bt in cleaningEnum)
            {
                cleaningTypeList.Add(new SelectListItem() { Value = ((int)bt).ToString(), Text = Helpers.Helpers.ReplaceEnumExtensionString(bt.ToString()), Selected = (selectValue != null && selectValue == ((int)bt).ToString()) ? true : false });
            }
            return cleaningTypeList;
        }

        private List<SelectListItem> createDefinedPackMaterialList(int accountId, string selectValue = null)
        {
            DefinedPackMaterialManager manager = new DefinedPackMaterialManager();
            return manager.GetDefinedPackMaterialAsSelectListItems(accountId, true, selectValue);
        }

        private List<Bill_DefinedPackMaterial> createBillDefinedPackMaterials(List<Bill_DefinedPackMaterial> selectedDefinedPackMaterials = null)
        {
            // In UI selected by dropdown
            List<Bill_DefinedPackMaterial> list = new List<Bill_DefinedPackMaterial>();
            int selectedCount = 0;

            if (selectedDefinedPackMaterials != null && selectedDefinedPackMaterials.Count > 0)
            {
                selectedDefinedPackMaterials.ForEach(x => list.Add(x));
                selectedCount = selectedDefinedPackMaterials.Count;
            }

            for (int i = selectedCount; i < 10; i++)
            {
                list.Add(new Bill_DefinedPackMaterial() { RentBuy = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private List<BillPackMaterial> createBillPackMaterials(List<BillPackMaterial> selectedPackMaterials = null)
        {
            List<BillPackMaterial> list = new List<BillPackMaterial>();
            int selectedCount = 0;

            if (selectedPackMaterials != null && selectedPackMaterials.Count > 0)
            {
                selectedPackMaterials.ForEach(x => list.Add(x));
                selectedCount = selectedPackMaterials.Count;
            }

            for (int i = selectedCount; i < 10; i++)
            {
                list.Add(new BillPackMaterial() { MieteKauf = MieteKaufType.Rental_fee });
            }

            return list;
        }

        private void selectDefinedSelectItem(ref List<SelectListItem> list, string value)
        {
            SelectListItem item = list.SingleOrDefault(a => a.Value == value);
            if (item != null) item.Selected = true;
        }

        private CustomerBillViewModel prepareSaveUmzugService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;
            if (decimal.TryParse(model.Bill.UmzugDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration))
            {
                // set price
                price = duration * model.Bill.UmzugPerPrice;
                model.Bill.UmzugCostService = price;

                // price segment 2
                if (!string.IsNullOrEmpty(model.Bill.UmzugDuration2))
                {
                    if ((model.Bill.UmzugPerPrice2 > 0)
                        && (decimal.TryParse(model.Bill.UmzugDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out duration)))
                    {
                        model.Bill.UmzugCostService2 = duration * model.Bill.UmzugPerPrice2;
                        price += model.Bill.UmzugCostService2.Value;
                    }
                    else
                    {
                        // remove UmzugDuration2
                        model.Bill.UmzugDuration2 = string.Empty;
                    }
                }
                else if (model.Bill.UmzugPerPrice2 > 0)
                {
                    // remove UmzugPerPrice2 if no UmzugDuration2 is set
                    model.Bill.UmzugPerPrice2 = 0;
                }

                decimal additionCosts = model.Bill.UmzugCostWay;
                if (model.Bill.UmzugCostAddit)
                {
                    model.UmzugPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                    additionCosts += model.Bill.UmzugCostAdditFreeTextPlus1Price + model.Bill.UmzugCostAdditFreeTextPlus2Price;
                }

                decimal subtractCosts = model.Bill.UmzugCostDiscount + model.Bill.UmzugCostDiscount2 + model.Bill.UmzugCostAdditFreeTextMinus1Price + model.Bill.UmzugCostAdditFreeTextMinus2Price;

                price = Math.Round(price + additionCosts - subtractCosts, 2);
                model.Bill.UmzugCostSub = price;
            }
            // if has highprice (take if real price is higher as highprice)
            if (model.Bill.UmzugCostHighSecurity && (model.Bill.UmzugCostHighSecurityPrice > 0) && (price > model.Bill.UmzugCostHighSecurityPrice)) price = model.Bill.UmzugCostHighSecurityPrice;
            // if is fixprice
            if (model.Bill.UmzugCostFix && (model.Bill.UmzugCostFixPrice > 0)) price = model.Bill.UmzugCostFixPrice;

            // set end price
            model.Bill.UmzugCost = Math.Round(price - model.Bill.UmzugCostDamage - model.Bill.UmzugCostPartialPayment - model.Bill.UmzugCostPartialByCash, 2);
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.UmzugCost;
            return model;
        }

        private CustomerBillViewModel prepareSavePackService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;
            if (decimal.TryParse(model.Bill.PackDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration))
            {
                // set price
                price = duration * model.Bill.PackPerPrice;
                model.Bill.PackCostService = price;

                // price segment 2
                if (!string.IsNullOrEmpty(model.Bill.PackDuration2))
                {
                    if ((model.Bill.PackPerPrice2 > 0)
                        && (decimal.TryParse(model.Bill.PackDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out duration)))
                    {
                        model.Bill.PackCostService2 = duration * model.Bill.PackPerPrice2;
                        price += model.Bill.PackCostService2.Value;
                    }
                    else
                    {
                        // remove PackDuration2
                        model.Bill.PackDuration2 = string.Empty;
                    }
                }
                else if (model.Bill.PackPerPrice2 > 0)
                {
                    // remove PackPerPrice2 if no PackDuration2 is set
                    model.Bill.PackPerPrice2 = 0;
                }

                decimal additionCosts = model.Bill.PackCostWay;
                if (model.Bill.PackCostAddit)
                {
                    model.PackPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                    additionCosts += model.Bill.PackCostAdditFreeTextPlus1Price + model.Bill.PackCostAdditFreeTextPlus2Price;
                }

                decimal subtractCosts = model.Bill.PackCostDiscount + model.Bill.PackCostDiscount2 + model.Bill.PackCostAdditFreeTextMinus1Price + model.Bill.PackCostAdditFreeTextMinus2Price;

                price = Math.Round(price + additionCosts - subtractCosts, 2);
                model.Bill.PackCostSub = price;
            }
            // if has highprice (take if real price is higher as highprice)
            if (model.Bill.PackCostHighSecurity && (model.Bill.PackCostHighSecurityPrice > 0) && (price > model.Bill.PackCostHighSecurityPrice)) price = model.Bill.PackCostHighSecurityPrice;
            // if is fixprice
            if (model.Bill.PackCostFix && (model.Bill.PackCostFixPrice > 0)) price = model.Bill.PackCostFixPrice;

            // set end price
            model.Bill.PackCost = Math.Round(price - model.Bill.PackCostDamage - model.Bill.PackCostPartialPayment - model.Bill.PackCostPartialByCash, 2);
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.PackCost;
            return model;
        }

        private CustomerBillViewModel prepareSavePackOutService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;
            if (decimal.TryParse(model.Bill.PackOutDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration))
            {
                // set price
                price = duration * model.Bill.PackOutPerPrice;
                model.Bill.PackOutCostService = price;

                // price segment 2
                if (!string.IsNullOrEmpty(model.Bill.PackOutDuration2))
                {
                    if ((model.Bill.PackOutPerPrice2 > 0)
                        && (decimal.TryParse(model.Bill.PackOutDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out duration)))
                    {
                        model.Bill.PackOutCostService2 = duration * model.Bill.PackOutPerPrice2;
                        price += model.Bill.PackOutCostService2.Value;
                    }
                    else
                    {
                        // remove PackOutDuration2
                        model.Bill.PackOutDuration2 = string.Empty;
                    }
                }
                else if (model.Bill.PackOutPerPrice2 > 0)
                {
                    // remove PackOutPerPrice2 if no PackOutDuration2 is set
                    model.Bill.PackOutPerPrice2 = 0;
                }

                decimal additionCosts = model.Bill.PackOutCostWay;
                if (model.Bill.PackOutCostAddit)
                {
                    model.PackOutPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                    additionCosts += model.Bill.PackOutCostAdditFreeTextPlus1Price + model.Bill.PackOutCostAdditFreeTextPlus2Price;
                }

                decimal subtractCosts = model.Bill.PackOutCostDiscount + model.Bill.PackOutCostDiscount2 + model.Bill.PackOutCostAdditFreeTextMinus1Price + model.Bill.PackOutCostAdditFreeTextMinus2Price;

                price = Math.Round(price + additionCosts - subtractCosts, 2);
                model.Bill.PackOutCostSub = price;
            }
            // if has highprice (take if real price is higher as highprice)
            if (model.Bill.PackOutCostHighSecurity && (model.Bill.PackOutCostHighSecurityPrice > 0) && (price > model.Bill.PackOutCostHighSecurityPrice)) price = model.Bill.PackOutCostHighSecurityPrice;
            // if is fixprice
            if (model.Bill.PackOutCostFix && (model.Bill.PackOutCostFixPrice > 0)) price = model.Bill.PackOutCostFixPrice;

            // set end price
            model.Bill.PackOutCost = Math.Round(price - model.Bill.PackOutCostDamage - model.Bill.PackOutCostPartialPayment - model.Bill.PackOutCostPartialByCash, 2);
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.PackOutCost;
            return model;
        }

        private CustomerBillViewModel prepareSaveReinigungService(CustomerBillViewModel model, int accountId, int sectorId)
        {
            BillCleaning helperBillCleaning = null;
            List<PriceAdditCostsViewModel> helperPriceAdditCosts = null;
            if (sectorId == 1)
            {
                helperBillCleaning = model.Bill.BillCleaning;
                helperPriceAdditCosts = model.ReinigungPriceAdditCosts;
            }
            else
            {
                helperBillCleaning = model.Bill.BillCleaning2;
                helperPriceAdditCosts = model.Reinigung2PriceAdditCosts;
            }

            // set endprice
            decimal subtractPartialPayments = helperBillCleaning.ReinigungCostDamage + helperBillCleaning.ReinigungCostPartialPayment + helperBillCleaning.ReinigungCostPartialByCash;
            decimal additionCosts = 0;
            if (helperBillCleaning.ReinigungCostAddit)
            {
                helperPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                additionCosts += helperBillCleaning.ReinigungCostAdditFreeTextPlus1Price + helperBillCleaning.ReinigungCostAdditFreeTextPlus2Price;
            }
            decimal subtractCosts = helperBillCleaning.ReinigungCostDiscount + helperBillCleaning.ReinigungCostDiscount2 + helperBillCleaning.ReinigungCostAdditFreeTextMinus1Price + helperBillCleaning.ReinigungCostAdditFreeTextMinus2Price;

            // if not sumcost is set in ui, then calculate
            if (helperBillCleaning.ReinigungCostSub <= 0)
            {
                decimal price = 0;
                // 2 possibility for calculate. take tarif flat-rate if set, else take hourlytarif
                if (!string.IsNullOrEmpty(helperBillCleaning.ReinigungRoom))
                {
                    price = helperBillCleaning.ReinigungPerPrice;
                }
                else if (decimal.TryParse(helperBillCleaning.ReinigungHourlyDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration))
                {
                    // set price
                    price = duration * helperBillCleaning.ReinigungHourlyPerPrice;
                    helperBillCleaning.ReinigungHourlyCostService = price;
                }

                helperBillCleaning.ReinigungCostSub = price + additionCosts - subtractCosts;
            }
            else
            {
                // set hourly costservice price, if hourlytarif was selected and cost is set manually in ui
                if (string.IsNullOrEmpty(helperBillCleaning.ReinigungRoom) 
                    && (decimal.TryParse(helperBillCleaning.ReinigungHourlyDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration)))
                {
                    // set cost service price
                    helperBillCleaning.ReinigungHourlyCostService = duration * helperBillCleaning.ReinigungHourlyPerPrice;
                }
            }


            helperBillCleaning.ReinigungCost = Math.Round(helperBillCleaning.ReinigungCostSub - subtractPartialPayments, 2);

            model.Bill.BillCost = model.Bill.BillCost + helperBillCleaning.ReinigungCost;

            if (sectorId == 1)
            {
                model.Bill.BillCleaning = helperBillCleaning;
            }
            else
            {
                model.Bill.BillCleaning2 = helperBillCleaning;
            }
            return model;
        }

        private CustomerBillViewModel prepareSaveEntsorgungService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;
            decimal priceVolume = 0;
            decimal additionCosts = 0;

            if (model.Bill.EntsorgungPerPrice > 0 && !string.IsNullOrEmpty(model.Bill.EntsorgungVolume) && (decimal.TryParse(model.Bill.EntsorgungVolume.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var volume)))
            {
                // set endprice
                priceVolume = volume * model.Bill.EntsorgungPerPrice;
                model.Bill.EntsorgungCostService = priceVolume;

                if (!string.IsNullOrEmpty(model.Bill.EntsorgungFixCost))
                {
                    decimal.TryParse(model.Bill.EntsorgungFixCost.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out additionCosts);
                }
            }

            if (model.Bill.EntsorgungPersonPerPrice > 0 && !string.IsNullOrEmpty(model.Bill.EntsorgungDuration) && (decimal.TryParse(model.Bill.EntsorgungDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration)))
            {
                // set endprice
                price = duration * model.Bill.EntsorgungPersonPerPrice;
                model.Bill.EntsorgungPersonCostService = price;

                if (model.Bill.EntsorgungCostWay > 0)
                {
                    additionCosts += model.Bill.EntsorgungCostWay;
                }
            }

            if (priceVolume > 0)
            {
                price += priceVolume;
            }

            if (price > 0)
            {
                if (model.Bill.EntsorgungCostAddit)
                {
                    model.EntsorgungPriceAdditCosts.Where(x => x.Selected).Select(x => x.Price).ToList().ForEach(x => additionCosts += x);
                    additionCosts += model.Bill.EntsorgungCostAdditFreeTextPlus1Price + model.Bill.EntsorgungCostAdditFreeTextPlus2Price;
                }

                decimal subtractCosts = model.Bill.EntsorgungCostDiscount + model.Bill.EntsorgungCostDiscount2 + model.Bill.EntsorgungCostAdditFreeTextMinus1Price + model.Bill.EntsorgungCostAdditFreeTextMinus2Price;

                price = Math.Round(price + additionCosts - subtractCosts, 2);
                model.Bill.EntsorgungCostSub = price;
            }
            // if has highprice (take if real price is higher as highprice)
            if (model.Bill.EntsorgungCostHighSecurity && (model.Bill.EntsorgungCostHighSecurityPrice > 0) && (price > model.Bill.EntsorgungCostHighSecurityPrice)) price = model.Bill.EntsorgungCostHighSecurityPrice;
            // if is fixprice
            if (model.Bill.EntsorgungCostFix && (model.Bill.EntsorgungCostFixPrice > 0)) price = model.Bill.EntsorgungCostFixPrice;

            // set end price
            model.Bill.EntsorgungCost = Math.Round(price - model.Bill.EntsorgungCostPartialPayment - model.Bill.EntsorgungCostPartialByCash, 2);
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.EntsorgungCost;
            return model;
        }

        private CustomerBillViewModel prepareSaveTransportService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;

            // which Tariff is selected: Flat_rate (CostFix) or HourlyEmployee (PriceRateId)
            if (model.Bill.BillTransport.TransportCostFixRatePrice > 0)
            {
                price = model.Bill.BillTransport.TransportCostFixRatePrice.Value;

                // remove other data
                model.Bill.BillTransport.TransportPerPrice = 0;
                model.Bill.BillTransport.TransportPerPrice2 = 0;
            }
            else
            {
                if (decimal.TryParse(model.Bill.BillTransport.TransportDuration.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var duration))
                {
                    // set price
                    price = duration * model.Bill.BillTransport.TransportPerPrice;
                    model.Bill.BillTransport.TransportCostService = price;

                    // price segment 2
                    if (!string.IsNullOrEmpty(model.Bill.BillTransport.TransportDuration2))
                    {
                        if ((model.Bill.BillTransport.TransportPerPrice2 > 0)
                            && (decimal.TryParse(model.Bill.BillTransport.TransportDuration2.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out duration)))
                        {
                            model.Bill.BillTransport.TransportCostService2 = duration * model.Bill.BillTransport.TransportPerPrice2;
                            price += model.Bill.BillTransport.TransportCostService2.Value;
                        }
                        else
                        {
                            // remove TransportDuration2
                            model.Bill.BillTransport.TransportDuration2 = string.Empty;
                        }
                    }
                    else if (model.Bill.BillTransport.TransportPerPrice2 > 0)
                    {
                        // remove TransportPerPrice2 if no TransportDuration2 is set
                        model.Bill.BillTransport.TransportPerPrice2 = 0;
                    }

                    // add some inserted additions
                    decimal additionCosts = model.Bill.BillTransport.TransportCostWay;
                    if (model.Bill.BillTransport.TransportCostAddit)
                    {
                        additionCosts += model.Bill.BillTransport.TransportCostAdditFreeTextPlus1Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus2Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus3Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus4Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus5Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus6Price +
                                         model.Bill.BillTransport.TransportCostAdditFreeTextPlus7Price;
                    }

                    price = price + additionCosts;
                    model.Bill.BillTransport.TransportCostSub = price;
                }

            }

            // subtract some inserted discounts
            decimal subtractCosts = model.Bill.BillTransport.TransportCostDiscount + model.Bill.BillTransport.TransportCostDiscount2 +
                                    model.Bill.BillTransport.TransportCostAdditFreeTextMinus1Price + model.Bill.BillTransport.TransportCostAdditFreeTextMinus2Price;

            price = Math.Round(price - subtractCosts, 2);
            model.Bill.BillTransport.TransportCostSub = price;

            // set also real CostFix-Field, because PDF-Print has already CostFix Rendering
            if (model.Bill.BillTransport.TransportCostFixRatePrice > 0)
            {
                model.Bill.BillTransport.TransportCostFixPrice = price;
            }

            // if has highprice (take if real price is higher as highprice)
            if (model.Bill.BillTransport.TransportCostHighSecurity && (model.Bill.BillTransport.TransportCostHighSecurityPrice > 0) && (price > model.Bill.BillTransport.TransportCostHighSecurityPrice)) price = model.Bill.BillTransport.TransportCostHighSecurityPrice;
            // if is fixprice
            if (model.Bill.BillTransport.TransportCostFix && (model.Bill.BillTransport.TransportCostFixPrice > 0)) price = model.Bill.BillTransport.TransportCostFixPrice;

            // set end price
            model.Bill.BillTransport.TransportCost = Math.Round(price - model.Bill.BillTransport.TransportCostDamage - model.Bill.BillTransport.TransportCostPartialPayment - model.Bill.BillTransport.TransportCostPartialByCash, 2);
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.BillTransport.TransportCost;
            return model;
        }

        private CustomerBillViewModel prepareSaveLagerungService(CustomerBillViewModel model, int accountId)
        {
            decimal price = 0;
            decimal subtractPartialPayments = model.Bill.LagerungCostPartialPayment + model.Bill.LagerungCostPartialByCash;
            decimal volume = 0;
            if (decimal.TryParse(model.Bill.LagerungVolume.Replace(',', '.'), System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out volume))
            {
                // set endprice
                price = volume * model.Bill.LagerungPerPrice;
                model.Bill.LagerungCostService = price;

                decimal additionCosts = model.Bill.LagerungCostAdditFreeTextPlus1Price + model.Bill.LagerungCostAdditFreeTextPlus2Price;
                decimal subtractCosts = model.Bill.LagerungCostDiscount + model.Bill.LagerungCostDiscount2 + model.Bill.LagerungCostAdditFreeTextMinus1Price + model.Bill.LagerungCostAdditFreeTextMinus2Price;

                model.Bill.LagerungCostSub = Math.Round(price + additionCosts - subtractCosts, 2);
                model.Bill.LagerungCost = Math.Round(model.Bill.LagerungCostSub - subtractPartialPayments, 2);
            }
            // take flat-rate, if selected and filled
            if (model.Bill.LagerungCostFix && (model.Bill.LagerungCostFixPrice > 0)) model.Bill.LagerungCost = model.Bill.LagerungCostFixPrice - subtractPartialPayments;
            model.Bill.BillCost = model.Bill.BillCost + model.Bill.LagerungCost;
            return model;
        }

        private CustomerBillViewModel prepareSavePackMaterialService(CustomerBillViewModel model, int accountId)
        {
            float price = 0f;

            // DefinedPackMaterial (UI: selected by dropdown)
            var bookedDefinedPackMaterials = model.BillDefinedPackMaterials.Where(x => x.DefinedPackMaterialId > 0 && x.CountNumber > 0).ToList();
            foreach (Bill_DefinedPackMaterial opm in bookedDefinedPackMaterials)
            {
                if (!string.IsNullOrEmpty(opm.PiecePrice)) opm.PiecePrice = opm.PiecePrice.Replace(',', '.');

                if (float.TryParse(opm.PiecePrice, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var pricePerPiece))
                {
                    float eachTotal = (opm.CountNumber * pricePerPiece);
                    opm.EndPrice = Helpers.Helpers.FloatToCurrencyFormat(eachTotal);
                    price += (eachTotal);
                }
            }

            // PackMaterial (UI: selected by textfield)
            var bookedPackMaterials = model.BillPackMaterials.Where(x => !string.IsNullOrEmpty(x.Text) && x.CountNumber > 0).ToList();
            foreach (BillPackMaterial opm in bookedPackMaterials)
            {
                if (!string.IsNullOrEmpty(opm.PricePerPiece)) opm.PricePerPiece = opm.PricePerPiece.Replace(',', '.');

                if (float.TryParse(opm.PricePerPiece, System.Globalization.NumberStyles.AllowDecimalPoint, System.Globalization.CultureInfo.InvariantCulture, out var pricePerPiece))
                {
                    float eachTotal = (opm.CountNumber * pricePerPiece);
                    opm.EndPrice = Helpers.Helpers.FloatToCurrencyFormat(eachTotal);
                    price += (eachTotal);
                }
            }

            price -= model.Bill.PackMaterialCostDiscount;
            if (model.Bill.PackMaterialDeliver)
            {
                price += model.Bill.PackMaterialDeliverPrice;
            }
            if (model.Bill.PackMaterialPickup)
            {
                price += model.Bill.PackMaterialPickupPrice;
            }

            model.Bill.PackMaterialCost = price.ToString().Replace(',', '.');
            model.Bill.BillCost = model.Bill.BillCost + (decimal)price;

            return model;
        }


        #endregion

    }
}