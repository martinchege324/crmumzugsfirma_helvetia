﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using System.Data.Entity;
using System.IO;
using System.Text;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using PagedList;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Helpers.CountryCode;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerHomeController : Controller
    {
        // Special Route for the whole control
        // GET: Intranet/Customer 
        public ActionResult Index(string sortOrder, int? page, int? reset)
        {
            // reset searchmodel
            if ((reset != null) && (reset == 1))
            {
                if (Session["CustomerSearchModel"] != null) Session.Remove("CustomerSearchModel");
                return RedirectToAction("Index");
            }

            CustomerSearchViewModel model = new CustomerSearchViewModel() { SearchFilter = new CustomerSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["CustomerSearchModel"] != null) model.SearchFilter = (CustomerSearchModel)Session["CustomerSearchModel"];

            return View(getPagedListFromDB(model, sortOrder, page));
        }

        // GET: Intranet/Print
        public ActionResult Print(string sortOrder)
        {
            CustomerSearchViewModel model = new CustomerSearchViewModel() { SearchFilter = new CustomerSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["CustomerSearchModel"] != null) model.SearchFilter = (CustomerSearchModel)Session["CustomerSearchModel"];
            model.PrintVersion = true;

            return PartialView("~/Areas/Intranet/Views/Customer/CustomerHome/ResultList.cshtml", getPagedListFromDB(model, sortOrder, 1, 99999));
        }

        // GET: Intranet/Csv
        public ActionResult Csv(string sortOrder)
        {
            CustomerSearchViewModel model = new CustomerSearchViewModel() { SearchFilter = new CustomerSearchModel() };
            if (Session["CustomerSearchModel"] != null) model.SearchFilter = (CustomerSearchModel)Session["CustomerSearchModel"];
            model.PrintVersion = true;

            List<Customer> customers = null;
            using (var db = new ApplicationDbContext())
            {
                customers = getListFromDB(db, model, sortOrder).ToList();
            }

            return File(mapListToCsv(customers), "text/csv", "Customer.csv");
        }

        // Special Route for the whole control
        // POST: Intranet/Customer 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(CustomerSearchViewModel model)
        {
            if (Session["CustomerSearchModel"] != null) Session.Remove("CustomerSearchModel");
            Session.Add("CustomerSearchModel", model.SearchFilter);
            return View(getPagedListFromDB(model, string.Empty, 1));
        }

        // GET: Admin/Customer/Edit/5
        // If Empty creates a new customer, else edit it
        public ActionResult Edit(int? id, string redir)
        {
            // check if redirect url defined
            Session.Remove("CustomerRedirUrl");
            if (!string.IsNullOrEmpty(redir))
            {
                Session.Add("CustomerRedirUrl", redir);
            }

            if (id == null)
            {
                // -- Create new account
                ViewBag.CountryCodeList = new CountryCodeManager().GetCountryCodeSelectListItems(string.Empty);

                ViewBag.Title = "Capture new customer";
                ViewBag.SubmitText = "Create";
                return View(new Customer() { MarketId = MarketType.Private_person });
            }

            // -- Edit customer

            // security check if its own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(id.Value, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // -- special validation fix
            if ((customer.MarketId == MarketType.Private_person) && (customer.CompanyName.Equals("-") || customer.CompanyContactPerson.Equals("-")))
            {
                customer.CompanyName = string.Empty;
                customer.CompanyContactPerson = string.Empty;
            }
            else if ((customer.MarketId == MarketType.Company) && (customer.LastName.Equals("-") || customer.FirstName.Equals("-")))
            {
                customer.TitleId = TitleType.Please_choose;
                customer.LastName = string.Empty;
                customer.FirstName = string.Empty;
            }

            ViewBag.CountryCodeList = new CountryCodeManager().GetCountryCodeSelectListItems(customer.CountryCode);

            ViewBag.Title = "Edit Customer";
            ViewBag.SubmitText = "Save";
            return View(customer);
        }

        // POST: Admin/Customer/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Customer customer)
        {
            if (isValidOffertenService(customer, ModelState))
            {
                UserIdentifier userIdentifier = (UserIdentifier)Session["UserIdentifier"];
                int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;
                if (customer.CustomerId > 0)
                {
                    // -- Edit customer

                    // security check if its own customer
                    if (customer.AccountId != accountId)
                    {
                        return RedirectToAction("NotFound", "Error", new { area = "" });
                    }

                    customer.ModifyDate = DateTime.Now;
                    customer.ModifyUserId = userIdentifier.UserId;

                    using (var db = new ApplicationDbContext())
                    {
                        db.Entry(customer).State = EntityState.Modified;
                        await db.SaveChangesAsync();
                    }
                }
                else
                {
                    // -- Create new account
                    // set default values
                    customer.AccountId = accountId;
                    customer.CreateDate = DateTime.Now;
                    
                    customer.CreateUserId = userIdentifier.UserId;
                    customer.IsActive = true;
                    customer.ContractType = ContractType.Lead;
                    customer.ContractTypeChangeDate = DateTime.Now;
                    customer.ContactStage = ContactStageType.Lead;

                    using (var db = new ApplicationDbContext())
                    {
                        // get next InnerCustomerId of current account
                        var nextInnerCustomerId = (1 + await db.Customers.Where(c => c.AccountId == accountId).OrderByDescending(o => o.CustomerInnerId).Select(s => s.CustomerInnerId).FirstOrDefaultAsync());
                        customer.CustomerInnerId = nextInnerCustomerId;

                        db.Customers.Add(customer);
                        await db.SaveChangesAsync();
                    }
                    return RedirectToRoute("Intranet_CustomerDetail", new { action = "Index", id = customer.CustomerId });
                }

                if (Session["CustomerRedirUrl"] != null)
                {
                    return Redirect(Session["CustomerRedirUrl"].ToString());
                }
                return RedirectToAction("Index");
            }
            else
            {
                // validator invalid
                ViewBag.CountryCodeList = new CountryCodeManager().GetCountryCodeSelectListItems(customer.CountryCode);

                if (customer.CustomerId <= 0)
                {
                    ViewBag.Title = "Capture new customer";
                    ViewBag.SubmitText = "Create";
                }
                else
                {
                    ViewBag.Title = "Edit Customer";
                    ViewBag.SubmitText = "Save";
                }
            }

            return View(customer);
        }

        // POST: Admin/RegisterAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            Customer customer = null;
            using (var db = new ApplicationDbContext())
            {
                customer = await db.Customers.FindAsync(id);
                if ((customer == null) || (customer.AccountId != accountId))
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                // get all dependent appointments. Remove google Calendar, and appointment itself
                AppointmentManager appointmentManager = new AppointmentManager();
                var appointmentList = await appointmentManager.GetAppointmentsByCustomer(customer.CustomerId);
                foreach (Appointment appointment in appointmentList)
                {
                    // delete calendar event
                    appointmentManager.CleanGoogleCalendar(appointment);
                }

                db.Customers.Remove(customer);
                await db.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        #region Helpers

        private CustomerSearchViewModel getPagedListFromDB(CustomerSearchViewModel model, string sortOrder, int? page, int? pageSize = null)
        {
            // sorting
            ViewBag.SortIdParm = String.IsNullOrEmpty(sortOrder) ? "id_asc" : "";
            ViewBag.SortNameParam = sortOrder == "name" ? "name_desc" : "name";
            ViewBag.SortCityParam = sortOrder == "city" ? "city_desc" : "city";
            ViewBag.SortStateParam = sortOrder == "satate" ? "state_desc" : "satate";
            ViewBag.SortInContactParam = sortOrder == "incontact" ? "incontact_desc" : "incontact";

            using (var db = new ApplicationDbContext())
            {
                IQueryable<Customer> query = getListFromDB(db, model, sortOrder);

                // paging (install in pm: Install-Package PagedList.Mvc)
                int pageNumber = (page ?? 1);
                model.ResultList = query.ToPagedList(pageNumber, pageSize ?? 10);
            }

            return model;
        }

        private IQueryable<Customer> getListFromDB(ApplicationDbContext db, CustomerSearchViewModel model,
            string sortOrder)
        {
            int accountId = ((UserIdentifier) Session["UserIdentifier"]).AccountId;

            IQueryable<Customer> query = db.Customers.Where(c => c.AccountId == accountId);

            // search filter
            if (model.SearchFilter.IsFilled())
            {
                query = mapFilterToQuery(query, model.SearchFilter);
            }

            // order
            switch (sortOrder)
            {
                case "id_asc":
                    query = query.OrderBy(s => s.CustomerId);
                    break;
                case "name":
                    query = query.OrderBy(s => s.LastName == "-").ThenBy(s => s.LastName);
                    break;
                case "name_desc":
                    query = query.OrderBy(s => s.LastName == "-").ThenByDescending(s => s.LastName)
                        .ThenByDescending(s => s.CompanyName);
                    break;
                case "city":
                    query = query.OrderBy(s => s.City);
                    break;
                case "city_desc":
                    query = query.OrderByDescending(s => s.City);
                    break;
                case "satate":
                    query = query.OrderBy(s => s.ContactStage);
                    break;
                case "state_desc":
                    query = query.OrderByDescending(s => s.ContactStage);
                    break;
                case "incontact":
                    query = query.OrderBy(s => s.ContactAlreadyHave);
                    break;
                case "incontact_desc":
                    query = query.OrderByDescending(s => s.ContactAlreadyHave);
                    break;
                default:
                    query = query.OrderByDescending(s => s.CustomerId);
                    break;
            }

            return query;
        }

        private IQueryable<Customer> mapFilterToQuery(IQueryable<Customer> query, CustomerSearchModel filter)
        {
            if (filter.MarketId > 0) query = query.Where(q => q.MarketId == filter.MarketId);
            if (!string.IsNullOrEmpty(filter.LastName)) query = query.Where(q => q.LastName.Contains(filter.LastName));
            if (!string.IsNullOrEmpty(filter.FirstName)) query = query.Where(q => q.FirstName.Contains(filter.FirstName));
            if (!string.IsNullOrEmpty(filter.CompanyName)) query = query.Where(q => q.CompanyName.Contains(filter.CompanyName));
            if (filter.Zip.HasValue) query = query.Where(q => q.Zip == filter.Zip.Value);
            if (!string.IsNullOrEmpty(filter.City)) query = query.Where(q => q.City == filter.City);
            if (filter.ContractType > 0) query = query.Where(q => q.ContractType == filter.ContractType);
            if (filter.DateCreateFrom.HasValue) query = query.Where(q => q.CreateDate > filter.DateCreateFrom.Value);
            if (filter.DateCreateTo.HasValue) query = query.Where(q => q.CreateDate < filter.DateCreateTo.Value);
            if (filter.NotReached) query = query.Where(q => !q.ContactAlreadyHave);
            return query;
        }

        private Stream mapListToCsv(List<Customer> customers)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(ms, Encoding.UTF8);
            csvWriter.WriteLine("Id;Name;Anrede;Kontaktperson_NurFirma;Adresse;Place;Country;Telephone;Email;Type;In_Kontakt;Was standing;Erstellungsdatum");

            CountryCodeManager countryCodeManager = new CountryCodeManager();

            customers.ForEach(c => csvWriter.WriteLine(c.CustomerInnerId.ToString()
                                                 + ";" + (c.MarketId == MarketType.Private_person ? c.LastName + " " + c.FirstName : c.CompanyName)
                                                 + ";" + (c.TitleId == TitleType.Mrs ? "Mrs" : "Mr")
                                                 + ";" + c.CompanyContactPerson
                                                 + ";" + c.Street
                                                 + ";" + c.Zip.ToString() + " " + c.City
                                                 + ";" + countryCodeManager.GetDescriptionByCode(c.CountryCode)
                                                 + ";" + (!string.IsNullOrEmpty(c.Phone) ? c.Phone : c.Mobile)
                                                 + ";" + c.Email
                                                 + ";" + c.MarketId.ToString()
                                                 + ";" + (c.ContactAlreadyHave ? "Yes" : "no")
                                                 + ";" + c.ContactStage.ToString()
                                                 + ";" + c.CreateDate.ToString()
            ));

            csvWriter.Flush();
            ms.Position = 0;
            return ms;
        }

        private bool isValidOffertenService(Customer customer, ModelStateDictionary modelState)
        {
            // fix hidden div errors
            if (customer.MarketId == MarketType.Private_person)
            {
                if (modelState.Keys.Contains("CompanyName")) modelState["CompanyName"].Errors.Clear();
                if (modelState.Keys.Contains("CompanyContactPerson")) modelState["CompanyContactPerson"].Errors.Clear();
                customer.CompanyName = "-";
                customer.CompanyContactPerson = "-";
            }
            else
            {
                if (modelState.Keys.Contains("TitleId")) modelState["TitleId"].Errors.Clear();
                if (modelState.Keys.Contains("LastName")) modelState["LastName"].Errors.Clear();
                if (modelState.Keys.Contains("FirstName")) modelState["FirstName"].Errors.Clear();
                customer.TitleId = TitleType.Please_choose;
                customer.LastName = "-";
                customer.FirstName = "-";
            }

            return modelState.IsValid;
        }

        #endregion

    }
}