﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers.Customers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class CustomerDetailController : Controller
    {
        // GET: Intranet/Customer/Details
        public async Task<ActionResult> Index(int? id)
        {
            if (!id.HasValue) return RedirectToAction("NotFound", "Error", new { area = "" });

            // security check if its own customer
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(id.Value, true);
            if (customer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(customer.CreateUserId);
                if (user != null)
                {
                    customer.CreateUserId = user.UserName;
                }
            }

            ViewBag.CustomerId = customer.CustomerId;
            ViewBag.Title = string.Format("Customer: {0}", customer.MarketId == MarketType.Private_person ? customer.LastName + " " + customer.FirstName : customer.CompanyName);

            return View(customer);
        }
    }
}