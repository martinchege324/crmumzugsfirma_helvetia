﻿using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Appointments;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Helpers;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class AppointmentsController : Controller
    {
        // Special Route for the whole control
        // GET: Intranet/Appointment
        public ActionResult Index(string sortOrder, int? page, int? reset)
        {
            // reset searchmodel
            if ((reset != null) && (reset == 1))
            {
                if (Session["AppointmentSearchModel"] != null) Session.Remove("AppointmentSearchModel");
                return RedirectToAction("Index");
            }

            AppointmentViewModel model = new AppointmentViewModel() { SearchFilter = new AppointmentSearchModel() { OnlyNextAppointments = true, AppointmentTypeId = Appointment.AppointmentType.Sightseeing } };
            ViewBag.CurrentSort = sortOrder;
            if (Session["AppointmentSearchModel"] != null) model.SearchFilter = (AppointmentSearchModel)Session["AppointmentSearchModel"];

            return View(getPagedListFromDB(model, sortOrder, page));
        }

        // GET: Intranet/Print
        public ActionResult Print(string sortOrder)
        {
            AppointmentViewModel model = new AppointmentViewModel() { SearchFilter = new AppointmentSearchModel() { OnlyNextAppointments = true, AppointmentTypeId = Appointment.AppointmentType.Sightseeing } };
            ViewBag.CurrentSort = sortOrder;
            if (Session["AppointmentSearchModel"] != null) model.SearchFilter = (AppointmentSearchModel)Session["AppointmentSearchModel"];
            model.PrintVersion = true;

            return PartialView("~/Areas/Intranet/Views/Appointments/ResultList.cshtml", getPagedListFromDB(model, sortOrder, 1, 99999));
        }

        // GET: Intranet/Csv
        public ActionResult Csv(string sortOrder)
        {
            AppointmentViewModel model = new AppointmentViewModel() { SearchFilter = new AppointmentSearchModel() { OnlyNextAppointments = true, AppointmentTypeId = Appointment.AppointmentType.Sightseeing } };
            if (Session["AppointmentSearchModel"] != null) model.SearchFilter = (AppointmentSearchModel)Session["AppointmentSearchModel"];
            if (string.IsNullOrEmpty(sortOrder)) sortOrder = "datefrom";
            model.PrintVersion = true;

            List<AppointmentResultModel> appointments = null;
            using (var db = new ApplicationDbContext())
            {
                appointments = getListFromDB(db, model, sortOrder).ToList();
            }

            return File(mapListToCsv(appointments), "text/csv", "Appointments.csv");
        }

        // Special Route for the whole control
        // POST: Intranet/Customer 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AppointmentViewModel model)
        {
            if (Session["AppointmentSearchModel"] != null) Session.Remove("AppointmentSearchModel");
            Session.Add("AppointmentSearchModel", model.SearchFilter);
            return View(getPagedListFromDB(model, string.Empty, 1));
        }


        // Special Route for the whole control
        // POST: Intranet/Appointments/Details/2
        public async Task<ActionResult> Details(int id)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            AppointmentManager manager = new AppointmentManager();
            Appointment appointment = manager.GetAppointmentById(id);
            if ((appointment == null) || (appointment.AccountId != accountId))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(appointment.CreateUserId);
                if (user != null)
                {
                    appointment.CreateUserId = user.UserName;
                }
            }

            return View(appointment);
        }

        #region Helpers

        private AppointmentViewModel getPagedListFromDB(AppointmentViewModel model, string sortOrder, int? page, int? pageSize = null)
        {
            // sorting: default datefrom for new functionality all date from today
            if (String.IsNullOrEmpty(sortOrder)) sortOrder = "datefrom";
            ViewBag.SortDateFromParam = sortOrder == "datefrom" ? "datefrom_desc" : "datefrom";
            ViewBag.SortDateToParam = sortOrder == "dateto" ? "dateto_desc" : "dateto";
            ViewBag.SortTypeParm = sortOrder == "type" ? "type_desc" : "type";

            // paging (install in pm: Install-Package PagedList.Mvc)


            using (var db = new ApplicationDbContext())
            {
                IQueryable<AppointmentResultModel> query = getListFromDB(db, model, sortOrder);

                // paging (install in pm: Install-Package PagedList.Mvc)
                int pageNumber = (page ?? 1);
                IPagedList<AppointmentResultModel> pagedList = query.ToPagedList(pageNumber, pageSize ?? 10);

                model.ResultList = pagedList;
            }

            return model;
        }

        private IQueryable<AppointmentResultModel> getListFromDB(ApplicationDbContext db, AppointmentViewModel model, string sortOrder)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            IQueryable<AppointmentResultModel> query = (from a in db.Appointments.Include("AppointmentView").Include("AppointmentService").Include("AppointmentDelivery")
                                                        join c in db.Customers on a.CustomerId equals c.CustomerId
                                 where a.AccountId == accountId
                                 select new AppointmentResultModel()
                                 {
                                     AppointmentId = a.AppointmentId,
                                     CustomerId = a.CustomerId,
                                     AccountId = a.AccountId,
                                     AppointmentTypeId = a.AppointmentTypeId,
                                     OfferInnerId = a.OfferInnerId,
                                     Where = a.Where,
                                     CalendarNote = a.CalendarNote,
                                     EmailToCustomer = a.EmailToCustomer,
                                     EmailAddress = a.EmailAddress,
                                     EmailNote = a.EmailNote,
                                     AppointmentView = a.AppointmentView,
                                     AppointmentService = a.AppointmentService,
                                     AppointmentDelivery = a.AppointmentDelivery,
                                     CustomerMarketId = c.MarketId,
                                     LastName = c.LastName,
                                     FirstName = c.FirstName,
                                     TitleId = c.TitleId,
                                     CompanyName = c.CompanyName,
                                     CompanyContactPerson = c.CompanyContactPerson,
                                     Phone = c.Phone,
                                     Email = c.Email,
                                     CreateDate = a.CreateDate,
                                     CreateUserName = a.CreateUserId
                                 });

            // search filter
            if (model.SearchFilter.IsFilled())
            {
                query = mapFilterToQuery(query, model.SearchFilter);
            }

            // order
            switch (sortOrder)
            {
                case "type":
                    query = query.OrderBy(s => s.AppointmentTypeId);
                    break;
                case "type_desc":
                    query = query.OrderByDescending(s => s.AppointmentTypeId);
                    break;
                case "datefrom":
                    query = query.OrderBy(s => s.AppointmentTypeId == Appointment.AppointmentType.Sightseeing ? s.AppointmentView.ViewDateTime :
                        s.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order ? 
                            (
                            s.AppointmentService.UmzugDateTime.HasValue ? s.AppointmentService.UmzugDateTime :
                            s.AppointmentService.PackDateTime.HasValue ? s.AppointmentService.PackDateTime :
                            s.AppointmentService.PackOutDateTime.HasValue ? s.AppointmentService.PackOutDateTime :
                            s.AppointmentService.ReinigungDateTime.HasValue ? s.AppointmentService.ReinigungDateTime :
                            s.AppointmentService.Reinigung2DateTime.HasValue ? s.AppointmentService.Reinigung2DateTime :
                            s.AppointmentService.EntsorgungDateTime.HasValue ? s.AppointmentService.EntsorgungDateTime :
                            s.AppointmentService.TransportDateTime) :
                        s.AppointmentTypeId == Appointment.AppointmentType.Delivery ? s.AppointmentDelivery.DeliveryDateTime : null);
                    break;
                case "datefrom_desc":
                    query = query.OrderByDescending(s => s.AppointmentTypeId == Appointment.AppointmentType.Sightseeing ? s.AppointmentView.ViewDateTime :
                        s.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order ?
                            (s.AppointmentService.UmzugDateTime.HasValue ? s.AppointmentService.UmzugDateTime :
                            s.AppointmentService.PackDateTime.HasValue ? s.AppointmentService.PackDateTime :
                            s.AppointmentService.PackOutDateTime.HasValue ? s.AppointmentService.PackOutDateTime :
                            s.AppointmentService.ReinigungDateTime.HasValue ? s.AppointmentService.ReinigungDateTime :
                            s.AppointmentService.Reinigung2DateTime.HasValue ? s.AppointmentService.Reinigung2DateTime :
                            s.AppointmentService.EntsorgungDateTime.HasValue ? s.AppointmentService.EntsorgungDateTime :
                            s.AppointmentService.TransportDateTime) :
                        s.AppointmentTypeId == Appointment.AppointmentType.Delivery ? s.AppointmentDelivery.DeliveryDateTime : null);
                    break;
                default:
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
            }

            return query;
        }

        private IQueryable<AppointmentResultModel> mapFilterToQuery(IQueryable<AppointmentResultModel> query, AppointmentSearchModel filter)
        {
            if (filter.AppointmentTypeId > 0) query = query.Where(q => q.AppointmentTypeId == filter.AppointmentTypeId);
            //if (filter.MeetPlace > 0) query = query.Where(q => q.MeetPlace == filter.MeetPlace);
            if (filter.DateCreateFrom.HasValue || filter.DateCreateTo.HasValue)
            {
                if (filter.DateCreateFrom.HasValue)
                {
                    var dateFrom = filter.DateCreateFrom.Value;
                    query = query.Where(s => s.AppointmentTypeId == Appointment.AppointmentType.Sightseeing ? s.AppointmentView.ViewDateTime > dateFrom :
                        s.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order ?
                            (s.AppointmentService.UmzugDateTime.HasValue ? s.AppointmentService.UmzugDateTime > dateFrom :
                                s.AppointmentService.PackDateTime.HasValue ? s.AppointmentService.PackDateTime > dateFrom :
                                s.AppointmentService.PackOutDateTime.HasValue ? s.AppointmentService.PackOutDateTime > dateFrom :
                                s.AppointmentService.ReinigungDateTime.HasValue ? s.AppointmentService.ReinigungDateTime > dateFrom :
                                s.AppointmentService.Reinigung2DateTime.HasValue ? s.AppointmentService.Reinigung2DateTime > dateFrom :
                                s.AppointmentService.EntsorgungDateTime.HasValue ? s.AppointmentService.EntsorgungDateTime > dateFrom :
                                s.AppointmentService.TransportDateTime > dateFrom) :
                            s.AppointmentTypeId != Appointment.AppointmentType.Delivery || s.AppointmentDelivery.DeliveryDateTime > dateFrom);
                }
                if (filter.DateCreateTo.HasValue)
                {
                    var dateTo = filter.DateCreateTo.Value;
                    query = query.Where(s => s.AppointmentTypeId == Appointment.AppointmentType.Sightseeing ? s.AppointmentView.ViewDateTime < dateTo :
                        s.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order ?
                            (s.AppointmentService.UmzugDateTime.HasValue ? s.AppointmentService.UmzugDateTime < dateTo :
                                s.AppointmentService.PackDateTime.HasValue ? s.AppointmentService.PackDateTime < dateTo :
                                s.AppointmentService.PackOutDateTime.HasValue ? s.AppointmentService.PackOutDateTime < dateTo :
                                s.AppointmentService.ReinigungDateTime.HasValue ? s.AppointmentService.ReinigungDateTime < dateTo :
                                s.AppointmentService.Reinigung2DateTime.HasValue ? s.AppointmentService.Reinigung2DateTime < dateTo :
                                s.AppointmentService.EntsorgungDateTime.HasValue ? s.AppointmentService.EntsorgungDateTime < dateTo :
                                s.AppointmentService.TransportDateTime < dateTo) :
                            s.AppointmentTypeId != Appointment.AppointmentType.Delivery || s.AppointmentDelivery.DeliveryDateTime < dateTo);
                }
            }
            else if (filter.OnlyNextAppointments)
            {
                var dateTo = DateTime.Now;
                query = query.Where(s => s.AppointmentTypeId == Appointment.AppointmentType.Sightseeing ? s.AppointmentView.ViewDateTime > dateTo :
                    s.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order ?
                        (s.AppointmentService.UmzugDateTime.HasValue ? s.AppointmentService.UmzugDateTime > dateTo :
                            s.AppointmentService.PackDateTime.HasValue ? s.AppointmentService.PackDateTime > dateTo :
                            s.AppointmentService.PackOutDateTime.HasValue ? s.AppointmentService.PackOutDateTime > dateTo :
                            s.AppointmentService.ReinigungDateTime.HasValue ? s.AppointmentService.ReinigungDateTime > dateTo :
                            s.AppointmentService.Reinigung2DateTime.HasValue ? s.AppointmentService.Reinigung2DateTime > dateTo :
                            s.AppointmentService.EntsorgungDateTime.HasValue ? s.AppointmentService.EntsorgungDateTime > dateTo :
                            s.AppointmentService.TransportDateTime > dateTo) :
                        s.AppointmentTypeId != Appointment.AppointmentType.Delivery || s.AppointmentDelivery.DeliveryDateTime > dateTo);
            }
            return query;
        }

        private Stream mapListToCsv(List<AppointmentResultModel> appointments)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(ms, Encoding.UTF8);
            csvWriter.WriteLine("Type;Service;Customer;Anrede;Kontaktperson_NurFirma;Telephone;Email;Adresse;Termin_am;Erstellungsdatum");

            foreach (var a in appointments)
            {
                csvWriter.WriteLine(a.AppointmentTypeId.ToString()
                    + ";" + a.BookedServices
                    + ";" + (a.CustomerMarketId == MarketType.Private_person ? a.LastName + " " + a.FirstName : a.CompanyName)
                    + ";" + (a.TitleId == TitleType.Mrs ? "Mrs" : "Mr")
                    + ";" + a.CompanyContactPerson
                    + ";" + a.Phone
                    + ";" + a.Email
                    + ";" + a.Where
                    + ";" + a.AppointmentDateTime?.ToString("dd:MM:yyyy-hh:mm")
                    + ";" + a.CreateDate.ToString()
                );
            }

            csvWriter.Flush();
            ms.Position = 0;
            return ms;
        }

        #endregion

    }
}