﻿using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class HomeController : Controller
    {
        // GET: Intranet/Home
        public ActionResult Index()
        {
            CustomerHomeViewModel model = new CustomerHomeViewModel();
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            BillManager billManager = new BillManager();
            model.OpenBillReminder = billManager.GetCountReminderBill(accountId);
            model.OpenBillPrecollection = billManager.GetCountPreCollectionBill(accountId);

            var receiptManager = new ReceiptManager();
            model.OpenReceiptsInPast = receiptManager.GetCountOpenReceipt(accountId);

            return View(model);
        }
    }
}