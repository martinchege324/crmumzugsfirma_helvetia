﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class ManageAccountController : Controller
    {

        public async Task<ActionResult> Index(ManageAccountId? message)
        {
            UserIdentifier userIdentifier = null;
            if (Session["UserIdentifier"] != null)
            {
                userIdentifier = ((UserIdentifier)Session["UserIdentifier"]);
            }

            if ((userIdentifier == null) || (userIdentifier.AccountId <= 0))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Title = string.Format("Account '{0}' to edit", userIdentifier.AccountName);
            // check if is just edited successful
            ViewBag.StatusMessage =
                message == ManageAccountId.EditSuccess ? "Die Änderungen wurden erfolgreich gespeichert."
                : "";

            using (var db = new ApplicationDbContext())
            {
                Account account = await db.Accounts.FindAsync(userIdentifier.AccountId);
                if (account == null)
                {
                    return RedirectToAction("NotFound", "Error", new { area = "" });
                }

                return View(account);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(Account account)
        {
            UserIdentifier userIdentifier = null;
            if (Session["UserIdentifier"] != null)
            {
                userIdentifier = ((UserIdentifier)Session["UserIdentifier"]);
            }
            if (userIdentifier.AccountId <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                using (var db = new ApplicationDbContext())
                {
                    account.ModifyDate = DateTime.Now;
                    account.ModifyUserId = userIdentifier.UserId;

                    db.Entry(account).State = EntityState.Modified;

                    await db.SaveChangesAsync();
                }
                return RedirectToAction("Index", new { Message = ManageAccountId.EditSuccess });
            }
            else
            {
                ViewBag.Title = string.Format("Account '{0}' to edit", userIdentifier.AccountName);
                ViewBag.ErrorMessage = "Es ist ein Fehler passiert. Bitte versuchen Sie nochmals.";
            }

            return View(account);
        }


        public enum ManageAccountId
        {
            EditSuccess,
            Error
        }
    
    }
}