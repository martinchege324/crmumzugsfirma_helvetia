﻿using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Helpers;
using System.Text;
using System.IO;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Receipts;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class ReceiptsController : Controller
    {
        // Special Route for the whole control
        // GET: Intranet/Receipts
        public ActionResult Index(string sortOrder, int? page, int? reset, int? openreceipt)
        {
            // reset searchmodel
            if ((reset != null) && (reset == 1))
            {
                if (Session["ReceiptSearchModel"] != null) Session.Remove("ReceiptSearchModel");
                return RedirectToAction("Index");
            }

            var model = new ReceiptViewModel() { SearchFilter = new ReceiptSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["ReceiptSearchModel"] != null) model.SearchFilter = (ReceiptSearchModel)Session["ReceiptSearchModel"];

            if (openreceipt.HasValue && openreceipt == 1)
            {
                // reset search filter and put paystate
                model.SearchFilter = new ReceiptSearchModel()
                {
                    ReceiptStateId = Receipt.ReceiptState.Open,
                    JobDateTo = DateTime.Today
                };

                // if no session of receipt searchfilter exists, so set the new searchfilter for paging
                if (Session["ReceiptSearchModel"] != null) Session.Remove("ReceiptSearchModel");
                Session.Add("ReceiptSearchModel", model.SearchFilter);
            }

            return View(getPagedListFromDB(model, sortOrder, page));
        }

        // Special Route for the whole control
        // POST: Intranet/Receipts 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(ReceiptViewModel model)
        {
            if (Session["ReceiptSearchModel"] != null) Session.Remove("ReceiptSearchModel");
            Session.Add("ReceiptSearchModel", model.SearchFilter);
            return View(getPagedListFromDB(model, string.Empty, 1));
        }

        // GET: Intranet/Print
        public ActionResult Print(string sortOrder)
        {
            ReceiptViewModel model = new ReceiptViewModel() { SearchFilter = new ReceiptSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["ReceiptSearchModel"] != null) model.SearchFilter = (ReceiptSearchModel)Session["ReceiptSearchModel"];
            model.PrintVersion = true;

            return PartialView("~/Areas/Intranet/Views/Receipts/ResultList.cshtml", getPagedListFromDB(model, sortOrder, 1, 99999));
        }

        // GET: Intranet/Csv
        public ActionResult Csv(string sortOrder)
        {
            ReceiptViewModel model = new ReceiptViewModel() { SearchFilter = new ReceiptSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["ReceiptSearchModel"] != null) model.SearchFilter = (ReceiptSearchModel)Session["ReceiptSearchModel"];
            model.PrintVersion = true;

            List<ReceiptResultModel> receipts = null;
            using (var db = new ApplicationDbContext())
            {
                receipts = getListFromDB(db, model, sortOrder).ToList();
            }

            return File(mapListToCsv(receipts), "text/csv", "Order Receipts.csv");
        }

        // Special Route for the whole control
        // POST: Intranet/Receipts/Details/2
        public async Task<ActionResult> Details(Guid id)
        {
            ReceiptManager manager = new ReceiptManager();

            var model = new CustomerReceiptViewModel
            {
                Receipt = manager.GetReceiptById(id, true)
            };

            if (model.Receipt == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(model.Receipt.CreateUserId);
                if (user != null)
                {
                    model.Receipt.CreateUserId = user.UserName;
                }
            }

            if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.Move)
                model.ReceiptUmzug = model.Receipt.ReceiptUmzugs.FirstOrDefault();
            else if (model.Receipt.ReceiptTypeId == Receipt.ReceiptType.cleaning)
                model.ReceiptReinigung = model.Receipt.ReceiptReinigungs.FirstOrDefault();

            return View(model);
        }

        #region Helpers

        private ReceiptViewModel getPagedListFromDB(ReceiptViewModel model, string sortOrder, int? page, int? pageSize = null)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            // sorting
            ViewBag.SortIdParam = String.IsNullOrEmpty(sortOrder) || sortOrder == "receiptid" ? "receiptid_desc" : "receiptid";
            ViewBag.SortDateCreateParam = sortOrder == "datecreate" ? "datecreate_desc" : "datecreate";
            ViewBag.SortJobDateParam = sortOrder == "jobdate" ? "jobdate_desc" : "jobdate";
            ViewBag.SortTypeIdParam = sortOrder == "typeid" ? "typeid_desc" : "typeid";
            ViewBag.SortStateIdParam = sortOrder == "stateid" ? "stateid_desc" : "stateid";

            // paging (install in pm: Install-Package PagedList.Mvc)


            using (var db = new ApplicationDbContext())
            {
                IQueryable<ReceiptResultModel> query = getListFromDB(db, model, sortOrder);

                // paging (install in pm: Install-Package PagedList.Mvc)
                int pageNumber = (page ?? 1);
                IPagedList<ReceiptResultModel> pagedList = query.ToPagedList(pageNumber, pageSize ?? 10);

                model.ResultList = pagedList;
            }
            return model;
        }

        private IQueryable<ReceiptResultModel> getListFromDB(ApplicationDbContext db, ReceiptViewModel model, string sortOrder)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            IQueryable<ReceiptResultModel> query = (from a in db.Receipts
                                                 join c in db.Customers on a.CustomerId equals c.CustomerId
                                                 where a.AccountId == accountId
                                                 select new ReceiptResultModel()
                                                 {
                                                     ReceiptId = a.ReceiptId,
                                                     ReceiptInnerId = a.ReceiptInnerId,
                                                     ReceiptInnerSubId = a.ReceiptInnerSubId,
                                                     CustomerId = a.CustomerId,
                                                     AccountId = a.AccountId,
                                                     OfferId = a.OfferId,
                                                     ReceiptTypeId = a.ReceiptTypeId,
                                                     ReceiptStateId = a.ReceiptStateId,
                                                     PayTypeId = a.PayTypeId,
                                                     TotalCost = a.TotalCost,
                                                     JobDate = a.JobDate,
                                                     JobTime = a.JobTime,
                                                     JobDateTime = a.JobDateTime,
                                                     CreateDate = a.CreateDate,
                                                     CreateUserName = a.CreateUserId,
                                                     CustomerMarketId = c.MarketId,
                                                     LastName = c.LastName,
                                                     FirstName = c.FirstName,
                                                     CompanyName = c.CompanyName,
                                                     ModifyDate = c.ModifyDate
                                                 });

            // search filter
            if (model.SearchFilter.IsFilled())
            {
                query = mapFilterToQuery(query, model.SearchFilter);
            }

            // order
            switch (sortOrder)
            {
                case "receiptid":
                    query = query.OrderBy(s => s.ReceiptInnerId);
                    break;
                case "receiptid_desc":
                    query = query.OrderByDescending(s => s.ReceiptInnerId);
                    break;
                case "datecreate":
                    query = query.OrderBy(s => s.CreateDate);
                    break;
                case "datecreate_desc":
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
                case "jobdate":
                    query = query.OrderBy(s => s.JobDate).ThenBy(s => s.JobTime);
                    break;
                case "jobdate_desc":
                    query = query.OrderByDescending(s => s.JobDate).ThenByDescending(s => s.JobTime);
                    break;
                case "typeid":
                    query = query.OrderBy(s => s.ReceiptTypeId);
                    break;
                case "typeid_desc":
                    query = query.OrderByDescending(s => s.ReceiptTypeId);
                    break;
                case "stateid":
                    query = query.OrderBy(s => s.ReceiptStateId);
                    break;
                case "stateid_desc":
                    query = query.OrderByDescending(s => s.ReceiptStateId);
                    break;
                default:
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
            }

            return query;
        }

        private IQueryable<ReceiptResultModel> mapFilterToQuery(IQueryable<ReceiptResultModel> query, ReceiptSearchModel filter)
        {
            if (!string.IsNullOrEmpty(filter.ReceiptInnerId)) query = query.Where(q => filter.ReceiptInnerId.StartsWith(q.ReceiptInnerId.ToString()));
            if (filter.ReceiptTypeId > 0) query = query.Where(q => q.ReceiptTypeId == filter.ReceiptTypeId);
            if (filter.ReceiptStateId > 0) query = query.Where(q => q.ReceiptStateId == filter.ReceiptStateId);
            if (filter.PayTypeId > 0) query = query.Where(q => q.PayTypeId == filter.PayTypeId);
            if (filter.DateCreateFrom.HasValue) query = query.Where(q => q.CreateDate.CompareTo(filter.DateCreateFrom.Value) >= 0);
            if (filter.DateCreateTo.HasValue)
            {
                DateTime filterDateCreateTo = filter.DateCreateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(q => q.CreateDate.CompareTo(filterDateCreateTo) <= 0);
            }
            if (filter.JobDateFrom.HasValue)
            {
                query = query.Where(q => q.JobDateTime.HasValue && q.JobDateTime.Value.CompareTo(filter.JobDateFrom.Value) >= 0);
                //string filterFrom = filter.JobDateFrom.Value.ToString("yyyyMMdd");
                //query = query.Where(q => ((!string.IsNullOrEmpty(q.JobDate))
                //                && ((q.JobDate.Substring(6, 4) + q.JobDate.Substring(3, 2) + q.JobDate.Substring(0, 2)).CompareTo(filterFrom) >= 0)));
            }
            if (filter.JobDateTo.HasValue)
            {
                DateTime filterJobDateTo = filter.JobDateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(q => q.JobDateTime.HasValue && q.JobDateTime.Value.CompareTo(filterJobDateTo) <= 0);
                //string filterTo = filter.JobDateTo.Value.ToString("yyyyMMdd");
                //query = query.Where(q => ((!string.IsNullOrEmpty(q.JobDate))
                //                && ((q.JobDate.Substring(6, 4) + q.JobDate.Substring(3, 2) + q.JobDate.Substring(0, 2)).CompareTo(filterTo) <= 0)));
            }
            return query;
        }

        private Stream mapListToCsv(List<ReceiptResultModel> receipts)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(ms, Encoding.UTF8);
            csvWriter.WriteLine("Id;Customer;Auftragsart;Service_appointment;Amount;Payment;Status;AbgeschlossenAm;Erstellungsdatum;Date Modified");

            receipts.ForEach(b => csvWriter.WriteLine(b.ReceiptInnerIdExtern
                + ";" + (b.CustomerMarketId == MarketType.Private_person ? b.LastName + " " + b.FirstName : b.CompanyName)
                + ";" + b.ReceiptTypeId
                + ";" + b.JobDate
                + ";" + b.TotalCost
                + ";" + b.PayTypeId
                + ";" + b.ReceiptStateId
                + ";" + (b.ClosedDate != DateTime.MinValue ? b.ClosedDate.ToString("dd.MM.yyyy") : "")
                + ";" + b.CreateDate.ToString("dd.MM.yyyy")
                + ";" + (b.ModifyDate != DateTime.MinValue ? b.ModifyDate.ToString("dd.MM.yyyy") : "")
                ));

            csvWriter.Flush();
            ms.Position = 0;
            return ms;
        }

        #endregion

    }
}