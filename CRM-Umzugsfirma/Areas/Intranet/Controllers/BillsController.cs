﻿using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Bills;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.Helpers;

namespace CRM_Umzugsfirma.Areas.Intranet.Controllers
{
    [CustomAuthorize(Roles = "AccountUser")]
    public class BillsController : Controller
    {
        // Special Route for the whole control
        // GET: Intranet/Bills
        public ActionResult Index(string sortOrder, int? page, int? reset, int? paystate)
        {
            // reset searchmodel
            if ((reset != null) && (reset == 1))
            {
                if (Session["BillSearchModel"] != null) Session.Remove("BillSearchModel");
                return RedirectToAction("Index");
            }

            BillViewModel model = new BillViewModel() { SearchFilter = new BillSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["BillSearchModel"] != null) model.SearchFilter = (BillSearchModel)Session["BillSearchModel"];

            if (paystate.HasValue && paystate.Value >= 1 && paystate.Value <=2)
            {
                model.SearchFilter.PaymentStateId = ((PaymentState)paystate.Value);
            }

            return View(getPagedListFromDB(model, sortOrder, page));
        }

        // GET: Intranet/Print
        public ActionResult Print(string sortOrder)
        {
            BillViewModel model = new BillViewModel() { SearchFilter = new BillSearchModel() };
            ViewBag.CurrentSort = sortOrder;
            if (Session["BillSearchModel"] != null) model.SearchFilter = (BillSearchModel)Session["BillSearchModel"];
            model.PrintVersion = true;

            return PartialView("~/Areas/Intranet/Views/Bills/ResultList.cshtml", getPagedListFromDB(model, sortOrder, 1, 99999));
        }

        // GET: Intranet/Csv
        public ActionResult Csv(string sortOrder)
        {
            BillViewModel model = new BillViewModel() { SearchFilter = new BillSearchModel() };
            if (Session["BillSearchModel"] != null) model.SearchFilter = (BillSearchModel)Session["BillSearchModel"];
            model.PrintVersion = true;

            List<BillResultModel> bills = null;
            using (var db = new ApplicationDbContext())
            {
                bills = getListFromDB(db, model, sortOrder).ToList();
            }

            return File(mapListToCsv(bills), "text/csv", "Bills.csv");
        }

        // Special Route for the whole control
        // POST: Intranet/Bills 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(BillViewModel model)
        {
            if (Session["BillSearchModel"] != null) Session.Remove("BillSearchModel");
            Session.Add("BillSearchModel", model.SearchFilter);
            return View(getPagedListFromDB(model, string.Empty, 1));
        }


        // Special Route for the whole control
        // POST: Intranet/Bills/Details/2
        public async Task<ActionResult> Details(int id)
        {
            BillManager manager = new BillManager();
            Bill bill = manager.GetBillById(id, true);
            if (bill == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            ApplicationUserManager userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = await userManager.FindByIdAsync(bill.CreateUserId);
                if (user != null)
                {
                    bill.CreateUserId = user.UserName;
                }
            }

            // translate service names
            string s = string.Empty;
            if (bill.UmzugActive) s += ",Move";
            if (bill.PackActive) s += ",Pack";
            if (bill.PackOutActive) s += ",Unpack";
            if (bill.EntsorgungActive) s += ",disposal";
            if (bill.ReinigungActive || bill.Reinigung2Active) s += ",cleaning";
            if (bill.TransportActive) s += ",transport";
            if (bill.LagerungActive) s += ",Storage";
            if (s.Length > 0) s = s.Substring(1);
            ViewBag.BookedServices = s;

            return View(bill);
        }

        #region Helpers

        private BillViewModel getPagedListFromDB(BillViewModel model, string sortOrder, int? page, int? pageSize = null)
        {
            // sorting
            ViewBag.SortIdParam = String.IsNullOrEmpty(sortOrder) || sortOrder == "billid" ? "billid_desc" : "billid";
            ViewBag.SortDateCreateParam = sortOrder == "datecreate" ? "datecreate_desc" : "datecreate";
            ViewBag.SortDatePayParam = sortOrder == "datepay" ? "datepay_desc" : "datepay";
            ViewBag.SortStateIdParam = sortOrder == "stateid" ? "stateid_desc" : "stateid";

            // paging (install in pm: Install-Package PagedList.Mvc)


            using (var db = new ApplicationDbContext())
            {
                IQueryable<BillResultModel> query = getListFromDB(db, model, sortOrder);

                // paging (install in pm: Install-Package PagedList.Mvc)
                int pageNumber = (page ?? 1);
                IPagedList<BillResultModel> pagedList = query.ToPagedList(pageNumber, pageSize ?? 10);

                model.ResultList = pagedList;
            }
            return model;
        }

        private IQueryable<BillResultModel> getListFromDB(ApplicationDbContext db, BillViewModel model, string sortOrder)
        {
            int accountId = ((UserIdentifier)Session["UserIdentifier"]).AccountId;

            IQueryable<BillResultModel> query = (from a in db.Bills
                                join c in db.Customers on a.CustomerId equals c.CustomerId
                                join bc_temp in db.BillCleanings.Where(s => s.SectorId == 1) on a.BillId equals bc_temp.BillId into gbc
                                from bc in gbc.DefaultIfEmpty()
                                join bc_temp2 in db.BillCleanings.Where(s => s.SectorId == 2) on a.BillId equals bc_temp2.BillId into gbc2
                                from bc2 in gbc2.DefaultIfEmpty()
                                join bt_temp in db.BillTransports.Where(s => s.SectorId == 1) on a.BillId equals bt_temp.BillId into gbt
                                from bt in gbt.DefaultIfEmpty()
                                where a.AccountId == accountId
                                select new BillResultModel()
                                {
                                    BillId = a.BillId,
                                    BillInnerId = a.BillInnerId,
                                    CustomerId = a.CustomerId,
                                    AccountId = a.AccountId,
                                    BillStateId = a.BillStateId,
                                    BillCost = a.BillCost,
                                    ServiceUmzug = a.UmzugActive,
                                    ServicePack = a.PackActive,
                                    ServicePackOut = a.PackOutActive,
                                    ServiceEntsorgung = a.EntsorgungActive,
                                    ServiceReinigung = a.ReinigungActive,
                                    ServiceReinigung2 = a.Reinigung2Active,
                                    ServiceTransport = a.TransportActive,
                                    ServiceLagerung = a.LagerungActive,
                                    DateUmzugFromString = a.UmzugDate,
                                    DatePackFromString = a.PackDate,
                                    DatePackOutFromString = a.PackOutDate,
                                    DateReinigungFromString = bc.ReinigungDate,
                                    DateReinigung2FromString = bc2.ReinigungDate,
                                    DateTransportFromString = bt.TransportDate,
                                    DateEntsorgungFromString = a.EntsorgungDate,
                                    CreateDate = a.CreateDate,
                                    
                                    CreateUserName = a.CreateUserId,
                                    ToPayDate = a.ToPayDate,
                                    CustomerMarketId = c.MarketId,
                                    LastName = c.LastName,
                                    FirstName = c.FirstName,
                                    CompanyName = c.CompanyName
                                });

            // search filter
            if (model.SearchFilter.IsFilled())
            {
                query = mapFilterToQuery(query, model.SearchFilter);
            }

            // order
            switch (sortOrder)
            {
                case "billid":
                    query = query.OrderBy(s => s.BillInnerId);
                    break;
                case "billid_desc":
                    query = query.OrderByDescending(s => s.BillInnerId);
                    break;
                case "datecreate":
                    query = query.OrderBy(s => s.CreateDate);
                    break;
                case "datecreate_desc":
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
                case "datepay":
                    query = query.OrderBy(s => s.ToPayDate);
                    break;
                case "datepay_desc":
                    query = query.OrderByDescending(s => s.ToPayDate);
                    break;
                case "stateid":
                    query = query.OrderBy(s => s.BillStateId);
                    break;
                case "stateid_desc":
                    query = query.OrderByDescending(s => s.BillStateId);
                    break;
                default:
                    query = query.OrderByDescending(s => s.CreateDate);
                    break;
            }

            return query;
        }

        private IQueryable<BillResultModel> mapFilterToQuery(IQueryable<BillResultModel> query, BillSearchModel filter)
        {
            if (filter.BillInnerId.HasValue) query = query.Where(q => q.BillInnerId == filter.BillInnerId.Value);
            if (filter.BillStateId > 0)
            {
                int searchId = ((filter.BillStateId == SearchBillState.Open) ? 0 : ((int)filter.BillStateId));
                Bill.BillState tempBillState = ((Bill.BillState)searchId);
                query = query.Where(q => q.BillStateId == tempBillState);
            }
            if (filter.PaymentStateId > 0)
            {
                if (filter.PaymentStateId == PaymentState.To_remember)
                {
                    query = query.Where(q => q.ToPayDate.CompareTo(DateTime.Now) < 0 && q.BillStateId < Bill.BillState.Memory);
                }
                else if (filter.PaymentStateId == PaymentState.To_remind)
                {
                    DateTime dt = DateTime.Now.AddDays(-14);
                    query = query.Where(q => q.ToPayDate.CompareTo(dt) < 0 && q.BillStateId < Bill.BillState.Reminder);
                }
            }
            if (filter.DateCreateFrom.HasValue)
            {
                query = query.Where(q => q.CreateDate.CompareTo(filter.DateCreateFrom.Value) >= 0);
            }
            if (filter.DateCreateTo.HasValue)
            {
                DateTime filterDateCreateTo = filter.DateCreateTo.Value.AddDays(1).AddSeconds(-1);
                query = query.Where(q => q.CreateDate.CompareTo(filterDateCreateTo) <= 0);
            }
            return query;
        }

        private Stream mapListToCsv(List<BillResultModel> bills)
        {
            MemoryStream ms = new MemoryStream();
            StreamWriter csvWriter = new StreamWriter(ms, Encoding.UTF8);
            csvWriter.WriteLine("Id;Customer;Dienstleistungen;Service-Date;Amount;Due date;Was standing;Erstellungsdatum");

            bills.ForEach(b => csvWriter.WriteLine(b.BillInnerId.ToString()
                                             + ";" + (b.CustomerMarketId == MarketType.Private_person ? b.LastName + " " + b.FirstName : b.CompanyName)
                                             + ";" + b.BookedServices
                                             + ";" + (!string.IsNullOrEmpty(b.DateUmzugFromString) ? b.DateUmzugFromString
                                                 : (!string.IsNullOrEmpty(b.DatePackFromString) ? b.DatePackFromString
                                                     : (!string.IsNullOrEmpty(b.DateReinigungFromString) ? b.DateReinigungFromString
                                                        : (!string.IsNullOrEmpty(b.DateReinigung2FromString) ? b.DateReinigung2FromString
                                                            : (!string.IsNullOrEmpty(b.DateTransportFromString) ? b.DateTransportFromString
                                                                : (!string.IsNullOrEmpty(b.DateEntsorgungFromString) ? b.DateEntsorgungFromString : ""))))))
                                             + ";" + b.BillCost
                                             + ";" + b.ToPayDate.ToString("dd.MM.yyyy")
                                             + ";" + b.BillStateId.ToString()
                                             + ";" + b.CreateDate.ToString()
            ));

            csvWriter.Flush();
            ms.Position = 0;
            return ms;
        }

        #endregion

    }
}