﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers
{
    public enum ServiceTypes
    {
        Move = 1,
        Packing_Service = 2,
        Unpacking_Service = 3,
        Disposal = 4,
        cleaning = 5,
        Transport = 6,
        Storage = 7
    }

    public class OfferSearchModel
    {
        [Display(Name = "Offernumber")]
        public int? OfferInnerId { get; set; }

        [Display(Name = "Just following service")]
        public ServiceTypes ServiceTypeId { get; set; }

        [Display(Name = "Was standing")]
        public Offer.OfferState OfferStateId { get; set; }
        [Display(Name = "Sightseeing")]
        public Offer.ViewAppointmentState ViewAppointmentStateId { get; set; }

        [Display(Name = "Service appointment between")]
        public DateTime? DateServiceFrom { get; set; }
        [Display(Name = "to")]
        public DateTime? DateServiceTo { get; set; }

        [Display(Name = "Offer created on")]
        public DateTime? DateCreateFrom { get; set; }
        [Display(Name = "to")]
        public DateTime? DateCreateTo { get; set; }

        public bool IsFilled()
        {
            return ((ServiceTypeId > 0) || (OfferStateId > 0) || (ViewAppointmentStateId > 0) || (OfferInnerId.HasValue) || (DateServiceFrom.HasValue) || (DateServiceTo.HasValue) || (DateCreateFrom.HasValue) || (DateCreateTo.HasValue));
        }
    }
}