﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers
{
    public class PriceAdditCostsViewModel
    {
        public int Id { get; set; }
        public string Descr { get; set; }
        public string DescrToShow { get; set; }
        public bool Selected { get; set; }
        public int Price { get; set; }
        public string MetaTag { get; set; }
        public int PricePerPiece { get; set; }
    }
}