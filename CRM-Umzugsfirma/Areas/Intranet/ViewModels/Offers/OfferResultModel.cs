﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers
{
    public class OfferResultModel
    {
        public int OfferId { get; set; }
        public int CustomerId { get; set; }
        public int AccountId { get; set; }

        [Display(Name = "Offers No.")]
        public int OfferInnerId { get; set; }
        [Display(Name = "Was standing")]
        public Offer.OfferState OfferStateId { get; set; }
        [Display(Name = "Sightseeing")]
        public Offer.ViewAppointmentState ViewAppointmentStateId { get; set; }
        public bool ServiceUmzug { get; set; }
        public bool ServicePack { get; set; }
        public bool ServicePackOut { get; set; }
        public bool ServiceEntsorgung { get; set; }
        public bool ServiceReinigung { get; set; }
        public bool ServiceReinigung2 { get; set; }
        public bool ServiceLagerung { get; set; }
        public bool ServiceTransport { get; set; }
        public bool ServicePackMaterial { get; set; }
        [Display(Name = "Service")]
        public string BookedServices { 
            get {
                string s = string.Empty;
                if (ServiceUmzug) s += ",Move";
                if (ServicePack) s += ",Packing";
                if (ServicePackOut) s += ",Unpacking";
                if (ServiceEntsorgung) s += ",Disposal";
                if (ServiceReinigung) s += ",cleaning";
                if (ServiceReinigung2) s += ",Cleaning2";
                if (ServiceLagerung) s += ",Storage";
                if (ServiceTransport) s += ",Transport";
                if (ServicePackMaterial) s += ",Packingmaterial";
                if (s.Length > 0) s = s.Substring(1);
                return s;
            }
        }
        public string DateUmzugFromString { get; set; }
        public string TimeUmzugFrom { get; set; }
        public DateTime? UmzugDateTime { get; set; }
        public string DatePackFromString { get; set; }
        public string TimePackFrom { get; set; }
        public DateTime? PackDateTime { get; set; }
        public string DatePackOutFromString { get; set; }
        public string TimePackOutFrom { get; set; }
        public DateTime? PackOutDateTime { get; set; }
        public string DateReinigungFromString { get; set; }
        public string TimeReinigungFrom { get; set; }
        public DateTime? ReinigungDateTime { get; set; }
        public string DateReinigung2FromString { get; set; }
        public string TimeReinigung2From { get; set; }
        public DateTime? Reinigung2DateTime { get; set; }
        public string DateTransportFromString { get; set; }
        public string TimeTransportFrom { get; set; }
        public DateTime? TransportDateTime { get; set; }
        [Display(Name = "Where")]
        public string Where { get; set; }

        [Display(Name = "Customer Type")]
        public MarketType CustomerMarketId { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public TitleType TitleId { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string CompanyContactPerson { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateUserName { get; set; }
    }
}