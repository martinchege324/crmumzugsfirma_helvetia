﻿using PagedList;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers
{
    public class OfferViewModel
    {
        public OfferSearchModel SearchFilter { get; set; }

        public OfferResultModel ResultListHeader { get; set; }
        public IPagedList<OfferResultModel> ResultList { get; set; }

        public bool PrintVersion { get; set; }
    }
}