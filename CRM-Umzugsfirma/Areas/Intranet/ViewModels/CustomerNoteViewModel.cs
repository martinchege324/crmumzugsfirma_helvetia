﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerNoteViewModel
    {
        public List<Note> Notes { get; set; }
        public Note NewNote { get; set; }
        public string NewNoteCreateUser { get; set; }
    }
}