﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerSearchModel
    {
        [Display(Name = "MarketId")]
        public MarketType MarketId { get; set; }
        [Display(Name = "Surname")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        [Display(Name = "Zip")]
        public int? Zip { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "Customer Source")]
        public string CustomerSource{ get; set; }
        [Display(Name = "Other Source")]
        public string OtherSource { get; set; }

        [Display(Name = "Status")]
        public ContractType ContractType { get; set; }

        public DateTime? DateCreateFrom { get; set; }
        public DateTime? DateCreateTo { get; set; }

        [Display(Name = "Only not reached")]
        public bool NotReached { get; set; }

        public bool IsFilled()
        {
            return ((MarketId > 0) || (!string.IsNullOrEmpty(LastName)) || (!string.IsNullOrEmpty(FirstName)) || (!string.IsNullOrEmpty(CompanyName)) || (Zip.HasValue && Zip.Value > 0)
                || (!string.IsNullOrEmpty(City)) || (ContractType > 0) || (DateCreateFrom.HasValue) || (DateCreateTo.HasValue) || (NotReached));
        }
    }
}