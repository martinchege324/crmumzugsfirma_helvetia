﻿using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerReceiptViewModel
    {
        public Receipt Receipt { get; set; }

        public ReceiptUmzug ReceiptUmzug { get; set; }

        public ReceiptReinigung ReceiptReinigung { get; set; }

        public Receipt.ReceiptState OldReceiptState { get; set; }

        public string ReceiptCostFixText { get; set; }
        public string ReceiptCostHighText { get; set; }

        public bool IsEditable { get; set; } = true;

        public bool IsEdit { get; set; } = false;
    }
}