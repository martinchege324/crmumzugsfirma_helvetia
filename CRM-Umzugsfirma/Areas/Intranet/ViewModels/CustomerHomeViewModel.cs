﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerHomeViewModel
    {
        public int OpenBillReminder { get; set; }
        public int OpenBillPrecollection { get; set; }
        public int OpenReceiptsInPast { get; set; }
    }
}