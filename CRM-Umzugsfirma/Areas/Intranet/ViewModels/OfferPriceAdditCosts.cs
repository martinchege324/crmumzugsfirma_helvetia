﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class OfferPriceAdditCosts
    {
        public int PriceAdditRateId { get; set; }
        public string Descr { get; set; }
        public int OverwrittenPrice { get; set; }
        public string MetaTag { get; set; }
    }
}