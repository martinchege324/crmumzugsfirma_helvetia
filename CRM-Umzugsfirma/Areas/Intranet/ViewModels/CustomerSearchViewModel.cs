﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using PagedList;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerSearchViewModel
    {
        public CustomerSearchModel SearchFilter { get; set; }

        public Customer ResultListHeader { get; set; }
        public IPagedList<Customer> ResultList { get; set; }

        public bool PrintVersion { get; set; }
    }
}