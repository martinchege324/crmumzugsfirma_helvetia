﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Bills
{
    public enum PaymentState {
        To_remember = 1,
        To_remind = 2
    }

    public enum SearchBillState
    {
        Open = 99,
        Memory = 2,
        Reminder = 3,
        Paid = 9
    }

    public class BillSearchModel
    {
        [Display(Name = "Invoice No")]
        public int? BillInnerId { get; set; }

        [Display(Name = "Statement Balance")]
        public SearchBillState BillStateId { get; set; }
        [Display(Name = "Due date")]
        public PaymentState PaymentStateId { get; set; }

        [Display(Name = "Invoice created on")]
        public DateTime? DateCreateFrom { get; set; }
        [Display(Name = "to")]
        public DateTime? DateCreateTo { get; set; }

        public bool IsFilled()
        {
            return ((BillInnerId.HasValue) || (BillStateId > 0) || (PaymentStateId > 0) || (DateCreateFrom.HasValue) || (DateCreateTo.HasValue));
        }
    }
}