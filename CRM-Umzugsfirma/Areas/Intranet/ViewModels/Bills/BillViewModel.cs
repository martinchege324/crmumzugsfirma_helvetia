﻿using PagedList;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Bills
{
    public class BillViewModel
    {
        public BillSearchModel SearchFilter { get; set; }

        public BillResultModel ResultListHeader { get; set; }
        public IPagedList<BillResultModel> ResultList { get; set; }

        public bool PrintVersion { get; set; }
    }
}