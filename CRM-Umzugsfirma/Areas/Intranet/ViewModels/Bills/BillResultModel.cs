﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Bills
{
    public class BillResultModel
    {
        public int BillId { get; set; }
        public int CustomerId { get; set; }
        public int AccountId { get; set; }

        [Display(Name = "Invoice-No.")]
        public int BillInnerId { get; set; }
        [Display(Name = "Was standing")]
        public Bill.BillState BillStateId { get; set; }
        [Display(Name = "Amount")]
        public decimal BillCost { get; set; }
        public bool ServiceUmzug { get; set; }
        public bool ServicePack { get; set; }
        public bool ServicePackOut { get; set; }
        public bool ServiceEntsorgung { get; set; }
        public bool ServiceReinigung { get; set; }
        public bool ServiceReinigung2 { get; set; }
        public bool ServiceLagerung { get; set; }
        public bool ServiceTransport { get; set; }
        [Display(Name = "Service")]
        public string BookedServices { 
            get {
                string s = string.Empty;
                if (ServiceUmzug) s += ",Move";
                if (ServicePack) s += ",Pack";
                if (ServicePackOut) s += ",Unpack";
                if (ServiceEntsorgung) s += ",Disposal";
                if (ServiceReinigung) s += ",cleaning";
                if (ServiceReinigung2) s += ",Cleaning2";
                if (ServiceTransport) s += ",Transport";
                if (ServiceLagerung) s += ",Storage";
                if (s.Length > 0) s = s.Substring(1);
                return s;
            }
        }
        public int ReminderPeriod { get; set; }
        public string DateUmzugFromString { get; set; }
        public string DatePackFromString { get; set; }
        public string DatePackOutFromString { get; set; }
        public string DateReinigungFromString { get; set; }
        public string DateReinigung2FromString { get; set; }
        public string DateTransportFromString { get; set; }
        public string DateEntsorgungFromString { get; set; }
        [Display(Name = "Created")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Created")]
        public DateTime LagerungReminderDate { get; set; }
        [Display(Name = "Reminder Date")]
        public DateTime ToPayDate { get; set; }

        [Display(Name = "Customer Type")]
        public MarketType CustomerMarketId { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        public string CreateUserName { get; set; }
    }
}