﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerBillViewModel
    {
        public Bill Bill { get; set; }

        public List<PriceAdditCostsViewModel> UmzugPriceAdditCosts { get; set; }

        public List<PriceAdditCostsViewModel> PackPriceAdditCosts { get; set; }

        public List<PriceAdditCostsViewModel> PackOutPriceAdditCosts { get; set; }

        public List<PriceAdditCostsViewModel> ReinigungPriceAdditCosts { get; set; }
        public List<PriceAdditCostsViewModel> Reinigung2PriceAdditCosts { get; set; }

        public List<PriceAdditCostsViewModel> EntsorgungPriceAdditCosts { get; set; }

        public List<Bill_DefinedPackMaterial> BillDefinedPackMaterials { get; set; }
        public List<Bill_DefinedPackMaterial_de> BillDefinedPackMaterials_de { get; set; }

        public List<BillPackMaterial> BillPackMaterials { get; set; }

        [Display(Name = "E-Mail Attachments")]
        public List<CheckListViewModel> EmailAttachments { get; set; }

        private bool _isEditable = true;
        public bool IsEditable
        {
            get
            {
                return _isEditable;
            }
            set
            {
                _isEditable = value;
            }
        }

        private bool _isEdit = false;
        public bool IsEdit
        {
            get
            {
                return _isEdit;
            }
            set
            {
                _isEdit = value;
            }
        }

    }
}