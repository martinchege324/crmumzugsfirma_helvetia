﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Appointments
{
    public class AppointmentResultModel
    {
        public int AppointmentId { get; set; }

        public int CustomerId { get; set; }

        public int AccountId { get; set; }

        [Display(Name = "Type")]
        public Appointment.AppointmentType AppointmentTypeId { get; set; }
        [Display(Name = "Offers No.")]
        public int OfferInnerId { get; set; }

        public AppointmentView AppointmentView { get; set; }
        public AppointmentService AppointmentService { get; set; }
        public AppointmentDelivery AppointmentDelivery { get; set; }

        public DateTime? AppointmentDateTime
        {
            get
            {
                switch (AppointmentTypeId)
                {
                    case Appointment.AppointmentType.Sightseeing:
                        return AppointmentView?.ViewDateTime;
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        return AppointmentService?.UmzugDateTime ?? AppointmentService?.PackDateTime ?? AppointmentService?.PackOutDateTime
                               ?? AppointmentService?.ReinigungDateTime ?? AppointmentService?.Reinigung2DateTime ?? AppointmentService?.EntsorgungDateTime ?? AppointmentService?.TransportDateTime;
                    case Appointment.AppointmentType.Delivery:
                        return AppointmentDelivery?.DeliveryDateTime;
                    default:
                        return null;
                }
            }
        }

        [Display(Name = "Service")]
        public string BookedServices { 
            get {
                string s = string.Empty;
                switch (AppointmentTypeId)
                {
                    case Appointment.AppointmentType.Sightseeing:
                        s = AppointmentView.MeetPlace.ToString();
                        break;
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        s = AppointmentService.BookedServices.Trim();
                        break;
                    case Appointment.AppointmentType.Delivery:
                        s = AppointmentDelivery.DeliveryProduct.ToString();
                        break;
                    default:
                        break;
                }
                return s;
            }
        }
        [Display(Name = "Where")]
        public string Where { get; set; }
        [Display(Name = "Calendar Note")]
        public string CalendarNote { get; set; }
        [Display(Name = "E-Mail to Customers")]
        public bool EmailToCustomer { get; set; }
        [Display(Name = "E-Mail Address")]
        public string EmailAddress { get; set; }
        [Display(Name = "Email Note")]
        public string EmailNote { get; set; }

        [Display(Name = "Customer Type")]
        public MarketType CustomerMarketId { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        public TitleType TitleId { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }
        public string CompanyContactPerson { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public DateTime CreateDate { get; set; }
        public string CreateUserName { get; set; }
    }
}