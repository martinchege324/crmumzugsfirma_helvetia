﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Appointments
{
    public class AppointmentSearchModel
    {
        [Display(Name = "Type")]
        public Appointment.AppointmentType AppointmentTypeId { get; set; }
        
        [Display(Name = "Just see the next events")]
        public bool OnlyNextAppointments { get; set; }

        public DateTime? DateCreateFrom { get; set; }
        public DateTime? DateCreateTo { get; set; }

        public bool IsFilled()
        {
            return ((AppointmentTypeId > 0) || (DateCreateFrom.HasValue) || (DateCreateTo.HasValue) || (OnlyNextAppointments));
        }
    }
}