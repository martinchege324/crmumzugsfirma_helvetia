﻿using PagedList;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Appointments
{
    public class AppointmentViewModel
    {
        public AppointmentSearchModel SearchFilter { get; set; }

        public AppointmentResultModel ResultListHeader { get; set; }
        public IPagedList<AppointmentResultModel> ResultList { get; set; }

        public bool PrintVersion { get; set; }
    }
}