﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.ComponentModel.DataAnnotations;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Receipts
{
    public class ReceiptResultModel
    {
        public Guid ReceiptId { get; set; }
        public int OfferId { get; set; }
        public int CustomerId { get; set; }
        public int AccountId { get; set; }

        [Display(Name = "Receipt No.")]
        public int ReceiptInnerId { get; set; }
        public int? ReceiptInnerSubId { get; set; }
        public string ReceiptInnerIdExtern
        {
            get
            {
                return string.Format("{0}{1}", ReceiptInnerId, (ReceiptInnerSubId.HasValue ? string.Format(".{0}", ReceiptInnerSubId.Value) : string.Empty));
            }
        }
        [Display(Name = "Type")]
        public Receipt.ReceiptType ReceiptTypeId { get; set; }
        [Display(Name = "Was standing")]
        public Receipt.ReceiptState ReceiptStateId { get; set; }

        [Display(Name = "Service appointment")]
        public string JobDate { get; set; }
        [Display(Name = "Service Time")]
        public string JobTime { get; set; }
        public DateTime? JobDateTime { get; set; }
        [Display(Name = "Amount")]
        public string TotalCost { get; set; }

        [Display(Name = "Created")]
        public DateTime CreateDate { get; set; }
        [Display(Name = "Completed")]
        public DateTime ClosedDate { get; set; }

        [Display(Name = "Payment")]
        public PayType PayTypeId { get; set; }


        [Display(Name = "Customer Type")]
        public MarketType CustomerMarketId { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

        public string CreateUserName { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}