﻿using PagedList;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Receipts
{
    public class ReceiptViewModel
    {
        public ReceiptSearchModel SearchFilter { get; set; }

        public ReceiptResultModel ResultListHeader { get; set; }
        public IPagedList<ReceiptResultModel> ResultList { get; set; }

        public bool PrintVersion { get; set; }
    }
}