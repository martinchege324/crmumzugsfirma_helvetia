﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.ComponentModel.DataAnnotations;
using CRM_Umzugsfirma.Areas.Intranet.Models.Enums;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels.Receipts
{
    public class ReceiptSearchModel
    {
        [Display(Name = "Receipt No")]
        public string ReceiptInnerId { get; set; }

        [Display(Name = "Type")]
        public Receipt.ReceiptType ReceiptTypeId { get; set; }

        [Display(Name = "Was standing")]
        public Receipt.ReceiptState ReceiptStateId { get; set; }

        [Display(Name = "Payment")]
        public PayType PayTypeId { get; set; }

        [Display(Name = "Receipt created on")]
        public DateTime? DateCreateFrom { get; set; }
        [Display(Name = "to")]
        public DateTime? DateCreateTo { get; set; }

        [Display(Name = "Service appointment between")]
        public DateTime? JobDateFrom { get; set; }
        [Display(Name = "to")]
        public DateTime? JobDateTo { get; set; }

        public bool IsFilled()
        {
            return ((!string.IsNullOrEmpty(ReceiptInnerId)) || (ReceiptTypeId > 0) || (ReceiptStateId > 0) || (PayTypeId > 0) || (DateCreateFrom.HasValue) || (DateCreateTo.HasValue)
                    || (JobDateFrom.HasValue) || (JobDateTo.HasValue));
        }
    }
}