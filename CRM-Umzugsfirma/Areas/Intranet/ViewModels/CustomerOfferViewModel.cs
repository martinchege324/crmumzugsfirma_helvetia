﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels.Offers;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet.ViewModels
{
    public class CustomerOfferViewModel
    {
        public Offer Offer { get; set; }

        [Display(Name = "Tariff")]
        public string HelperUmzugPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> UmzugPriceAdditCosts { get; set; }

        [Display(Name = "Tariff")]
        public string HelperPackPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> PackPriceAdditCosts { get; set; }

        [Display(Name = "Tariff")]
        public string HelperPackOutPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> PackOutPriceAdditCosts { get; set; }

        [Display(Name = "Tariff (flat rate)")]
        public string HelperReinigungPriceRateId { get; set; }
        [Display(Name = "Tariff (hourly rate)")]
        public string HelperReinigungHourlyPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> ReinigungPriceAdditCosts { get; set; }

        [Display(Name = "Tariff (flat rate)")]
        public string HelperReinigung2PriceRateId { get; set; }
        [Display(Name = "Tariff (hourly rate)")]
        public string HelperReinigung2HourlyPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> Reinigung2PriceAdditCosts { get; set; }

        [Display(Name = "Tariff")]
        public string HelperEntsorgungPersonPriceRateId { get; set; }
        [Display(Name = "Volume Tariff")]
        public string HelperEntsorgungPriceRateId { get; set; }
        public List<PriceAdditCostsViewModel> EntsorgungPriceAdditCosts { get; set; }

        [Display(Name = "Tariff")]
        public string HelperTransportPriceRateId { get; set; }

        [Display(Name = "Tariff")]
        public string HelperLagerungPriceRateId { get; set; }

        public List<Offer_DefinedPackMaterial> OfferDefinedPackMaterials { get; set; }

        public List<OfferPackMaterial> OfferPackMaterials { get; set; }

        [Display(Name = "Email Attachments")]
        public List<CheckListViewModel> EmailAttachments { get; set; }

        public int InnerAccountId { get; set; }

        private bool _isEditable = true;
        public bool IsEditable
        {
            get
            {
                return _isEditable;
            }
            set
            {
                _isEditable = value;
            }
        }

        private bool _isEdit = false;
        public bool IsEdit
        {
            get
            {
                return _isEdit;
            }
            set
            {
                _isEdit = value;
            }
        }

    }
}