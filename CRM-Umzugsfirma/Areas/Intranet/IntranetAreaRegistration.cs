﻿using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Intranet
{
    public class IntranetAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Intranet";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Intranet_CustomerMeet",
                "Intranet/Customer/Details/Meet/{action}/{customerId}/{id}",
                new { controller = "CustomerMeet", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_CustomerOffer",
                "Intranet/Customer/Details/Offer/{action}/{customerId}/{id}",
                new { controller = "CustomerOffer", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_CustomerReceipt",
                "Intranet/Customer/Details/Receipt/{action}/{customerId}/{id}",
                new { controller = "CustomerReceipt", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_CustomerBill",
                "Intranet/Customer/Details/Bill/{action}/{customerId}/{id}",
                new { controller = "CustomerBill", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_CustomerNote",
                "Intranet/Customer/Details/Note/{action}/{customerId}/{id}",
                new { controller = "CustomerNote", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_CustomerDetail",
                "Intranet/Customer/Details/{id}",
                new { controller = "CustomerDetail", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_Customer",
                "Intranet/Customer/{action}/{id}",
                new { controller = "CustomerHome", action = "Index", id = UrlParameter.Optional }
            );
            context.MapRoute(
                "Intranet_default",
                "Intranet/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}