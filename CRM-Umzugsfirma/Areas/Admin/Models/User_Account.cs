﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Areas.Admin.Models
{
    public class User_Account
    {
        [StringLength(128)]
        public string UserId { get; set; }

        [Required]
        public int AccountId { get; set; }

        public virtual Account Account { get; set; }
    }
}