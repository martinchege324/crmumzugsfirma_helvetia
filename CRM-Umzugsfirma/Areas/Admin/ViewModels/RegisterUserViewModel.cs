﻿using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Admin.ViewModels
{
    public class RegisterUserViewModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Rolle")]
        public List<CheckListViewModel> Roles { get; set; }
        [Display(Name = "Rolle")]
        public string Role { get; set; }

        [Display(Name = "Account")]
        public string Account { get; set; }
        public List<SelectListItem> AccountList { get; set; }
 
    }
}