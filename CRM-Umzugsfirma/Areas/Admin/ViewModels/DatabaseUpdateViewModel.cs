﻿using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Areas.Admin.ViewModels
{
    public class DatabaseUpdateViewModel
    {
        [Required]
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Key")]
        public string Key { get; set; }
    }
}