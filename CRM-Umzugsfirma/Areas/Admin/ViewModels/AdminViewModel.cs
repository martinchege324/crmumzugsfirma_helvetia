﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CRM_Umzugsfirma.Models;

namespace CRM_Umzugsfirma.Areas.Admin.ViewModels
{
    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Roles")]
        public List<CheckListViewModel> Roles { get; set; }
    }

    public class ConfigurationViewModel
    {
        [Required]
        [Display(Name = "What")]
        public string What { get; set; }
    }
}