﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Areas.Admin.ViewModels;
using CRM_Umzugsfirma.Helpers;

namespace CRM_Umzugsfirma.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class RegisterAccountsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private ApplicationUserManager _userManager;

        public RegisterAccountsController()
        {

        }

        public RegisterAccountsController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin/RegisterAccounts
        public async Task<ActionResult> Index()
        {
            return View(await db.Accounts.ToListAsync());
        }

        // GET: Admin/RegisterAccounts/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = await db.Accounts.FindAsync(id);
            if (account == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // try to read username and overwrite userid for readable user info
            var user = await UserManager.FindByIdAsync(account.CreateUserId);
            if (user != null)
            {
                account.CreateUserId = user.UserName;
            }
            user = await UserManager.FindByIdAsync(account.ModifyUserId);
            if (user != null)
            {
                account.ModifyUserId = user.UserName;
            }

            return View(account);
        }

        // GET: Admin/RegisterAccounts/Edit/5
        // If Empty creates a new account, else edit it
        public async Task<ActionResult> Edit(int? id)
        {
            // in intranet edit, there is no possibilty for return empty - new account
            //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            if (id == null)
            {
                // -- Create new account
                ViewBag.Title = "Account erstellen";
                ViewBag.SubmitText = "Create";
                return View();
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // -- Edit account
            ViewBag.Title = "Account to edit";
            ViewBag.SubmitText = "Save";

            // check in intranet, if id is session_accountid
            // --

            Account account = await db.Accounts.FindAsync(id);
            if (account == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            return View(account);
        }

        // POST: Admin/RegisterAccounts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Account account)
        {

            if (ModelState.IsValid)
            {
                if (account.AccountId > 0)
                {
                    // -- Edit account
                    account.ModifyDate = DateTime.Now;
                    account.ModifyUserId = User.Identity.GetUserId();

                    db.Entry(account).State = EntityState.Modified;
                    db.Entry(account.AccountEmailConfig).State = EntityState.Modified;
                }
                else
                {
                    // -- Create new account
                    // set default values
                    account.CreateDate = DateTime.Now;
                    account.CreateUserId = User.Identity.GetUserId();
                    account.IsActive = true;

                    db.Accounts.Add(account);
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            else
            {
                if (account.AccountId <= 0)
                {
                    ViewBag.Title = "Account erstellen";
                    ViewBag.SubmitText = "Create";
                }
                else
                {
                    ViewBag.Title = "Account to edit";
                    ViewBag.SubmitText = "Save";
                }
            }

            return View(account);
        }

        // GET: Admin/RegisterAccounts/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = await db.Accounts.FindAsync(id);
            if (account == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
            return View(account);
        }

        // POST: Admin/RegisterAccounts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Account account = await db.Accounts.FindAsync(id);
            db.Accounts.Remove(account);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
