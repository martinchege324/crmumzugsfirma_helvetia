﻿using CRM_Umzugsfirma.Areas.Admin.ViewModels;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class DatabaseUpdateController : Controller
    {
        // GET: Admin/DatabaseUpdate
        public ActionResult Index()
        {
            return View();
        }

        // POST: Admin/DatabaseUpdate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Index(DatabaseUpdateViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if ((model.Id > 0) && (!string.IsNullOrEmpty(model.Key)))
            {
                ViewBag.Successful = true;

                try
                {
                    // Req 49: Change 'Helvetia Transporte GmbH' to 'Helvetia Transporte AG' and 'Ihr Helvetia Team' to 'Ihr Helvetia Transporte Team'
                    if (model.Id == 3 && model.Key == "49")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [Account] set [AccountName] = 'Helvetia Transporte AG', [ContactName] = 'Ihr Helvetia Transporte Team' where [AccountId] = 4");
                            await db.Database.ExecuteSqlCommandAsync("update [AccountEmailConfig] set [SenderDisplayName] = 'Helvetia Transporte AG' where [AccountId] = 4");
                            await db.Database.ExecuteSqlCommandAsync("update [EmailType] set [MailSubject] = REPLACE([MailSubject],' GmbH',' AG') where [AccountId] = 4 and [MailSubject] like '%GmbH%'");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 61: Add new Offer.BuildungType and change Enum-Value of an existing - Geschäft / Büro is now 10 instead of 3
                    if (model.Id == 14 && model.Key == "61")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutBuildingType] = 10 where [AdrOutBuildingType] = 3");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInBuildingType] = 10 where [AdrInBuildingType] = 3");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutBuildingType2] = 10 where [AdrOutBuildingType2] = 3");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInBuildingType2] = 10 where [AdrInBuildingType2] = 3");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutBuildingType3] = 10 where [AdrOutBuildingType3] = 3");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInBuildingType3] = 10 where [AdrInBuildingType3] = 3");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 71: Added Real Dates in DB for Offer-Table. Set all new DateTime-Fields from string values
                    if (model.Id == 23 && model.Key == "71")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            // correct a mismatched datetime-format
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [UmzugDate] = '27.07.2017' where OfferId = 8167");
                            // update fields
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [UmzugDateTime] = CONVERT(datetime,[UmzugDate],104) where ([UmzugDate] is not null)");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [UmzugInDateTime] = CONVERT(datetime,[UmzugInDate],104) where ([UmzugInDate] is not null)");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackDateTime] = CONVERT(datetime,[PackDate],104) where ([PackDate] is not null)");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [EntsorgungDateTime] = CONVERT(datetime,[EntsorgungDate],104) where ([EntsorgungDate] is not null)");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [ReinigungDateTime] = CONVERT(datetime,[ReinigungDate],104) where ([ReinigungDate] is not null)");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 64: Add CountryCode for all addresses in the system
                    if (model.Id == 17 && model.Key == "64")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [Customer] set [CountryCode] = 'CH' where isnull([CountryCode],'') = ''");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutCountryCode] = 'CH' where isnull([AdrOutCountryCode],'') = '' and [AdrOutZip] > 0");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutCountryCode2] = 'CH' where isnull([AdrOutCountryCode2],'') = '' and [AdrOut2] = 1");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrOutCountryCode3] = 'CH' where isnull([AdrOutCountryCode3],'') = '' and [AdrOut3] = 1");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInCountryCode] = 'CH' where isnull([AdrInCountryCode],'') = '' and [AdrInZip] > 0");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInCountryCode2] = 'CH' where isnull([AdrInCountryCode2],'') = '' and [AdrIn2] = 1");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [AdrInCountryCode3] = 'CH' where isnull([AdrInCountryCode3],'') = '' and [AdrIn3] = 1");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 50: Offer / Bill with additional property of 'inclusive tax / exclusive tax'
                    if (model.Id == 4 && model.Key == "50")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [CostInclTax] = 1 where [CostInclTax] = 0 and [CreateDate] < '20180107'");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [CostInclTax] = 1 where [CostInclTax] = 0 and [CreateDate] < '20180107'");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 63: Unpacking (identical copy of Packing)
                    if (model.Id == 16 && model.Key == "63")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostWay] = 0 where [PackOutCostWay] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostChangedPers] = 0 where [PackOutCostChangedPers] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostChangedPrice] = 0 where [PackOutCostChangedPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostAdditFreeTextPlus1Price] = 0 where [PackOutCostAdditFreeTextPlus1Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostAdditFreeTextPlus2Price] = 0 where [PackOutCostAdditFreeTextPlus2Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostAdditFreeTextMinus1Price] = 0 where [PackOutCostAdditFreeTextMinus1Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostDiscount] = 0 where [PackOutCostDiscount] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Offer] set [PackOutCostDiscount2] = 0 where [PackOutCostDiscount2] is null");

                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutPerPrice] = 0 where [PackOutPerPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostService] = 0 where [PackOutCostService] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostService2] = 0 where [PackOutCostService2] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostWay] = 0 where [PackOutCostWay] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostAdditFreeTextPlus1Price] = 0 where [PackOutCostAdditFreeTextPlus1Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostAdditFreeTextPlus2Price] = 0 where [PackOutCostAdditFreeTextPlus2Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostAdditFreeTextMinus1Price] = 0 where [PackOutCostAdditFreeTextMinus1Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostAdditFreeTextMinus2Price] = 0 where [PackOutCostAdditFreeTextMinus2Price] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostDiscount] = 0 where [PackOutCostDiscount] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostDiscount2] = 0 where [PackOutCostDiscount2] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostSub] = 0 where [PackOutCostSub] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostFixPrice] = 0 where [PackOutCostFixPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostHighSecurityPrice] = 0 where [PackOutCostHighSecurityPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostDamage] = 0 where [PackOutCostDamage] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostPartialPayment] = 0 where [PackOutCostPartialPayment] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [Bill] set [PackOutCostPartialByCash] = 0 where [PackOutCostPartialByCash] is null");
                            await db.Database.ExecuteSqlCommandAsync("update[Bill] set [PackOutCost] = 0 where [PackOutCost] is null");

                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [  PriceRate]([AccountId],[ServiceType],[Descr],[CountFirst],[CountSecond],[CountPrice],[DescrOnChangeFirst],[DescrOnChangeSecond],[DescrOnChangePrice],[CountPriceRangeMax])
                                SELECT [AccountId],'packout',[Descr],[CountFirst],[CountSecond],[CountPrice],[DescrOnChangeFirst],[DescrOnChangeSecond],[DescrOnChangePrice],[CountPriceRangeMax]
                                    FROM [  PriceRate] WHERE [ServiceType] = 'pack'");
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [PriceAdditRate]([AccountId],[ServiceType],[Descr],[Price],[MetaTag],[CalcType],[DescrToShow])
                                SELECT [AccountId],'packout',[Descr],[Price],[MetaTag],[CalcType],[DescrToShow]
                                    FROM [PriceAdditRate] WHERE [ServiceType] = 'pack'");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 69: defined PackMaterial from shop of account
                    if (model.Id == 22 && model.Key == "69")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[DefinedPackMaterial]([AccountId],[Title],[Description],[PriceRent],[PriceBuy])
                                VALUES(4,'Bücherkarton',null,2.00,3.50),(4,'Ordnerkarton',null,2.50,4.50),(4,'Wäsche- Geschirrkarton',null,3.50,6.90),
                                    (4,'Kleiderkarton incl. Stange',null,10.00,20.00),(4,'Geschirrkarton mit Einlagen',null,6.90,9.90),(4,'Klebebandrolle',null,null,4.50),
                                    (4,'Seidenpapier',null,null,7.90),(4,'Luftpolsterfolie',null,null,4.00),(4,'Kartonaufkleber A6',null,null,0.10),
                                    (4,'Handstretchfolie',null,null,16.00),(4,'Matratzenhülle',null,null,6.00)");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 68: Cleancode Appointment - Split Appointment to Sub-Tables with AppointmentType
                    if (model.Id == 21 && model.Key == "68")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO AppointmentView ([AppointmentId], [MeetPlace], [ViewDate], [ViewTime], [ViewDateTime], [GoogleEventViewId])
                                SELECT [AppointmentId], [MeetPlace], [DateFrom], [TimeFrom], case when nullif([DateFrom],'') is not null then CONVERT(datetime,RIGHT([DateFrom],4)+SUBSTRING([DateFrom],4,2)+SUBSTRING([DateFrom],1,2)+' '+[TimeFrom]) else null end, [GoogleEventViewId]
                                FROM [Appointment]
                                WHERE ([AppointmentTypeId] = 1)");
                            await db.Database.ExecuteSqlCommandAsync("UPDATE [Appointment] SET [TimeUmzugFrom2] = '8:00' WHERE [AppointmentId] = 2988");
                            await db.Database.ExecuteSqlCommandAsync(
                                @"INSERT INTO [dbo].[AppointmentService] ([AppointmentId],[ServiceUmzug],[UmzugDate],[UmzugTime],[UmzugDateTime],[DurationUmzug],[CountPersonUmzug],[CountVehUmzug],[CountTrailerUmzug],[GoogleEventUmzugId]
                                                                           ,[DateUmzugMoreAppointment],[UmzugDate2],[UmzugTime2],[UmzugDateTime2],[DurationUmzug2],[CountPersonUmzug2],[CountVehUmzug2],[CountTrailerUmzug2],[GoogleEventUmzugId2]
                                                                           ,[UmzugDate3],[UmzugTime3],[UmzugDateTime3],[DurationUmzug3],[CountPersonUmzug3],[CountVehUmzug3],[CountTrailerUmzug3],[GoogleEventUmzugId3]
                                                                           ,[ServicePack],[PackDate],[PackTime],[PackDateTime],[DurationPack],[CountPersonPack],[CountVehPack],[CountTrailerPack],[GoogleEventPackId]
                                                                           ,[ServicePackOut],[PackOutDate],[PackOutTime],[PackOutDateTime],[DurationPackOut],[CountPersonPackOut],[CountVehPackOut],[CountTrailerPackOut],[GoogleEventPackOutId]
                                                                           ,[ServiceEntsorgung],[EntsorgungDate],[EntsorgungTime],[EntsorgungDateTime],[DurationEntsorgung],[CountPersonEntsorgung],[CountVehEntsorgung],[CountTrailerEntsorgung],[GoogleEventEntsorgungId]
                                                                           ,[ServiceReinigung],[ReinigungDate],[ReinigungTime],[ReinigungDateTime],[DateReinigungRelease],[TimeReinigungRelease],[ReleaseTimeReinigung],[DurationReinigung],[GoogleEventReinigungId]
		                                                                   ,[ServiceLagerung],[LagerungDate],[LagerungTime],[LagerungDateTime],[DurationLagerung],[GoogleEventLagerungId]
                                                                           ,[ServiceTransport],[TransportDate],[TransportTime],[TransportDateTime],[DurationTransport],[CountPersonTransport],[CountVehTransport],[CountTrailerTransport],[GoogleEventTransportId])
                                SELECT [AppointmentId],[ServiceUmzug],[DateUmzugFrom],[TimeUmzugFrom],case when nullif([DateUmzugFrom],'') is not null then CONVERT(datetime,RIGHT([DateUmzugFrom],4)+SUBSTRING([DateUmzugFrom],4,2)+SUBSTRING([DateUmzugFrom],1,2)+' '+[TimeUmzugFrom]) else null end as [UmzugDateTime],[DurationUmzug],[CountPersonUmzug],[CountVehUmzug],[CountTrailerUmzug],[GoogleEventUmzugId]
                                        ,[DateUmzugMoreAppointment],[DateUmzugFrom2],[TimeUmzugFrom2],case when nullif([DateUmzugFrom2],'') is not null then CONVERT(datetime,RIGHT([DateUmzugFrom2],4)+SUBSTRING([DateUmzugFrom2],4,2)+SUBSTRING([DateUmzugFrom2],1,2)+' '+[TimeUmzugFrom2]) else null end as [UmzugDateTime2],[DurationUmzug2],[CountPersonUmzug2],[CountVehUmzug2],[CountTrailerUmzug2],[GoogleEventUmzugId2]
                                        ,[DateUmzugFrom3],[TimeUmzugFrom3],case when nullif([DateUmzugFrom3],'') is not null then CONVERT(datetime,RIGHT([DateUmzugFrom3],4)+SUBSTRING([DateUmzugFrom3],4,2)+SUBSTRING([DateUmzugFrom3],1,2)+' '+[TimeUmzugFrom3]) else null end as [UmzugDateTime3],[DurationUmzug3],[CountPersonUmzug3],[CountVehUmzug3],[CountTrailerUmzug3],[GoogleEventUmzugId3]
                                        ,[ServicePack],[DatePackFrom],[TimePackFrom],case when nullif([DatePackFrom],'') is not null then CONVERT(datetime,RIGHT([DatePackFrom],4)+SUBSTRING([DatePackFrom],4,2)+SUBSTRING([DatePackFrom],1,2)+' '+[TimePackFrom]) else null end as [PackDateTime],[DurationPack],[CountPersonPack],[CountVehPack],[CountTrailerPack],[GoogleEventPackId]
                                        ,[ServicePackOut],[DatePackOutFrom],[TimePackOutFrom],case when nullif([DatePackOutFrom],'') is not null then CONVERT(datetime,RIGHT([DatePackOutFrom],4)+SUBSTRING([DatePackOutFrom],4,2)+SUBSTRING([DatePackOutFrom],1,2)+' '+[TimePackOutFrom]) else null end as [PackOutDateTime],[DurationPackOut],[CountPersonPackOut],[CountVehPackOut],[CountTrailerPackOut],[GoogleEventPackOutId]
	                                    ,[ServiceEntsorgung],[DateEntsorgungFrom],[TimeEntsorgungFrom],case when nullif([DateEntsorgungFrom],'') is not null then CONVERT(datetime,RIGHT([DateEntsorgungFrom],4)+SUBSTRING([DateEntsorgungFrom],4,2)+SUBSTRING([DateEntsorgungFrom],1,2)+' '+[TimeEntsorgungFrom]) else null end as [EntsorgungDateTime],[DurationEntsorgung],[CountPersonEntsorgung],[CountVehEntsorgung],[CountTrailerEntsorgung],[GoogleEventEntsorgungId]
                                        ,[ServiceReinigung],[DateReinigungFrom],[TimeReinigungFrom],case when nullif([DateReinigungFrom],'') is not null then CONVERT(datetime,RIGHT([DateReinigungFrom],4)+SUBSTRING([DateReinigungFrom],4,2)+SUBSTRING([DateReinigungFrom],1,2)+' '+[TimeReinigungFrom]) else null end as [ReinigungDateTime],[DateReinigungRelease],[TimeReinigungRelease],[ReleaseTimeReinigung],[DurationReinigung],[GoogleEventReinigungId]
                                        ,[ServiceLagerung],[DateLagerungFrom],[TimeLagerungFrom],case when nullif([DateLagerungFrom],'') is not null then CONVERT(datetime,RIGHT([DateLagerungFrom],4)+SUBSTRING([DateLagerungFrom],4,2)+SUBSTRING([DateLagerungFrom],1,2)+' '+[TimeLagerungFrom]) else null end as [LagerungDateTime],[DurationLagerung],[GoogleEventLagerungId]
                                        ,[ServiceTransport],[DateTransportFrom],[TimeTransportFrom],case when nullif([DateTransportFrom],'') is not null then CONVERT(datetime,RIGHT([DateTransportFrom],4)+SUBSTRING([DateTransportFrom],4,2)+SUBSTRING([DateTransportFrom],1,2)+' '+[TimeTransportFrom]) else null end as [TransportDateTime],[DurationTransport],[CountPersonTransport],[CountVehTransport],[CountTrailerTransport],[GoogleEventTransportId]
                                FROM [Appointment]
                                WHERE ([AppointmentTypeId] = 2)");
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [EmailType] ([AccountId],[Name],[MailSubject])
                                VALUES (4,'DeliveryAppointment.Packingmaterial.Delivery','Delivery / Zügelshop - Helvetia Transporte AG'),
                                    (4,'DeliveryAppointment.Packingmaterial.Pickup','Pickup / Zügelshop - Helvetia Transporte AG'),
                                    (4,'DeliveryAppointment.Schlossatelier','Ihr Liefertermin - Helvetia Transporte AG')");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    // Req 72: Cleancode Offer-cleaning und Bill-cleaning - Split Offer-cleaning to Sub-Tables with OfferCleaning
                    if (model.Id == 24 && model.Key == "72")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[OfferCleaning] ([OfferId],[SectorId],[ReinigungDate],[ReinigungDateTime],[ReinigungTime],[ReinigungDateCommit],[ReinigungTimeCommit],[ReinigungPriceRateId],[ReinigungPriceRateDescr],[ReinigungCostChangedPrice],[ReinigungCostAddit],[ReinigungCostAdditFreeTextPlus1],[ReinigungCostAdditFreeTextPlus1Price],[ReinigungCostAdditFreeTextPlus2],[ReinigungCostAdditFreeTextPlus2Price],[ReinigungCostAdditFreeTextMinus1],[ReinigungCostAdditFreeTextMinus1Price],[ReinigungCostSum],[ReinigungCostFix],[ReinigungCostFixPrice])
                                SELECT [OfferId],1 as [SectorId],[ReinigungDate],[ReinigungDateTime],[ReinigungTime],[ReinigungDateCommit],[ReinigungTimeCommit],[ReinigungPriceRateId],[ReinigungPriceRateDescr],[ReinigungCostChangedPrice],[ReinigungCostAddit],[ReinigungCostAdditFreeTextPlus1],[ReinigungCostAdditFreeTextPlus1Price],[ReinigungCostAdditFreeTextPlus2],[ReinigungCostAdditFreeTextPlus2Price],[ReinigungCostAdditFreeTextMinus1],[ReinigungCostAdditFreeTextMinus1Price],[ReinigungCostSum],[ReinigungCostFix],[ReinigungCostFixPrice]
                                FROM [dbo].[Offer]");
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[BillCleaning]([BillId],[SectorId],[ReinigungDate],[ReinigungRoom],[ReinigungTarifText],[ReinigungPerPrice],[ReinigungCostAddit],[ReinigungCostAdditFreeTextPlus1],[ReinigungCostAdditFreeTextPlus1Price],[ReinigungCostAdditFreeTextPlus2],[ReinigungCostAdditFreeTextPlus2Price],[ReinigungCostAdditFreeTextMinus1],[ReinigungCostAdditFreeTextMinus1Price],[ReinigungCostAdditFreeTextMinus2],[ReinigungCostAdditFreeTextMinus2Price],[ReinigungCostDiscount],[ReinigungCostDiscount2],[ReinigungCostSub],[ReinigungCostFix],[ReinigungCostFixPrice],[ReinigungCostDamage],[ReinigungCostPartialPayment],[ReinigungCostPartialByCash],[ReinigungCost])
                                SELECT [BillId],1 as [SectorId],[ReinigungDate],[ReinigungRoom],[ReinigungTarifText],[ReinigungPerPrice],[ReinigungCostAddit],[ReinigungCostAdditFreeTextPlus1],[ReinigungCostAdditFreeTextPlus1Price],[ReinigungCostAdditFreeTextPlus2],[ReinigungCostAdditFreeTextPlus2Price],[ReinigungCostAdditFreeTextMinus1],[ReinigungCostAdditFreeTextMinus1Price],[ReinigungCostAdditFreeTextMinus2],[ReinigungCostAdditFreeTextMinus2Price],[ReinigungCostDiscount],[ReinigungCostDiscount2],[ReinigungCostSub],[ReinigungCostFix],[ReinigungCostFixPrice],[ReinigungCostDamage],[ReinigungCostPartialPayment],[ReinigungCostPartialByCash],[ReinigungCost]
                                FROM [dbo].[Bill]");
                            await db.Database.ExecuteSqlCommandAsync(@"update [OfferCleaning] set [AdditServiceDuebellocher] = 2 where [AdditServiceDuebellocher] is null");
                            await db.Database.ExecuteSqlCommandAsync(@"update [OfferCleaning] set [AdditServiceHochdruckreiniger] = 2 where [AdditServiceHochdruckreiniger] is null");
                            await db.Database.ExecuteSqlCommandAsync(@"update [OfferCleaning] set [ReinigungType] = 1 where [ReinigungType] is null");
                            await db.Database.ExecuteSqlCommandAsync(@"update [BillCleaning] set [ReinigungType] = 1 where [ReinigungType] is null");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 24 && model.Key == "72.2")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update [OfferCleaning] set [ReinigungHourlyCostChangedPers] = 0 where [ReinigungHourlyCostChangedPers] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [OfferCleaning] set [ReinigungHourlyCostChangedPrice] = 0 where [ReinigungHourlyCostChangedPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [OfferCleaning] set [ReinigungPriceRateDescr] = replace(replace([ReinigungPriceRateDescr],'cleaning ',''),'-Wohnung incl. Abnahmegarantie','') where [ReinigungPriceRateDescr] is not null");
                            await db.Database.ExecuteSqlCommandAsync("update [BillCleaning] set [ReinigungRoom] = replace([ReinigungTarifText],'-Wohnung','') where [ReinigungTarifText] is not null");
                            await db.Database.ExecuteSqlCommandAsync("update [BillCleaning] set [ReinigungHourlyPerPrice] = 0 where [ReinigungHourlyPerPrice] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [BillCleaning] set [ReinigungHourlyCostService] = 0 where [ReinigungHourlyCostService] is null");
                            await db.Database.ExecuteSqlCommandAsync("update [  PriceRate] set [Descr] = replace(replace([Descr],'cleaning ',''),'-Wohnung incl. Abnahmegarantie',' à'), [DescrOnChangeFirst] = '[a] Room', [DescrOnChangeSecond] = 'Wohnung incl. Abnahmegarantie' where ServiceType = 'reinigung'");
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[  PriceRate]([AccountId],[ServiceType],[Descr],[CountFirst],[CountSecond],[CountPrice],[DescrOnChangeFirst],[DescrOnChangeSecond],[DescrOnChangePrice],[CountPriceRangeMax])
                                VALUES (4, 'reinigunghourly', '1 Mitarbeiter à CHF 120.- / Stunde', 1, 0, 120, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0),(4, 'reinigunghourly', '2 Mitarbeiter à CHF 160.- / Stunde', 2, 0, 160, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0),(4, 'reinigunghourly', '3 Mitarbeiter à CHF 200.- / Stunde', 3, 0, 200, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0),(4, 'reinigunghourly', '4 Mitarbeiter à CHF 260.- / Stunde', 4, 0, 260, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0),(4, 'reinigunghourly', '5 Mitarbeiter à CHF 320.- / Stunde', 5, 0, 320, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0),(4, 'reinigunghourly', '6 Mitarbeiter à CHF 360.- / Stunde', 6, 0, 360, '[a] Mitarbeiter', NULL, 'à CHF [a].- / Stunde', 0)");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 24 && model.Key == "72.3")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [PriceAdditRate]([AccountId],[ServiceType],[Descr],[Price],[MetaTag],[CalcType],[DescrToShow])
                                SELECT [AccountId],'reinigung2',[Descr],[Price],[MetaTag],[CalcType],[DescrToShow]
                                  FROM [PriceAdditRate] WHERE [AccountId] = 4 AND [ServiceType] = 'reinigung'");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 15 && model.Key == "62")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [  PriceRate]([AccountId],[ServiceType],[Descr],[CountFirst],[CountSecond],[CountPrice],[DescrOnChangeFirst],[DescrOnChangeSecond],[DescrOnChangePrice],[CountPriceRangeMax])
                                SELECT [AccountId],'transport',replace([Descr],'Umzugsmitarbeiter','Mitarbeiter'),[CountFirst],[CountSecond],[CountPrice],replace([DescrOnChangeFirst],'Umzugsmitarbeiter','Mitarbeiter'),[DescrOnChangeSecond],[DescrOnChangePrice],[CountPriceRangeMax]
                                    FROM [  PriceRate] WHERE [ServiceType] = 'umzug' And AccountId In (1,4)");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 12 && model.Key == "59")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[AccountEmployee]([AccountId],[TitleId],[LastName],[FirstName],[Zip],[AccountDepartment],[IsActive],[IsDeleted],[CreateDate],[CreateUserId])
                                VALUES (4,2,'Yurdakul','Ali',0,0,1,0,getdate(),null),(4,2,'Fuhrer','Marc',0,0,1,0,getdate(),null),(4,2,'Loretz','Cyrill',0,0,1,0,getdate(),null),(4,2,'Mohammadi','Sirwan',0,0,1,0,getdate(),null)");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 9 && model.Key == "56")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[EmailMarketing]([AccountId],[Accessor],[Description],[CreatedByUser],[EmailSubject],[EmailContent],[CreateDate])
                                VALUES (4,'autojob.OpenOfferWithoutFeedback','Automatisches Email (nach 2 Tagen) an alle Customer mit einer offener Offer.',0,'Ihre Offer von [Firmenname]','[Email-Text ist im System hinterlegt.]',Getdate())");
                            await db.Database.ExecuteSqlCommandAsync(@"UPDATE [dbo].[Offer] SET [CreateDate] = '2018-02-21 12:44:51.0000000' where [OfferId] = 10396");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 10 && model.Key == "57")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync(@"INSERT INTO [dbo].[EmailMarketing]([AccountId],[Accessor],[Description],[CreatedByUser],[EmailSubject],[EmailContent],[CreateDate])
                                VALUES (4,'autojob.AfterFinishedServiceThankmail','Automatisches Email an alle Customer mit durchgeführtem Dienstleistung. 3 Tage nach Ausführung der Dienstleistung.',0,'[Firmenname] - Besten Dank für die Zusammenarbeit','[Email-Text ist im System hinterlegt.]',Getdate())");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                    if (model.Id == 19 && model.Key == "66")
                    {
                        using (var db = new ApplicationDbContext())
                        {
                            await db.Database.ExecuteSqlCommandAsync("update Bill set ReceiptId = (cast(cast(0 as binary) as uniqueidentifier)) where ReceiptId is null");
                            await db.Database.ExecuteSqlCommandAsync("update Appointment set OfferId = 0 where OfferInnerId = 0");
                            await db.Database.ExecuteSqlCommandAsync(@"update Appointment set OfferId = o.OfferId 
                                                                        from Appointment a
                                                                            inner join Offer o on (a.OfferInnerId = o.OfferInnerId and a.AccountId = o.AccountId)
                                                                        where a.OfferInnerId <> 0 and a.OfferId is null");
                            await db.Database.ExecuteSqlCommandAsync("insert into EmailType (AccountId, Name, MailSubject) values (4, 'Receipt.ReceiptUmzug', 'Receipt - Helvetia Transporte AG')");
                            await db.Database.ExecuteSqlCommandAsync("insert into EmailType (AccountId, Name, MailSubject) values (4, 'Receipt.ReceiptReinigung', 'Receipt - Helvetia Transporte AG')");
                        }
                        ViewBag.ResultText = "Successful";
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Successful = false;
                    ViewBag.ResultText = string.Format("Error: {0}", ex.Message);
                }
            }

            return View();
        }
    }
}