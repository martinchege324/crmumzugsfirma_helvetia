﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Admin.ViewModels;
using CRM_Umzugsfirma.Areas.Admin.Models;
using System.Data.Entity;
using CRM_Umzugsfirma.Helpers;

namespace CRM_Umzugsfirma.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class RegisterUsersController : Controller
    {
        private ApplicationUserManager _userManager;
        private RoleManager<ApplicationRole> _roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new UserLoginDbContext()));

        public RegisterUsersController()
        {
        }

        public RegisterUsersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Admin/RegisterUsers
        public ActionResult Index()
        {
            List<RegisterUserViewModel> modelUsers = new List<RegisterUserViewModel>();

            List<ApplicationUser> users;
            using (var db = new UserLoginDbContext())
            {
                users = db.Users.ToList();
            }
            foreach (ApplicationUser user in users)
            {
                RegisterUserViewModel registerUser = new RegisterUserViewModel();
                registerUser.UserId = user.Id;
                registerUser.UserName = user.UserName;
                registerUser.Email = user.Email;
                registerUser.Role = UserManager.GetRoles(user.Id).Aggregate((i, j) => i + ", " + j);
                using (var dbAccount = new ApplicationDbContext())
                {
                    registerUser.Account = dbAccount.User_Accounts.Where(u => u.UserId == user.Id).Select(a => a.Account.AccountName).SingleOrDefault();
                }
                modelUsers.Add(registerUser);
            }
            return View(modelUsers);

        }

        // GET: Admin/RegisterUsers/Edit/5
        // If Empty creates a new account, else edit it
        public async Task<ActionResult> Edit(string id)
        {
            RegisterUserViewModel registerUser = new RegisterUserViewModel() { Roles = new List<CheckListViewModel>() };

            // role-container
            List<ApplicationRole> roles = _roleManager.Roles.OrderBy(r => r.Name).ToList();
            roles.ForEach(r => registerUser.Roles.Add(new CheckListViewModel() { Id = r.Id, Name = r.Name, Selected = false }));

            // account-container
            registerUser.AccountList = createAccountList();

            if (string.IsNullOrEmpty(id))
            {
                // -- Create new user
                ViewBag.Title = "Benutzer erstellen";
                ViewBag.SubmitText = "Create";

                ViewBag.AccountList = registerUser.AccountList;
                return View(registerUser);
            }

            // -- Edit user
            ViewBag.Title = "Benutzer to edit";
            ViewBag.SubmitText = "Save";

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            registerUser.UserId = user.Id;
            registerUser.UserName = user.UserName;
            registerUser.Email = user.Email;
            // select roles
            List<string> userRoles = UserManager.GetRoles(user.Id).ToList();
            foreach (string str in userRoles)
            {
                var chk = registerUser.Roles.SingleOrDefault(r => r.Name == str);
                if (chk != null) chk.Selected = true;
            }
            // select account
            using (var dbAccount = new ApplicationDbContext())
            {
                registerUser.Account = dbAccount.User_Accounts.Where(u => u.UserId == registerUser.UserId).Select(a => a.Account.AccountId).SingleOrDefault().ToString();
            }
            if (!string.IsNullOrEmpty(registerUser.Account))
            {
                selectDefinedSelectItem(registerUser.AccountList, registerUser.Account);
            }

            ViewBag.AccountList = registerUser.AccountList;
            return View(registerUser);
        }

        // POST: Admin/RegisterUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(RegisterUserViewModel registerUser)
        {

            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(registerUser.UserId))
                {
                    // -- Edit user
                    // pw change
                    /* -- it does not work
                    if (!string.IsNullOrEmpty(registerUser.Password) && registerUser.Password.Length >= 6)
                    {
                        ApplicationUser appUser = await UserManager.FindByIdAsync(registerUser.UserId);
                        String hashedPassword = UserManager.PasswordHasher.HashPassword(registerUser.Password);
                        if (appUser != null && !string.IsNullOrEmpty(hashedPassword))
                        {
                            UserStore<ApplicationUser> store = new UserStore<ApplicationUser>();
                            await store.SetPasswordHashAsync(appUser, hashedPassword);
                        }
                    }*/

                    // check for changed roles
                    List<string> currentRoles = UserManager.GetRoles(registerUser.UserId).ToList();
                    List<string> newRoles = registerUser.Roles.Where(r => r.Selected).Select(s => s.Name).ToList();
                    currentRoles.Sort();
                    newRoles.Sort();
                    if (!currentRoles.SequenceEqual(newRoles))
                    {
                        // add all new roles (exists in newlist, missing in currentlist) add to db
                        newRoles.Except(currentRoles).ToList().ForEach(delegate(String roleName)
                        {
                            UserManager.AddToRole(registerUser.UserId, roleName);
                        });
                        // remove all missing roles in newlist
                        currentRoles.Except(newRoles).ToList().ForEach(delegate(String roleName)
                        {
                            UserManager.RemoveFromRole(registerUser.UserId, roleName);
                        });
                    }

                    // check for changed accountid
                    using (var db = new ApplicationDbContext())
                    {
                        var user_Account = db.User_Accounts.Where(u => u.UserId == registerUser.UserId).SingleOrDefault();

                        if (user_Account == null)
                        {
                            if (registerUser.Account != null)
                            {
                                // create new, of non existing
                                user_Account = new User_Account();
                                user_Account.UserId = registerUser.UserId;
                                user_Account.AccountId = int.Parse(registerUser.Account);
                                db.User_Accounts.Add(user_Account);
                                await db.SaveChangesAsync();
                            }
                        }
                        else
                        {
                            if (registerUser.Account == null)
                            {
                                // delete actual user account, because user has no address anymore
                                db.User_Accounts.Remove(user_Account);
                                await db.SaveChangesAsync();
                            }
                            else if (user_Account.AccountId != int.Parse(registerUser.Account))
                            {
                                // edit of changing
                                user_Account.AccountId = int.Parse(registerUser.Account);
                                db.Entry(user_Account).State = EntityState.Modified;
                                await db.SaveChangesAsync();
                            }
                        }
                    }
                }
                else
                {
                    // -- Create new user

                    // create user
                    var user = new ApplicationUser { UserName = registerUser.Email, Email = registerUser.Email };
                    var result = await UserManager.CreateAsync(user, registerUser.Password);
                    if (result.Succeeded)
                    {
                        // confirm email adress
                        var token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        result = await UserManager.ConfirmEmailAsync(user.Id, token);

                        // add roles
                        var selectedRoles = registerUser.Roles.Where(m => m.Selected).ToList();
                        foreach (var role in selectedRoles)
                        {
                            result = await UserManager.AddToRoleAsync(user.Id, role.Name);
                            if (!result.Succeeded)
                            {
                                AddErrors(result);
                                ViewBag.ResultText = string.Format("Error on add role '{0}'", role.Name);
                                return View(registerUser);
                            }
                        }

                        // add account
                        if (!string.IsNullOrEmpty(registerUser.Account))
                        {
                            using (var db = new ApplicationDbContext())
                            {
                                User_Account user_Account = new User_Account();
                                user_Account.UserId = user.Id;
                                user_Account.AccountId = int.Parse(registerUser.Account);

                                db.User_Accounts.Add(user_Account);
                                await db.SaveChangesAsync();
                            }
                        }
                    }
                }

                return RedirectToAction("Index");
            }

            ViewBag.AccountList = createAccountList(registerUser.Account);
            return View(registerUser);
        }


        // GET: Admin/RegisterUsers/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            RegisterUserViewModel registerUser = new RegisterUserViewModel();

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            registerUser.UserId = user.Id;
            registerUser.UserName = user.UserName;
            registerUser.Email = user.Email;

            return View(registerUser);
        }

        // POST: Admin/RegisterUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            // delete actual user account, because user has no address anymore
            using (var db = new ApplicationDbContext())
            {
                var userAccounts = db.User_Accounts.Where(ua => ua.UserId == id).ToList();
                foreach (User_Account ua in userAccounts)
                {
                    db.User_Accounts.Remove(ua);
                }
                await db.SaveChangesAsync();
            }

            await UserManager.DeleteAsync(user);

            return RedirectToAction("Index");
        }

        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private List<SelectListItem> createAccountList(string selectValue = null)
        {
            List<SelectListItem> accountList = new List<SelectListItem>();
            accountList.Add(new SelectListItem() { Value = "", Text = "Please choose" });
            using (var dbAccount = new ApplicationDbContext())
            {
                dbAccount.Accounts.Where(a => a.IsActive).ToList().ForEach(a => accountList.Add(new SelectListItem() { Value = a.AccountId.ToString(), Text = a.AccountName, Selected = ((selectValue != null && selectValue == a.AccountId.ToString()) ? true : false) }));
            }

            return accountList;
        }

        private void selectDefinedSelectItem(List<SelectListItem> list, string value)
        {
            SelectListItem item = list.SingleOrDefault(a => a.Value == value);
            if (item != null) item.Selected = true;
        }



        #endregion

    }
}
