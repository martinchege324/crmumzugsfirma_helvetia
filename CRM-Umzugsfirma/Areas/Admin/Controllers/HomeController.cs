﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Admin.ViewModels;
using CRM_Umzugsfirma.Helpers;

namespace CRM_Umzugsfirma.Areas.Admin.Controllers
{
    [CustomAuthorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        private RoleManager<ApplicationRole> _roleManager = new RoleManager<ApplicationRole>(new RoleStore<ApplicationRole>(new UserLoginDbContext()));

        public HomeController()
        {
        }

        public HomeController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: /Admin
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /Admin/Configuration
        [HttpGet]
        public ActionResult Configuration()
        {
            return View();
        }

        //
        // POST: /Admin/Configuration
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Configuration(ConfigurationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(model.What))
            {
                if (model.What == "initialize-roles")
                {
                    // add role: AccountManager
                    IdentityResult result;
                    var exists = await _roleManager.RoleExistsAsync("AccountManager");
                    if (!exists)
                    {
                        result = _roleManager.Create(new ApplicationRole("AccountManager"));
                        if (!result.Succeeded)
                        {
                            ViewBag.ResultText = "Error on creating role: AccountManager";
                            return View();
                        }
                    }
                    // add role: AccountUser
                    exists = await _roleManager.RoleExistsAsync("AccountUser");
                    if (!exists)
                    {
                        result = _roleManager.Create(new ApplicationRole("AccountUser"));
                        if (!result.Succeeded)
                        {
                            ViewBag.ResultText = "Error on creating role: AccountUser";
                            return View();
                        }
                    }
                    ViewBag.ResultText = "Initialization of roles is done.";
                }
            }

            return View();
        }

        //
        // GET: /Admin/ConfigurationAdmin
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ConfigurationAdmin(string who)
        {
            if (who == "iwanttostart")
            {
                return View();
            }

            return RedirectToAction("Index", "Home");
        }

        //
        // POST: /Admin/ConfigurationAdmin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ConfigurationAdmin(ConfigurationViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (!string.IsNullOrEmpty(model.What))
            {
                if (model.What == "initialize-db-seed")
                {
                    // //System.Data.Entity.Database.SetInitializer(new InitDbInitializer());
                    //ApplicationDbContext db = new ApplicationDbContext();
                    //db.Database.Initialize(true); 
                }
                else if (model.What == "initialize-uygar")
                {
                    IdentityResult result;

                    // create admin role
                    var exists = await _roleManager.RoleExistsAsync("Admin");
                    if (!exists)
                    {
                        result = await _roleManager.CreateAsync(new ApplicationRole("Admin"));
                        if (!result.Succeeded)
                        {
                            ViewBag.ResultText = "Error on creating role: admin";
                            return View();
                        }
                    }

                    // create user uygar@eker.ch
                    var user = await UserManager.FindByEmailAsync("uygar@eker.ch");
                    if (user == null)
                    {
                        user = new ApplicationUser { UserName = "uygar@eker.ch", Email = "uygar@eker.ch" };
                        result = await UserManager.CreateAsync(user, "uygar9");
                        if (!result.Succeeded)
                        {
                            ViewBag.ResultText = "Error on creating user: uygar@eker.ch";
                            return View();
                        }

                        // confirm email adress
                        var token = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                        result = await UserManager.ConfirmEmailAsync(user.Id, token);

                        // add role to user
                        result = await UserManager.AddToRoleAsync(user.Id, "Admin");
                        if (!result.Succeeded)
                        {
                            ViewBag.ResultText = "Error on add role to user: uygar@eker.ch";
                            return View();
                        }
                    }


                    ViewBag.ResultText = "Initialization of admin is done.";
                }
            }

            return View();
        }

    }
}