﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System.Configuration;
using CRM_Umzugsfirma.Managers;
using System.Text;
using CRM_Umzugsfirma.Managers.Calendar;

namespace CRM_Umzugsfirma.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        public ActionResult Check(string type = null)
        {
            switch (type)
            {
                case "machinekeyset":
                    type = "MachineKeySet";
                    break;
                case "persistkeyset":
                    type = "PersistKeySet";
                    break;
                case "userkeyset":
                    type = "UserKeySet";
                    break;
                case "defaultkeyset":
                    type = "DefaultKeySet";
                    break;
                case "userprotected":
                    type = "UserProtected";
                    break;
                default:
                    type = "Exportable";
                    break;
            }
            StringBuilder sb = new StringBuilder();
            CalendarService cs = AuthenticateServiceAccount(ref sb, type);

            if (cs != null)
            {
                sb.AppendLine("cs != null");
                GoogleCalendarCalendarListManager calendarListManager = new GoogleCalendarCalendarListManager();
                CalendarList calList = calendarListManager.List(cs, null);
                if (calList != null)
                {
                    List<string> authorizedCalendars = calList.Items.Select(c => c.Id).ToList();
                    sb.AppendLine("authorizedCalendars:");
                    authorizedCalendars.ForEach(a => sb.AppendLine(a.ToString()));
                }
            }

            ViewBag.Content = sb.ToString();

            return View();
        }

        private CalendarService AuthenticateServiceAccount(ref StringBuilder sb, string type)
        {
            string serviceAccountEmail = ConfigurationManager.AppSettings["GoogleServiceAccount"];
            string keyFilePath = ConfigurationManager.AppSettings["GoogleServiceKey"];

            FileManager fileManager = new FileManager();
            keyFilePath = fileManager.GetAbsoluteFilePath(keyFilePath);

            sb.AppendLine(string.Format("serviceAccountEmail: {0}", serviceAccountEmail));
            sb.AppendLine(string.Format("keyFilePath: {0}", keyFilePath));

            // check the file exists
            if (!System.IO.File.Exists(keyFilePath))
            {
                //Console.WriteLine("An Error occurred - Key file does not exist");
                sb.AppendLine("An Error occurred - Key file does not exist");
                return null;
            }
            sb.AppendLine("File found.");

            string[] scopes = new string[] {
                CalendarService.Scope.Calendar  ,  // Manage your calendars
                CalendarService.Scope.CalendarReadonly    // View your Calendars
            };

            //var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            byte[] keyByte = System.IO.File.ReadAllBytes(keyFilePath);
            sb.AppendLine(string.Format("p12-key readed in byte-array. length: {0}", keyByte.Length));
            try
            {
                var certificate = new X509Certificate2();
                if (type == "Exportable")
                {
                    sb.AppendLine("Start read certifacte EXPORTABLE: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.Exportable)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.Exportable);
                }
                else if (type == "MachineKeySet")
                {
                    sb.AppendLine("Start read certifacte MACHINEKEYSET: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.MachineKeySet)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.MachineKeySet);
                }
                else if (type == "PersistKeySet")
                {
                    sb.AppendLine("Start read certifacte PersistKeySet: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.PersistKeySet)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.PersistKeySet);
                }
                else if (type == "UserKeySet")
                {
                    sb.AppendLine("Start read certifacte UserKeySet: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.UserKeySet)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.UserKeySet);
                }
                else if (type == "DefaultKeySet")
                {
                    sb.AppendLine("Start read certifacte DefaultKeySet: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.DefaultKeySet)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.DefaultKeySet);
                }
                else if (type == "UserProtected")
                {
                    sb.AppendLine("Start read certifacte UserProtected: var certificate = new X509Certificate2(keyByte, \"notasecret\", X509KeyStorageFlags.UserProtected)");
                    certificate = new X509Certificate2(keyByte, "notasecret", X509KeyStorageFlags.UserProtected);
                }
                
                sb.AppendLine("Certificate readed.");
                if (certificate.Handle == new IntPtr(0))
                {
                    sb.AppendLine("  No data to show.");
                }
                else
                {
                    sb.AppendLine(string.Format("  Name = {0}", certificate.SubjectName.Name));
                    sb.AppendLine(string.Format("  Effective Date = {0}", certificate.GetEffectiveDateString()));
                    sb.AppendLine(string.Format("  PubliKey = {0}", certificate.GetPublicKeyString()));
                }
                ServiceAccountCredential credential = new ServiceAccountCredential(
                    new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));

                // Create the service.
                CalendarService service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "crm-umzug",
                });
                sb.AppendLine("3");
                return service;
            }
            catch (Exception ex)
            {
                sb.AppendLine(string.Format("Error happended : MESSAGE: {0} ----- INNER-EXCEPTION: {1}", ex.Message, ex.InnerException));
                Console.WriteLine(ex.InnerException);
                return null;
            }
        }
    }
}