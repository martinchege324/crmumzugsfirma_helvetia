﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Managers.Jobs;
using CRM_Umzugsfirma.Managers.Offers;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CRM_Umzugsfirma.ViewModels.Job;

namespace CRM_Umzugsfirma.Controllers
{
    public class JobController : Controller
    {
        // GET: Job
        public ActionResult Index()
        {
            return RedirectToAction("NotFound", "Error", new { area = "" });
        }

        // GET: job/offerswithoutfeedback
        public ActionResult OffersWithoutFeedback(string id)
        {
            if (id == "CsUj49rZ8qCyaMg")
            {
                // AccountId: 4
                int accountId = 4;
                var createdDate = DateTime.Today.AddDays(-2);
                var autoJobViewModel = new AutoJobViewModel()
                {
                    IdList = new List<string>()
                };

                var openOfferWithoutFeedbackManager = new OpenOfferWithoutFeedbackManager();
                AutoJob autoJobRunsToday = openOfferWithoutFeedbackManager.GetAutoJobRunsTodaySuccessful();
                if (autoJobRunsToday == null || autoJobRunsToday.AutoJobId <= 0)
                {
                    // Prepare AutoJob, that will be stored in DB
                    var autoJob = new AutoJob()
                    {
                        Type = AutoJob.AutoJobType.OpenOfferWithoutFeedback,
                        AccountId = accountId,
                        CalledUrl = Request.Url != null ? Request.Url.OriginalString : string.Empty,
                        CalledSourceIP = Request.UserHostAddress,
                        Successful = true,
                        AffectedDbTable = "Offer"
                    };
                    var autoJobSentEmailList = new List<AutoJobSentEmail>();

                    try
                    {
                        var offersWaitingWithoutFeedback = openOfferWithoutFeedbackManager.GetAllWaitingOffersWithoutFeedback(accountId, createdDate);
                        if (offersWaitingWithoutFeedback.Count > 0)
                        {
                            autoJobViewModel.FoundObject = true;
                            // sent emails
                            autoJobSentEmailList = openOfferWithoutFeedbackManager.SendEmailToAffected(this, offersWaitingWithoutFeedback, this.ControllerContext.RequestContext);

                            // set all Id's to ViewModel for visualize
                            autoJobSentEmailList.ForEach(x => autoJobViewModel.IdList.Add($"{x.AffectedId} ({x.SentEmailAddress})"));
                        }
                    }
                    catch (Exception ex)
                    {
                        autoJob.Successful = false;
                        autoJob.ErrorMessage = ex.Message;
                    }

                    autoJob.AffectedCount = autoJobSentEmailList.Count;
                    foreach (var autoJobSentEmail in autoJobSentEmailList)
                    {
                        autoJob.AffectedIdList += $"{(!string.IsNullOrEmpty(autoJob.AffectedIdList) ? ";" : string.Empty)}{autoJobSentEmail.AffectedId}";
                        autoJob.AffectedEmailList += $"{(!string.IsNullOrEmpty(autoJob.AffectedEmailList) ? ";" : string.Empty)}{autoJobSentEmail.SentEmailAddress}";
                    }

                    autoJobViewModel.AutoJobId = openOfferWithoutFeedbackManager.CreateAutoJob(autoJob);
                    autoJobViewModel.ResultMessageToShow = "Folgende Offers haben eine Email erhalten";

                    // store also all sent emails to db (now there is autojobid for fk)
                    if (autoJobViewModel.FoundObject && autoJobSentEmailList.Count > 0)
                    {
                        var autoJobSentEmailManager = new AutoJobSentEmailManager();
                        foreach (AutoJobSentEmail autoJobSentEmail in autoJobSentEmailList)
                        {
                            autoJobSentEmail.AutoJobId = autoJobViewModel.AutoJobId;
                            autoJobSentEmailManager.CreateAutoJobSentEmail(autoJobSentEmail);
                        }
                    }

                }
                else
                {
                    autoJobViewModel.InfoMessageToShow = $"Dieser Job wurde heute bereits um '{autoJobRunsToday.CreateDate}' ausgeführt. Dabei wurden {autoJobRunsToday.AffectedCount} Elemente gefunden: {autoJobRunsToday.AffectedIdList}";
                }

                return View("JobSummary", autoJobViewModel);
            }
            else
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
        }

        // GET: job/afterfinishedservicethankmail
        public ActionResult AfterFinishedServiceThankmail(string id)
        {
            if (id == "GnVdD6ua3fd2Q8HG")
            {
                // AccountId: 4
                int accountId = 4;
                var finishedDate = DateTime.Today.AddDays(-3);
                var autoJobViewModel = new AutoJobViewModel()
                {
                    IdList = new List<string>()
                };

                var afterFinishedServiceThankmailManager = new AfterFinishedServiceThankmailManager();
                AutoJob autoJobRunsToday = afterFinishedServiceThankmailManager.GetAutoJobRunsTodaySuccessful();
                if (autoJobRunsToday == null || autoJobRunsToday.AutoJobId <= 0)
                {
                    // Prepare AutoJob, that will be stored in DB
                    var autoJob = new AutoJob()
                    {
                        Type = AutoJob.AutoJobType.AfterFinishedServiceThankmail,
                        AccountId = accountId,
                        CalledUrl = Request.Url != null ? Request.Url.OriginalString : string.Empty,
                        CalledSourceIP = Request.UserHostAddress,
                        Successful = true,
                        AffectedDbTable = "Appointment"
                    };
                    var autoJobSentEmailList = new List<AutoJobSentEmail>();

                    try
                    {
                        var finishedServices = afterFinishedServiceThankmailManager.GetAllFinishedServices(accountId, finishedDate);
                        if (finishedServices.Count > 0)
                        {
                            autoJobViewModel.FoundObject = true;
                            // sent emails
                            autoJobSentEmailList = afterFinishedServiceThankmailManager.SendEmailToAffected(this, finishedServices, this.ControllerContext.RequestContext);

                            // set all Id's to ViewModel for visualize
                            autoJobSentEmailList.ForEach(x => autoJobViewModel.IdList.Add($"{x.AffectedId} ({x.SentEmailAddress})"));
                        }
                    }
                    catch (Exception ex)
                    {
                        autoJob.Successful = false;
                        autoJob.ErrorMessage = ex.Message;
                    }

                    autoJob.AffectedCount = autoJobSentEmailList.Count;
                    foreach (var autoJobSentEmail in autoJobSentEmailList)
                    {
                        autoJob.AffectedIdList += $"{(!string.IsNullOrEmpty(autoJob.AffectedIdList) ? ";" : string.Empty)}{autoJobSentEmail.AffectedId}";
                        autoJob.AffectedEmailList += $"{(!string.IsNullOrEmpty(autoJob.AffectedEmailList) ? ";" : string.Empty)}{autoJobSentEmail.SentEmailAddress}";
                    }

                    autoJobViewModel.AutoJobId = afterFinishedServiceThankmailManager.CreateAutoJob(autoJob);
                    autoJobViewModel.ResultMessageToShow = "Folgende Services (Appointments) haben eine Email erhalten";

                    // store also all sent emails to db (now there is autojobid for fk)
                    if (autoJobViewModel.FoundObject && autoJobSentEmailList.Count > 0)
                    {
                        var autoJobSentEmailManager = new AutoJobSentEmailManager();
                        foreach (AutoJobSentEmail autoJobSentEmail in autoJobSentEmailList)
                        {
                            autoJobSentEmail.AutoJobId = autoJobViewModel.AutoJobId;
                            autoJobSentEmailManager.CreateAutoJobSentEmail(autoJobSentEmail);
                        }
                    }

                }
                else
                {
                    autoJobViewModel.InfoMessageToShow = $"Dieser Job wurde heute bereits um '{autoJobRunsToday.CreateDate}' ausgeführt. Dabei wurden {autoJobRunsToday.AffectedCount} Elemente gefunden: {autoJobRunsToday.AffectedIdList}";
                }

                return View("JobSummary", autoJobViewModel);
            }
            else
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }
        }


    }
}