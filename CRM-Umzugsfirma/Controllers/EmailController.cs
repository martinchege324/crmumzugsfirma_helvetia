﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.ViewModels.Mail;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_Umzugsfirma.ViewModels.Mail.Appointment;

namespace CRM_Umzugsfirma.Controllers
{
    public class EmailController : Controller
    {
        public ActionResult ViewAppointment(int id = 1)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            AppointmentManager appointmentManager = new AppointmentManager();
            Appointment appointment = appointmentManager.GetRandomAppointmentByAccount(account.AccountId);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(appointment.CustomerId);

            AppointmentViewMail appointmentViewMail = new AppointmentViewMail();
            appointmentViewMail.Date = appointment.AppointmentView.ViewDate;
            appointmentViewMail.Time = appointment.AppointmentView.ViewTime;

            appointmentViewMail.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", id),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View(string.Format("{0}/text/ViewAppointment.cshtml", appointmentViewMail.MainMailData.PathAccount), appointmentViewMail);
        }

        public ActionResult ServiceAppointment(int id = 1)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            AppointmentManager appointmentManager = new AppointmentManager();
            Appointment appointment = appointmentManager.GetRandomAppointmentByAccount(account.AccountId, Appointment.AppointmentType.Confirmation_of_the_order);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(appointment.CustomerId);

            AppointmentServiceMail appointmentServiceMail = new AppointmentServiceMail();
            appointmentServiceMail.DateUmzug = "13.05.2015";
            appointmentServiceMail.TimeUmzug = "08:30";
            appointmentServiceMail.DatePack = "13.05.2015";
            appointmentServiceMail.TimePack = "08:30";
            appointmentServiceMail.DatePackOut = "13.05.2015";
            appointmentServiceMail.TimePackOut = "14:30";
            appointmentServiceMail.DateEntsorgung = "13.05.2015";
            appointmentServiceMail.TimeEntsorgung = "08:30";
            appointmentServiceMail.DateReinigung = "13.05.2015";
            appointmentServiceMail.TimeReinigung = "08:30";
            appointmentServiceMail.DateReinigung2 = "17.05.2015";
            appointmentServiceMail.TimeReinigung2 = "14:30";
            appointmentServiceMail.DateLagerung = "13.05.2015";
            appointmentServiceMail.TimeLagerung = "08:30";
            appointmentServiceMail.DateTransport = "13.05.2015";
            appointmentServiceMail.TimeTransport = "08:30";

            appointmentServiceMail.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", id),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            appointmentServiceMail.Comment = "Wie abgemacht kommen unser Team bereits 15 Minuten vorher.\r\nDas Team wird dann in Rücksprache mit uns Ihnen den Mehraufwand übermitteln.\r\nBei Interesse können Sie es dann dem Team mitteilen.\r\n\r\nBesten Dank!";

            return View(string.Format("{0}/text/ServiceAppointment.cshtml", appointmentServiceMail.MainMailData.PathAccount), appointmentServiceMail);
        }

        public ActionResult DeliveryAppointment(int id = 1, AppointmentDelivery.DeliveryProductType deliveryProduct = AppointmentDelivery.DeliveryProductType.Packing_Material, AppointmentDelivery.DeliveryType deliveryType = AppointmentDelivery.DeliveryType.Delivery)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            AppointmentManager appointmentManager = new AppointmentManager();
            Appointment appointment = appointmentManager.GetRandomAppointmentByAccount(account.AccountId, Appointment.AppointmentType.Delivery);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(appointment.CustomerId);

            AppointmentDeliveryMail appointmentDeliveryMail = new AppointmentDeliveryMail();
            appointmentDeliveryMail.Date = appointment.AppointmentDelivery.DeliveryDate;
            appointmentDeliveryMail.TimeFrom = appointment.AppointmentDelivery.DeliveryTimeFrom;
            appointmentDeliveryMail.TimeTo = appointment.AppointmentDelivery.DeliveryTimeTo;

            appointmentDeliveryMail.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", id),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            var deliveryKeyAccessor = $"{deliveryProduct.ToString()}{(deliveryProduct == AppointmentDelivery.DeliveryProductType.Packing_Material ? $"-{deliveryType.ToString()}" : string.Empty)}";
            return View($"{appointmentDeliveryMail.MainMailData.PathAccount}/text/DeliveryAppointment-{deliveryKeyAccessor}.cshtml", appointmentDeliveryMail);
        }

        public ActionResult Offer(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            OfferManager offerManager = new OfferManager();
            Offer offer = offerManager.GetRandomOfferByAccount(account.AccountId);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(offer.CustomerId);

            if (offer == null) return null;

            var code = HttpUtility.UrlEncode(offerManager.GetCryptedOfferCode(offer));
            var acceptLink = Helpers.Helpers.ContentFullPath(Url.Action("A", "AcceptOffer", new { id = code }));

            OfferMail offerMail = new OfferMail()
            {
                ViewAppointmentStateId = CRM_Umzugsfirma.Areas.Intranet.Models.Offer.ViewAppointmentState.no,
                LinkOfferAccept = acceptLink
            };
            offerMail.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", id),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View(string.Format("{0}/text/Offer.cshtml", offerMail.MainMailData.PathAccount), offerMail);
        }

        public ActionResult Bill(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            BillManager billManager = new BillManager();
            Bill bill = billManager.GetRandomBillByAccount(account.AccountId);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(bill.CustomerId);

            if (bill == null) return null;

            BillMail billMailModel = new BillMail()
            {
                BillCost = bill.BillCost,
                PreCollectionCost = bill.PreCollectionCost,
                BillToPayUntil = bill.ToPayDate
            };
            billMailModel.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", account.InnerAccountId),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View(string.Format("{0}/text/Bill.cshtml", billMailModel.MainMailData.PathAccount), billMailModel);
        }

        public ActionResult Reminder(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            BillManager billManager = new BillManager();
            Bill bill = billManager.GetRandomBillByAccount(account.AccountId);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(bill.CustomerId);

            if (bill == null) return null;

            BillMail billMailModel = new BillMail()
            {
                BillCost = bill.BillCost,
                PreCollectionCost = bill.PreCollectionCost,
                BillToPayUntil = bill.ToPayDate
            };
            billMailModel.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", account.InnerAccountId),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View(string.Format("{0}/text/Bill-Reminder.cshtml", billMailModel.MainMailData.PathAccount), billMailModel);
        }

        public ActionResult Precollection(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountByInnerId(id);

            BillManager billManager = new BillManager();
            Bill bill = billManager.GetRandomBillByAccount(account.AccountId);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(bill.CustomerId);

            if (bill == null) return null;

            BillMail billMailModel = new BillMail()
            {
                BillCost = bill.BillCost,
                PreCollectionCost = 20f,
                BillToPayUntil = bill.ToPayDate
            };
            billMailModel.MainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = string.Format("~/Content/accountdata/{0}/mail", account.InnerAccountId),
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View(string.Format("{0}/text/Bill-Precollection.cshtml", billMailModel.MainMailData.PathAccount), billMailModel);
        }
    }
}