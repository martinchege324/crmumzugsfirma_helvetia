﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.Controllers
{
    [Authorize]
    public class UserAccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public UserAccountController()
        {
        }

        public UserAccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager )
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set 
            { 
                _signInManager = value; 
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return RedirectToAction("Login");
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (Session["SessionLoadDone"] != null)
                {
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    // no session, so logout
                    cleanSession();
                    AuthenticationManager.SignOut();
                    return RedirectToAction("Login", "UserAccount");
                }
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, true, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToAction("Load", "UserAccount", new { ReturnUrl = returnUrl });
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.Failure:
                default:
                    ModelState.AddModelError("", "Your email and password do not match.");
                    return View(model);
            }
        }


        [AllowAnonymous]
        public ActionResult Load(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                UserIdentifier userIdentifier = new UserIdentifier();
                userIdentifier.UserId = User.Identity.GetUserId();
                userIdentifier.UserName = User.Identity.Name;
                Session.Add("UserName", User.Identity.Name);
                userIdentifier.Area = "Admin";
                Session.Add("UserArea", "Admin");

                if (User.IsInRole("AccountUser"))
                {
                    userIdentifier.Area = "Intranet";
                    Session.Add("UserArea", "Intranet");

                    // load data from Account (Pk_AccountId, innerId, ..). Check if Account.IsActive=true. Else signout and turn to login.
                    AccountManager amanager = new AccountManager();
                    Account account = amanager.GetAccountByUserId(userIdentifier.UserId);
                    if ((account == null) || (!account.IsActive))
                    {
                        // logout redirect
                        cleanSession();
                        AuthenticationManager.SignOut();
                        return RedirectToAction("Login", "UserAccount");
                    }
                    userIdentifier.AccountId = account.AccountId;
                    Session.Add("AccountId", account.AccountId);
                    userIdentifier.InnerAccountId = account.InnerAccountId;
                    Session.Add("InnerAccountId", account.InnerAccountId);
                    userIdentifier.AccountName = account.AccountName;
                    Session.Add("AccountName", account.AccountName);
                    userIdentifier.AccountEmail = account.Email;
                    userIdentifier.GoogleEmail = account.GoogleEmail;
                }

                Session.Add("UserIdentifier", userIdentifier);
                Session.Add("SessionLoadDone", true);
            }

            return RedirectToLocal(returnUrl);
        }


        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        //
        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                string code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "UserAccount", new { userId = user.Id, code = code }, protocol: Request.Url.Scheme);		
                await UserManager.SendEmailAsync(user.Id, "Passwort zurücksetzen", "Um das Passwort neu zu setzen, bitte <a href=\"" + callbackUrl + "\">hier</a> klicken");
                return RedirectToAction("ForgotPasswordConfirmation", "UserAccount");
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "UserAccount");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "UserAccount");
            }
            AddErrors(result);
            return View();
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            cleanSession();
            AuthenticationManager.SignOut();
            return RedirectToAction("Login", "UserAccount");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void cleanSession()
        {
            Session.Clear();
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            if (User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("Admin"))
                {
                    return RedirectToAction("Index", "Home", new { area = "admin" });
                }
                else if (User.IsInRole("AccountUser"))
                {
                    // change to index, but as long as account manage is on developing, go to manage
                    return RedirectToAction("Index", "Home", new { area = "intranet" });
                }
            }

            return RedirectToAction("Login", "UserAccount");
        }

        #endregion
    }
}