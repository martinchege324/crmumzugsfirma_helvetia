﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.Managers.Pdf;
using CRM_Umzugsfirma.ViewModels.Mail;
using CRM_Umzugsfirma.ViewModels.Print;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Controllers
{
    public class PrintController : Controller
    {
        // GET: print/footer/1
        public ActionResult Footer(int id = 1)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(id);

            MainMailData model = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountZip = account.Zip,
                AccountCity = account.City,
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage
            };

            return View(string.Format("~/Content/accountdata/{0}/print/text/_FooterPartial.cshtml", model.InnerAccountId), model);
        }

        // GET: print/offer/
        public ActionResult Offer(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            OfferPrintViewModel model = new OfferPrintViewModel();

            OfferManager offerManager = new OfferManager();
            model.Offer = offerManager.GetOfferByCryptedOfferCode(HttpUtility.UrlDecode(id));
            if (model.Offer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (model.Offer.UmzugCostAddit) model.UmzugAdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "umzug");
            else model.UmzugAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Offer.PackCostAddit) model.PackAdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "pack");
            else model.PackAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Offer.PackOutCostAddit) model.PackOutAdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "packout");
            else model.PackOutAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Offer.OfferCleaning != null && model.Offer.OfferCleaning.ReinigungCostAddit) model.ReinigungAdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "cleaning");
            else model.ReinigungAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Offer.OfferCleaning2 != null && model.Offer.OfferCleaning2.ReinigungCostAddit) model.Reinigung2AdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "cleaning2");
            else model.Reinigung2AdditCost = new List<OfferPriceAdditCosts>();
            if (model.Offer.EntsorgungCostAddit) model.EntsorgungAdditCost = offerManager.GetSelectedAdditionalCosts(model.Offer.OfferId, "disposal");
            else model.EntsorgungAdditCost = new List<OfferPriceAdditCosts>();

            if (model.Offer.PackMaterialActive)
            {
                model.DefinedPackMaterials = offerManager.GetDefinedPackMaterials(model.Offer.OfferId, true);
                foreach (Offer_DefinedPackMaterial odpm in model.DefinedPackMaterials)
                {
                    odpm.PiecePrice = Helpers.Helpers.NumberCashBeatiful(odpm.PiecePrice);
                    odpm.EndPrice = Helpers.Helpers.NumberCashBeatiful(odpm.EndPrice);
                }
                model.PackMaterials = offerManager.GetPackMaterials(model.Offer.OfferId);
                foreach (OfferPackMaterial opm in model.PackMaterials)
                {
                    opm.PricePerPiece = Helpers.Helpers.NumberCashBeatiful(opm.PricePerPiece);
                    opm.EndPrice = Helpers.Helpers.NumberCashBeatiful(opm.EndPrice);
                }
            }
            else
            {
                model.DefinedPackMaterials = new List<Offer_DefinedPackMaterial>();
                model.PackMaterials = new List<OfferPackMaterial>();
            }

            model.Offer.PackMaterialCostSum = Helpers.Helpers.NumberCashBeatiful(model.Offer.PackMaterialCostSum);

            AccountManager accountManager = new AccountManager();
            model.Account = accountManager.GetAccountById(model.Offer.AccountId);
            model.AdrOutTitle = offerManager.GetTextAdrOutTitle(model.Offer);
            model.AdrInTitle = offerManager.GetTextAdrInTitle(model.Offer);

            CustomerManager customerManager = new CustomerManager();
            model.Customer = customerManager.GetCustomerById(model.Offer.CustomerId);
            model.CustomerSalutation = customerManager.GetSalutation(model.Customer);

            model.PathAccount = string.Format("~/Content/accountdata/{0}/print", model.Account.InnerAccountId);

            return View(string.Format("~/Content/accountdata/{0}/print/text/Offer.cshtml", model.Account.InnerAccountId), model);
        }

        public ActionResult OfferP(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            OfferManager offerManager = new OfferManager();
            Offer offer = offerManager.GetOfferByCryptedOfferCode(HttpUtility.UrlDecode(id));
            if (offer == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            PdfManager pdfManager = new PdfManager();
            return pdfManager.GetForOfferAsAction(offer, this.ControllerContext.RequestContext, string.Format("Offer-{0}.pdf", offer.OfferInnerId));
        }

        // GET: print/receipt/
        public ActionResult Receipt(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            var model = new ReceiptPrintViewModel();

            var receiptManager = new ReceiptManager();
            model.Receipt = receiptManager.GetReceiptByCryptedReceiptCode(HttpUtility.UrlDecode(id));
            if (model.Receipt == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            switch (model.Receipt.ReceiptTypeId)
            {
                case Areas.Intranet.Models.Receipt.ReceiptType.Move:
                    model.ReceiptUmzug = model.Receipt.ReceiptUmzugs.FirstOrDefault();
                    break;
                case Areas.Intranet.Models.Receipt.ReceiptType.cleaning:
                    model.ReceiptReinigung = model.Receipt.ReceiptReinigungs.FirstOrDefault();
                    break;
            }

            var accountManager = new AccountManager();
            model.Account = accountManager.GetAccountById(model.Receipt.AccountId);

            var customerManager = new CustomerManager();
            model.Customer = customerManager.GetCustomerById(model.Receipt.CustomerId);
            model.CustomerSalutation = customerManager.GetSalutation(model.Customer);

            model.PathAccount = $"~/Content/accountdata/{model.Account.InnerAccountId}/print";

            return View($"~/Content/accountdata/{model.Account.InnerAccountId}/print/text/Receipt{((int)(model.Receipt.ReceiptTypeId))}.cshtml", model);
        }

        public ActionResult ReceiptP(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            var receipt = new ReceiptManager().GetReceiptByCryptedReceiptCode(HttpUtility.UrlDecode(id));
            if (receipt == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            Account account = new AccountManager().GetAccountById(receipt.AccountId);

            PdfManager pdfManager = new PdfManager();
            //return pdfManager.GetForReceiptAsAction(receipt, this.ControllerContext.RequestContext, $"Receipt-{receipt.ReceiptInnerIdExtern}.pdf");
            return pdfManager.GetForReceiptAsAction(receipt, this.ControllerContext.RequestContext, "Receipt.pdf");
        }

        public ActionResult MailReceipt(int id)
        {
            Account account = new AccountManager().GetAccountById(id);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomersByAccountId(id).FirstOrDefault();

            if (customer == null) return RedirectToAction("NotFound", "Error", new { area = "" });

            var mainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = $"{account.Zip} {account.City}",
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View($"~/Content/accountdata/{account.InnerAccountId}/mail/text/Receipt.cshtml", mainMailData);
        }

        // GET: print/bill/
        public ActionResult Bill(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            BillPrintViewModel model = new BillPrintViewModel();

            BillManager billManager = new BillManager();
            model.Bill = billManager.GetBillByCryptedBillCode(HttpUtility.UrlDecode(id));
            if (model.Bill == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            if (model.Bill.UmzugCostAddit) model.UmzugAdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "umzug");
            else model.UmzugAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Bill.PackCostAddit) model.PackAdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "pack");
            else model.PackAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Bill.PackOutCostAddit) model.PackOutAdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "packout");
            else model.PackOutAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Bill.BillCleaning != null && model.Bill.BillCleaning.ReinigungCostAddit) model.ReinigungAdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "cleaning");
            else model.ReinigungAdditCost = new List<OfferPriceAdditCosts>();
            if (model.Bill.BillCleaning2 != null && model.Bill.BillCleaning2.ReinigungCostAddit) model.Reinigung2AdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "cleaning2");
            else model.Reinigung2AdditCost = new List<OfferPriceAdditCosts>();
            if (model.Bill.EntsorgungCostAddit) model.EntsorgungAdditCost = billManager.GetSelectedAdditionalCosts(model.Bill.BillId, "disposal");
            else model.EntsorgungAdditCost = new List<OfferPriceAdditCosts>();

            if (model.Bill.PackMaterialActive)
            {
                model.DefinedPackMaterials = billManager.GetDefinedPackMaterials(model.Bill.BillId, true);
                foreach (Bill_DefinedPackMaterial bdpm in model.DefinedPackMaterials)
                {
                    bdpm.PiecePrice = Helpers.Helpers.NumberCashBeatiful(bdpm.PiecePrice);
                    bdpm.EndPrice = Helpers.Helpers.NumberCashBeatiful(bdpm.EndPrice);
                }
                model.PackMaterials = billManager.GetPackMaterials(model.Bill.BillId);
                foreach (BillPackMaterial bpm in model.PackMaterials)
                {
                    bpm.PricePerPiece = Helpers.Helpers.NumberCashBeatiful(bpm.PricePerPiece);
                    bpm.EndPrice = Helpers.Helpers.NumberCashBeatiful(bpm.EndPrice);
                }
            }
            else
            {
                model.DefinedPackMaterials = new List<Bill_DefinedPackMaterial>();
                model.PackMaterials = new List<BillPackMaterial>();
            }
            model.Bill.PackMaterialCost = Helpers.Helpers.NumberCashBeatiful(model.Bill.PackMaterialCost);

            AccountManager accountManager = new AccountManager();
            model.Account = accountManager.GetAccountById(model.Bill.AccountId);

            CustomerManager customerManager = new CustomerManager();
            model.Customer = customerManager.GetCustomerById(model.Bill.CustomerId);
            model.CustomerSalutation = customerManager.GetSalutation(model.Customer);

            model.PathAccount = string.Format("~/Content/accountdata/{0}/print", model.Account.InnerAccountId);

            return View(string.Format("~/Content/accountdata/{0}/print/text/Bill.cshtml", model.Account.InnerAccountId), model);
        }

        public ActionResult BillP(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            BillManager billManager = new BillManager();
            Bill bill = billManager.GetBillByCryptedBillCode(HttpUtility.UrlDecode(id));
            if (bill == null)
            {
                return RedirectToAction("NotFound", "Error", new { area = "" });
            }

            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(bill.AccountId);

            PdfManager pdfManager = new PdfManager();
            if ((account.InnerAccountId == 7) && (DateTime.Now > new DateTime(2016, 01, 01, 0, 0, 0)))
            {
                return pdfManager.GetForBillAsAction(bill, this.ControllerContext.RequestContext, "Invoice.pdf");
            }
            else
            {
                return pdfManager.GetForBillAsAction(bill, this.ControllerContext.RequestContext, string.Format("Invoice-{0}.pdf", bill.BillInnerId));
            }
        }

        public ActionResult MailJobOpenOfferWithoutFeedback(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(id);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomersByAccountId(id).FirstOrDefault();

            MainMailData mainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = $"{account.Zip} {account.City}",
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View($"{mainMailData.PathAccount}/text/JobOpenOfferWithoutFeedback.cshtml", mainMailData);
        }

        public ActionResult MailJobAfterFinishedServiceThankmail(int id)
        {
            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(id);

            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomersByAccountId(id).FirstOrDefault();

            MainMailData mainMailData = new MainMailData()
            {
                InnerAccountId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = $"{account.Zip} {account.City}",
                AccountPhone = account.Phone,
                AccountFax = account.Fax,
                AccountMail = account.Email,
                AccountWebAddress = account.WebPage,
                AccountContactName = account.ContactName,
                PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                PathFooterLogo = "",
                CustomerSalutation = customerManager.GetSalutation(customer),
            };

            return View($"{mainMailData.PathAccount}/text/JobAfterFinishedServiceThankmail.cshtml", mainMailData);
        }

    }
}