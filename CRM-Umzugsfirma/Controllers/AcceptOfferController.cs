﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Controllers
{
    public class AcceptOfferController : Controller
    {
        // GET: AcceptOffer
        public ActionResult A(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return View("Empty");
            }
            OfferManager offerManager = new OfferManager();
            Offer offer = offerManager.GetOfferByCryptedOfferCode(HttpUtility.UrlDecode(id));
            if (offer == null)
            {
                return View("Empty");
            }

            AccountManager accountManager = new AccountManager();
            Account account = accountManager.GetAccountById(offer.AccountId);
            CustomerManager customerManager = new CustomerManager();
            Customer customer = customerManager.GetCustomerById(offer.CustomerId);

            AcceptOfferViewModel model = new AcceptOfferViewModel()
            {
                OfferId = offer.OfferId,
                OfferInnerId = offer.OfferInnerId,
                AccountId = account.AccountId,
                AccountInnerId = account.InnerAccountId,
                AccountName = account.AccountName,
                AccountStreet = account.Street,
                AccountCity = string.Format("{0} {1}", account.Zip, account.City),
                AccountPhone = account.Phone,
                AccountMail = account.Email,
                AccountContactName = account.ContactName,
                CustomerId = customer.CustomerId,
                CustomerInnerId = customer.CustomerInnerId,
                CustomerMarketId = customer.MarketId,
                Name = customerManager.GetTitledName(customer),
                Salutation = customerManager.GetSalutation(customer),
                CustomerPhone = customer.Phone,
                CustomerMobile = customer.Mobile,
                CustomerEmail = customer.Email
            };

            return View(model);
        }

        // POST: AcceptOffer
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult A(AcceptOfferViewModel model, string id)
        {
            bool isSuccessful = false;
            if (ModelState.IsValid)
            {
                model.LinkToOffer = Helpers.Helpers.ContentFullPath(Url.RouteUrl("Intranet_CustomerOffer", new { action = "Details", customerid = model.CustomerId, id = model.OfferId }));

                EmailSenderFactory emailSenderFactory = new EmailSenderFactory();
                EmailSender emailSender = emailSenderFactory.CreateEmailSender("main");

                string body = this.RenderPartialViewToString("~/Views/AcceptOffer/Mail.cshtml", model);
                // send email
                isSuccessful = emailSender.SendEmail(model.AccountMail, "Offers-Confirmation", body);
            }
            if (isSuccessful)
            {
                ViewBag.SuccessMessage = "a";
                OfferManager offerManager = new OfferManager();
                Offer offer = offerManager.GetOfferById(model.OfferId);
                if (offer != null)
                {
                    offerManager.UpdateOfferState(offer, Offer.OfferState.confirmed);
                }
                CustomerManager customerManager = new CustomerManager();
                Customer customer = customerManager.GetCustomerById(model.CustomerId);
                if (customer.ContactStage < ContactStageType.Contract)
                {
                    customerManager.UpdateContactStageType(customer, ContactStageType.Contract);
                }
                if (customer.ContractType == ContractType.Lead)
                {
                    customerManager.UpdateContractType(customer, ContractType.Customer);
                }
            }
            else
            {
                ViewBag.ErrorMessage = "a";
            }

            return View(model);
        }

    }
}