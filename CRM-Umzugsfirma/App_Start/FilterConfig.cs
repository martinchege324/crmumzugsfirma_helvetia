﻿using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
