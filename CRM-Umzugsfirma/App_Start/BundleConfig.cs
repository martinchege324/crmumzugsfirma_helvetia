﻿using System.Web;
using System.Web.Optimization;

namespace CRM_Umzugsfirma
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jQueryFixes.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/customval").Include(
                        "~/Scripts/custom-validator.js"));
            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
                    "~/Scripts/datepicker/bootstrap-datepicker.js",
                    "~/Scripts/datepicker/locales/bootstrap-datepicker.de.js",
                    "~/Scripts/datepicker/bootstrap-timepicker.js"));

            bundles.Add(new ScriptBundle("~/bundles/offer").Include(
                        "~/Scripts/custom/offer.js",
                        "~/Scripts/custom/emailchangetext.js",
                        "~/Scripts/custom-validator.js"));
            bundles.Add(new ScriptBundle("~/bundles/receipt").Include(
                "~/Scripts/custom/receipt.js",
                "~/Scripts/custom/emailchangetext.js",
                "~/Scripts/custom-validator.js"));
            bundles.Add(new ScriptBundle("~/bundles/bill").Include(
                        "~/Scripts/custom/bill.js",
                        "~/Scripts/custom/emailchangetext.js",
                        "~/Scripts/custom-validator.js"));
            bundles.Add(new ScriptBundle("~/bundles/meet").Include(
                "~/Scripts/custom/emailchangetext.js",
                "~/Scripts/custom-validator.js"));


            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap.vertical-tabs.css",
                      "~/Content/general.min.css",
                      "~/Content/main.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/datepicker").Include(
                      "~/Content/datepicker.css",
                      "~/Content/bootstrap-timepicker.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/pagedlist").Include(
                      "~/Content/PagedList.css"));
            bundles.Add(new StyleBundle("~/Content/css/print").Include(
                "~/Content/print.min.css"));

            bundles.Add(new StyleBundle("~/Content/css/public").Include(
                      "~/Content/css-custom/public.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/intranet").Include(
                      "~/Content/css-custom/intranet.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/admin").Include(
                      "~/Content/css-custom/admin.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/intranet/page").Include(
                      "~/Content/css-custom/intranet/page-design.min.css"));
            bundles.Add(new StyleBundle("~/Content/css/intranet/offer").Include(
                      "~/Content/css-custom/intranet/offer.min.css"));

        }
    }
}
