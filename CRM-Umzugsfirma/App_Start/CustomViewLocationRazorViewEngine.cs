﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.App_Start
{
    public class CustomViewLocationRazorViewEngine : RazorViewEngine
    {
        public CustomViewLocationRazorViewEngine() : base()
        {
            List<string> existingPaths = new List<string>(AreaViewLocationFormats);
            existingPaths.Add("~/Areas/{2}/Views/Customer/{1}/{0}.cshtml");
            AreaViewLocationFormats = existingPaths.ToArray();

            existingPaths = new List<string>(ViewLocationFormats);
            existingPaths.Add("~/Views/Customer/{1}/{0}.cshtml");
            ViewLocationFormats = existingPaths.ToArray();
        }


    }
}