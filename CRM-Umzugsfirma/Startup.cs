﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CRM_Umzugsfirma.Startup))]
namespace CRM_Umzugsfirma
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
