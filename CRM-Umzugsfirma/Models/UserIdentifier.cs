﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Models
{
    public class UserIdentifier
    {
        public string UserId { get; set; }
        public string UserName { get; set; }

        public string Area { get; set; }

        public int AccountId { get; set; }
        public int InnerAccountId { get; set; }
        public string AccountName { get; set; }

        public string AccountEmail { get; set; }
        public string GoogleEmail { get; set; }
    }
}