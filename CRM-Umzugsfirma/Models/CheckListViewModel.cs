﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Models
{
    public class CheckListViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}