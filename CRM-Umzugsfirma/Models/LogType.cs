﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Models
{
    public enum LogType : int
    {
        Analyse = 0,
        Info = 1,
        Warning = 7,
        Error = 9,
    }
}