﻿using CRM_Umzugsfirma.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CRM_Umzugsfirma.Models
{
    public class Log
    {
        public int LogId { get; set; }

        public LogType LogTypeId { get; set; }

        public DateTime CreateDate { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        [StringLength(500)]
        public string RequestUrl { get; set; }
        [StringLength(50)]
        public string RequestMethod { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }
    }
}