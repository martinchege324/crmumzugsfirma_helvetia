﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Models
{
    public class EmailConfigurationModel
    {
        public string SmtpHost { get; set; }
        public int SmtpPort { get; set; }
        public bool SmtpUseSSL { get; set; }
        public string SenderAddress { get; set; }
        public string SenderPass { get; set; }

        public string DisplayName { get; set; }
        public string ReplyAdress { get; set; }

        public string BackupEmailAddress { get; set; }

        public string SenderAddressSales { get; set; }
        public string SenderAddressAccounting { get; set; }
    }
}