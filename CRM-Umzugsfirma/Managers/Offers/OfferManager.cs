﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WebGrease.Css.Extensions;

namespace CRM_Umzugsfirma.Managers.Offers
{
    public class OfferManager
    {
        public List<Offer> GetOffersByCustomer(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Offers.Include("OfferCleanings").Include("OfferTransports").Where(n => n.CustomerId == customerId).OrderBy(n => n.CreateDate).ToList();
            }
        }

        public Offer GetOfferById(int offerId, bool checkAccountIdFromSession = false, int checkAccountId = 0)
        {
            if ((checkAccountIdFromSession) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                checkAccountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }
            
            using (var db = new ApplicationDbContext())
            {
                Offer offer = db.Offers.Include("OfferCleanings").Include("OfferTransports").FirstOrDefault(n => n.OfferId == offerId);
                if ((checkAccountId > 0) && (offer.AccountId != checkAccountId))
                {
                    return null;
                }
                return offer;
            }
        }

        public Offer GetOfferByInnerId(int innerOfferId, int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Offers.Include("OfferCleanings").Include("OfferTransports").FirstOrDefault(x => x.OfferInnerId == innerOfferId && x.AccountId == accountId);
            }
        }

        public List<OfferPriceAdditCosts> GetSelectedAdditionalCosts(int offerId, string serviceArea)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from op in db.Offer_PriceAddits
                             join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                             where op.OfferId == offerId && par.ServiceType == serviceArea
                             select new OfferPriceAdditCosts()
                             {
                                 PriceAdditRateId = par.PriceAdditRateId,
                                 Descr = par.DescrToShow,
                                 OverwrittenPrice = op.EndPrice,
                                 MetaTag = par.MetaTag
                             }).ToList<OfferPriceAdditCosts>();
            }
        }

        public List<OfferPriceAdditCosts> GetSelectedAdditionalCosts(int offerId, string serviceArea, string metaTag)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from op in db.Offer_PriceAddits
                    join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                    where op.OfferId == offerId && par.ServiceType == serviceArea && par.MetaTag == metaTag
                    select new OfferPriceAdditCosts()
                    {
                        PriceAdditRateId = par.PriceAdditRateId,
                        Descr = par.DescrToShow,
                        OverwrittenPrice = op.EndPrice,
                        MetaTag = par.MetaTag
                    }).ToList<OfferPriceAdditCosts>();
            }
        }

        /// <summary>Get selected DefinedPackMaterial. This packmaterials are defined from shop and will only be selected by dropdown</summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        public List<Offer_DefinedPackMaterial> GetDefinedPackMaterials(int offerId, bool includeDefinedPackMaterial = false)
        {
            using (var db = new ApplicationDbContext())
            {
                var offerDefinedPackMaterial = db.Offer_DefinedPackMaterials.AsQueryable();
                if (includeDefinedPackMaterial)
                {
                    offerDefinedPackMaterial = offerDefinedPackMaterial.Include(s => s.DefinedPackMaterial);
                }
                return offerDefinedPackMaterial.Where(x => x.OfferId == offerId).ToList();
            }
        }

        /// <summary>Get selected freetext PackMaterial. This packmaterials are free textfields that can be set free text by user in textfield</summary>
        /// <param name="offerId"></param>
        /// <returns></returns>
        public List<OfferPackMaterial> GetPackMaterials(int offerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.OfferPackMaterials.Where(x => x.OfferId == offerId).ToList();
            }
        }

        /// <summary>Get next possible inneroffer id of account. smallest count is 1000 (as defined)</summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public int GetNextPossibleInnerOfferId(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                int nextId = (1 + db.Offers.Where(c => c.AccountId == accountId).OrderByDescending(o => o.OfferInnerId).Select(s => s.OfferInnerId).FirstOrDefault());
                if (nextId < 1000) nextId = 1000;

                return nextId;
            }
        }

        public int CreateOffer(Offer offer, Customer customer, string createUserId = null)
        {
            // set defaults
            offer.CreateDate = DateTime.Now;
            offer.CreateUserId = getUserId(createUserId);
            offer.AccountId = customer.AccountId;
            offer.CustomerId = customer.CustomerId;
            offer.OfferStateId = Offer.OfferState.waiting_for_customer;
            offer.OfferInnerId = GetNextPossibleInnerOfferId(customer.AccountId);

            // clean depencies if not really needed
            offer = cleanDependencySubtables(offer);
            // remove dependant model-list from main model and add later manually all sub-table values
            var offerCleanings = offer.OfferCleanings;
            offer.OfferCleanings = null;
            var offerTransports = offer.OfferTransports;
            offer.OfferTransports = null;

            using (var db = new ApplicationDbContext())
            {
                db.Offers.Add(offer);
                db.SaveChanges();

                // add manually all sub-table values
                bool saveChange = false;
                if (offerCleanings != null && offerCleanings.Count > 0)
                {
                    foreach (OfferCleaning offerCleaning in offerCleanings)
                    {
                        offerCleaning.OfferId = offer.OfferId;
                        db.OfferCleanings.Add(offerCleaning);
                    }
                    saveChange = true;
                }
                if (offerTransports != null && offerTransports.Count > 0)
                {
                    foreach (OfferTransport offerTransport in offerTransports)
                    {
                        offerTransport.OfferId = offer.OfferId;
                        db.OfferTransports.Add(offerTransport);
                    }
                    saveChange = true;
                }

                if (saveChange)
                {
                    db.SaveChanges();
                }
            }

            // update, if already had contact with customer. And customers contact stage to offer
            CustomerManager customerManager = new CustomerManager();
            if (offer.ViewAppointmentStateId == Offer.ViewAppointmentState.made)
            {
                customer.ContactAlreadyHave = true;
            }
            customerManager.UpdateContactStageType(customer, ContactStageType.Offer);

            return offer.OfferId;
        }

        public int EditOffer(Offer offer, string modifyUserId = null)
        {
            offer.ModifyDate = DateTime.Now;
            modifyUserId = getUserId(modifyUserId);
            if (!string.IsNullOrEmpty(modifyUserId)) offer.ModifyUserId = modifyUserId;

            // clean depencies if not really needed
            offer = cleanDependencySubtables(offer);

            using (var db = new ApplicationDbContext())
            {
                db.Entry(offer).State = EntityState.Modified;

                // Dependant Model-Lists: try to update or remove all depencies sub-table
                var offerCleaning = db.OfferCleanings.FirstOrDefault(s => s.OfferId == offer.OfferId && s.SectorId == 1);
                if (offer.ReinigungActive)
                {
                    db.Entry(offer.OfferCleaning).State = (offerCleaning != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (offerCleaning != null)
                {
                    db.Entry(offer.OfferCleaning).State = EntityState.Deleted;
                }
                var offerCleaning2 = db.OfferCleanings.FirstOrDefault(s => s.OfferId == offer.OfferId && s.SectorId == 2);
                if (offer.Reinigung2Active)
                {
                    db.Entry(offer.OfferCleaning2).State = (offerCleaning2 != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (offerCleaning2 != null)
                {
                    db.Entry(offer.OfferCleaning2).State = EntityState.Deleted;
                }
                var offerTransport = db.OfferTransports.FirstOrDefault(s => s.OfferId == offer.OfferId && s.SectorId == 1);
                if (offer.TransportActive)
                {
                    db.Entry(offer.OfferTransport).State = (offerTransport != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (offerTransport != null)
                {
                    db.Entry(offer.OfferTransport).State = EntityState.Deleted;
                }

                db.SaveChanges();
            }

            return offer.OfferId;
        }

        public bool DeleteOffer(int offerId)
        {
            using (var db = new ApplicationDbContext())
            {
                Offer offer = db.Offers.Find(offerId);
                if (offer == null) return false;

                // delete before all receipts of offer - delete on cascade got a circular so that not works
                var receiptManager = new ReceiptManager();
                receiptManager.DeleteReceiptByOfferId(offerId);

                // delete offer
                db.Offers.Remove(offer);
                db.SaveChanges();
            }
            return true;
        }

        public Offer GetRandomOfferByAccount(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Offers.Include("OfferCleanings").Include("OfferTransports").FirstOrDefault(a => a.AccountId == accountId);
            }
        }

        public bool SetSelectedAdditionalCosts(int offerId, CustomerOfferViewModel model)
        {
            using (var db = new ApplicationDbContext())
            {
                // Move
                var umzugAddit = (from op in db.Offer_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                  where op.OfferId == offerId && par.ServiceType == "umzug"
                                  select op).ToList();

                // remove all before inserted umzug addit
                if (umzugAddit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(umzugAddit);
                    db.SaveChanges();
                }
                // insert all selected umzug addit
                if (model.Offer.UmzugActive && model.Offer.UmzugCostAddit)
                {
                    var selectedUmzugAddit = model.UmzugPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedUmzugAddit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // Pack
                var packAddit = (from op in db.Offer_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                  where op.OfferId == offerId && par.ServiceType == "pack"
                                  select op).ToList();

                // remove all before inserted pack addit
                if (packAddit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(packAddit);
                    db.SaveChanges();
                }
                // insert all selected pack addit
                if (model.Offer.PackActive && model.Offer.PackCostAddit)
                {
                    var selectedPackAddit = model.PackPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedPackAddit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // Unpack
                var packOutAddit = (from op in db.Offer_PriceAddits
                    join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                    where op.OfferId == offerId && par.ServiceType == "packout"
                    select op).ToList();

                // remove all before inserted packout addit
                if (packOutAddit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(packOutAddit);
                    db.SaveChanges();
                }
                // insert all selected packout addit
                if (model.Offer.PackOutActive && model.Offer.PackOutCostAddit)
                {
                    var selectedPackOutAddit = model.PackOutPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedPackOutAddit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // cleaning
                var reinigungAddit = (from op in db.Offer_PriceAddits
                                 join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                 where op.OfferId == offerId && par.ServiceType == "cleaning"
                                 select op).ToList();

                // remove all before inserted cleaning addit
                if (reinigungAddit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(reinigungAddit);
                    db.SaveChanges();
                }
                // insert all selected cleaning addit
                if (model.Offer.ReinigungActive && model.Offer.OfferCleaning.ReinigungCostAddit)
                {
                    var selectedReinigungAddit = model.ReinigungPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedReinigungAddit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // Cleaning2
                var reinigung2Addit = (from op in db.Offer_PriceAddits
                    join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                    where op.OfferId == offerId && par.ServiceType == "reinigungg2"
                                       select op).ToList();

                // remove all before inserted cleaning2 addit
                if (reinigung2Addit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(reinigung2Addit);
                    db.SaveChanges();
                }
                // insert all selected cleaning2 addit
                if (model.Offer.Reinigung2Active && model.Offer.OfferCleaning2.ReinigungCostAddit)
                {
                    var selectedReinigung2Addit = model.Reinigung2PriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedReinigung2Addit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // disposal
                var entsorgungAddit = (from op in db.Offer_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                  where op.OfferId == offerId && par.ServiceType == "disposal"
                                  select op).ToList();

                // remove all before inserted disposal addit
                if (entsorgungAddit.Count > 0)
                {
                    db.Offer_PriceAddits.RemoveRange(entsorgungAddit);
                    db.SaveChanges();
                }
                // insert all selected disposal addit
                if (model.Offer.EntsorgungActive && model.Offer.EntsorgungCostAddit)
                {
                    var selectedEntsorgungAddit = model.EntsorgungPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedEntsorgungAddit.ForEach(x => db.Offer_PriceAddits.Add(new Offer_PriceAddit() { OfferId = offerId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }

            }
            return true;
        }

        /// <summary>Add selected DefinedPackMaterial. This packmaterials are defined from shop and will only be selected by dropdown</summary>
        /// <param name="offerId"></param>
        /// <param name="packMaterials"></param>
        /// <returns></returns>
        public bool SetDefinedPackMaterials(int offerId, List<Offer_DefinedPackMaterial> packMaterials)
        {
            using (var db = new ApplicationDbContext())
            {
                var existsPackMaterials = db.Offer_DefinedPackMaterials.Where(x => x.OfferId == offerId).ToList();
                if (existsPackMaterials != null && existsPackMaterials.Count > 0)
                {
                    db.Offer_DefinedPackMaterials.RemoveRange(existsPackMaterials);
                    db.SaveChanges();
                }
                if (packMaterials != null && packMaterials.Count > 0)
                {
                    var dataContaining = packMaterials.Where(x => x.DefinedPackMaterialId > 0 && x.CountNumber > 0).ToList();
                    dataContaining.ForEach(x => db.Offer_DefinedPackMaterials.Add(new Offer_DefinedPackMaterial()
                    {
                        OfferId = offerId,
                        DefinedPackMaterialId = x.DefinedPackMaterialId,
                        RentBuy = x.RentBuy,
                        PiecePrice = x.PiecePrice,
                        CountNumber = x.CountNumber,
                        EndPrice = x.EndPrice
                    }));
                    db.SaveChanges();
                }
            }
            return true;
        }

        /// <summary>Add selected freetext PackMaterial. This packmaterials are free textfields that can be set free text by user in textfield</summary>
        /// <param name="offerId"></param>
        /// <param name="packMaterials"></param>
        /// <returns></returns>
        public bool SetPackMaterials(int offerId, List<OfferPackMaterial> packMaterials)
        {
            using (var db = new ApplicationDbContext())
            {
                var existsPackMaterials = db.OfferPackMaterials.Where(x => x.OfferId == offerId).ToList();
                if (existsPackMaterials != null && existsPackMaterials.Count > 0)
                {
                    db.OfferPackMaterials.RemoveRange(existsPackMaterials);
                    db.SaveChanges();
                }
                if (packMaterials != null && packMaterials.Count > 0)
                {
                    var dataContaining = packMaterials.Where(x => !string.IsNullOrEmpty(x.Text) && x.CountNumber > 0).ToList();
                    dataContaining.ForEach(x => db.OfferPackMaterials.Add(new OfferPackMaterial()
                    {
                        OfferId = offerId,
                        Text = x.Text,
                        MieteKauf = x.MieteKauf,
                        PricePerPiece = x.PricePerPiece,
                        CountNumber = x.CountNumber,
                        EndPrice = x.EndPrice
                    }));
                    db.SaveChanges();
                }
            }
            return true;
        }

        public bool UpdateOfferState(Offer offer, Offer.OfferState newOfferState, string modifyUserId = null)
        {
            offer.OfferStateId = newOfferState;
            if (newOfferState == Offer.OfferState.confirmed)
            {
                offer.AcceptDate = DateTime.Now;
            }
            else if (newOfferState == Offer.OfferState.Appointment_created)
            {
                offer.ReleaseDate = DateTime.Now;
                offer.ReleaseUserId = getUserId(modifyUserId);
            }
            EditOffer(offer, modifyUserId);
            return true;
        }

        public string GetCryptedOfferCode(Offer offer)
        {
            string defineVal = string.Format("{0}|{1}|{2}|{3}", offer.OfferId, offer.OfferInnerId, offer.CustomerId, offer.AccountId);
            return CRM_Umzugsfirma.Helpers.CryptorEngine.Encrypt(defineVal);
        }

        public Offer GetOfferByCryptedOfferCode(string decryptedString)
        {
            string decryptedVal = CRM_Umzugsfirma.Helpers.CryptorEngine.Decrypt(decryptedString);
            string[] helper = decryptedVal.Split('|');
            if (helper.Length == 4)
            {
                int offerId;
                int offerInnerId;
                int customerId;
                int accountId;
                int.TryParse(helper[0], out offerId);
                int.TryParse(helper[1], out offerInnerId);
                int.TryParse(helper[2], out customerId);
                int.TryParse(helper[3], out accountId);
                if (offerId > 0 && offerInnerId > 0 && customerId > 0 && accountId > 0)
                {
                    Offer offer = GetOfferById(offerId);
                    if (offer == null || offer.OfferInnerId != offerInnerId || offer.CustomerId != customerId || offer.AccountId != accountId)
                    {
                        return null;
                    }
                    return offer;
                }
            }
            return null;
        }

        public string GetTextAdrOutTitle(Offer offer)
        {
            if (offer.UmzugActive)
            {
                return "From";
            }
            else if (offer.ReinigungActive || offer.Reinigung2Active)
            {
                return "cleaning Address";
            }
            else if (offer.TransportActive)
            {
                return "Beladeadresse";
            }
            else
            {
                return "Hauptadresse";
            }
        }

        public string GetTextAdrInTitle(Offer offer)
        {
            if (offer.UmzugActive)
            {
                return "To";
            }
            else if (offer.TransportActive)
            {
                return "Unloading Address";
            }
            else
            {
                return "To";
            }
        }

        private Offer cleanDependencySubtables(Offer offer)
        {
            if (!offer.ReinigungActive)
            {
                var offerCleaning1 = offer.OfferCleanings.FirstOrDefault(s => s.SectorId == 1);
                if (offerCleaning1 != null) offer.OfferCleanings.Remove(offerCleaning1);
            }
            if (!offer.Reinigung2Active)
            {
                var offerCleaning2 = offer.OfferCleanings.FirstOrDefault(s => s.SectorId == 2);
                if (offerCleaning2 != null) offer.OfferCleanings.Remove(offerCleaning2);
            }
            if (!offer.TransportActive)
            {
                var offerTransport = offer.OfferTransports.FirstOrDefault(s => s.SectorId == 1);
                if (offerTransport != null) offer.OfferTransports.Remove(offerTransport);
            }

            return offer;
        }

        private string getUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }
    }
}