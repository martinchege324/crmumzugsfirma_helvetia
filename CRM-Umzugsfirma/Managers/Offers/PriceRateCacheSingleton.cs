﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.Managers.Offers
{
    public sealed class PriceRateCacheSingleton
    {
        private static volatile PriceRateCacheSingleton _instance;
        private static object syncRoot = new Object();

        private Dictionary<int, Dictionary<string, List<  PriceRate>>> _priceRatesChache;
        private Dictionary<int, Dictionary<string, List<PriceAdditRate>>> _priceAdditRatesChache;

        private PriceRateCacheSingleton()
        {
            _priceRatesChache = new Dictionary<int, Dictionary<string, List<  PriceRate>>>();
            _priceAdditRatesChache = new Dictionary<int, Dictionary<string, List<PriceAdditRate>>>();

            // load client email-settings from db
            using (var dbContext = new CRM_Umzugsfirma.DB.ApplicationDbContext())
            {
                // load pricerates from db
                List<  PriceRate> allPriceRates = dbContext.PriceRates.OrderBy(p => p.AccountId).ThenBy(p => p.ServiceType).ThenBy(p => p.Descr).ToList();

                int lastAccountId = 0;
                Dictionary<string, List<  PriceRate>> tempAccountData = null;
                foreach (  PriceRate pr in allPriceRates)
                {
                    if (lastAccountId != pr.AccountId)
                    {
                        if (tempAccountData != null && tempAccountData.Count > 0)
                        {
                            _priceRatesChache.Add(lastAccountId, tempAccountData);
                        }
                        tempAccountData = new Dictionary<string, List<  PriceRate>>();
                        lastAccountId = pr.AccountId;
                    }
                    if (!tempAccountData.ContainsKey(pr.ServiceType))
                    {
                        tempAccountData.Add(pr.ServiceType, new List<  PriceRate>());
                    }
                    tempAccountData[pr.ServiceType].Add(pr);
                }
                // put the last content
                if (tempAccountData != null && tempAccountData.Count > 0)
                {
                    _priceRatesChache.Add(lastAccountId, tempAccountData);
                }

                // load priceadditrates from db
                List<PriceAdditRate> allPriceAdditRates = dbContext.PriceAdditRates.OrderBy(p => p.AccountId).ThenBy(p => p.ServiceType).ThenByDescending(p => p.MetaTag).ThenBy(p => p.Descr).ToList();

                lastAccountId = 0;
                Dictionary<string, List<PriceAdditRate>> tempAccountAdditData = null;
                foreach (PriceAdditRate pr in allPriceAdditRates)
                {
                    if (lastAccountId != pr.AccountId)
                    {
                        if (tempAccountAdditData != null && tempAccountAdditData.Count > 0)
                        {
                            _priceAdditRatesChache.Add(lastAccountId, tempAccountAdditData);
                        }
                        tempAccountAdditData = new Dictionary<string, List<PriceAdditRate>>();
                        lastAccountId = pr.AccountId;
                    }
                    if (!tempAccountAdditData.ContainsKey(pr.ServiceType))
                    {
                        tempAccountAdditData.Add(pr.ServiceType, new List<PriceAdditRate>());
                    }
                    tempAccountAdditData[pr.ServiceType].Add(pr);
                }
                // put the last content
                if (tempAccountAdditData != null && tempAccountAdditData.Count > 0)
                {
                    _priceAdditRatesChache.Add(lastAccountId, tempAccountAdditData);
                }
            }

        }

        public static PriceRateCacheSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new PriceRateCacheSingleton();
                        }
                    }
                }

                return _instance;
            }
        }

        public Dictionary<string, List<  PriceRate>> GetPriceRate(int accountId)
        {
            if (!_priceRatesChache.ContainsKey(accountId))
            {
                return null;
            }

            return _priceRatesChache[accountId];
        }

        public Dictionary<string, List<PriceAdditRate>> GetPriceAdditRate(int accountId)
        {
            if (!_priceAdditRatesChache.ContainsKey(accountId))
            {
                return null;
            }

            return _priceAdditRatesChache[accountId];
        }
    }
}