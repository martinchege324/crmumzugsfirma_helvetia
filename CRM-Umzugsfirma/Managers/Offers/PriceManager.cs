﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CRM_Umzugsfirma.Managers.Offers
{
    public class PriceManager
    {
        public List<  PriceRate> GetPriceRatesByServiceType(int accountId, string serviceArea)
        {
            Dictionary<string, List<  PriceRate>> priceRates = PriceRateCacheSingleton.Instance.GetPriceRate(accountId);
            if ((priceRates == null) || (!priceRates.ContainsKey(serviceArea))) return null;

            return priceRates[serviceArea];
        }

        public List<PriceAdditRate> GetPriceAdditRatesByServiceType(int accountId, string serviceArea)
        {
            Dictionary<string, List<PriceAdditRate>> priceAdditRates = PriceRateCacheSingleton.Instance.GetPriceAdditRate(accountId);
            if ((priceAdditRates == null) || (!priceAdditRates.ContainsKey(serviceArea))) return null;

            return priceAdditRates[serviceArea];
        }

        public   PriceRate GetPriceRateById(int accountId, string serviceArea, int id)
        {
            List<  PriceRate> priceRates = GetPriceRatesByServiceType(accountId, serviceArea);
            if (priceRates == null) return null;

            return priceRates.Find(p => p.PriceRateId == id);
        }

        public PriceAdditRate GetPriceAdditRateById(int accountId, string serviceArea, int id)
        {
            List<PriceAdditRate> priceAdditRates = GetPriceAdditRatesByServiceType(accountId, serviceArea);
            if (priceAdditRates == null) return null;

            return priceAdditRates.Find(p => p.PriceAdditRateId == id);
        }

    }
}