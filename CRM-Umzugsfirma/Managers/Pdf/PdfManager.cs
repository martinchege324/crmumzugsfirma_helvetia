﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Managers.Offers;
using Rotativa;
using Rotativa.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CRM_Umzugsfirma.Managers.Pdf
{
    public class PdfManager
    {
        public ActionAsPdf GetForOfferAsAction(Offer offer, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10 --footer-spacing 5 --footer-right \"Page [page] / [topage]  \"",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = offer.AccountId })));

            OfferManager offerManager = new OfferManager();
            return new ActionAsPdf("Offer", new { id = offerManager.GetCryptedOfferCode(offer) })
            {
                FileName = fileName != null ? fileName : null,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 10, 27, 10),
                CustomSwitches = customSwitches
            };
        }

        public UrlAsPdf GetForOfferAsUrl(Offer offer, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10 --footer-spacing 5 --footer-right \"Page [page] / [topage]  \"",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = offer.AccountId })));

            OfferManager offerManager = new OfferManager();
            string url = Helpers.Helpers.ContentFullPath(u.Action("Offer", "Print", new { area = "", id = offerManager.GetCryptedOfferCode(offer) }));
            return new UrlAsPdf(url)
            {
                FileName = fileName != null ? fileName : null,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 10, 27, 10),
                CustomSwitches = customSwitches
            };
        }

        public ActionAsPdf GetForReceiptAsAction(Receipt receipt, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = receipt.AccountId })));

            ReceiptManager receiptManager = new ReceiptManager();
            return new ActionAsPdf("Receipt", new { id = receiptManager.GetCryptedReceiptCode(receipt) })
            {
                FileName = fileName,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 20, 20, 20),
                CustomSwitches = customSwitches
            };
        }

        public UrlAsPdf GetForReceiptAsUrl(Receipt receipt, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10 --footer-right \"Page [page] / [topage]  \"",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = receipt.AccountId })));

            ReceiptManager receiptManager = new ReceiptManager();
            string url = Helpers.Helpers.ContentFullPath(u.Action("Receipt", "Print", new { area = "", id = receiptManager.GetCryptedReceiptCode(receipt) }));
            return new UrlAsPdf(url)
            {
                FileName = fileName != null ? fileName : null,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 10, 20, 10),
                CustomSwitches = customSwitches
            };
        }

        public ActionAsPdf GetForBillAsAction(Bill bill, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10 --footer-right \"Page [page] / [topage]  \"",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = bill.AccountId })));

            BillManager billManager = new BillManager();
            return new ActionAsPdf("Bill", new { id = billManager.GetCryptedBillCode(bill) })
            {
                FileName = fileName != null ? fileName : null,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 10, 20, 10),
                CustomSwitches = customSwitches
            };
        }

        public UrlAsPdf GetForBillAsUrl(Bill bill, RequestContext rc, string fileName = null)
        {
            UrlHelper u = new UrlHelper(rc);
            string customSwitches = string.Format("--print-media-type --footer-html \"{0}\" --footer-font-size 10 --footer-right \"Page [page] / [topage]  \"",
                Helpers.Helpers.ContentFullPath(u.Action("Footer", "Print", new { area = "", id = bill.AccountId })));

            BillManager billManager = new BillManager();
            string url = Helpers.Helpers.ContentFullPath(u.Action("Bill", "Print", new { area = "", id = billManager.GetCryptedBillCode(bill) }));
            return new UrlAsPdf(url)
            {
                FileName = fileName != null ? fileName : null,
                PageSize = Size.A4,
                PageOrientation = Orientation.Portrait,
                PageHeight = 297,
                PageWidth = 210,
                PageMargins = new Margins(10, 10, 20, 10),
                CustomSwitches = customSwitches
            };
        }
    }
}