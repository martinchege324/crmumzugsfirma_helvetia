﻿using CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class EmailMarketingManager
    {
        public EmailMarketing Get(int id, bool checkAccountIdFromSession = false, int checkAccountId = 0)
        {
            if ((checkAccountIdFromSession) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                checkAccountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }

            using (var db = new ApplicationDbContext())
            {
                EmailMarketing emailMarketing = db.EmailMarketings.Find(id);
                if ((checkAccountId > 0) && (emailMarketing.AccountId != checkAccountId))
                {
                    return null;
                }
                return emailMarketing;
            }
        }

        public EmailMarketing GetByAccessor(int accountId, string accessor)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.EmailMarketings.Include("EmailMarketingAttachments").FirstOrDefault(x => x.AccountId == accountId && x.Accessor == accessor);
            }
        }

        public List<EmailMarketing> GetAll(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.EmailMarketings.Where(x => x.AccountId == accountId).ToList();
            }
        }

        #region Customer EmailMarketing

        public List<EmailMarketingCustomer> GetAllEmailMarketingByCustomerId(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.EmailMarketingCustomers.Include("EmailMarketing").Where(x => x.CustomerId == customerId).ToList();
            }
        }

        public EmailMarketingCustomer GetEmailMarketingCustomer(int emailMarketingCustomerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.EmailMarketingCustomers.Include("EmailMarketing").FirstOrDefault(x => x.EmailMarketingCustomerId == emailMarketingCustomerId);
            }
        }

        public int AddEmailMarketingCustomer(EmailMarketingCustomer emailMarketingCustomer)
        {
            emailMarketingCustomer.CreateDate = DateTime.Now;

            using (var db = new ApplicationDbContext())
            {
                db.EmailMarketingCustomers.Add(emailMarketingCustomer);
                db.SaveChanges();
            }

            return emailMarketingCustomer.EmailMarketingCustomerId;
        }

        #endregion

        public int Edit(EmailMarketing emailMarketing)
        {
            emailMarketing.ModifyDate = DateTime.Now;

            using (var db = new ApplicationDbContext())
            {
                db.Entry(emailMarketing).State = EntityState.Modified;
                db.SaveChanges();
            }

            return emailMarketing.EmailMarketingId;
        }

    }
}