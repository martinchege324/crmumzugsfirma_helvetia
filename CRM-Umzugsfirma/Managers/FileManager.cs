﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class FileManager
    {

        public Stream GetStreamOfFile(string absoluteFileName)
        {
            if (string.IsNullOrEmpty(absoluteFileName)) return null;
            if (!File.Exists(absoluteFileName)) return null;

            return File.OpenRead(absoluteFileName);
        }

        #region Path

        /// <summary>Get Absolute Path of Application</summary>
        /// <param name="relativeFileName">With "~/" it is absolute path from application path. With "xxx/" its relative path from current file (caller method). Let it empty for current filepath, or put only "~/" for appliation path.</param>
        /// <returns></returns>
        public string GetAbsoluteFilePath(string relativeFileName)
        {
            string filePath = null;
            if (HttpContext.Current != null && HttpContext.Current.Server != null)
            {
                filePath = HttpContext.Current.Server.MapPath(relativeFileName);
            }

            return filePath;
        }

        public string GetPathMailAttachments(int id, string filename)
        {
            return Path.Combine(string.Format("~/Content/accountdata/{0}/mail/attachments/", id), filename);
        }

        #endregion
    }
}