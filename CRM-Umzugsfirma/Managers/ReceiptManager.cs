﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebGrease.Css.Extensions;

namespace CRM_Umzugsfirma.Managers
{
    public class ReceiptManager
    {
        public List<Receipt> GetReceiptsByCustomer(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Receipts.Include("ReceiptUmzugs").Include("ReceiptReinigungs").AsNoTracking().Where(s => s.CustomerId == customerId).OrderBy(n => n.CreateDate).ToList();
            }
        }

        public Receipt GetReceiptById(Guid receiptId, bool checkAccountIdFromSession = false, int checkAccountId = 0)
        {
            if ((checkAccountIdFromSession) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                checkAccountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }
            
            using (var db = new ApplicationDbContext())
            {
                Receipt receipt = db.Receipts.Include("ReceiptUmzugs").Include("ReceiptReinigungs").Include("Customer").FirstOrDefault(s => s.ReceiptId == receiptId);
                if ((receipt == null) || ((checkAccountId > 0) && (receipt.AccountId != checkAccountId)))
                {
                    return null;
                }
                return receipt;
            }
        }

        /// <summary>Get next possible innerreceipt id of account. smallest count is 1000 (as defined)</summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public int GetNextPossibleReceiptInnerId(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                int nextId = (1 + db.Receipts.AsNoTracking().Where(c => c.AccountId == accountId).OrderByDescending(o => o.ReceiptInnerId).Select(s => s.ReceiptInnerId).FirstOrDefault());
                if (nextId < 1000) nextId = 1000;

                return nextId;
            }
        }

        /// <summary>Get next possible innerreceipt id of account. smallest count is 1000 (as defined)</summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public int GetNextPossibleReceiptInnerSubId(int accountId, int offerId)
        {
            using (var db = new ApplicationDbContext())
            {
                int? nextId = (1 + db.Receipts.AsNoTracking().Where(c => c.AccountId == accountId && c.OfferId == offerId).OrderByDescending(o => o.ReceiptInnerSubId).Select(s => s.ReceiptInnerSubId).FirstOrDefault());

                return nextId ?? 1;
            }
        }

        public int GetCountOpenReceipt(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Receipts.AsNoTracking().Count(s => (s.AccountId == accountId && s.ReceiptStateId == Receipt.ReceiptState.Open && (!s.JobDateTime.HasValue || s.JobDateTime.Value.CompareTo(DateTime.Now) < 0)));
            }
        }

        public Guid CreateReceipt(Receipt receipt, Customer customer, Offer offer, string createUserId = null)
        {
            // set defaults
            receipt.ReceiptId = Guid.NewGuid();
            receipt.CreateDate = DateTime.Now;
            receipt.CreateUserId = GetUserId(createUserId);
            receipt.AccountId = customer.AccountId;
            receipt.CustomerId = customer.CustomerId;
            receipt.OfferId = offer.OfferId;
            receipt.ReceiptStateId = Receipt.ReceiptState.Open;
            receipt.ReceiptInnerId = offer.OfferInnerId;// GetNextPossibleInnerReceiptId(customer.AccountId);
            receipt.ReceiptInnerSubId = GetNextPossibleReceiptInnerSubId(customer.AccountId, offer.OfferId);

            using (var db = new ApplicationDbContext())
            {
                db.Receipts.Add(receipt);
                db.SaveChanges();
            }

            return receipt.ReceiptId;
        }

        public Guid EditReceipt(Receipt receipt, string modifyUserId = null, bool changeDate = true)
        {
            receipt.ModifyDate = DateTime.Now;
            modifyUserId = GetUserId(modifyUserId);
            if (!string.IsNullOrEmpty(modifyUserId)) receipt.ModifyUserId = modifyUserId;

            using (var db = new ApplicationDbContext())
            {
                db.Entry(receipt).State = EntityState.Modified;

                // also save ReceiptUmzug/ReceiptReinigung
                switch (receipt.ReceiptTypeId)
                {
                    case Receipt.ReceiptType.Move:
                    {
                        receipt.ReceiptUmzugs.ForEach(s => db.Entry(s).State = EntityState.Modified);
                        break;
                    }
                    case Receipt.ReceiptType.cleaning:
                    {
                        receipt.ReceiptReinigungs.ForEach(s => db.Entry(s).State = EntityState.Modified);
                        break;
                    }
                }

                db.SaveChanges();
            }

            return receipt.ReceiptId;
        }

        public bool UpdateReceiptState(Receipt receipt, Receipt.ReceiptState newReceiptState, string modifyUserId = null)
        {
            receipt.ReceiptStateId = newReceiptState;
            EditReceipt(receipt, modifyUserId, false);
            return true;
        }

        public bool CloseReceipt(Receipt receipt, string modifyUserId = null)
        {
            // set closed date
            receipt.ClosedDate = DateTime.Now;
            receipt.ClosedUserId = GetUserId(modifyUserId);
            // close receipt (change state)
            return UpdateReceiptState(receipt, Receipt.ReceiptState.Completed, modifyUserId);
        }

        public bool DeleteReceipt(Guid receiptId)
        {
            using (var db = new ApplicationDbContext())
            {
                Receipt receipt = db.Receipts.Find(receiptId);
                if (receipt == null) return false;

                db.Receipts.Remove(receipt);
                db.SaveChanges();
            }
            return true;
        }

        public bool DeleteReceiptByOfferId(int offerId)
        {
            using (var db = new ApplicationDbContext())
            {
                var receipts = db.Receipts.Where(s => s.OfferId == offerId).ToList();
                if (receipts == null || receipts.Count <= 0) return false;

                db.Receipts.RemoveRange(receipts);
                db.SaveChanges();
            }
            return true;
        }

        public Receipt GetRandomReceiptByAccount(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Receipts.FirstOrDefault(a => a.AccountId == accountId);
            }
        }

        public string GetCryptedReceiptCode(Receipt receipt)
        {
            var defineVal = $"{receipt.ReceiptId}|{receipt.ReceiptInnerId}|{receipt.OfferId}|{receipt.CustomerId}|{receipt.AccountId}";
            return Helpers.CryptorEngine.Encrypt(defineVal);
        }

        public Receipt GetReceiptByCryptedReceiptCode(string decryptedString)
        {
            string decryptedVal = Helpers.CryptorEngine.Decrypt(decryptedString);
            string[] helper = decryptedVal.Split('|');
            if (helper.Length != 5) return null;
            Guid receiptId;
            int receiptInnerId;
            int offerId;
            int customerId;
            int accountId;
            Guid.TryParse(helper[0], out receiptId);
            int.TryParse(helper[1], out receiptInnerId);
            int.TryParse(helper[2], out offerId);
            int.TryParse(helper[3], out customerId);
            int.TryParse(helper[4], out accountId);
            if (receiptId != Guid.Empty && receiptInnerId > 0 && customerId > 0 && accountId > 0)
            {
                Receipt receipt = GetReceiptById(receiptId);
                if (receipt == null || receipt.ReceiptInnerId != receiptInnerId || receipt.CustomerId != customerId || receipt.AccountId != accountId)
                {
                    return null;
                }
                return receipt;
            }
            return null;
        }

        public string GetUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }
    }
}