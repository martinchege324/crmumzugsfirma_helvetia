﻿using System;
using System.Web;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;

namespace CRM_Umzugsfirma.Managers
{
    public class LogManager
    {
        public void LogAnalyse(string message)
        {
            Log(LogType.Analyse, message);
        }

        public void LogInfo(string message)
        {
            Log(LogType.Info, message);
        }

        public void LogError(Exception ex)
        {
            Log(LogType.Error, ex.Message, ex.StackTrace);
        }

        public void Log(LogType logType, string message, string stacktrace = null)
        {
            try
            {
                var log = new Log
                {
                    CreateDate = DateTime.Now,
                    LogTypeId = logType,
                    Message = message,
                    UserId = getUserId()
                };
                if (!string.IsNullOrEmpty(stacktrace))
                {
                    log.StackTrace = stacktrace;
                }
                log = EnrichData(log);

                using (var db = new ApplicationDbContext())
                {
                    db.Logs.Add(log);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {

            }
        }

        #region Private Methods
        private Log EnrichData(Log log)
        {
            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                log.RequestUrl = HttpContext.Current.Request.RawUrl;
                log.RequestMethod = HttpContext.Current.Request.HttpMethod;
            }

            return log;
        }

        private string getUserId()
        {
            if ((HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                return ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return null;
        }
        #endregion
    }
}