﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Policy;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using CRM_Umzugsfirma.Managers.Offers;

namespace CRM_Umzugsfirma.Managers.Email
{
    public class EmailManager
    {
        public EmailType GetEmailTypeByAccountId(int accountId, string area)
        {
            return EmailCacheSingleton.Instance.GetEmailType(accountId).Where(e => e.Name.Equals(area)).SingleOrDefault();
        }

        public List<EmailAttachment> GetEmailAttachmentsByAccountId(int accountId)
        {
            return EmailCacheSingleton.Instance.GetEmailAttachment(accountId).ToList();
        }

        public List<Attachment> GetAttachmentofModel(IEnumerable<string> attachmentNames, int accountId, int innerAccountId)
        {
            if (attachmentNames.Count() > 0)
            {
                List<Attachment> attachments = new List<Attachment>();
                EmailManager emailManager = new EmailManager();
                List<EmailAttachment> emailAttachments = emailManager.GetEmailAttachmentsByAccountId(accountId);
                FileManager fileManager = new FileManager();

                foreach (string attachmentName in attachmentNames)
                {
                    EmailAttachment attachmentModel = emailAttachments.Where(e => e.Name.ToLower() == attachmentName.ToLower()).SingleOrDefault();
                    if (attachmentModel == null) continue;

                    Stream fileStream = fileManager.GetStreamOfFile(fileManager.GetAbsoluteFilePath(fileManager.GetPathMailAttachments(innerAccountId, attachmentModel.FileName)));
                    if (fileStream != null)
                    {
                        string attachmentFileName = attachmentModel.Name;
                        if (attachmentModel.FileName.IndexOf('.') > 0)
                        {
                            attachmentFileName = string.Format("{0}{1}", attachmentFileName, attachmentModel.FileName.Substring(attachmentModel.FileName.LastIndexOf('.')));
                        }
                        attachments.Add(new Attachment(fileStream, attachmentFileName, attachmentModel.MimeType));
                    }
                }
                return attachments;
            }
            return null;
        }

        #region Offer

        public string ReplacementsForOfferMail(string body, Offer offer, RequestContext rc)
        {
            const string acceptLinkKeyname = "##SystemKey-NICHT-ENTFERNEN-AcceptLink##";
            if (!body.Contains(acceptLinkKeyname)) return body;

            var code = HttpUtility.UrlEncode((new OfferManager()).GetCryptedOfferCode(offer));
            var u = new UrlHelper(rc);
            return body.Replace(acceptLinkKeyname, Helpers.Helpers.ContentFullPath(u.Action("A", "AcceptOffer", new { area = "", id = code })));
        }

        #endregion


    }
}