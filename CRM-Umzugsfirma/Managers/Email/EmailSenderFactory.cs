﻿using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Managers.Email
{
    public class EmailSenderFactory
    {
        
        public EmailSender CreateEmailSender(string key)
        {
            EmailConfigurationModel emailConfiguration = EmailCacheSingleton.Instance.GetEmailConfiguration(key);

            if (emailConfiguration == null)
            {
                /*
                // security, that in bad situation main email of crm-umzugsfirma should be taken. -- Then delete throw at the end of this section
                emailConfiguration = EmailCacheSingleton.Instance.GetEmailConfiguration("main");
                if (emailConfiguration == null)
                {
                    throw new Exception(string.Format("The key '{0}' doesn't exists in this object.", key));
                }*/
                
                throw new Exception(string.Format("The key '{0}' doesn't exists in this object.", key));
            }

            return new EmailSender(emailConfiguration);
        }

    }
}