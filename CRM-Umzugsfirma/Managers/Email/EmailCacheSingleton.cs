﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.Managers.Email
{
    public sealed class EmailCacheSingleton
    {
        private static volatile EmailCacheSingleton _instance;
        private static object syncRoot = new Object();

        private Dictionary<string, EmailConfigurationModel> emailCache;
        private Dictionary<int, List<EmailType>> emailTypeCache;
        private Dictionary<int, List<EmailAttachment>> emailAttachmentCache;

        private EmailCacheSingleton()
        {
            emailCache = new Dictionary<string, EmailConfigurationModel>();
            emailTypeCache = new Dictionary<int, List<EmailType>>();
            emailAttachmentCache = new Dictionary<int, List<EmailAttachment>>();

            // add main email-setting of crm client
            EmailConfigurationModel emailConfigurationModel = new EmailConfigurationModel()
            {
                SmtpHost = ConfigurationManager.AppSettings["EmailSmtpHost"],
                SmtpPort = int.Parse(ConfigurationManager.AppSettings["EmailSmtpPort"]),
                SmtpUseSSL = bool.Parse(ConfigurationManager.AppSettings["EmailSmtpSSL"]),
                SenderAddress = ConfigurationManager.AppSettings["EmailSenderAdress"],
                SenderPass = ConfigurationManager.AppSettings["EmailMainPass"],
                DisplayName = ConfigurationManager.AppSettings["EmailDisplayName"],
                ReplyAdress = ConfigurationManager.AppSettings["EmailReplyAdress"],
                BackupEmailAddress = ConfigurationManager.AppSettings["EmailBackupAddress"]
            };
            if (!string.IsNullOrEmpty(emailConfigurationModel.ReplyAdress))
            {
                emailConfigurationModel.ReplyAdress = emailConfigurationModel.ReplyAdress.Trim().Replace(",", ";");
            }
            emailCache.Add("main", emailConfigurationModel);

            // load client email-settings from db
            using (var dbContext = new CRM_Umzugsfirma.DB.ApplicationDbContext())
            {
                // load emailtypes from db
                List<EmailType> emailTypes = dbContext.EmailTypes.ToList();
                // load emailattachments from db
                List<EmailAttachment> emailAttachments = dbContext.EmailAttachments.ToList();
                // load accounts (emailconfiguration) from db
                List<Account> accounts = dbContext.Accounts.Where(a => a.IsActive).ToList<Account>();
                foreach (Account account in accounts)
                {
                    // emailconfiguration
                    if (emailCache.ContainsKey(account.AccountId.ToString())) continue;
                    emailCache.Add(account.AccountId.ToString(), new EmailConfigurationModel()
                    {
                        SmtpHost = account.AccountEmailConfig.Host,
                        SmtpPort = account.AccountEmailConfig.Port,
                        SmtpUseSSL = account.AccountEmailConfig.UseSSL,
                        SenderAddress = account.AccountEmailConfig.SenderAccount,
                        SenderPass = account.AccountEmailConfig.SenderAccountPass,
                        DisplayName = account.AccountEmailConfig.SenderDisplayName,
                        ReplyAdress = (!string.IsNullOrEmpty(account.AccountEmailConfig.ReplyAddress) ? account.AccountEmailConfig.ReplyAddress.Trim().Replace(",", ";") : null),
                        BackupEmailAddress = emailConfigurationModel.BackupEmailAddress,
                        SenderAddressSales = account.AccountEmailConfig.SenderAddressSales,
                        SenderAddressAccounting = account.AccountEmailConfig.SenderAddressAccounting
                    });

                    // emailtype
                    if (!emailTypeCache.ContainsKey(account.AccountId))
                    {
                        emailTypeCache.Add(account.AccountId, emailTypes.Where(e => e.AccountId == account.AccountId).ToList());
                    }

                    // emailattachment
                    if (!emailAttachmentCache.ContainsKey(account.AccountId))
                    {
                        emailAttachmentCache.Add(account.AccountId, emailAttachments.Where(e => e.AccountId == account.AccountId).OrderBy(e => e.SortOrder).ToList());
                    }
                }

            }

        }

        public static EmailCacheSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new EmailCacheSingleton();
                        }
                    }
                }

                return _instance;
            }
        }

        public EmailConfigurationModel GetEmailConfiguration(string key)
        {
            if (!emailCache.ContainsKey(key))
            {
                return null;
            }

            return emailCache[key];
        }

        public List<EmailType> GetEmailType(int key)
        {
            if (!emailTypeCache.ContainsKey(key))
            {
                return null;
            }

            return emailTypeCache[key];
        }

        public List<EmailAttachment> GetEmailAttachment(int key)
        {
            if (!emailAttachmentCache.ContainsKey(key))
            {
                return null;
            }

            return emailAttachmentCache[key];
        }

    }
}