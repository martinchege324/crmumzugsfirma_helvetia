﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using CRM_Umzugsfirma.Models;

namespace CRM_Umzugsfirma.Managers.Email
{
    public class EmailSender
    {
        private EmailConfigurationModel _emailConfiguration;

        public EmailSender(EmailConfigurationModel emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

        public bool SendEmail(string recipients, string subject, string body)
        {
            return SendEmail(_emailConfiguration.SenderAddress, recipients, subject, body);
        }

        public bool SendEmail(string recipients, string subject, string body, bool sentToBackup)
        {
            return SendEmail(_emailConfiguration.SenderAddress, recipients, subject, body, sentToBackup);
        }

        public bool SendEmail(string recipients, string subject, string body, List<Attachment> attachments)
        {
            return SendEmail(_emailConfiguration.SenderAddress, recipients, subject, body, attachments);
        }

        public bool SendEmail(string recipients, string subject, string body, List<Attachment> attachments, bool sentToBackup)
        {
            return SendEmail(_emailConfiguration.SenderAddress, recipients, subject, body, attachments, sentToBackup);
        }

        public bool SendEmail(string from, string recipients, string subject, string body)
        {
            return SendEmail(from, recipients, subject, body, null);
        }

        public bool SendEmail(string from, string recipients, string subject, string body, bool sentToBackup)
        {
            return SendEmail(from, recipients, subject, body, null, sentToBackup);
        }

        public bool SendEmail(string from, string recipients, string subject, string body, List<Attachment> attachments)
        {
            return SendEmail(from, recipients, subject, body, attachments, false);
        }

        public bool SendEmail(string from, string recipients, string subject, string body, List<Attachment> attachments, bool sentToBackup)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(from, _emailConfiguration.DisplayName);

            string[] recipientsArr = recipients.Replace(",", ";").Trim().Split(';');
            for (int i = 0; i < recipientsArr.Length; i++)
            {
                mail.To.Add(recipientsArr[i]);
            }
            if (!string.IsNullOrEmpty(_emailConfiguration.ReplyAdress))
            {
                string[] replyList = _emailConfiguration.ReplyAdress.Split(';');
                for (int i = 0; i < replyList.Length; i++)
                {
                    mail.ReplyToList.Add(new MailAddress(replyList[i], _emailConfiguration.DisplayName));
                }
            }

            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;

            if (attachments != null)
            {
                for (int i = 0; i < attachments.Count; i++)
                {
                    mail.Attachments.Add(attachments[i]);
                }
            }

            if (sentToBackup && !string.IsNullOrEmpty(_emailConfiguration.BackupEmailAddress))
            {
                mail.Bcc.Add(_emailConfiguration.BackupEmailAddress);
            }

            return send(mail);
        }

        public bool SendEmailFromSales(string recipients, string subject, string body, List<Attachment> attachments)
        {
            string fromAddress = !string.IsNullOrEmpty(_emailConfiguration.SenderAddressSales) ? _emailConfiguration.SenderAddressSales : _emailConfiguration.SenderAddress;
            return SendEmail(fromAddress, recipients, subject, body, attachments);
        }

        public bool SendEmailFromAccounting(string recipients, string subject, string body, List<Attachment> attachments)
        {
            string fromAddress = !string.IsNullOrEmpty(_emailConfiguration.SenderAddressAccounting) ? _emailConfiguration.SenderAddressAccounting : _emailConfiguration.SenderAddress;
            return SendEmail(fromAddress, recipients, subject, body, attachments);
        }

        private bool send(MailMessage mail)
        {
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = _emailConfiguration.SmtpHost;
            smtpClient.Port = _emailConfiguration.SmtpPort;
            smtpClient.EnableSsl = _emailConfiguration.SmtpUseSSL;
            smtpClient.Credentials = new System.Net.NetworkCredential(_emailConfiguration.SenderAddress, _emailConfiguration.SenderPass);
            smtpClient.Timeout = 60000;

            try
            {
                smtpClient.Send(mail);
                //await smtpClient.SendMailAsync(mail);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
    }
}