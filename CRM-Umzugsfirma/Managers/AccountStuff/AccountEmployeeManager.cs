﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Managers.AccountStuff
{
    public class AccountEmployeeManager
    {
        public List<AccountEmployee> GetAll(int accountId)
        {
            return AccountCacheSingleton.Instance.GetAccountEmployeeList(accountId);
        }

        public AccountEmployee GetById(int accountEmployeeId)
        {
            return AccountCacheSingleton.Instance.GetAllEmployeeList().FirstOrDefault(s => s.AccountEmployeeId == accountEmployeeId);
        }

        public List<AccountEmployee> GetAllCustomerConsultants(int accountId)
        {
            var allEmployees = GetAll(accountId);

            // if Role-Management is defined, then join with RoleManager-Data (which is also cached) and return only those with right role
            return allEmployees.Where(e => e.IsActive).ToList();
        }

        public List<SelectListItem> GetAllCustomerConsultantsAsSelectListItem(int accountId, bool pleaseSelectItem, string selectValue = null)
        {
            var selectListItems = new List<SelectListItem>();
            if (pleaseSelectItem)
            {
                selectListItems.Add(new SelectListItem() { Value = "", Text = "Please choose" });
            }

            var customerConsultants = GetAllCustomerConsultants(accountId);
            if (customerConsultants == null) return selectListItems;
            
            foreach (var customerConsultant in customerConsultants)
            {
                selectListItems.Add(new SelectListItem()
                {
                    Value = customerConsultant.AccountEmployeeId.ToString(),
                    Text = $"{customerConsultant.FirstName} {customerConsultant.LastName}",
                    Selected = (selectValue != null && customerConsultant.AccountEmployeeId.ToString().Equals(selectValue))
                });
            }
            return selectListItems;
        }

    }
}