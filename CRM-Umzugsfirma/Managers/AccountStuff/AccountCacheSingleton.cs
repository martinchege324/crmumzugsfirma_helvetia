﻿using System.Collections.Generic;
using System.Linq;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;

namespace CRM_Umzugsfirma.Managers.AccountStuff
{
    public sealed class AccountCacheSingleton
    {
        private static volatile AccountCacheSingleton _instance;
        private static readonly object syncRoot = new object();

        private readonly Dictionary<int, List<AccountEmployee>> _accountEmployeeCache;
        private readonly List<AccountEmployee> _allEmployeeCache;

        private AccountCacheSingleton()
        {
            _accountEmployeeCache = new Dictionary<int, List<AccountEmployee>>();
            _allEmployeeCache = new List<AccountEmployee>();

            // load data from db
            using (var dbContext = new ApplicationDbContext())
            {
                // load DefinedPackMaterials from db
                _allEmployeeCache = dbContext.AccountEmployees.OrderBy(p => p.AccountId).ThenBy(p => p.FirstName).ThenBy(p => p.LastName).ToList();

                // put per account a list of all data in dictionary
                var existAccountData = _allEmployeeCache.Select(s => s.AccountId).Distinct().ToList();
                existAccountData.ForEach(a => _accountEmployeeCache.Add(a, _allEmployeeCache.Where(s => s.AccountId == a).ToList()));
            }

        }

        public static AccountCacheSingleton Instance
        {
            get
            {
                if (_instance != null) return _instance;

                lock (syncRoot)
                {
                    if (_instance == null)
                    {
                        _instance = new AccountCacheSingleton();
                    }
                }

                return _instance;
            }
        }

        public List<AccountEmployee> GetAllEmployeeList()
        {
            return _allEmployeeCache ?? new List<AccountEmployee>();
        }

        public List<AccountEmployee> GetAccountEmployeeList(int accountId)
        {
            if (_accountEmployeeCache != null && _accountEmployeeCache.ContainsKey(accountId))
            {
                return _accountEmployeeCache[accountId];
            }

            return new List<AccountEmployee>();
        }

    }
}