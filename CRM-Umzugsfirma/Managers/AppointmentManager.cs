﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers.Calendar;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class AppointmentManager
    {
        public async Task<List<Appointment>> GetAppointmentsByCustomer(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return await db.Appointments.Include("AppointmentView").Include("AppointmentService").Include("AppointmentDelivery").Where(n => n.CustomerId == customerId).OrderBy(n => n.CreateDate).ToListAsync();
            }
        }

        public Appointment GetAppointmentById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Appointments.Include("AppointmentView").Include("AppointmentService").Include("AppointmentDelivery").FirstOrDefault(s => s.AppointmentId == id);
            }
        }

        public async Task<List<Appointment>> GetAppointmentsByCustomerOfferId(int customerId, int innerOfferId)
        {
            using (var db = new ApplicationDbContext())
            {
                return await db.Appointments.Include("AppointmentView").Include("AppointmentService").Include("AppointmentDelivery").Where(n => n.CustomerId == customerId && n.OfferInnerId == innerOfferId).OrderBy(n => n.CreateDate).ToListAsync();
            }
        }


        public bool CreateAppointment(Appointment appointment, int accountId = 0, string createUserId = null)
        {
            // set defaults
            appointment.AccountId = getAccountId(accountId);
            appointment.CreateDate = DateTime.Now;
            appointment.CreateUserId = getUserId(createUserId);

            // clean depencies if not really needed
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Sightseeing && appointment.AppointmentView != null) appointment.AppointmentView = null;
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Confirmation_of_the_order && appointment.AppointmentService != null) appointment.AppointmentService = null;
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Delivery && appointment.AppointmentDelivery != null) appointment.AppointmentDelivery = null;

            using (var db = new ApplicationDbContext())
            {
                db.Appointments.Add(appointment);
                db.SaveChanges();
            }
            return true;
        }

        public bool EditAppointment(Appointment appointment, string modifyUserId = null)
        {
            // set defaults
            appointment.ModifyDate = DateTime.Now;
            appointment.ModifyUserId = getUserId(modifyUserId);

            // clean depencies if not really needed
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Sightseeing && appointment.AppointmentView != null) appointment.AppointmentView = null;
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Confirmation_of_the_order && appointment.AppointmentService != null) appointment.AppointmentService = null;
            if (appointment.AppointmentTypeId != Appointment.AppointmentType.Delivery && appointment.AppointmentDelivery != null) appointment.AppointmentDelivery = null;

            using (var db = new ApplicationDbContext())
            {
                db.Entry(appointment).State = EntityState.Modified;

                switch (appointment.AppointmentTypeId)
                {
                    case Appointment.AppointmentType.Sightseeing:
                        db.Entry(appointment.AppointmentView).State = EntityState.Modified;
                        break;
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        db.Entry(appointment.AppointmentService).State = EntityState.Modified;
                        break;
                    case Appointment.AppointmentType.Delivery:
                        db.Entry(appointment.AppointmentDelivery).State = EntityState.Modified;
                        break;
                }

                db.SaveChanges();
            }
            return true;
        }

        public bool DeleteAppointment(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                Appointment appointment = db.Appointments.Find(id);
                if (appointment == null) return false;

                db.Appointments.Remove(appointment);
                db.SaveChanges();
            }
            return true;
        }

        public Appointment GetRandomAppointmentByAccount(int accountId, Appointment.AppointmentType type = Appointment.AppointmentType.Sightseeing)
        {
            using (var db = new ApplicationDbContext())
            {
                var appointments = db.Appointments.Where(a => a.AccountId == accountId && a.AppointmentTypeId == type);
                switch (type)
                {
                    case Appointment.AppointmentType.Confirmation_of_the_order:
                        appointments = appointments.Include("AppointmentService");
                        break;
                    case Appointment.AppointmentType.Delivery:
                        appointments = appointments.Include("AppointmentDelivery");
                        break;
                    case Appointment.AppointmentType.Sightseeing:
                    default:
                        appointments = appointments.Include("AppointmentView");
                        break;
                }
                return appointments.FirstOrDefault();
            }
        }

        public void CleanGoogleCalendar(Appointment appointment)
        {
            // delete calendar event
            GoogleCalendarManager googleCalendarManager = new GoogleCalendarManager(appointment.AccountId);
            if (!string.IsNullOrEmpty(appointment.AppointmentView?.GoogleEventViewId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentView.GoogleEventViewId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventUmzugId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventUmzugId2))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId2);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventUmzugId3))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventUmzugId3);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventPackId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventPackId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventPackOutId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventPackOutId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventReinigungId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventReinigungId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventReinigung2Id))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventReinigung2Id);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventEntsorgungId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventEntsorgungId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventLagerungId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventLagerungId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentService?.GoogleEventTransportId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentService.GoogleEventTransportId);
            }
            if (!string.IsNullOrEmpty(appointment.AppointmentDelivery?.GoogleEventDeliveryId))
            {
                googleCalendarManager.DeleteEvent(appointment.AppointmentDelivery.GoogleEventDeliveryId);
            }
        }



        #region Helpers

        private string getUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }

        private int getAccountId(int accountId)
        {
            if ((accountId <= 0) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                accountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }

            return accountId;
        }

        #endregion
    }
}