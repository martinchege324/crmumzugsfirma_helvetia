﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;

namespace CRM_Umzugsfirma.Managers.Calendar
{
    public class GoogleCalendarEventManager
    {
        private CalendarService _service;

        public GoogleCalendarEventManager(CalendarService service)
        {
            _service = service;
        }

        #region Delete

        /// <summary>
        /// Deletes an event
        /// Documentation:https://developers.google.com/google-apps/calendar/v3/reference/events/delete
        /// </summary>
        /// <param name="id">Calendar identifier.</param>
        /// <param name="eventid">Event identifier.</param>
        /// <returns>If successful, this method returns an empty response body. </returns>
        public string Delete(string id, string eventid)
        {
            try
            {
                return _service.Events.Delete(id, eventid).Execute();
            }
            catch (Exception)
            {
                return null;
            }
        }


        #endregion

        #region Get

        /// <summary>
        /// Returns an event.
        /// Documentation:https://developers.google.com/google-apps/calendar/v3/reference/events/get
        /// </summary>
        /// <param name="id">Calendar identifier.</param>
        /// <param name="eventid">Event identifier.</param>
        /// <returns>Events resorce: https://developers.google.com/google-apps/calendar/v3/reference/events#resource </returns>
        public Event Get(string id, string eventid)
        {
            try
            {
                return _service.Events.Get(id, eventid).Execute();
            }
            catch (Exception)
            {
                return null;
            }
        }


        #endregion

        #region Insert

        /// <summary>
        /// Adds an entry to the user's calendar list.  
        /// Documentation:https://developers.google.com/google-apps/calendar/v3/reference/calendarList/insert
        /// </summary>
        /// <param name="id">Calendar identifier.</param>
        /// <param name="body">an event</param>
        /// <returns>event resorce: https://developers.google.com/google-apps/calendar/v3/reference/events#resource </returns>
        public Event Insert(string id, Event body)
        {
            try
            {
                return _service.Events.Insert(body, id).Execute();
            }
            catch (Exception)
            {
                return null;
            }
        }


        #endregion


        #region List

        /// <summary>
        /// There are several query Parameters that are optional this will allow you to send the ones you want.
        /// </summary>
        public class OptionalValues
        {
            private Boolean showDeleted { get; set; }
            private int maxResults { get; set; }

            /// <summary>
            /// Whether to include deleted events (with status equals "cancelled") in the result. Cancelled instances of recurring events
            /// (but not the underlying recurring event) will still be included if showDeleted and singleEvents are both False.
            /// If showDeleted and singleEvents are both True, only single instances of deleted events (but not the underlying 
            /// recurring events) are returned. Optional. The default is False.
            /// Documentation: https://developers.google.com/google-apps/calendar/v3/reference/events/list
            /// </summary>            
            public Boolean ShowDeleted { get { return showDeleted; } set { showDeleted = value; } }

            /// <summary>
            /// Maximum number of entries returned on one result page. By default the value is 100 entries. The page size can never be larger than 2500 entries. Optional. 
            /// Documentation: https://developers.google.com/google-apps/calendar/v3/reference/events/list
            /// </summary>
            public int MaxResults { get { return maxResults; } set { maxResults = value; } }

            /// <summary>
            /// Constructor sets up the default values, for things that can't be null.
            /// </summary>
            public OptionalValues()
            {
                this.maxResults = 100;
                this.showDeleted = false;
            }
        }



        public Events List(string id, OptionalValues optionalValues)
        {

            var request = _service.Events.List(id);


            if (optionalValues == null)
            {
                request.MaxResults = 100;
            }
            else
            {
                request.MaxResults = optionalValues.MaxResults;
                request.ShowDeleted = optionalValues.ShowDeleted;
            }

            return ProcessResults(request);


        }


        // Just loops though getting all the rows.  
        private Events ProcessResults(EventsResource.ListRequest request)
        {
            try
            {
                Events result = request.Execute();
                List<Event> allRows = new List<Event>();

                //// Loop through until we arrive at an empty page
                while (result.Items != null)
                {
                    //Add the rows to the final list
                    allRows.AddRange(result.Items);

                    // We will know we are on the last page when the next page token is
                    // null.
                    // If this is the case, break.
                    if (result.NextPageToken == null)
                    {
                        break;
                    }
                    // Prepare the next page of results
                    request.PageToken = result.NextPageToken;

                    // Execute and process the next page request
                    result = request.Execute();

                }
                Events allData = result;
                allData.Items = (List<Event>)allRows;
                return allData;
            }
            catch (Exception)
            {
                return null;
            }
        }
        #endregion


        #region update

        /// <summary>
        /// Updates an event.
        /// Documentation:https://developers.google.com/google-apps/calendar/v3/reference/events/update
        /// </summary>
        /// <param name="id">Calendar identifier.</param>
        /// <param name="eventid">Event identifier.</param>
        /// <param name="body">Changes you want to make:  Use var body = DaimtoEventHelper.get(service,id);  
        ///                    to get body then change that and pass it to the method.</param>
        /// <returns>event resorce:https://developers.google.com/google-apps/calendar/v3/reference/events#resource  </returns>
        public Event Update(string id, string eventid, Event body)
        {

            try
            {
                return _service.Events.Update(body, id, eventid).Execute();
            }
            catch (Exception)
            {
                return null;
            }
        }


        #endregion

    }
}