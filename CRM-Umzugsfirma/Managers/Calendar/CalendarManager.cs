﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace CRM_Umzugsfirma.Managers.Calendar
{
    public class CalendarManager
    {
        public Attachment GetIcsCalendarAsAttachment(string subject, string location, DateTime startDate, DateTime endDate, string attachmentName)
        {
            string calendarContent = GetIcsCalendarAsString(subject, location, startDate, endDate);

            Attachment attachment = Attachment.CreateAttachmentFromString(calendarContent, attachmentName);
            attachment.ContentType = new System.Net.Mime.ContentType("text/calendar; method=REQUEST");
            return attachment;
        }

        public string GetIcsCalendarAsString(string subject, string location, DateTime startDate, DateTime endDate)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("BEGIN:VCALENDAR");
            sb.AppendLine("VERSION:2.0");
            sb.AppendLine("PRODID:-//Schedule a Meeting");
            sb.AppendLine("BEGIN:VEVENT");
            sb.AppendLine("DTSTART:" + startDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
            sb.AppendLine("DTEND:" + endDate.ToUniversalTime().ToString("yyyyMMdd\\THHmmss\\Z"));
            sb.AppendLine("SUMMARY:" + subject);
            sb.AppendLine("LOCATION:" + location);
            sb.AppendLine("END:VEVENT");
            sb.AppendLine("END:VCALENDAR");
            return sb.ToString();
        }

        public DateTime ParseFromString(string date, string time)
        {
            DateTime tempDate = DateTime.MinValue;
            if (DateTime.TryParse(date, out tempDate))
            {
                // add time
                if (!string.IsNullOrEmpty(time))
                {
                    time = time.Replace('.', ':').Replace(';', ':');
                    // check if time has minute
                    if (time.Contains(':'))
                    {
                        string[] s = time.Split(':');
                        int iHour = 0;
                        int.TryParse(s[0], out iHour);
                        if (iHour > 0 && iHour <= 24)
                        {
                            tempDate = tempDate.AddHours((double)iHour);
                        }
                        int iMinute = 0;
                        int.TryParse(s[1], out iMinute);
                        if (iMinute > 0 && iMinute <= 60)
                        {
                            tempDate = tempDate.AddMinutes((double)iMinute);
                        }
                    }
                    else
                    {
                        int iHour = 0;
                        int.TryParse(time, out iHour);
                        if (iHour > 0 && iHour <= 24)
                        {
                            tempDate = tempDate.AddHours((double)iHour);
                        }
                    }
                }
            }
            return tempDate;
        }

        public DateTime[] ParseFromString(string startDate, string startTime, string endDate, string endTime, int defaultHourOnEmptyEndDate = 1)
        {
            DateTime dtStartDate = ParseFromString(startDate, startTime);
            DateTime dtEndDate = DateTime.MinValue;
            if (string.IsNullOrEmpty(endDate) || string.IsNullOrEmpty(endTime))
            {
                if (dtStartDate != null)
                {
                    dtEndDate = dtStartDate.AddHours(defaultHourOnEmptyEndDate);
                }
            }
            else
            {
                dtEndDate = ParseFromString(endDate, endTime);
            }

            return new DateTime[2] { dtStartDate, dtEndDate };
        }

        public DateTime[] ParseFromString(string startDate, string startTime, int duration)
        {
            DateTime dtStartDate = ParseFromString(startDate, startTime);
            DateTime dtEndDate = DateTime.MinValue;
            if (dtStartDate != null)
            {
                dtEndDate = dtStartDate.AddHours(duration);
            }

            return new DateTime[2] { dtStartDate, dtEndDate };
        }

        public int AvgDuration(string duration)
        {
            int avgDuration = 1;
            if (string.IsNullOrEmpty(duration)) return avgDuration;

            if (duration.Contains("-"))
            {
                // if 7-8, then 8 will returned
                string maxTime = duration.Split('-')[1].Trim();
                if (int.TryParse(maxTime, out avgDuration))
                {
                    return avgDuration;
                }
                else
                {
                    // 7 - 8 Hours. loop if its something like 7 - 11 Hours
                    int helpInt = getNextPossibleNumber(maxTime);
                    if (helpInt > 0) return helpInt;

                    return avgDuration;
                }
            }
            else
            {
                int helpInt = getNextPossibleNumber(duration);
                if (helpInt > 0) return helpInt;

                return avgDuration;
            }
        }

        private int getNextPossibleNumber(string input)
        {
            if (string.IsNullOrEmpty(input)) return 0;

            int possibleNumber = 0;
            int helpInt = 0;
            for (int i = 1; i <= input.Length; i++)
            {
                if (int.TryParse(input.Substring(0, i), out helpInt))
                {
                    possibleNumber = helpInt;
                }
                else
                {
                    break;
                }
            }
            return possibleNumber;
        }
    }
}