﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System.Configuration;

namespace CRM_Umzugsfirma.Managers.Calendar
{
    public class GoogleCalendarManager
    {
        private string _accountGoogleAccount;
        private GoogleCalendarEventManager _eventManager;
        private LogManager _logManager;

        public GoogleCalendarManager(int accountId)
        {
            _accountGoogleAccount = GoogleCalendarCacheSingleton.Instance.GetCalendarEmailByAccountId(accountId);
            _eventManager = new GoogleCalendarEventManager(GoogleCalendarCacheSingleton.Instance.CalendarService);
            _logManager = new LogManager();
        }

        public Event InsertEvent(Event body)
        {
            if (string.IsNullOrEmpty(_accountGoogleAccount)) return null;

            var insertedEvent = _eventManager.Insert(_accountGoogleAccount, body);
            _logManager.LogAnalyse($"Google Event '{insertedEvent.Id}' is inserted with subject '{insertedEvent.Summary}' and location '{insertedEvent.Location}'");

            return insertedEvent;
        }

        public Event GetEvent(string eventId)
        {
            if (string.IsNullOrEmpty(_accountGoogleAccount)) return null;

            return _eventManager.Get(_accountGoogleAccount, eventId);
        }

        public string DeleteEvent(string eventId)
        {
            if (string.IsNullOrEmpty(_accountGoogleAccount)) return null;

            string returnMessage = _eventManager.Delete(_accountGoogleAccount, eventId);
            _logManager.LogAnalyse($"Google Event '{eventId}' is deleted: {returnMessage}");

            return returnMessage;
        }

        public Event UpdateEvent(string eventId, Event body)
        {
            if (string.IsNullOrEmpty(_accountGoogleAccount)) return null;

            return _eventManager.Update(_accountGoogleAccount, eventId, body);
        }

        public Event GetEventObject()
        {
            return new Event();
        }

        public Event GetEventObject(string startDate, string startTime, string endDate, string endTime, int defaultHourOnEmptyEndDate = 1)
        {
            Event ev = GetEventObject();
            CalendarManager calendarManager = new CalendarManager();
            DateTime[] calendarParsedDates = calendarManager.ParseFromString(startDate, startTime, endDate, endTime, defaultHourOnEmptyEndDate);

            // null if no startdate is set
            if (calendarParsedDates[0] == null || calendarParsedDates[0] == DateTime.MinValue) return null;
            ev.Start = new EventDateTime() { DateTime = calendarParsedDates[0], TimeZone = "Europe/Zurich" };
            ev.End = new EventDateTime() { DateTime = calendarParsedDates[1], TimeZone = "Europe/Zurich" };

            return ev;
        }

        public Event GetEventObject(string title, string location, string description, string startDate, string startTime, string endDate = null, string endTime = null, int defaultHourOnEmptyEndDate = 1)
        {
            Event ev = GetEventObject(startDate, startTime, endDate, endTime, defaultHourOnEmptyEndDate);
            ev.Summary = title;
            if (!string.IsNullOrEmpty(location)) ev.Location = location;
            if (!string.IsNullOrEmpty(description)) ev.Description = description;

            return ev;
        }

        public Event GetEventAllDayObject(string startDate, string duration = null)
        {
            DateTime tempDate = DateTime.MinValue;
            if (DateTime.TryParse(startDate, out tempDate))
            {
                Event ev = GetEventObject();
                ev.Start = new EventDateTime() { Date = tempDate.ToString("yyyy-MM-dd") };
                // if endDate is empty, take startdate. In both cases, at the end 1 day is added for google calendar (all-day event)
                DateTime dtEndDate = tempDate;
                CalendarManager calendarManager = new CalendarManager();
                if (!string.IsNullOrEmpty(duration))
                {
                    int durationNumber = calendarManager.AvgDuration(duration);
                    if (durationNumber > 0)
                    {
                        dtEndDate = dtEndDate.AddDays((durationNumber / 24));
                    }
                }

                ev.End = new EventDateTime() { Date = dtEndDate.AddDays(1).ToString("yyyy-MM-dd") };
                return ev;
            }
            return null;
        }

        public Event GetEventAllDayObject(string title, string location, string description, string startDate)
        {
            Event ev = GetEventAllDayObject(startDate);
            ev.Summary = title;
            if (!string.IsNullOrEmpty(location)) ev.Location = location;
            if (!string.IsNullOrEmpty(description)) ev.Description = description;

            return ev;
        }

        public Event GetEventAllDayObject(string title, string location, string description, string startDate, string duration)
        {
            Event ev = GetEventAllDayObject(startDate, duration);
            ev.Summary = title;
            if (!string.IsNullOrEmpty(location)) ev.Location = location;
            if (!string.IsNullOrEmpty(description)) ev.Description = description;

            return ev;
        }

    }
}