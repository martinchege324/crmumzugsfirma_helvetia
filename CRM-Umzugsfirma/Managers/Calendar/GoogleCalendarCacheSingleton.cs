﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System.Configuration;
using CRM_Umzugsfirma.Areas.Intranet.Models;

namespace CRM_Umzugsfirma.Managers.Calendar
{
    public sealed class GoogleCalendarCacheSingleton
    {
        private static volatile GoogleCalendarCacheSingleton _instance;
        private static object syncRoot = new Object();

        private bool _initialized = false;
        public CalendarService CalendarService
        {
            get;
            private set;
        }
        private Dictionary<int, string> calendarAccounts;


        private GoogleCalendarCacheSingleton()
        {
            // read service account and key
            CalendarService = AuthenticateServiceAccount();

            // read all accounts into cache (only accounts, that have already authorized their calendar with service account)
            GoogleCalendarCalendarListManager calendarListManager = new GoogleCalendarCalendarListManager();
            CalendarList calList = calendarListManager.List(CalendarService, null);
            List<string> authorizedCalendars = calList.Items.Select(c => c.Id).ToList();

            calendarAccounts = new Dictionary<int, string>();
            using (var dbContext = new CRM_Umzugsfirma.DB.ApplicationDbContext())
            {
                List<Account> accounts = dbContext.Accounts.Where(a => a.IsActive).ToList<Account>();
                foreach (Account account in accounts)
                {
                    if ((!authorizedCalendars.Contains(account.GoogleEmail.ToLower())) || (calendarAccounts.ContainsKey(account.AccountId))) continue;
                    calendarAccounts.Add(account.AccountId, account.GoogleEmail.ToLower());
                }
            }

            _initialized = true;
        }

        public static GoogleCalendarCacheSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new GoogleCalendarCacheSingleton();
                        }
                    }
                }

                return _instance;
            }
        }

        public string GetCalendarEmailByAccountId(int accountId)
        {
            if ((!_initialized) || (!calendarAccounts.ContainsKey(accountId)))
            {
                return null;
            }

            return calendarAccounts[accountId];
        }

        /// <summary>
        /// Authenticating to Google using a Service account
        /// Documentation: https://developers.google.com/accounts/docs/OAuth2#serviceaccount
        /// </summary>
        /// <returns></returns>
        private CalendarService AuthenticateServiceAccount()
        {
            string serviceAccountEmail = ConfigurationManager.AppSettings["GoogleServiceAccount"];
            string keyFilePath = ConfigurationManager.AppSettings["GoogleServiceKey"];

            FileManager fileManager = new FileManager();
            keyFilePath = fileManager.GetAbsoluteFilePath(keyFilePath);

            // check the file exists
            if (!File.Exists(keyFilePath))
            {
                Console.WriteLine("An Error occurred - Key file does not exist");
                return null;
            }

            string[] scopes = new string[] {
                CalendarService.Scope.Calendar  ,  // Manage your calendars
                CalendarService.Scope.CalendarReadonly    // View your Calendars
            };

            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.Exportable);
            try
            {
                ServiceAccountCredential credential = new ServiceAccountCredential(
                    new ServiceAccountCredential.Initializer(serviceAccountEmail)
                    {
                        Scopes = scopes
                    }.FromCertificate(certificate));

                // Create the service.
                CalendarService service = new CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "crm-umzug",
                });
                return service;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException);
                return null;
            }
        }


    }
}