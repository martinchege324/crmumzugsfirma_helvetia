﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class CustomerManager
    {
        public Customer GetCustomerById(int id, bool checkAccountIdFromSession = false, int checkAccountId = 0)
        {
            if ((checkAccountIdFromSession) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                checkAccountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }

            using (var db = new ApplicationDbContext())
            {
                Customer customer = db.Customers.Find(id);
                if ((checkAccountId > 0) && (customer.AccountId != checkAccountId))
                {
                    return null;
                }
                return customer;
            }
        }

        public List<Customer> GetCustomersByAccountId(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Customers.Where(c => c.AccountId == accountId).ToList();
            }
        }

        public int EditCustomer(Customer customer, string modifyUserId = null)
        {
            customer.ModifyDate = DateTime.Now;
            customer.ModifyUserId = getUserId(modifyUserId);

            using (var db = new ApplicationDbContext())
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
            }

            return customer.CustomerId;
        }

        public int UpdateContactStageType(Customer customer, ContactStageType newStage, string modifyUserId = null)
        {
            customer.ContactStage = newStage;
            return EditCustomer(customer, modifyUserId);
        }

        public int UpdateContractType(Customer customer, ContractType newContractType, string modifyUserId = null)
        {
            customer.ContractType = newContractType;
            return EditCustomer(customer, modifyUserId);
        }

        private string getUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }

        #region BeautifuledText

        public string GetTitledName(Customer customer)
        {
            if (customer.MarketId == MarketType.Private_person)
            {
                string name = string.Format("{0} {1}", customer.LastName, customer.FirstName);
                switch (customer.TitleId)
                {
                    case TitleType.Mrs:
                        return string.Format("Mrs {0}", name);
                    case TitleType.Mr:
                        return string.Format("Mr {0}", name);
                    default:
                        return name;
                }
            }
            else
            {
                return string.Format("{0}{1}", customer.CompanyName, !string.IsNullOrEmpty(customer.CompanyContactPerson) ? string.Format(" ({0})", customer.CompanyContactPerson) : string.Empty);
            }
        }

        public string GetSalutation(Customer customer)
        {
            if (customer.MarketId == MarketType.Private_person)
            {
                switch (customer.TitleId)
                {
                    case TitleType.Mrs:
                        return "Dear Mrs " + customer.LastName;
                    case TitleType.Mr:
                        return "Dear Mr " + customer.LastName;
                    default:
                        return "Dear " + customer.FirstName + " " + customer.LastName;
                }
            }
            else
            {
                return string.Format("Dear {0}", !string.IsNullOrEmpty(customer.CompanyContactPerson) ? customer.CompanyContactPerson : customer.CompanyName);
            }
        }

        public string GetPhonePrioMobile(Customer customer)
        {
            if (!string.IsNullOrEmpty(customer.Mobile))
            {
                return customer.Mobile;
            }
            return customer.Phone;
        }

        public string GetPhoneAggregate(Customer customer)
        {
            string phone = string.Empty;
            if (!string.IsNullOrEmpty(customer.Mobile)) phone = string.Format("{0} / {1}", phone, customer.Mobile);
            if (!string.IsNullOrEmpty(customer.Phone)) phone = string.Format("{0} / {1}", phone, customer.Phone);
            if (phone.StartsWith(" / ")) phone = phone.Substring(3);
            return phone;
        }

        #endregion
    }
}