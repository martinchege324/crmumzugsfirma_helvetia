﻿using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class AccountManager
    {

        public Account GetAccountByUserId(string userId)
        {
            AccountMapper mapper = new AccountMapper();
            return mapper.GetAccountByUserId(userId);
        }

        public Account GetAccountById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Accounts.Find(id);
            }
        }

        public Account GetAccountByInnerId(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Accounts.Where(a => a.InnerAccountId == id).SingleOrDefault();
            }
        }
    }
}