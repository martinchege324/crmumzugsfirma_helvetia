﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class NoteManager
    {
        public async Task<List<Note>> GetNotesById(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return await db.Notes.Where(n => n.CustomerId == customerId && !n.IsDeleted).OrderBy(n => n.CreateDate).ToListAsync();
            }
        }

        public Note GetNoteById(int noteId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Notes.Find(noteId);
            }
        }

        public bool CreateNote(Note note, string createUserId = null)
        {
            // set defaults
            note.CreateDate = DateTime.Now;
            note.IsDeleted = false;
            note.CreateUserId = getUserId(createUserId);

            using (var db = new ApplicationDbContext())
            {
                db.Notes.Add(note);
                db.SaveChanges();
            }
            return true;
        }

        public bool CreateNote(string body, int customerId, string createUserId = null)
        {
            Note note = new Note();
            note.Body = body;
            note.CustomerId = customerId;

            return CreateNote(note);
        }

        public async Task<bool> DeleteNote(int noteId, string deleteUserId = null)
        {
            Note note = GetNoteById(noteId);
            if (note == null) return false;

            note.IsDeleted = true;
            note.DeleteDate = DateTime.Now;
            note.DeleteUserId = getUserId(deleteUserId);

            using (var db = new ApplicationDbContext())
            {
                db.Entry(note).State = EntityState.Modified;
                await db.SaveChangesAsync();
            }
            return true;
        }

        private string getUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }
    }
}