﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.ViewModels;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace CRM_Umzugsfirma.Managers
{
    public class BillManager
    {
        public List<Bill> GetBillsByCustomer(int customerId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Bills.Include("BillCleanings").Include("BillTransports").Where(n => n.CustomerId == customerId).OrderBy(n => n.CreateDate).ToList();
            }
        }

        public Bill GetBillById(int billId, bool checkAccountIdFromSession = false, int checkAccountId = 0)
        {
            if ((checkAccountIdFromSession) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                checkAccountId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).AccountId;
            }
            
            using (var db = new ApplicationDbContext())
            {
                Bill bill = db.Bills.Include("BillCleanings").Include("BillTransports").FirstOrDefault(s => s.BillId == billId);
                if ((checkAccountId > 0) && (bill.AccountId != checkAccountId))
                {
                    return null;
                }
                return bill;
            }
        }

        public Bill GetBillByInnerId(int innerBillId, int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Bills.Include("BillCleanings").Include("BillTransports").FirstOrDefault(x => x.BillInnerId == innerBillId && x.AccountId == accountId);
            }
        }

        public List<OfferPriceAdditCosts> GetSelectedAdditionalCosts(int billId, string serviceArea)
        {
            using (var db = new ApplicationDbContext())
            {
                return (from op in db.Bill_PriceAddits
                             join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                        where op.BillId == billId && par.ServiceType == serviceArea
                             select new OfferPriceAdditCosts()
                             {
                                 PriceAdditRateId = par.PriceAdditRateId,
                                 Descr = par.DescrToShow,
                                 OverwrittenPrice = op.EndPrice,
                             }).ToList<OfferPriceAdditCosts>();
            }
        }

        /// <summary>Get selected DefinedPackMaterial. This packmaterials are defined from shop and will only be selected by dropdown</summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public List<Bill_DefinedPackMaterial> GetDefinedPackMaterials(int billId, bool includeDefinedPackMaterial = false)
        {
            using (var db = new ApplicationDbContext())
            {
                var billDefinedPackMaterials = db.Bill_DefinedPackMaterials.AsQueryable();
                if (includeDefinedPackMaterial)
                {
                    billDefinedPackMaterials = billDefinedPackMaterials.Include(s => s.DefinedPackMaterial);
                }
                return billDefinedPackMaterials.Where(x => x.BillId == billId).ToList();
            }
        }

        /// <summary>Get selected freetext PackMaterial. This packmaterials are free textfields that can be set free text by user in textfield</summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public List<BillPackMaterial> GetPackMaterials(int billId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.BillPackMaterials.Where(x => x.BillId == billId).ToList();
            }
        }

        /// <summary>Get next possible innerbill id of account. smallest count is 1000 (as defined)</summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public int GetNextPossibleInnerBillId(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                int nextId = (1 + db.Bills.Where(c => c.AccountId == accountId).OrderByDescending(o => o.BillInnerId).Select(s => s.BillInnerId).FirstOrDefault());
                if (nextId < 1000) nextId = 1000;

                return nextId;
            }
        }

        public int GetCountReminderBill(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Bills.Where(b => b.AccountId == accountId && b.ToPayDate.CompareTo(DateTime.Now) < 0 && b.BillStateId < Bill.BillState.Memory).Count();
            }
        }

        public int GetCountPreCollectionBill(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                DateTime dt = DateTime.Now.AddDays(-14);
                return db.Bills.Where(b => b.AccountId == accountId && b.ToPayDate.CompareTo(dt) < 0 && b.BillStateId < Bill.BillState.Reminder).Count();
            }
        }

        public int CreateBill(Bill bill, Customer customer, string createUserId = null)
        {
            // set defaults
            
            bill.CreateUserId = getUserId(createUserId);
            bill.ToPayDate = bill.CreateDate.AddDays(((int)bill.PaymentTermId));
            bill.AccountId = customer.AccountId;
            bill.CustomerId = customer.CustomerId;
            bill.BillStateId = Bill.BillState.Open;
            bill.BillInnerId = GetNextPossibleInnerBillId(customer.AccountId);

            // clean depencies if not really needed
            bill = cleanDependencySubtables(bill);
            // remove dependant model-list from main model and add later manually all sub-table values
            var billCleanings = bill.BillCleanings;
            bill.BillCleanings = null;
            var billTransports = bill.BillTransports;
            bill.BillTransports = null;

            using (var db = new ApplicationDbContext())
            {
                db.Bills.Add(bill);
                db.SaveChanges();

                // add manually all sub-table values
                bool saveChange = false;
                if (billCleanings != null && billCleanings.Count > 0)
                {
                    foreach (var billCleaning in billCleanings)
                    {
                        billCleaning.BillId = bill.BillId;
                        db.BillCleanings.Add(billCleaning);
                    }
                    saveChange = true;
                }
                if (billTransports != null && billTransports.Count > 0)
                {
                    foreach (var billTransport in billTransports)
                    {
                        billTransport.BillId = bill.BillId;
                        db.BillTransports.Add(billTransport);
                    }
                    saveChange = true;
                }

                if (saveChange)
                {
                    db.SaveChanges();
                }
            }

            return bill.BillId;
        }

        public int EditBill(Bill bill, string modifyUserId = null, bool changeDate = true)
        {
            bill.ModifyDate = DateTime.Now;
            modifyUserId = getUserId(modifyUserId);
            if (!string.IsNullOrEmpty(modifyUserId)) bill.ModifyUserId = modifyUserId;

            // clean depencies if not really needed
            bill = cleanDependencySubtables(bill);

            using (var db = new ApplicationDbContext())
            {
                db.Entry(bill).State = EntityState.Modified;

                // Dependant model-list: try to update or remove all depencies sub-table
                var billCleaning = db.BillCleanings.FirstOrDefault(s => s.BillId == bill.BillId && s.SectorId == 1);
                if (bill.ReinigungActive)
                {
                    db.Entry(bill.BillCleaning).State = (billCleaning != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (billCleaning != null)
                {
                    db.Entry(bill.BillCleaning).State = EntityState.Deleted;
                }
                var billCleaning2 = db.BillCleanings.FirstOrDefault(s => s.BillId == bill.BillId && s.SectorId == 2);
                if (bill.Reinigung2Active)
                {
                    db.Entry(bill.BillCleaning2).State = (billCleaning2 != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (billCleaning2 != null)
                {
                    db.Entry(bill.BillCleaning2).State = EntityState.Deleted;
                }
                var billTransport = db.BillTransports.FirstOrDefault(s => s.BillId == bill.BillId && s.SectorId == 1);
                if (bill.TransportActive)
                {
                    db.Entry(bill.BillTransport).State = (billTransport != null) ? EntityState.Modified : EntityState.Added;
                }
                else if (billTransport != null)
                {
                    db.Entry(bill.BillTransport).State = EntityState.Deleted;
                }

                db.SaveChanges();
            }

            return bill.BillId;
        }

        public bool DeleteBill(int billId)
        {
            using (var db = new ApplicationDbContext())
            {
                Bill bill = db.Bills.Find(billId);
                if (bill == null) return false;

                db.Bills.Remove(bill);
                db.SaveChanges();
            }
            return true;
        }

        public Bill GetRandomBillByAccount(int accountId)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.Bills.Include("BillCleanings").Include("BillTransports").FirstOrDefault(a => a.AccountId == accountId);
            }
        }

        public bool SetSelectedAdditionalCosts(int billId, CustomerBillViewModel model)
        {
            using (var db = new ApplicationDbContext())
            {
                // Move
                var umzugAddit = (from op in db.Bill_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                  where op.BillId == billId && par.ServiceType == "umzug"
                                  select op).ToList();

                // remove all before inserted umzug addit
                if (umzugAddit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(umzugAddit);
                    db.SaveChanges();
                }
                // insert all selected umzug addit
                if (model.Bill.UmzugActive && model.Bill.UmzugCostAddit)
                {
                    var selectedUmzugAddit = model.UmzugPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedUmzugAddit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // Pack
                var packAddit = (from op in db.Bill_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                 where op.BillId == billId && par.ServiceType == "pack"
                                  select op).ToList();

                // remove all before inserted pack addit
                if (packAddit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(packAddit);
                    db.SaveChanges();
                }
                // insert all selected pack addit
                if (model.Bill.PackActive && model.Bill.PackCostAddit)
                {
                    var selectedPackAddit = model.PackPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedPackAddit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // Unpack
                var packOutAddit = (from op in db.Bill_PriceAddits
                    join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                    where op.BillId == billId && par.ServiceType == "packout"
                    select op).ToList();

                // remove all before inserted packout addit
                if (packOutAddit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(packOutAddit);
                    db.SaveChanges();
                }
                // insert all selected packout addit
                if (model.Bill.PackOutActive && model.Bill.PackOutCostAddit)
                {
                    var selectedPackOutAddit = model.PackOutPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedPackOutAddit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // cleaning
                var reinigungAddit = (from op in db.Bill_PriceAddits
                                 join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                      where op.BillId == billId && par.ServiceType == "cleaning"
                                 select op).ToList();

                // remove all before inserted cleaning addit
                if (reinigungAddit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(reinigungAddit);
                    db.SaveChanges();
                }
                // insert all selected cleaning addit
                if (model.Bill.ReinigungActive && model.Bill.BillCleaning.ReinigungCostAddit)
                {
                    var selectedReinigungAddit = model.ReinigungPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedReinigungAddit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // cleaning 2
                var reinigung2Addit = (from op in db.Bill_PriceAddits
                    join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                    where op.BillId == billId && par.ServiceType == "reinigung2"
                                       select op).ToList();

                // remove all before inserted cleaning2 addit
                if (reinigung2Addit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(reinigung2Addit);
                    db.SaveChanges();
                }
                // insert all selected cleaning2 addit
                if (model.Bill.Reinigung2Active && model.Bill.BillCleaning2.ReinigungCostAddit)
                {
                    var selectedReinigung2Addit = model.Reinigung2PriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedReinigung2Addit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
                // disposal
                var entsorgungAddit = (from op in db.Bill_PriceAddits
                                  join par in db.PriceAdditRates on op.PriceAdditRateId equals par.PriceAdditRateId
                                  where op.BillId == billId && par.ServiceType == "disposal"
                                  select op).ToList();

                // remove all before inserted disposal addit
                if (entsorgungAddit.Count > 0)
                {
                    db.Bill_PriceAddits.RemoveRange(entsorgungAddit);
                    db.SaveChanges();
                }
                // insert all selected disposal addit
                if (model.Bill.EntsorgungActive && model.Bill.EntsorgungCostAddit)
                {
                    var selectedEntsorgungAddit = model.EntsorgungPriceAdditCosts.Where(a => a.Selected).ToList();
                    selectedEntsorgungAddit.ForEach(x => db.Bill_PriceAddits.Add(new Bill_PriceAddit() { BillId = billId, PriceAdditRateId = x.Id, EndPrice = x.Price }));
                    db.SaveChanges();
                }
            }
            return true;
        }

        /// <summary>Add selected DefinedPackMaterial. This packmaterials are defined from shop and will only be selected by dropdown</summary>
        /// <param name="billId"></param>
        /// <param name="packMaterials"></param>
        /// <returns></returns>
        public bool SetDefinedPackMaterials(int billId, List<Bill_DefinedPackMaterial> packMaterials)
        {
            using (var db = new ApplicationDbContext())
            {
                var existsPackMaterials = db.Bill_DefinedPackMaterials.Where(x => x.BillId == billId).ToList();
                if (existsPackMaterials != null && existsPackMaterials.Count > 0)
                {
                    db.Bill_DefinedPackMaterials.RemoveRange(existsPackMaterials);
                    db.SaveChanges();
                }
                if (packMaterials != null && packMaterials.Count > 0)
                {
                    var dataContaining = packMaterials.Where(x => x.DefinedPackMaterialId > 0 && x.CountNumber > 0).ToList();
                    dataContaining.ForEach(x => db.Bill_DefinedPackMaterials.Add(new Bill_DefinedPackMaterial()
                    {
                        BillId = billId,
                        DefinedPackMaterialId = x.DefinedPackMaterialId,
                        RentBuy = x.RentBuy,
                        PiecePrice = x.PiecePrice,
                        CountNumber = x.CountNumber,
                        EndPrice = x.EndPrice
                    }));
                    db.SaveChanges();
                }
            }
            return true;
        }

        /// <summary>Add selected freetext PackMaterial. This packmaterials are free textfields that can be set free text by user in textfield</summary>
        /// <param name="billId"></param>
        /// <param name="packMaterials"></param>
        /// <returns></returns>
        public bool SetPackMaterials(int billId, List<BillPackMaterial> packMaterials)
        {
            using (var db = new ApplicationDbContext())
            {
                var existsPackMaterials = db.BillPackMaterials.Where(x => x.BillId == billId).ToList();
                if (existsPackMaterials != null && existsPackMaterials.Count > 0)
                {
                    db.BillPackMaterials.RemoveRange(existsPackMaterials);
                    db.SaveChanges();
                }
                if (packMaterials != null && packMaterials.Count > 0)
                {
                    var dataContaining = packMaterials.Where(x => !string.IsNullOrEmpty(x.Text) && x.CountNumber > 0).ToList();
                    dataContaining.ForEach(x => db.BillPackMaterials.Add(new BillPackMaterial()
                    {
                        BillId = billId,
                        Text = x.Text,
                        MieteKauf = x.MieteKauf,
                        PricePerPiece = x.PricePerPiece,
                        CountNumber = x.CountNumber,
                        EndPrice = x.EndPrice
                    }));
                    db.SaveChanges();
                }
            }
            return true;
        }

        public bool UpdateBillState(Bill bill, Bill.BillState newBillState, string modifyUserId = null)
        {
            if (newBillState == Bill.BillState.Paid)
            {
                bill.BillStateId = newBillState;
                bill.PayedDate = DateTime.Now;
                bill.PayedUserId = getUserId(modifyUserId);
                EditBill(bill, modifyUserId, false);
            }
            else if (newBillState == Bill.BillState.Memory)
            {
                bill.BillStateId = newBillState;
                bill.ReminderDate = DateTime.Now;
                bill.ReminderUserId = getUserId(modifyUserId);
                EditBill(bill, modifyUserId, false);
            }
            else if (newBillState == Bill.BillState.Reminder)
            {
                bill.BillStateId = newBillState;
                bill.PreCollectionDate = DateTime.Now;
                bill.PreCollectionUserId = getUserId(modifyUserId);
                EditBill(bill, modifyUserId);
            }
            return true;
        }

        public string GetCryptedBillCode(Bill bill)
        {
            string defineVal = string.Format("{0}|{1}|{2}|{3}|{4}", bill.BillId, bill.BillInnerId, bill.OfferId, bill.CustomerId, bill.AccountId);
            return CRM_Umzugsfirma.Helpers.CryptorEngine.Encrypt(defineVal);
        }

        public Bill GetBillByCryptedBillCode(string decryptedString)
        {
            string decryptedVal = CRM_Umzugsfirma.Helpers.CryptorEngine.Decrypt(decryptedString);
            string[] helper = decryptedVal.Split('|');
            if (helper.Length == 5)
            {
                int billId;
                int billInnerId;
                int offerId;
                int customerId;
                int accountId;
                int.TryParse(helper[0], out billId);
                int.TryParse(helper[1], out billInnerId);
                int.TryParse(helper[2], out offerId);
                int.TryParse(helper[3], out customerId);
                int.TryParse(helper[4], out accountId);
                if (billId > 0 && billInnerId > 0 && customerId > 0 && accountId > 0)
                {
                    Bill bill = GetBillById(billId);
                    if (bill == null || bill.BillInnerId != billInnerId || bill.CustomerId != customerId || bill.AccountId != accountId)
                    {
                        return null;
                    }
                    return bill;
                }
            }
            return null;
        }

        private Bill cleanDependencySubtables(Bill bill)
        {
            if (!bill.ReinigungActive)
            {
                var billCleaning1 = bill.BillCleanings.FirstOrDefault(s => s.SectorId == 1);
                if (billCleaning1 != null) bill.BillCleanings.Remove(billCleaning1);
            }
            if (!bill.Reinigung2Active)
            {
                var billCleaning2 = bill.BillCleanings.FirstOrDefault(s => s.SectorId == 2);
                if (billCleaning2 != null) bill.BillCleanings.Remove(billCleaning2);
            }
            if (!bill.TransportActive)
            {
                var billTransport = bill.BillTransports.FirstOrDefault(s => s.SectorId == 1);
                if (billTransport != null) bill.BillTransports.Remove(billTransport);
            }

            return bill;
        }

        private string getUserId(string userId)
        {
            if (string.IsNullOrEmpty(userId) && (HttpContext.Current != null) && (HttpContext.Current.Session["UserIdentifier"] != null))
            {
                userId = ((UserIdentifier)HttpContext.Current.Session["UserIdentifier"]).UserId;
            }

            return userId;
        }
    }
}