﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using CRM_Umzugsfirma.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Managers.Offers;

namespace CRM_Umzugsfirma.Managers.PackMaterial
{
    public sealed class DefinedPackMaterialCacheSingleton
    {
        private static volatile DefinedPackMaterialCacheSingleton _instance;
        private static object syncRoot = new Object();

        private Dictionary<int, List<DefinedPackMaterial>> _definedPackMaterialCache;

        private DefinedPackMaterialCacheSingleton()
        {
            _definedPackMaterialCache = new Dictionary<int, List<DefinedPackMaterial>>();

            // load data from db
            using (var dbContext = new ApplicationDbContext())
            {
                // load DefinedPackMaterials from db
                List<DefinedPackMaterial> allDefinedPackMaterials = dbContext.DefinedPackMaterials.OrderBy(p => p.AccountId).ThenBy(p => p.Title).ToList();

                // put per account a list of all data in dictionary
                var existAccountData = allDefinedPackMaterials.Select(s => s.AccountId).Distinct().ToList();
                existAccountData.ForEach(a => _definedPackMaterialCache.Add(a, allDefinedPackMaterials.Where(s => s.AccountId == a).ToList()));
            }

        }

        public static DefinedPackMaterialCacheSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new DefinedPackMaterialCacheSingleton();
                        }
                    }
                }

                return _instance;
            }
        }

        public List<DefinedPackMaterial> GetDefinedPackMaterialList(int accountId)
        {
            if (!_definedPackMaterialCache.ContainsKey(accountId))
            {
                return null;
            }

            return _definedPackMaterialCache[accountId];
        }

    }
}