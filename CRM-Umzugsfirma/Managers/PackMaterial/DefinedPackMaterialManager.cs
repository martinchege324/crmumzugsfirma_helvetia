﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Managers.PackMaterial
{
    public class DefinedPackMaterialManager
    {
        public List<DefinedPackMaterial> GetDefinedPackMaterials(int accountId)
        {
            return DefinedPackMaterialCacheSingleton.Instance.GetDefinedPackMaterialList(accountId);
        }

        public DefinedPackMaterial GetPriceRateById(int accountId, int id)
        {
            List<DefinedPackMaterial> definedPackMaterials = GetDefinedPackMaterials(accountId);

            return definedPackMaterials?.Find(p => p.DefinedPackMaterialId == id);
        }

        public List<SelectListItem> GetDefinedPackMaterialAsSelectListItems(int accountId, bool pleaseSelectItem, string selectValue = null)
        {
            var selectListItems = new List<SelectListItem>();
            if (pleaseSelectItem)
            {
                selectListItems.Add(new SelectListItem() { Value = "0", Text = "Please choose" });
            }

            List<DefinedPackMaterial> definedPackMaterials = GetDefinedPackMaterials(accountId);
            if (definedPackMaterials != null)
            {
                // get pricerateid from '1|200|3|1'
                if (selectValue != null && selectValue.Contains('|')) selectValue = selectValue.Split('|')[0];

                foreach (DefinedPackMaterial definedPackMaterial in definedPackMaterials)
                {
                    selectListItems.Add(new SelectListItem()
                    {
                        Value = string.Format("{0}|{1}|{2}", definedPackMaterial.DefinedPackMaterialId, definedPackMaterial.PriceRent, definedPackMaterial.PriceBuy),
                        Text = definedPackMaterial.Title,
                        Selected = ((selectValue != null && definedPackMaterial.DefinedPackMaterialId.ToString().Equals(selectValue)) ? true : false)
                    });
                }
            }
            return selectListItems;
        }

    }
}