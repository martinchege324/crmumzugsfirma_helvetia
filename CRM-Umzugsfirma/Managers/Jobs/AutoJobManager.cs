﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using System;
using System.Data.Entity;
using System.Linq;

namespace CRM_Umzugsfirma.Managers.Jobs
{
    public class AutoJobManager
    {
        protected readonly AutoJob.AutoJobType AutoJobType;

        public AutoJobManager(AutoJob.AutoJobType autoJobType)
        {
            AutoJobType = autoJobType;
        }

        public AutoJob GetAutoJobById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.AutoJobs.SingleOrDefault(x => x.AutoJobId == id);
            }
        }

        public AutoJob GetAutoJobRunsTodaySuccessful()
        {
            DateTime todayEnd = DateTime.Today.AddDays(1).AddMilliseconds(-1);
            using (var db = new ApplicationDbContext())
            {
                return db.AutoJobs.FirstOrDefault(x => x.Type == AutoJobType && x.Successful && x.CreateDate > DateTime.Today && x.CreateDate < todayEnd);
            }

        }

        public int CreateAutoJob(AutoJob autoJob)
        {
            if (autoJob == null)
            {
                return 0;
            }

            autoJob.Type = AutoJobType;
            // set create date
            if (autoJob.CreateDate == null || autoJob.CreateDate == DateTime.MinValue)
            {
                autoJob.CreateDate = DateTime.Now;
            }

            using (var db = new ApplicationDbContext())
            {
                db.AutoJobs.Add(autoJob);
                db.SaveChanges();
            }
            return autoJob.AutoJobId;
        }

        public int EditAutoJob(AutoJob autoJob)
        {
            autoJob.ModifyDate = DateTime.Now;

            using (var db = new ApplicationDbContext())
            {
                db.Entry(autoJob).State = EntityState.Modified;
                db.SaveChanges();
            }

            return autoJob.AutoJobId;
        }

    }
}