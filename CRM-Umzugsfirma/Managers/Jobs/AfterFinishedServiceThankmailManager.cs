﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace CRM_Umzugsfirma.Managers.Jobs
{
    public class AfterFinishedServiceThankmailManager : AutoJobManager
    {
        public AfterFinishedServiceThankmailManager() : base(AutoJob.AutoJobType.AfterFinishedServiceThankmail)
        {
        }

        /// <summary>
        /// Get all services that are completed
        /// </summary>
        /// <param name="accountId">AccountId of searching services</param>
        /// <param name="finishedDay">When the services is done</param>
        /// <returns></returns>
        public List<Appointment> GetAllFinishedServices(int accountId, DateTime finishedDay)
        {
            DateTime finishedDayEnd = finishedDay.AddDays(1).AddMilliseconds(-1);

            /*
             * Service (Termineinträge of Dienstleistungen), die:
             * - vor 3 Tagen eine Dienstleistungen ausgeführt wurde
             * - eine Email-Adresse (in Terminversand or Customer) hinterlegt ist
             */
            using (var db = new ApplicationDbContext())
            {
                return (from a in db.Appointments
                        join c in db.Customers on a.CustomerId equals c.CustomerId
                        from appointmentService in db.AppointmentServices.Where(ase => ase.AppointmentId == a.AppointmentId).DefaultIfEmpty()
                        from sentEmails in db.AutoJobSentEmails.Where(ajse => ajse.Type == AutoJobType && ajse.AffectedId == a.AppointmentId).DefaultIfEmpty()
                        where (a.AccountId == accountId)
                            && (a.AppointmentTypeId == Appointment.AppointmentType.Confirmation_of_the_order)
                            && (!string.IsNullOrEmpty(a.EmailAddress) || !string.IsNullOrEmpty(c.Email))
                            && ((appointmentService != null) 
                                    && ((appointmentService.UmzugDateTime >= finishedDay && appointmentService.UmzugDateTime <= finishedDayEnd)
                                        || (appointmentService.PackDateTime >= finishedDay && appointmentService.PackDateTime <= finishedDayEnd)
                                        || (appointmentService.PackOutDateTime >= finishedDay && appointmentService.PackOutDateTime <= finishedDayEnd)
                                        || (appointmentService.EntsorgungDateTime >= finishedDay && appointmentService.EntsorgungDateTime <= finishedDayEnd)
                                        || (appointmentService.ReinigungDateTime >= finishedDay && appointmentService.ReinigungDateTime <= finishedDayEnd)
                                        || (appointmentService.Reinigung2DateTime >= finishedDay && appointmentService.Reinigung2DateTime <= finishedDayEnd)
                                        || (appointmentService.LagerungDateTime >= finishedDay && appointmentService.LagerungDateTime <= finishedDayEnd)
                                        || (appointmentService.TransportDateTime >= finishedDay && appointmentService.TransportDateTime <= finishedDayEnd)))
                            && (sentEmails.AffectedId == null) // remove joined sentEmails, that means, that these appointments already get an email for this task
                        select a).Include(s => s.Customer.Account).ToList();
            }
        }

        public List<AutoJobSentEmail> SendEmailToAffected(ControllerBase controller, List<Appointment> affectedAppointments, RequestContext rc)
        {
            List<AutoJobSentEmail> autoJobSentEmailList = new List<AutoJobSentEmail>();

            var customerManager = new CustomerManager();
            var accountManager = new AccountManager();

            foreach (var appointment in affectedAppointments)
            {
                // read data from appointment if already loaded, else load again from db
                var customer = appointment.Customer ?? customerManager.GetCustomerById(appointment.CustomerId, true);
                var account = customer.Account ?? accountManager.GetAccountById(appointment.AccountId);

                var mainMailData = new MainMailData()
                {
                    InnerAccountId = account.InnerAccountId,
                    AccountName = account.AccountName,
                    AccountStreet = account.Street,
                    AccountCity = $"{account.Zip} {account.City}",
                    AccountPhone = account.Phone,
                    AccountFax = account.Fax,
                    AccountMail = account.Email,
                    AccountWebAddress = account.WebPage,
                    AccountContactName = account.ContactName,
                    PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                    PathFooterLogo = "",
                    CustomerSalutation = customerManager.GetSalutation(customer),
                };

                // sent email to customer
                var subject = $"{account.AccountName} - Thank you for your cooperation";
                var body = ControllerExtensions.RenderPartialViewToString(controller, $"{mainMailData.PathAccount}/text/JobAfterFinishedServiceThankmail.cshtml", mainMailData);

                var emailSender = (new EmailSenderFactory()).CreateEmailSender(account.AccountId.ToString());
                // send email
                var emailAddress = !string.IsNullOrEmpty(appointment.EmailAddress) ? appointment.EmailAddress : customer.Email;
                bool emailSent = emailSender.SendEmail(emailAddress, subject, body, true);

                if (emailSent)
                {
                    // store into cache for save into db
                    autoJobSentEmailList.Add(new AutoJobSentEmail()
                    {
                        Type = AutoJobType,
                        AffectedDbTable = "Appointment",
                        AffectedId = appointment.AppointmentId,
                        SentEmailAddress = emailAddress
                    });

                    // create an entry in Email Marketing
                    var emailMarketingManager = new EmailMarketingManager();
                    var emailMarketingMaster = emailMarketingManager.GetByAccessor(4, "autojob.AfterFinishedServiceThankmail");
                    //string emailAttachmentNameList = string.Join(";", emailMarketingMaster.EmailMarketingAttachments.Select(x => x.FileFullName));
                    EmailMarketingCustomer emailMarketingCustomer = new EmailMarketingCustomer()
                    {
                        CustomerId = customer.CustomerId,
                        EmailMarketingId = emailMarketingMaster.EmailMarketingId,
                        AffectedDbTable = "Appointment",
                        AffectedId = appointment.AppointmentId,
                        AffectedInnerId = appointment.AppointmentId,
                        EmailAddress = emailAddress,
                        EmailSubject = subject,
                        EmailContent = body
                        //,EmailAttachmentNameList = emailAttachmentNameList
                    };
                    emailMarketingManager.AddEmailMarketingCustomer(emailMarketingCustomer);
                }
            }
            return autoJobSentEmailList;
        }

    }
}