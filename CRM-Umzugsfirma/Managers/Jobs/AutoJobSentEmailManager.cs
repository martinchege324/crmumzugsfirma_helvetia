﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.DB;
using System.Data.Entity;
using System.Linq;

namespace CRM_Umzugsfirma.Managers.Jobs
{
    public class AutoJobSentEmailManager
    {
        public AutoJobSentEmail GetAutoJobSentEmailById(int id)
        {
            using (var db = new ApplicationDbContext())
            {
                return db.AutoJobSentEmails.SingleOrDefault(x => x.AutoJobSentEmailId == id);
            }
        }

        public int CreateAutoJobSentEmail(AutoJobSentEmail autoJobSentEmail)
        {
            using (var db = new ApplicationDbContext())
            {
                db.AutoJobSentEmails.Add(autoJobSentEmail);
                db.SaveChanges();
            }
            return autoJobSentEmail.AutoJobSentEmailId;
        }

        public int EditAutoJobSentEmail(AutoJobSentEmail autoJobSentEmail)
        {
            using (var db = new ApplicationDbContext())
            {
                db.Entry(autoJobSentEmail).State = EntityState.Modified;
                db.SaveChanges();
            }

            return autoJobSentEmail.AutoJobSentEmailId;
        }

    }
}