﻿using CRM_Umzugsfirma.Areas.Intranet.Models;
using CRM_Umzugsfirma.Areas.Intranet.Models.EmailMarketing;
using CRM_Umzugsfirma.DB;
using CRM_Umzugsfirma.Helpers;
using CRM_Umzugsfirma.Managers.Email;
using CRM_Umzugsfirma.Managers.Offers;
using CRM_Umzugsfirma.ViewModels.Mail;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

namespace CRM_Umzugsfirma.Managers.Jobs
{
    public class OpenOfferWithoutFeedbackManager : AutoJobManager
    {
        public OpenOfferWithoutFeedbackManager() : base(AutoJob.AutoJobType.OpenOfferWithoutFeedback)
        {
        }

        /// <summary>
        /// Get all offers that have the state open and has not yet responsed
        /// </summary>
        /// <param name="accountId">AccountId of searching offers</param>
        /// <param name="createdDay">When the offers are created</param>
        /// <returns></returns>
        public List<Offer> GetAllWaitingOffersWithoutFeedback(int accountId, DateTime createdDay)
        {
            DateTime createdDayEnd = createdDay.AddDays(1).AddMilliseconds(-1);

            /*
             * Offer, die:
             * - offen sind (status = wartet_auf_Kunde)
             * - vor 2 Tage erstellt wurden
             * - Emailversand of der Offer erhalten haben und eine Email-Adresse hinterlegt ist
             * - Service_appointment in Zukunft ist
             */
            using (var db = new ApplicationDbContext())
            {
                return (from o in db.Offers
                        from sentEmails in db.AutoJobSentEmails.Where(ajse => ajse.Type == AutoJobType && ajse.AffectedId == o.OfferId).DefaultIfEmpty()
                            from offerreinigung in db.OfferCleanings.Where(oc => oc.OfferId == o.OfferId && oc.SectorId == 1).DefaultIfEmpty()
                            from offerreinigung2 in db.OfferCleanings.Where(oc2 => oc2.OfferId == o.OfferId && oc2.SectorId == 2).DefaultIfEmpty()
                            from offertransport in db.OfferTransports.Where(ot => ot.OfferId == o.OfferId && ot.SectorId == 1).DefaultIfEmpty()
                        where (o.AccountId == accountId)
                            && (o.OfferStateId == Offer.OfferState.waiting_for_customer)
                            //&& ((o.EmailSendDate != null) && (o.EmailSendDate >= createdDay && o.EmailSendDate <= createdDayEnd))
                            && ((o.CreateDate != null) && (o.CreateDate >= createdDay && o.CreateDate <= createdDayEnd))
                            && (o.EmailToCustomer) && (!string.IsNullOrEmpty(o.EmailAddress))
                            && ((o.UmzugDateTime > DateTime.Now) || (o.PackDateTime > DateTime.Now) || (o.PackOutDateTime > DateTime.Now) || (o.EntsorgungDateTime > DateTime.Now)
                                || (o.ReinigungActive && offerreinigung != null && offerreinigung.ReinigungDateTime > DateTime.Now)
                                || (o.Reinigung2Active && offerreinigung2 != null && offerreinigung2.ReinigungDateTime > DateTime.Now)
                                || (o.TransportActive && offertransport != null && offertransport.TransportDateTime > DateTime.Now))
                            && (sentEmails.AffectedId == null) // remove joined sentEmails, that means, that these offers already get an email for this task
                        select o).Include(s => s.Customer.Account).ToList();
            }
        }

        public List<AutoJobSentEmail> SendEmailToAffected(ControllerBase controller, List<Offer> affectedOffers, RequestContext rc)
        {
            List<AutoJobSentEmail> autoJobSentEmailList = new List<AutoJobSentEmail>();

            var customerManager = new CustomerManager();
            var accountManager = new AccountManager();

            foreach (var offer in affectedOffers)
            {
                // read data from offer if already loaded, else load again from db
                var account = offer.Account ?? accountManager.GetAccountById(offer.AccountId);
                var customer = offer.Customer ?? customerManager.GetCustomerById(offer.CustomerId, true);

                var mainMailData = new MainMailData()
                {
                    InnerAccountId = account.InnerAccountId,
                    AccountName = account.AccountName,
                    AccountStreet = account.Street,
                    AccountCity = $"{account.Zip} {account.City}",
                    AccountPhone = account.Phone,
                    AccountFax = account.Fax,
                    AccountMail = account.Email,
                    AccountWebAddress = account.WebPage,
                    AccountContactName = account.ContactName,
                    PathAccount = $"~/Content/accountdata/{account.InnerAccountId}/mail",
                    PathFooterLogo = "",
                    CustomerSalutation = customerManager.GetSalutation(customer),
                };

                // sent email to customer
                var subject = $"Ihre Offer of {account.AccountName}";
                var body = ControllerExtensions.RenderPartialViewToString(controller, $"{mainMailData.PathAccount}/text/JobOpenOfferWithoutFeedback.cshtml", mainMailData);

                var emailSender = (new EmailSenderFactory()).CreateEmailSender(account.AccountId.ToString());
                // send email
                bool emailSent = emailSender.SendEmail(offer.EmailAddress, subject, body, true);

                if (emailSent)
                {
                    // store into cache for save into db
                    autoJobSentEmailList.Add(new AutoJobSentEmail()
                    {
                        Type = AutoJobType,
                        AffectedDbTable = "Offer",
                        AffectedId = offer.OfferId,
                        SentEmailAddress = offer.EmailAddress
                    });

                    // create an entry in Email Marketing
                    var emailMarketingManager = new EmailMarketingManager();
                    var emailMarketingMaster = emailMarketingManager.GetByAccessor(4, "autojob.OpenOfferWithoutFeedback");
                    //string emailAttachmentNameList = string.Join(";", emailMarketingMaster.EmailMarketingAttachments.Select(x => x.FileFullName));
                    EmailMarketingCustomer emailMarketingCustomer = new EmailMarketingCustomer()
                    {
                        CustomerId = customer.CustomerId,
                        EmailMarketingId = emailMarketingMaster.EmailMarketingId,
                        AffectedDbTable = "Offer",
                        AffectedId = offer.OfferId,
                        AffectedInnerId = offer.OfferInnerId,
                        EmailAddress = offer.EmailAddress,
                        EmailSubject = subject,
                        EmailContent = body
                        //,EmailAttachmentNameList = emailAttachmentNameList
                    };
                    emailMarketingManager.AddEmailMarketingCustomer(emailMarketingCustomer);
                }
            }
            return autoJobSentEmailList;
        }

    }
}