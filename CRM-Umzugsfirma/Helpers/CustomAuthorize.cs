﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Helpers
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool hasRight = base.AuthorizeCore(httpContext);

            // if hasRight, check for security if Session is set
            if (hasRight && Roles.ToString().Contains("AccountUser"))
            {
                if ((httpContext == null) || (httpContext.Session["UserIdentifier"] == null))
                {
                    hasRight = false;
                }
            }

            return hasRight;
        }

    }
}