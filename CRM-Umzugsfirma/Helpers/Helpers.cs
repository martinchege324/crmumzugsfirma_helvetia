﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace CRM_Umzugsfirma.Helpers
{
    public static class Helpers
    {
        #region Time

        public static string GetDateFormatted(string startDate, string format = null)
        {
            if (string.IsNullOrEmpty(format)) format = "dddd, dd. MMMM yyyy";

            DateTime date;
            if (!DateTime.TryParse(startDate, out date)) return string.Empty;

            return date.ToString(format, CultureInfo.CreateSpecificCulture("en-GB"));
        }

        public static string GetTimeFormatted(string time, char separator = ':')
        {
            if (!time.Contains(':')) return string.Empty;

            if (separator == ':') return time;
            return time.Replace(':', separator);
        }

        public static string CorrectTimeFormat(string time)
        {
            if (!string.IsNullOrEmpty(time))
            {
                time = time.Replace('.', ':').Replace('-', ':');
                if (!time.Contains(':')) time = time + ":00";
                if (time.EndsWith(":")) time = time + "00";
            }
            return time;
        }

        public static DateTime? ConvertStringToDateTime(string date, string time = null)
        {
            // convert date
            if (!DateTime.TryParse(date, out var tempDateTime)) return null;

            // convert time
            if (!string.IsNullOrEmpty(time))
            {
                time = CorrectTimeFormat(time);
                var timeSplit = time.Split(':');
                if (timeSplit.Length == 2)
                {
                    if (int.TryParse(timeSplit[0], out var hours))
                    {
                        tempDateTime = tempDateTime.AddHours(hours);
                    }
                    if (int.TryParse(timeSplit[1], out var minutes))
                    {
                        tempDateTime = tempDateTime.AddMinutes(minutes);
                    }
                }
            }

            return tempDateTime;
        }

        public static string DateTimeForSorting(string datetime)
        {
            if (!string.IsNullOrEmpty(datetime))
            {
                DateTime dt;
                if (DateTime.TryParse(datetime, out dt))
                {
                    return dt.ToString("yyyyMMdd");
                }
            }
            return null;
        }

        public static string CorrectTimeFormatWith0(string time)
        {
            if (!string.IsNullOrEmpty(time))
            {
                if (time.IndexOf(':') == 1)
                {
                    return "0" + time;
                }
                return time;
            }
            return null;
        }

        #endregion

        #region Path

        public static string ContentFullPath(string virtualPath)
        {
            var result = string.Empty;

            if (HttpContext.Current != null && HttpContext.Current.Request != null)
            {
                result = string.Format("{0}://{1}{2}",
                                       HttpContext.Current.Request.Url.Scheme,
                                       HttpContext.Current.Request.Url.Authority,
                                       VirtualPathUtility.ToAbsolute(virtualPath));
            }
            return result;
        }

        #endregion

        #region Number

        public static string NumberCashBeatiful(string cash)
        {
            if (string.IsNullOrEmpty(cash)) return string.Empty;

            cash = cash.Replace(',','.');
            if (!cash.Contains('.')) return cash;

            string[] cashSplit = cash.Split('.');
            if (string.IsNullOrEmpty(cashSplit[1])) return cashSplit[0];

            if (cashSplit[1].Length == 1) cash = string.Format("{0}.{1}0", cashSplit[0], cashSplit[1]);

            return cash;
        }

        public static string NumberCashPrintable(string cash)
        {
            cash = NumberCashBeatiful(cash);
            if (!cash.Contains('.')) cash = string.Format("{0}.-", cash); ;

            return cash;
        }

        public static string NumberCashPrintable(decimal cash)
        {
            return NumberCashPrintable(cash.ToString());
        }

        public static string NumberCashPrintableWithChf(decimal cash)
        {
            return string.Format("CHF {0}", NumberCashPrintable(cash.ToString()));
        }

        public static int GetHigherPrice(string priceVal)
        {
            int price = 0;
            if (!priceVal.Contains('-'))
            {
                int.TryParse(priceVal, out price);
            }
            else
            {
                string[] str = priceVal.Split('-');
                if (str.Length == 2)
                {
                    int.TryParse(str[1], out price);
                }
            }
            return price;
        }

        public static string FloatToCurrencyFormat(float value)
        {
            return string.Format(new System.Globalization.NumberFormatInfo() { NumberGroupSeparator = "'" }, "{0:#,0.00}", value);
        }

        #endregion

        #region LocationPlace

        public static string LocationDescription(int zip, string city, string countryCode = null)
        {
            string locationDescription = $"{countryCode}{(!string.IsNullOrEmpty(countryCode) ? "-" : string.Empty)}{zip} {city}";
            if (locationDescription.Trim() == "0") locationDescription = string.Empty;

            return locationDescription;
        }

        #endregion

        #region Descriptions

        public static string ReplaceEnumExtensionString(string input)
        {
            return input.Replace("_bind_", "-").Replace("_point_", ".").Replace("_slash_", " / ").Replace('_', ' ');
        }

        #endregion

        #region Strings

        public static string ValueOrUnderscore(string input, int countUnderscore, string startTemplate = null, string endTemplate = null)
        {
            var returnValue = startTemplate ?? string.Empty;

            if (!string.IsNullOrEmpty(input))
            {
                returnValue = $"{returnValue}{input}";
            }
            else
            {
                for (int i = 0; i < countUnderscore; i++)
                {
                    returnValue = $"{returnValue}_";
                }
            }

            return $"{returnValue}{endTemplate}";
        }

        public static string ValueCashOrUnderscore(string input, int countUnderscore, string startTemplate = null, string endTemplate = null)
        {
            if (!string.IsNullOrEmpty(input))
            {
                input = NumberCashPrintable(input);
            }

            return ValueOrUnderscore(input, countUnderscore, startTemplate, endTemplate);
        }

        #endregion

        #region Enum

        public static T GetAttribute<T>(Enum value) where T : Attribute
        {
            var type = value.GetType();
            var memberInfo = type.GetMember(value.ToString());
            var attributes = memberInfo[0].GetCustomAttributes(typeof(T), false);
            return (T)attributes.FirstOrDefault();//attributes.Length > 0 ? (T)attributes[0] : null;
        }

        public static string EnumDisplayName(Enum value)
        {
            var attribute = GetAttribute<DisplayAttribute>(value);
            return attribute == null ? value.ToString() : attribute.Name;
        }


        #endregion

    }
}