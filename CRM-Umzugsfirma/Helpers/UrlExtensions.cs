﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Helpers
{
    public static class UrlExtensions
    {
        public static string ContentFullPath(this UrlHelper url, string virtualPath)
        {
            var result = string.Empty;
            Uri requestUrl = url.RequestContext.HttpContext.Request.Url;

            result = string.Format("{0}://{1}{2}",
                                   requestUrl.Scheme,
                                   requestUrl.Authority,
                                   VirtualPathUtility.ToAbsolute(virtualPath));
            return result;
        }
    }
}