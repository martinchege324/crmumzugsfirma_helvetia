﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using CRM_Umzugsfirma.Models;
using System.Text;
using CRM_Umzugsfirma.Helpers.CountryCode;

namespace CRM_Umzugsfirma.Helpers
{
    public static class HtmlExtensions
    {
        #region Path

        public static string MapApplicationPath(this HtmlHelper htmlHelper, string url)
        {
            if (!url.StartsWith("~"))
            {
                if (!url.StartsWith("/"))
                {
                    url = string.Format("~/{0}", url);
                }
                else
                {
                    url = string.Format("~{0}", url);
                }
            }

            return System.Web.Hosting.HostingEnvironment.MapPath(url);
        }

        public static string UrlCurrentRouteData(HtmlHelper htmlHelper, string routeName)
        {
            if (htmlHelper.ViewContext.RouteData.Values.Keys.Contains(routeName))
            {
                return htmlHelper.ViewContext.RouteData.Values[routeName].ToString();
            }
            return string.Empty;
        }

        #endregion

        #region AccountSpecials

        public static MvcHtmlString GetAccountImage(this HtmlHelper htmlHelper, string accountId, string url, string title, string cssClass, bool resposiveImage = true)
        {
            if (string.IsNullOrEmpty(accountId)) return null;

            url = string.Format("/accountdata/{0}/{1}", accountId, url);
            url = checkImagesPrefix(url);

            return RenderImage(htmlHelper, url, title, cssClass, resposiveImage);
        }


        #endregion

        #region Images

        public static MvcHtmlString GetContentImage(this HtmlHelper htmlHelper, string url, string title, bool resposiveImage = true)
        {
            return GetContentImage(htmlHelper, url, title, string.Empty, resposiveImage);
        }

        public static MvcHtmlString GetContentImage(this HtmlHelper htmlHelper, string url, string title, string cssClass, bool resposiveImage = true)
        {
            url = string.Format("/content/{0}", url);
            url = checkImagesPrefix(url);

            return RenderImage(htmlHelper, url, title, cssClass, resposiveImage);
        }

        public static MvcHtmlString GetStaticImage(this HtmlHelper htmlHelper, string url, string title, string cssClass, bool resposiveImage = true)
        {
            url = checkImagesPrefix(url);

            return RenderImage(htmlHelper, url, title, cssClass, resposiveImage);
        }

        private static MvcHtmlString RenderImage(this HtmlHelper htmlHelper, string url, string title, string cssClass, bool resposiveImage = true)
        {
            if (resposiveImage)
            {
                if (!string.IsNullOrEmpty(cssClass))
                {
                    cssClass = string.Format("{0} img-responsive", cssClass);
                }
                else
                {
                    cssClass = "img-responsive";
                }
            }

            MvcHtmlString mvcString = new MvcHtmlString(string.Format("<img src=\"{0}\" title=\"{1}\"{2} />",
                url,
                title,
                !string.IsNullOrEmpty(cssClass) ? string.Format(" class=\"{0}\"", cssClass) : string.Empty));

            return mvcString;
        }

        private static string checkImagesPrefix(string url)
        {
            url = string.Format("/content/images/{0}", url);

            return url.Replace("//", "/");
        }

        #endregion

        #region HtmlElements
        public static MvcHtmlString MyLabelFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, string cssClass = "")
        {
            if (!string.IsNullOrEmpty(cssClass)) cssClass = string.Format("control-label {0}", cssClass);
            else cssClass = "control-label";
            return LabelExtensions.LabelFor(htmlHelper, expression, new { @class = cssClass });
        }

        public static MvcHtmlString MyLabelFor<TModel, TValue>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TValue>> expression, bool onlyForScreenReader)
        {
            string cssClass = "control-label";
            if (onlyForScreenReader) cssClass += " sr-only";
            return LabelExtensions.LabelFor(htmlHelper, expression, new { @class = cssClass });
        }

        public static MvcHtmlString MyTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string cssClass = "", string controlType = "")
        {
            if (!string.IsNullOrEmpty(cssClass)) cssClass = string.Format("form-control {0}", cssClass);
            else cssClass = "form-control";
            return InputExtensions.TextBoxFor(htmlHelper, expression, new { @class = cssClass, type = (!string.IsNullOrEmpty(controlType) ? controlType : "text") });
        }

        public static MvcHtmlString MyTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool setPlacholder, string inputPlaceholder, string controlType = "")
        {
            if (string.IsNullOrEmpty(inputPlaceholder)) inputPlaceholder = GetDisplayName(ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData));
            if (!setPlacholder) inputPlaceholder = string.Empty;
            return InputExtensions.TextBoxFor(htmlHelper, expression, new { @class = "form-control", type = (!string.IsNullOrEmpty(controlType) ? controlType : "text"), placeholder = inputPlaceholder });
        }

        public static MvcHtmlString MyDropdownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, IEnumerable<SelectListItem> selectList, object htmlAttributes = null)
        {
            return SelectExtensions.DropDownListFor(htmlHelper, expression, selectList, (htmlAttributes != null ? htmlAttributes : new { @class = "form-control" }));
        }

        public static MvcHtmlString MyCheckboxFor<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression)
        {
            return InputExtensions.CheckBoxFor(htmlHelper, expression, new { @class = "control-checkbox" });
        }

        public static MvcHtmlString MyDatepickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, bool showComponent = true, bool pastSelectable = false, string cssClass = null)
        {
            if (showComponent)
            {
                return MvcHtmlString.Create(string.Format("<div class=\"input-group date datepicker-control{1}{2}\">{0}<span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-th\"></i></span></div>",
                    InputExtensions.TextBoxFor(htmlHelper, expression, new { @class = "form-control" }), pastSelectable ? " past-select" : string.Empty,
                    !string.IsNullOrEmpty(cssClass) ? string.Format(" {0}", cssClass) : string.Empty));
            }
            else
            {
                return InputExtensions.TextBoxFor(htmlHelper, expression, new { @class = string.Format("form-control datepicker-control{0}{1}", pastSelectable ? " past-select" : string.Empty,
                    !string.IsNullOrEmpty(cssClass) ? string.Format(" {0}", cssClass) : string.Empty)});
            }
        }

        public static MvcHtmlString MyTimepickerFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string id, bool showComponent = true, bool initEmpty = false, string initSpecificTime = null, string cssClass = null)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("<script type=\"text/javascript\">");
            sb.AppendLine("$(document).ready(function () {");
            sb.AppendFormat("$('#{0}').timepicker({1}", id, "{");
            if (initEmpty)
            {
                sb.AppendLine("  defaultTime: false");
            }
            else if (!string.IsNullOrEmpty(initSpecificTime))
            {
                sb.AppendLine(string.Format("  defaultTime: '{0}'", initSpecificTime));
            }
            sb.AppendLine("});");
            sb.AppendLine("});");
            sb.AppendLine("</script>");
            cssClass = string.Format("form-control input-small {0}", cssClass);
            if (showComponent)
            {
                return MvcHtmlString.Create(string.Format("<div class=\"input-group bootstrap-timepicker\">{0}<span class=\"input-group-addon\"><i class=\"glyphicon glyphicon-time\"></i></span></div>{1}",
                    InputExtensions.TextBoxFor(htmlHelper, expression, new { id = id, @class = cssClass, type = "text" }), sb.ToString()));
            }
            else
            {
                return MvcHtmlString.Create(string.Format("<div class=\"bootstrap-timepicker\">{0}</div>{1}",
                    InputExtensions.TextBoxFor(htmlHelper, expression, new { id = id, @class = cssClass, type = "text" }), sb.ToString()));
            }
        }

        public static MvcHtmlString CheckboxFormGroup<TModel>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, bool>> expression, bool labelOnLeft = true, bool labelLight = false, bool disableFormGroup = false, string cssClass = null)
        {
            string cssLabel = string.Empty;
            if (labelLight) cssLabel = "light-label";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(string.Format("<div class=\"{0}checkbox-group {1} {2}\">", !disableFormGroup ? "form-group " : string.Empty, labelOnLeft ? "label-onleft" : "label-onright", cssClass));
            if (labelOnLeft) sb.AppendLine(MyLabelFor(htmlHelper, expression, cssLabel).ToString());
            sb.AppendLine(MyCheckboxFor(htmlHelper, expression).ToString());
            if (!labelOnLeft) sb.AppendLine(MyLabelFor(htmlHelper, expression, cssLabel).ToString());
            sb.AppendLine("</div>");

            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString RadioButtonForEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string collapseGroup = null, string idNameAdditional = null)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var names = Enum.GetNames(metaData.ModelType);
            var sb = new StringBuilder();
            var value = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;
            var idx = 0;
            foreach (var name in names)
            {
                idx++;

                var id = string.Format("{0}_{1}_{2}",
                    htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, metaData.PropertyName, name);
                if (id.StartsWith("_")) id = id.Remove(0, 1);
                if (!string.IsNullOrEmpty(idNameAdditional)) id = string.Format("{0}_{1}", id, idNameAdditional);

                string radioContent;
                if (string.IsNullOrEmpty(collapseGroup))
                {
                    // standard radio
                    radioContent = htmlHelper.RadioButtonFor(expression, name, new { id = id }).ToHtmlString();
                }
                else
                {
                    // render collapse
                    var targetContainer = $"container-{id}";
                    bool currentExpanded = (value == null && idx == 1) || (value?.ToString() == name);

                    radioContent = htmlHelper.RadioButtonFor(expression, name, new { id = id, data_toggle = "collapse", data_target = "#" + targetContainer, data_parent = "#" + collapseGroup, aria_expanded = currentExpanded, aria_controls = targetContainer }).ToHtmlString();
                }

                sb.AppendFormat("<span class=\"radio-inline\">{2} <label for=\"{0}\">{1}</label></span>",
                    id, HttpUtility.HtmlEncode(name.Replace("_", " ")), radioContent);
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        public static MvcHtmlString RadioButtonSearchForEnum<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression)
        {
            var metaData = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            var names = Enum.GetNames(metaData.ModelType);
            var sb = new StringBuilder();
            var value = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData).Model;

            // render All Anzeigen
            string radioContent;
            string id = string.Format("{0}_All", metaData.PropertyName);
            // --- Important. Call in View model =>, and not s or x or something else
            string groupName = expression.Body.ToString().Replace("model.", string.Empty);
            // standard radio
            radioContent = htmlHelper.RadioButton(groupName, "", ((int)value == 0), new { id = id }).ToHtmlString();
            sb.AppendFormat("<span class=\"radio-inline\">{2} <label for=\"{0}\">{1}</label></span>",
                id, HttpUtility.HtmlEncode("All"), radioContent);

            foreach (var name in names)
            {
                id = string.Format("{0}_{1}_{2}",
                    htmlHelper.ViewData.TemplateInfo.HtmlFieldPrefix, metaData.PropertyName, name);
                if (id.StartsWith("_")) id = id.Remove(0, 1);

                // standard radio
                radioContent = htmlHelper.RadioButtonFor(expression, name, new { id = id }).ToHtmlString();

                sb.AppendFormat("<span class=\"radio-inline\">{2} <label for=\"{0}\">{1}</label></span>",
                    id, HttpUtility.HtmlEncode("Just \"" + name.Replace("_", " ") + "\""), radioContent);
            }
            return MvcHtmlString.Create(sb.ToString());
        }

        public static IHtmlString CheckBoxesForEnumModel<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            if (!typeof(TModel).IsEnum)
            {
                throw new ArgumentException("this helper can only be used with enums");
            }
            var sb = new StringBuilder();
            foreach (Enum item in Enum.GetValues(typeof(TModel)))
            {
                var ti = htmlHelper.ViewData.TemplateInfo;
                var id = ti.GetFullHtmlFieldId(item.ToString());
                var name = ti.GetFullHtmlFieldName(string.Empty);
                var label = new TagBuilder("label");
                label.Attributes["for"] = id;
                label.SetInnerText(item.ToString());
                sb.AppendLine(label.ToString());

                var checkbox = new TagBuilder("input");
                checkbox.Attributes["id"] = id;
                checkbox.Attributes["name"] = name;
                checkbox.Attributes["type"] = "checkbox";
                checkbox.Attributes["value"] = item.ToString();
                var model = htmlHelper.ViewData.Model as Enum;
                if (model.HasFlag(item))
                {
                    checkbox.Attributes["checked"] = "checked";
                }
                sb.AppendLine(checkbox.ToString());
            }

            return new HtmlString(sb.ToString());
        } 
       
        #endregion

        #region MyControls

        public static MvcHtmlString BootstrapModal(this HtmlHelper htmlHelper, string type, string id, string title, string text, bool showOnLoad, string buttonText = "", string cssClass = null)
        {
            if (type == "small")
            {
                type = "modal-sm";
            }
            else
            {
                type = "modal-lg";
            }

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div id=\"{0}\" class=\"modal fade{1}\" role=\"dialog\" aria-hidden=\"true\">", id, (!string.IsNullOrEmpty(cssClass) ? string.Format(" {0}", cssClass) : string.Empty));
            sb.AppendFormat("<div class=\"modal-dialog {0}\">", type);
            sb.AppendLine("<div class=\"modal-content\">");
            sb.AppendLine("<div class=\"modal-header\">");
            sb.AppendLine("<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>");
            sb.AppendFormat("<h4 class=\"modal-title\" id=\"myModalLabel\">{0}</h4>", title);
            sb.AppendLine("</div>");
            sb.AppendLine("<div class=\"modal-body\">");
            sb.AppendLine(text);
            sb.AppendLine("</div>");
            sb.AppendLine("<div class=\"modal-footer\">");
            sb.AppendFormat("<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">OK</button>", (!string.IsNullOrEmpty(buttonText) ? buttonText : "OK"));
            sb.AppendLine("</div>");
            sb.AppendLine("</div></div></div>");
            if (showOnLoad)
            {
                sb.AppendLine("<script type=\"text/javascript\">");
                sb.AppendLine(string.Format("$(document).ready(function() {0}", "{"));
                sb.AppendLine(string.Format("$('#{0}').modal('show');", id));
                sb.AppendLine(string.Format("{0});", "}"));
                sb.AppendLine("</script>");
            }

            return new MvcHtmlString(sb.ToString());
        }

        public static MvcHtmlString BootstrapModalActionResult(this HtmlHelper htmlHelper, bool isSuccess, string type, string id, string title, string text, bool showOnLoad, string cssClass = null)
        {
            return BootstrapModal(htmlHelper, type, id, title, text, showOnLoad, "Schliessen", string.Format("{0} {1}-form", cssClass, isSuccess ? "success" : "error").TrimStart());
        }

        #region NavigationMenu
        /*
        public static MvcHtmlString RenderMenu(this HtmlHelper htmlHelper, UrlHelper urlHelper)
        {
            List<MenuItem> menuList = NavigationManager.Instance.MenuContainer;
            string currentController = UrlCurrentRouteData(htmlHelper, "controller");

            StringBuilder menu = new StringBuilder();
            menu.Append("<nav class=\"nav-menu\"><ul class=\"main\">");
            foreach (MenuItem item in menuList)
            {
                menu.AppendFormat("<li{0}>", ((item.UrlController.Equals(currentController, StringComparison.CurrentCultureIgnoreCase)) ? " class=\"active\"" : string.Empty));
                menu.AppendFormat("<a href=\"{0}\" role=\"button\">{1}</a>", urlHelper.Action(item.UrlAction, item.UrlController), item.Name);
                menu.Append("</li>");
            }
            menu.Append("</ul><ul class=\"sub-menu\">");
            foreach (MenuItem item in menuList)
            {
                if ((item.SubMenuItems != null) && (item.UrlController.Equals(currentController, StringComparison.CurrentCultureIgnoreCase)))
                {
                    foreach (MenuItem subitem in item.SubMenuItems)
                    {
                        menu.AppendFormat("<li><a href=\"{0}{2}\" role=\"button\">{1}</a></li>", urlHelper.Action(subitem.UrlAction, subitem.UrlController), subitem.Name,
                            (!string.IsNullOrEmpty(subitem.UrlAnker) ? string.Format("#{0}", subitem.UrlAnker) : string.Empty));
                    }

                }
            }
            menu.Append("</ul></nav>");

            return new MvcHtmlString(menu.ToString());
        }

        public static MvcHtmlString RenderMenuMobile(this HtmlHelper htmlHelper, UrlHelper urlHelper, bool renderSearchBox)
        {
            List<MenuItem> menuList = NavigationManager.Instance.MenuContainer;
            string currentController = UrlCurrentRouteData(htmlHelper, "controller");

            StringBuilder menu = new StringBuilder();
            menu.Append("<div id=\"navbar\" class=\"navbar-collapse collapse\"><ul class=\"nav navbar-nav\">");
            // search area
            if (renderSearchBox)
            {
                menu.AppendLine("<li class=\"nav-search-btn\">");
                menu.AppendLine(SearchFormWebmart(htmlHelper, "Was suchen Sie?").ToString());
                menu.AppendLine("</li>");
            }
            // menu items
            foreach (MenuItem item in menuList)
            {
                menu.AppendLine("<li>");
                if ((item.SubMenuItems != null) && (item.SubMenuItems.Count > 0))
                {
                    menu.AppendFormat("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-expanded=\"false\">{0}<span class=\"caret\"></span></a>", item.Name);
                    menu.AppendLine("<ul class=\"dropdown-menu\" role=\"menu\">");
                    foreach (MenuItem subitem in item.SubMenuItems)
                    {
                        menu.AppendFormat("<li><a href=\"{0}{2}\" role=\"button\">{1}</a></li>", urlHelper.Action(subitem.UrlAction, subitem.UrlController), subitem.Name,
                            (!string.IsNullOrEmpty(subitem.UrlAnker) ? string.Format("#{0}", subitem.UrlAnker) : string.Empty));
                    }
                    menu.AppendLine("</ul>");
                }
                else
                {
                    menu.AppendFormat("<a href=\"{0}\" role=\"button\">{1}</a>", urlHelper.Action(item.UrlAction, item.UrlController), item.Name);
                }
                menu.AppendLine("</li>");
            }
            menu.AppendLine("</ul></div>");

            return new MvcHtmlString(menu.ToString());
        }
        */
        #endregion

        #endregion

        #region Convertors

        #region Currency

        public static string FloatToCurrencyCHF(this HtmlHelper htmlHelper, float value, bool inclTax = true)
        {
            return $"{string.Format(new System.Globalization.NumberFormatInfo() { NumberGroupSeparator = "'" }, "CHF {0:#,0.00}", value)}{(inclTax ? " VAT included." : string.Empty)}";
        }

        public static string DecimalToCurrencyCHF(this HtmlHelper htmlHelper, decimal value)
        {
            return string.Format(new System.Globalization.NumberFormatInfo() { NumberGroupSeparator = "'" }, "CHF {0:#,0.00}", value);
        }


        public static string NumberCashBeatiful(this HtmlHelper htmlHelper, string cash)
        {
            if (string.IsNullOrEmpty(cash)) return string.Empty;

            cash = cash.Replace(',', '.');
            if (!cash.Contains('.')) return cash;

            string[] cashSplit = cash.Split('.');
            if (string.IsNullOrEmpty(cashSplit[1])) return cashSplit[0];

            if (cashSplit[1].Length == 1) cash = string.Format("{0}.{1}0", cashSplit[0], cashSplit[1]);

            return cash;
        }

        public static string NumberCashPrintable(this HtmlHelper htmlHelper, string cash)
        {
            cash = NumberCashBeatiful(htmlHelper, cash);
            if (cash.EndsWith(".00")) cash = cash.Replace(".00", string.Empty);
            if (!cash.Contains('.')) cash = string.Format("{0}.-", cash); ;

            return cash;
        }

        public static string NumberCashPrintableWithCHF(this HtmlHelper htmlHelper, string cash)
        {
            cash = NumberCashBeatiful(htmlHelper, cash);
            if (cash.EndsWith(".00")) cash = cash.Replace(".00", string.Empty);
            if (!cash.Contains('.')) cash = $"{cash}.-"; ;
            return $"CHF {cash}";
        }

        public static string NumberCashPrintable(this HtmlHelper htmlHelper, float cash)
        {
            return NumberCashPrintable(htmlHelper, cash.ToString());
        }

        public static string NumberCashPrintable(this HtmlHelper htmlHelper, decimal cash)
        {
            return NumberCashPrintable(htmlHelper, cash.ToString());
        }

        public static string NumberCashPrintableWithChf(this HtmlHelper htmlHelper, decimal cash)
        {
            return string.Format("CHF {0}", NumberCashPrintable(htmlHelper, cash.ToString()));
        }

        #endregion

        #region LocationPlace

        public static string CountryCodeDescription(this HtmlHelper htmlHelper, string countryCodeCode)
        {
            return new CountryCodeManager().GetDescriptionByCode(countryCodeCode);
        }

        public static string LocationDescription(this HtmlHelper htmlHelper, int zip, string city, string countryCode = null)
        {
            return Helpers.LocationDescription(zip, city, countryCode);
            //return $"{countryCode}{(!string.IsNullOrEmpty(countryCode) ? "-" : string.Empty)}{zip} {city}";
        }

        #endregion

        #endregion

        #region Helpers

        private static string GetDisplayName(ModelMetadata metadata)
        {
            return (((metadata != null) && (!string.IsNullOrWhiteSpace(metadata.DisplayName))) ? metadata.DisplayName : string.Empty);
        }

        #endregion

    }
}