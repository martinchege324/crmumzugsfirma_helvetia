﻿using System;
using System.Collections.Generic;

namespace CRM_Umzugsfirma.Helpers.CountryCode
{
    public class CountryCodeSingleton
    {
        private static volatile CountryCodeSingleton _instance;
        private static object syncRoot = new Object();

        private readonly List<CountryCode> _countryCodes;

        private CountryCodeSingleton()
        {
            _countryCodes = new List<CountryCode>()
            {
                new CountryCode() {Code = "CH", Description = "Switzerland"},
                new CountryCode() {Code = "FL", Description = "Principality of Liechtenstein"},
                new CountryCode() {Code = "DE", Description = "Germany"},
                new CountryCode() {Code = "AT", Description = "Austria"},
                new CountryCode() {Code = "IT", Description = "Italy"},
                new CountryCode() {Code = "FR", Description = "France"}
            };

        }

        public static CountryCodeSingleton Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (_instance == null)
                        {
                            _instance = new CountryCodeSingleton();
                        }
                    }
                }

                return _instance;
            }
        }

        public List<CountryCode> GetCountryCodes()
        {
            return _countryCodes;
        }

    }
}