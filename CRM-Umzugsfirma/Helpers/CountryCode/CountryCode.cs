﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_Umzugsfirma.Helpers.CountryCode
{
    public class CountryCode
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}