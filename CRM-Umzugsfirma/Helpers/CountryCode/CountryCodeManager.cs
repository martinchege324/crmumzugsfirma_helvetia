﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_Umzugsfirma.Helpers.CountryCode
{
    public class CountryCodeManager
    {
        protected List<CountryCode> CountryCodes => CountryCodeSingleton.Instance.GetCountryCodes();

        public CountryCodeManager()
        {
        }

        public string GetDescriptionByCode(string code)
        {
            return CountryCodes.FirstOrDefault(s => s.Code == code)?.Description;
        }

        public List<SelectListItem> GetCountryCodeSelectListItems(string selectValue = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (string.IsNullOrEmpty(selectValue)) selectValue = CountryCodes.Select(s => s.Code).FirstOrDefault();
            CountryCodes.ForEach(c => list.Add(new SelectListItem() { Value = c.Code, Text = c.Description, Selected = (selectValue == c.Code) }));
            return list;
        }
    }
}