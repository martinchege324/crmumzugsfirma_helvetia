﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace CRM_Umzugsfirma.Helpers
{
    public static class CryptorEngine
    {
        private static string _key = "jNkvN8hl5K31GLh4l7hA";

        public static string Encrypt(string toEncrypt)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(toEncrypt);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    toEncrypt = Convert.ToBase64String(ms.ToArray());
                }
            }
            return toEncrypt.Replace("/", "_slsh_");
        }

        public static string Decrypt(string toDecrypt)
        {
            try
            {
                toDecrypt = toDecrypt.Replace(" ", "+").Replace("_slsh_", "/");
                byte[] cipherBytes = Convert.FromBase64String(toDecrypt);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_key, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        toDecrypt = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
                return toDecrypt;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
    }
}